package com.uppstreymi.jibby;

import java.util.List;

public class ActiveViewDTO {

	private List<Integer> itemTypes;
	private ColAOptionsDTO colAOptions;
	private Integer colAActiveProjectId;
	private Integer colBSelectedItemId;
	private List<Integer> expendItems;
	private Integer colCActiveView;
	private List<Integer> colCDashboardOptions;
	private Integer myActionListTab;
	private Integer myActionListDoneEnable;
	private Integer myActionMyItemsViewOption;
	private Integer myActionAssignToMeViewOption;
	private Integer myActionAssignToOthersViewOption;
	private Integer sortOption;
	private Integer projectSortOption;
	private Integer assignToOthersFilter;
	
	public ActiveViewDTO(){}
	
	public ActiveViewDTO(List<Integer> itemTypes,Integer sortOption,Integer projectSortOption,ColAOptionsDTO colAOptions,Integer colAActiveProjectId, Integer colBSelectedItemId
			,List<Integer> expendItems,Integer colCActiveView,List<Integer> colCDashboardOptions,Integer myActionListTab,
			Integer myActionListDoneEnable,Integer myActionMyItemsViewOption,Integer myActionAssignToMeViewOption,Integer myActionAssignToOthersViewOption){
		
		this.itemTypes=itemTypes;
		this.sortOption = sortOption;
		this.projectSortOption=projectSortOption;
		this.colAOptions=colAOptions;
		this.colAActiveProjectId=colAActiveProjectId;
		this.colBSelectedItemId=colBSelectedItemId;
		this.expendItems=expendItems;
		this.colCActiveView=colCActiveView;
		this.colCDashboardOptions=colCDashboardOptions;
		this.myActionListTab=myActionListTab;
		this.myActionListDoneEnable=myActionListDoneEnable;
		this.myActionMyItemsViewOption=myActionMyItemsViewOption;
		this.myActionAssignToMeViewOption=myActionAssignToMeViewOption;
		this.myActionAssignToOthersViewOption=myActionAssignToOthersViewOption;
	}
	
	public void setItemTypes(List<Integer> itemTypes){
		this.itemTypes=itemTypes;
	}
	public List<Integer> getItemTypes(){
		return this.itemTypes;
	}
	
	public void setSortOption(Integer sortOption){
		this.sortOption=sortOption;
	}
	public Integer getSortOption(){
		return this.sortOption;
	}
	
	public void setProjectSortOption(Integer projectSortOption){
		this.projectSortOption=projectSortOption;
	}
	public Integer getProjectSortOption(){
		return this.projectSortOption;
	}
	
	public void setColAOptions(ColAOptionsDTO colAOptions){
		this.colAOptions=colAOptions;
	}
	public ColAOptionsDTO getColAOptions(){
		return this.colAOptions;
	}
	
	public void setColAActiveProjectId(Integer colAActiveProjectId){
		this.colAActiveProjectId=colAActiveProjectId;
	}
	public Integer getColAActiveProjectId(){
		return this.colAActiveProjectId;
	}
	
	public void setColBSelectedItemId(Integer colBSelectedItemId){
		this.colBSelectedItemId=colBSelectedItemId;
	}
	public Integer getColBSelectedItemId(){
		return this.colBSelectedItemId;
	}
	
	public void setExpendItems(List<Integer> expendItems){
		this.expendItems=expendItems;
	}
	public List<Integer> getExpendItems(){
		return this.expendItems;
	}
	
	public void setColCActiveView(Integer colCActiveView){
		this.colCActiveView=colCActiveView;
	}
	public Integer getColCActiveView(){
		return this.colCActiveView;
	}
	
	public void setColCDashboardOptions(List<Integer> colCDashboardOptions){
		this.colCDashboardOptions=colCDashboardOptions;
	}
	public List<Integer> getColCDashboardOptions(){
		return this.colCDashboardOptions;
	}
	
	public void setMyActionListTab(Integer myActionListTab){
		this.myActionListTab=myActionListTab;
	}
	public Integer getMyActionListTab(){
		return this.myActionListTab;
	}
	
	public void setActionListDoneEnable(Integer myActionListDoneEnable){
		this.myActionListDoneEnable=myActionListDoneEnable;
	}
	public Integer getActionListDoneEnable(){
		return this.myActionListDoneEnable;
	}
	
	public void setMyActionMyItemsViewOption(Integer myActionMyItemsViewOption){
		this.myActionMyItemsViewOption=myActionMyItemsViewOption;
	}
	public Integer getMyActionMyItemsViewOption(){
		return this.myActionMyItemsViewOption;
	}
	public void setMyActionAssignToMeViewOption(Integer myActionAssignToMeViewOption){
		this.myActionAssignToMeViewOption=myActionAssignToMeViewOption;
	}
	public Integer getMyActionAssignToMeViewOption(){
		return this.myActionAssignToMeViewOption;
	}
	public void setMyActionAssignToOthersViewOption(Integer myActionAssignToOthersViewOption){
		this.myActionAssignToOthersViewOption=myActionAssignToOthersViewOption;
	}
	public Integer getMyActionAssignToOthersViewOption(){
		return this.myActionAssignToOthersViewOption;
	}

	public Integer getAssignToOthersFilter() {
		return assignToOthersFilter;
	}
	public void setAssignToOthersFilter(Integer assignToOthersFilter) {
		this.assignToOthersFilter = assignToOthersFilter;
	}
}
