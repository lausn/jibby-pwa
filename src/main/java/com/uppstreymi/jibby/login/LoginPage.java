package com.uppstreymi.jibby.login;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.ProjectLoadEvent;
import com.uppstreymi.jibby.backend.sync.manager.SynchronizeManager;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.customer.CustomerPopUp;
import com.uppstreymi.jibby.customer.CustomerPopUpEvent;
import com.uppstreymi.jibby.item.events.SyncModelGenerateEvent;
import com.uppstreymi.jibby.item.events.UserEvent;
import com.uppstreymi.jibby.login.auth.FacebookAnswer;
import com.uppstreymi.jibby.login.auth.FacebookCustomApi;
import com.uppstreymi.jibby.login.auth.Google2Api;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.page.ItemView;
import com.uppstreymi.jibby.utils.HasLogger;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.server.RequestHandler;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinServletResponse;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("login-page")
@JsModule("./src/views/login/login-page.js")
@Route(value = "login")
@RouteAlias(value ="")
@Viewport("width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover")
@UIScoped
public class LoginPage extends PolymerTemplate<LoginPage.LoginPageModel>   implements AfterNavigationObserver,HasLogger,RequestHandler{

	@Id("email")
	private TextField txtEmail;

	@Id("password")
	private PasswordField txtPassword;

	@Id("signin")
	private Button btnSignin;


	@Id("gplusLoginButton")
	private Anchor gplusLoginButton;
	
	@Id("facebookLoginButton")
	private Anchor facebookLoginButton;
	
	@Id("lblForget")
	private Anchor lblForget;

	@Inject private DbService dbService;
	@Inject private SessionModel sessionModel;
	@Inject private AppModel appModel;
	@Inject private Event<ProjectLoadEvent> projectLoadEvent;
	@Inject private Event<SyncModelGenerateEvent> syncGenerateEvent;
	@Inject SynchronizeManager syncManager;
	@Inject private CustomerPopUp customerPopUP;
	@Inject private javax.enterprise.event.Event<CustomerPopUpEvent> customerEvent;
	@Inject private Event<UserEvent> userEvent;
	@Inject private ForgetPasswordDesign forgetPassword;

	OAuthService service,fbService;

	ResourceBundle rs=ResourceBundle.getBundle("config",Locale.ENGLISH);
	Logger logger=LogManager.getLogger(LoginPage.class);

	public LoginPage() {
		
		btnSignin.addClickListener(e->login());
		btnSignin.addClickShortcut(Key.ENTER);
	}
	
	@PostConstruct
	public void init(){
		addGoogleButton();
		lblForget.getElement().addEventListener("click",e->{forgetPasswordEvent();});
	}
	
	public void forgetPasswordEvent(){
		Dialog dia = new Dialog();
		dia.setWidth("315px");
		dia.setHeight("125px");
		forgetPassword.setup();
		dia.add(forgetPassword);
		dia.open();
		
//		getUI().getCurrent().addWindow(forgetPassword);
//		forgetPassword.setup();
	}

	private void addGoogleButton(){
		service = createGoogleService();
		String url = service.getAuthorizationUrl(null);
		gplusLoginButton.setHref(url);
//		gplusLoginButton.getElement().setProperty("innerHTML", "<img src='images/authenticbutton/btn_google_signin_dark_normal_web.png'>");
		
		fbService = createFacebookService();
		String serviceUrl = fbService.getAuthorizationUrl(null);
		facebookLoginButton.setHref(serviceUrl);
		VaadinSession.getCurrent().addRequestHandler(this);

	}

	private OAuthService createFacebookService() {
		ServiceBuilder sb = new ServiceBuilder();
		sb.provider(FacebookCustomApi.class);
		sb.apiKey(rs.getString("fbkey"));
		sb.apiSecret(rs.getString("fbsecret"));
		sb.scope("email");
		String callBackUrl = Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());
		if (callBackUrl.contains("#")) {
			callBackUrl = callBackUrl.substring(0, callBackUrl.indexOf("#"));
		}
		sb.callback(callBackUrl + "?auth=fb");
		return sb.build();
	}
	
	
	
	private OAuthService createGoogleService() {
		ServiceBuilder sb = new ServiceBuilder();
		sb.provider(Google2Api.class);
		sb.apiKey(rs.getString("gpluskey"));
		sb.apiSecret(rs.getString("gplussecret"));
		sb.scope("openid email profile");
		//sb.scope("https://mail.google.com/ https://www.googleapis.com/auth/gmail.send  https://www.googleapis.com/auth/userinfo.email");
		String callBackUrl =Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());
		if(callBackUrl.contains("#")) {
			callBackUrl = callBackUrl.substring(0, callBackUrl.indexOf("#"));
		}
		sb.callback(callBackUrl+"?auth=gp");
		return sb.build();
	}

	private void login() {

		try {
			if (!txtEmail.isInvalid() && !txtPassword.isInvalid()) {
				JBUserEntity user = dbService.logIn(txtEmail.getValue(), txtPassword.getValue());
				if (user != null) {
					logger.info("{} logged in successfully. {}",user.getFirstName(), new Date());
					if (user.getType() != Arrays.asList(JibbyConfig.userTypes).indexOf("notActivated"))
						showCustomer(user, false);
					else
						Utils.showWarning("User account is not activated!");
				} else {
					getModel().setError(true);
				}
			}
		} catch (Exception e) {
			logger.error("An exception thrown in mobile login",e);
		}
	}

	private void showCustomer(JBUserEntity user, boolean isSocial) {
		List<JBCustomerEntity> customers=dbService.getCustomersByUser(user.getId());
		if(customers.size()==1)
			setUserTologin(user,customers.get(0), isSocial);
		else {
			customerEvent.fire(new CustomerPopUpEvent(customers, user, isSocial));
			if(!customerPopUP.isOpened())
				customerPopUP.open();
		}
	}

	private void setup(@Observes CustomerPopUpEvent event) {
		if(event.getUser()!=null && event.getCustomer()!=null) {
			if(customerPopUP.isOpened())
				customerPopUP.close();
			setUserTologin(event.getUser(),event.getCustomer(), event.isSocial());
		}
	}

	private void setUserTologin(JBUserEntity user,JBCustomerEntity customer,boolean isSocial){
		syncManager.addUserDataChangeModel(user.getId());
		sessionModel.setUser(user);
		sessionModel.setCustomer(customer);
		setActiveView(user);
		if(sessionModel.getActiveViewDTO()!=null) {
			projectLoadEvent.fire(new ProjectLoadEvent(true));
			//navigate();
			userEvent.fire(new UserEvent(user,false));
			syncGenerateEvent.fire(new SyncModelGenerateEvent(true));
			if(!isSocial)
				navigate();
		}else {
			sessionModel.setUser(null);
			sessionModel.setCustomer(null);
			Notification.show("An exception occured !", 3000, Position.MIDDLE);
		}


	}


	private void navigate() {
		UI.getCurrent().navigate(ItemView.class);
	}

	private void setActiveView(JBUserEntity user){
		if(user.getActiveUI()==null || user.getActiveUI().equals("null") || user.getActiveUI().trim().isEmpty()){
			user.setActiveUI(rs.getString("DEFAULT_ACTIVE_VIEW"));
			user=dbService.saveUser(user);
			sessionModel.setUser(user);
		}
		
		try{
			ObjectMapper objectMapper=new ObjectMapper();
			ActiveViewDTO activeView=objectMapper.readValue(user.getActiveUI(),ActiveViewDTO.class);
			sessionModel.setActiveViewDTO(activeView);
		}catch(Exception e){
			logger.error("An exception thrown in mobile login",e);
		}

	}

	public interface LoginPageModel extends TemplateModel {
		void setError(boolean error);
	}

	@Override
	public void afterNavigation(AfterNavigationEvent arg0) {
		if(sessionModel.getUser()!=null)
			UI.getCurrent().navigate(ItemView.class);
	}

	@Override
	public boolean handleRequest(VaadinSession session, VaadinRequest request, VaadinResponse response)
			throws IOException {
		
		if (request.getParameter("code") != null && sessionModel.getUser()==null) {
			String code = request.getParameter("code");
			Verifier v = new Verifier(code);

			if(request.getParameter("auth").equalsIgnoreCase("fb")){
				
				createJavaCertifiedSSL();
				Token t = fbService.getAccessToken(Token.empty(), v);
				OAuthRequest r=new OAuthRequest(Verb.GET, "https://graph.facebook.com/me?fields=id,email,first_name,gender,last_name");
				fbService.signRequest(t, r);
				Response resp = r.send();
				FacebookAnswer answer = new Gson().fromJson(resp.getBody(),FacebookAnswer.class);

				logger.info("Fb login mobile "+resp.getBody()+" "+answer.id+" "+answer.email+" "+answer.gender+" "+answer.first_name+" "+answer.last_name);
				
				mailLogin(answer.email,answer.first_name);
				((VaadinServletResponse) response).getHttpServletResponse().
				sendRedirect(Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl()));

			}else{
				createJavaCertifiedSSL();
				Token t = service.getAccessToken(Token.empty(), v);
				OAuthRequest r = new OAuthRequest(Verb.GET,	"https://openidconnect.googleapis.com/v1/userinfo");

				service.signRequest(t, r);
				Response resp = r.send();

				JSONObject json = new JSONObject(resp.getBody());

				String email = json.getString("email");
				String name = json.getString("name");
				mailLogin(email,name);
				((VaadinServletResponse) response).getHttpServletResponse().
				sendRedirect(Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl()));
			}

			return true;
		}

		return false;
	}

	private void mailLogin(String email,String name){
		JBUserEntity user;
		try {
			user = dbService.getUserByEmail(email);
			if(user!=null) {
				if(user.getType()!=Arrays.asList(JibbyConfig.userTypes).indexOf("notActivated"))
					showCustomer(user, true);
				else
					Utils.showWarning("User account is not activated!");
			}else {
				getModel().setError(true);
			}
		}
		catch (Exception e) {
			logger.error("An exception thrown in mobile mailLogin",e);
		}
	}

	private void createJavaCertifiedSSL() {
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}};

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			logger.error("An exception thrown in mobile login createJavaCertifiedSSL",e);
		}
	}

}
