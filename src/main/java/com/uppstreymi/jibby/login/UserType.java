package com.uppstreymi.jibby.login;

public enum UserType {
	Admin,VIP,Disabled,Licensed,Subscribed,Unsubscribed,Presubscribed,Expired,Legacy,Trial,Invited,PaidMember,LifeMember,FreeMember;
}
