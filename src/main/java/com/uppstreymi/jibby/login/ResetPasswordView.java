package com.uppstreymi.jibby.login;

import com.vaadin.flow.templatemodel.TemplateModel;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;


@Tag("reset-password-view")
@Route("reset-password")
@JsModule("./src/views/login/reset-password-view.js")
@UIScoped
public class ResetPasswordView extends PolymerTemplate<ResetPasswordView.ResetPasswordViewModel>  implements HasUrlParameter<String>{

	String url="";
	String GUID_STR = "";

	@Id("txtUserName")
	private TextField txtUserName;
	@Id("txtPassword")
	private PasswordField txtPassword;
	@Id("txtRePassword")
	private PasswordField txtRePassword;
	@Id("btnReset")
	private Button btnReset;
	@Id("lblMessage")
	private Label lblMessage;


	@Inject private DbService service;
	@Inject private SessionModel sessionModel;
	@Inject private AppModel appModel;
	private Logger logger=LogManager.getLogger(ResetPasswordView.class);
	public ResetPasswordView() {

	}
	@PostConstruct
	protected void init() {

	}

	private void cmpEvent() {

		btnReset.addClickShortcut(Key.ENTER);
		btnReset.addClickListener(clickEvent -> {
			resetPassword();
		});
	}

	public void txtClear(){
		txtUserName.setValue("");
		txtPassword.setValue("");
		txtRePassword.setValue("");
		lblMessage.setText("");
	}

	private void resetPassword(){
		if(!isEmptyCheck()){
			if(passwordMatch())
				getUserInfo();
			else
				lblMessage.getElement().setProperty("innerHTML","<strong style='color:red'>Password does'nt match</strong>");
		}
	}

	private boolean isEmptyCheck(){
		if(!txtUserName.getValue().isEmpty()){
			if(!txtPassword.getValue().isEmpty()){
				if(!txtRePassword.getValue().isEmpty()){
					return false;
				}else{
					lblMessage.getElement().setProperty("innerHTML","<strong style='color:red'>Please enter retype password!</strong>");
				}
			}else{
				lblMessage.getElement().setProperty("innerHTML","<strong style='color:red'>Please enter new password!</strong>");
			}
		}else{

			lblMessage.getElement().setProperty("innerHTML","<strong style='color:red'>Please enter an email address!</strong>");
		}

		return true;
	}

	private boolean passwordMatch(){
		if(txtPassword.getValue().equals(txtRePassword.getValue()))
			return true;
		return false;
	}

	private void getUserInfo(){
		try {

			JBUserEntity user = service.getUserByEmailAndTempguid(txtUserName.getValue(), GUID_STR);
			if(user!=null){
				if(user.getTempguid()!=null){
					user.setPassword(Utils.sha1(txtPassword.getValue()));
					user.setTempguid(null);
					service.saveUser(user);

					List<JBCustomerEntity> customers = service.getCustomersByUser(user.getId());
					if(customers.size()==1) {
						sessionModel.setUser(user);
						sessionModel.setLoggedIn(true);
						setActiveView(user);
						sessionModel.setCustomer(customers.get(0));
					}

					url=Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());
					UI.getCurrent().navigate("");
				}
			}
			else{

				lblMessage.getElement().setProperty("innerHTML","<strong style='color:red'>Your Password reset process is expired!</strong>");
			}
		} catch (Exception e) {
			logger.error("ResetPassword getUserInfo an exception thrown:",e);
		}
	}

	private void setActiveView(JBUserEntity user){
		if(user.getActiveUI()!=null){
			try{
				ObjectMapper objectMapper=new ObjectMapper();
				ActiveViewDTO activeView=objectMapper.readValue(user.getActiveUI(),ActiveViewDTO.class);
				sessionModel.setActiveViewDTO(activeView);
			}catch(Exception e){
				logger.error("ResetPassword setActiveView an exception thrown:",e);
			}
		}
	}

	public interface ResetPasswordViewModel extends TemplateModel {

	}

	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
		Location location = event.getLocation();
		QueryParameters queryParameters = location.getQueryParameters();

		Map<String, List<String>> parametersMap = queryParameters.getParameters();
		GUID_STR = parametersMap.get("guid") != null ? parametersMap.get("guid").get(0) : null;
		if(GUID_STR!=null || GUID_STR!=""){
			cmpEvent();
			txtClear();
		}
		else 
			UI.getCurrent().navigate("");
	}
}
