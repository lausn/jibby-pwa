package com.uppstreymi.jibby.login;

import com.vaadin.flow.templatemodel.TemplateModel;

import java.io.InputStream;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.service.HttpMailgunService;
import com.uppstreymi.jibby.service.MailgunService;
import com.uppstreymi.jibby.ui.Props;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;


@Tag("forget-password-design")
@JsModule("./src/views/login/forget-password-design.js")
@UIScoped
public class ForgetPasswordDesign extends PolymerTemplate<ForgetPasswordDesign.ForgetPasswordDesignModel> {

	@Id("flayout")
	private Div flayout;
	@Id("txtEmail")
	private TextField txtEmail;
	@Id("lblmessage")
	private Div lblmessage;
	@Id("btnSend")
	private Button btnSend;
	@Id("btnCancel")
	private Button btnCancel;

	@Inject DbService service;

	@Inject SessionModel sessionModel;
	@Inject private AppModel appModel;
	private MailgunService mailService = new MailgunService();

	private HttpMailgunService httpMailgunService = new HttpMailgunService();
	private Logger logger=LogManager.getLogger(ForgetPasswordDesign.class);
	public ForgetPasswordDesign() {

	}
	public void setup(){
		txtClear();
		lblmessage.setVisible(false);
		btnSend.setVisible(true);
		flayout.setVisible(true);
	}
	@PostConstruct
	private void init(){
		cmpEvent();
	}

	private void cmpEvent(){
		btnSend.addClickListener(event ->sendAction());
		btnCancel.addClickListener(event ->cancelBtnAction());
	}

	private void sendAction(){		
		if(!txtEmail.getValue().isEmpty()){
			JBUserEntity user;

			try {
				user = service.getUserByEmail(txtEmail.getValue());
				if(user==null){
					lblmessage.setVisible(true);
					lblmessage.getElement().setProperty("innerHTML","<strong style='color:red'>Email incorrect. Please try again.</strong>");
				}
				else{

					if(user.getPassword()!=null || !user.getPassword().isEmpty()){
						UUID uuid = UUID.randomUUID();
						String UUIDString = uuid.toString();
						String additionalURL = "reset-password?guid="+UUIDString;
						String url="";

						url=Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());

						additionalURL = url+additionalURL;
						additionalURL = additionalURL.replaceAll("(?<!http:|https:)//", "/");

						String strHtml = IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("mailHTML/forgetpassword.html"), "UTF-8");
						strHtml = strHtml.replace("#name#",user.getFirstName()+" "+user.getLastName());
						strHtml = strHtml.replace("#additionalURL#",additionalURL);

						String fromAddress = "noreply@jibby.com";
						String fromName = Props.UITitle;
						String toName = user.getFirstName()+" "+user.getLastName();
						String subject = "Forget Password";
						String cc = "";
						String bcc ="";
						String text = "";
						String cal = null;
						String toAddress = user.getEmail();

						Integer status = httpMailgunService.sendHTTPMail(fromAddress, fromName, toAddress, toName, subject, cc, bcc, text,
								strHtml, "", null, "", null);

						if (status != null && status==200) {
							user.setTempguid(UUIDString);
							service.saveUser(user);
							btnSend.setVisible(false);
							flayout.setVisible(false);
							lblmessage.setVisible(true);
							String message = "We've sent an email to <strong style='color:#024f67'>"+user.getEmail()+"</strong>. Click the link in the email to reset your password. if you don't see the email,check other places it might be, like your junk, spam, social, or other folders.";
							lblmessage.getElement().setProperty("innerHTML",message);
							lblmessage.getElement().setAttribute("style", "margin-top: 26px;");
						}
					}
				}
			} catch (Exception e) {
				logger.error("ForgetPassword sendAction an exception thrown:",e);

				String text = "<strong style='color:red'>Sorry! try later with your e-mail.</strong>";
				lblmessage.getElement().setProperty("innerHTML",text);
			}
		}
		else{
			lblmessage.getElement().setProperty("innerHTML","<strong style='color:red'>Please enter an email address!</strong>");
		}
	}

	private void cancelBtnAction(){
		closeWindow();
	}

	private void closeWindow() {
		Dialog dia = (Dialog) getParent().get();
		dia.close();
	}

	public void txtClear(){
		txtEmail.setValue("");
		lblmessage.setText("");
	}
	public interface ForgetPasswordDesignModel extends TemplateModel {

	}
}
