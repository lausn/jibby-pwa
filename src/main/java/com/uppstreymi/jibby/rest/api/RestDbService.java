package com.uppstreymi.jibby.rest.api;



import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.manager.SyncProcessor;

@RequestScoped
@Path("monitorservice")
public class RestDbService {

	private static final String JSON_UTF8 = "application/json; charset=UTF-8";
	private Logger logger=LogManager.getLogger(RestDbService.class);
	
	@Inject SyncProcessor syncProcessor;

	@Context
	private HttpServletResponse servletResponse;
	
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("sync")
	public String jibbySync(String json) {
		servletResponse.addHeader("Access-Control-Allow-Origin", "*");
		servletResponse.addHeader("Access-Control-Allow-Credentials", "true");
		servletResponse.addHeader("Access-Control-Allow-Methods", "POST");
		servletResponse.addHeader("Access-Control-Max-Age", "86400");
		servletResponse.addHeader("x-responded-by", "cors-response-filter");
		try {
			syncProcessor.addEvent(json);
			return "success";
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("An exception thrown in jibbySync in RestDbService",e);
		}
		return "failed";
	}

}
