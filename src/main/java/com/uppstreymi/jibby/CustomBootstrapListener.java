package com.uppstreymi.jibby;


import org.jsoup.nodes.Element;

import com.vaadin.flow.server.BootstrapListener;
import com.vaadin.flow.server.BootstrapPageResponse;

/**
 * Modifies the Vaadin bootstrap page (the HTTP response) in order to
 * <ul>
 * <li>add service worker</li>
 * <li>add a link to the web app manifest</li>
 * <li>add links to favicons</li>
 * </ul>
 */
public class CustomBootstrapListener implements BootstrapListener {

	 
	@Override
	public void modifyBootstrapPage(BootstrapPageResponse response) {
		// TODO Auto-generated method stub
		final Element head = response.getDocument().head();
		
		// manifest needs to be prepended before scripts or it won't be loaded
		head.prepend("<meta name='theme-color' content='#10708e'>");
		head.prepend("<meta name='mobile-web-app-capable' content='yes'>");
		head.prepend("<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent'>");
		head.prepend("<meta name='apple-mobile-web-app-capable' content='yes'>");
		head.prepend("<link rel='manifest' href='manifest.webmanifest'>");
	
		// add icons tags
		head.append("<link rel='shortcut icon' href='icons/favicon.ico'>");
	}
}
