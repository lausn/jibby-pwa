package com.uppstreymi.jibby.customer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.dialog.Dialog;

@UIScoped
public class CustomerPopUp extends Dialog{
	
	@Inject CustomerView customerPopupImpl;
	public CustomerPopUp() {
		
	}
	
	@PostConstruct
	public void init(){
		
		setWidth("275px");
		setCloseOnOutsideClick(false);
		this.add(customerPopupImpl);
	}
	
}
