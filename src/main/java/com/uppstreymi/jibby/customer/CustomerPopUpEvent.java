package com.uppstreymi.jibby.customer;

import java.util.List;

import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;

public class CustomerPopUpEvent {

	private List<JBCustomerEntity> customers;
	private JBCustomerEntity customer;
	private JBUserEntity user;
	private boolean isSocial = false;
	
	public CustomerPopUpEvent(JBCustomerEntity customer,JBUserEntity user, boolean isSocial) {
		this.customer=customer;
		this.user=user;
		this.isSocial = isSocial;
	}
	
	public CustomerPopUpEvent(List<JBCustomerEntity> customers,JBUserEntity user, boolean isSocial) {
		this.customers=customers;
		this.user=user;
		this.isSocial = isSocial;
	}
	
	public List<JBCustomerEntity> getCustomers(){
		return this.customers;
	}
	
	public JBCustomerEntity getCustomer(){
		return this.customer;
	}
	
	public JBUserEntity getUser(){
		return this.user;
	}
	
	public boolean isSocial() {
		return this.isSocial;
	}
}
