package com.uppstreymi.jibby.customer;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBLicenseInvitation;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("customer-view")
@JsModule("./src/views/customer/customer-view.js")
@UIScoped
public class CustomerView extends PolymerTemplate<CustomerView.CustomerViewModel>{

	@Id("nSelect")
	private Select<JBCustomerEntity> nSelect;
	@Id("btnSelect")
	private Button btnSelect;
	
	private JBUserEntity user;
	private JBCustomerEntity customer;
	private boolean isSocial;
	
	@Inject private javax.enterprise.event.Event<CustomerPopUpEvent> customerEvent;
	public CustomerView() {
		nSelect.setItemLabelGenerator(e->{
			return e.getName();
		});
		
		btnSelect.addClickListener(e->{
			customer=nSelect.getValue();
			if(customer!=null) {
				customerEvent.fire(new CustomerPopUpEvent(customer, user, this.isSocial));
			}
		});
		
	}
	
	public void setup(@Observes CustomerPopUpEvent event) {
		if(event.getUser()!=null && event.getCustomers()!=null) {
			nSelect.setItems(event.getCustomers());
			user=event.getUser();
			this.isSocial = event.isSocial();
			customer=null;
			nSelect.clear();
			nSelect.setValue(event.getCustomers().get(0));
		}
	}
	
	public interface CustomerViewModel extends TemplateModel {
	}
}
