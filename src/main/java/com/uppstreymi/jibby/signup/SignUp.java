package com.uppstreymi.jibby.signup;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.vaadin.marcus.shortcut.Shortcut;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.mail.smtp.SMTPTransport;
import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBCustomerUsersEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.login.auth.FacebookAnswer;
import com.uppstreymi.jibby.login.auth.FacebookCustomApi;
import com.uppstreymi.jibby.login.auth.Google2Api;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.service.MailgunService;
import com.uppstreymi.jibby.utils.HasLogger;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.RequestHandler;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinServletResponse;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("sign-up")
@JsModule("./src/views/signup/sign-up.js")
@Route(value = "signup")
@UIScoped
public class SignUp extends PolymerTemplate<SignUp.SignUpModel> implements HasLogger,RequestHandler{

	@Id("fullname")
	private TextField txtfullname;
	@Id("email")
	private TextField txtemail;
	@Id("password")
	private PasswordField txtpassword;
	@Id("re-password")
	private PasswordField rePassword;

	@Id("gplusLoginButton")
	private Anchor gplusLoginButton;

	@Id("facebookLoginButton")
	private Anchor facebookLoginButton;

	@Id("signup")
	private Button btnSignup;

	@Inject
	private ISignUpNewEntity iSignupNewEntity;

	@Inject 
	private AppModel appModel;

	@Inject
	private MailgunService mailService;
	@Inject private SessionModel sessionModel;

	Logger logger=LogManager.getLogger(SignUp.class);
	OAuthService service,fbService;

	@Inject private DbService dbservice;
	private BeanValidationBinder<JBUserEntity> binder = new BeanValidationBinder<>(JBUserEntity.class);
	ResourceBundle rs=ResourceBundle.getBundle("config",Locale.ENGLISH);

	public SignUp() {
		Shortcut.add(txtfullname, "Enter", btnSignup::click);
		Shortcut.add(txtemail, "Enter", btnSignup::click);
		Shortcut.add(txtpassword, "Enter", btnSignup::click);
		Shortcut.add(rePassword, "Enter", btnSignup::click);
		binder.setBean(newBean());
		setBinder();
	}

	@PostConstruct
	private void init() {
		btnSignup.addClickListener(e->signupCheck());
		addGoogleButton();
		addFbButton();
	}

	private void addGoogleButton(){
		service = createGoogleService();
		String url = service.getAuthorizationUrl(null);
		gplusLoginButton.setHref(url);
//		gplusLoginButton.getElement().setProperty("innerHTML", "<img src='images/authenticbutton/btn_google_signin_dark_normal_web.png'>");
		VaadinSession.getCurrent().addRequestHandler(this);
	}

	private OAuthService createGoogleService() {
		ServiceBuilder sb = new ServiceBuilder();
		sb.provider(Google2Api.class);
		sb.apiKey(rs.getString("gpluskey"));
		sb.apiSecret(rs.getString("gplussecret"));
		sb.scope("openid email profile");
		String callBackUrl =Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());
		if(callBackUrl.contains("#")) {
			callBackUrl = callBackUrl.substring(0, callBackUrl.indexOf("#"));
		}
		sb.callback(callBackUrl+"?auth=gp");
		return sb.build();
	}

	private OAuthService createFacebookService() {
		ServiceBuilder sb = new ServiceBuilder();
		sb.provider(FacebookCustomApi.class);
		sb.apiKey(rs.getString("fbkey"));
		sb.apiSecret(rs.getString("fbsecret"));
		sb.scope("email");
		String callBackUrl = Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());
		if (callBackUrl.contains("#")) {
			callBackUrl = callBackUrl.substring(0, callBackUrl.indexOf("#"));
		}
		sb.callback(callBackUrl + "?auth=fb");
		return sb.build();
	}

	private void addFbButton(){
		fbService = createFacebookService();
		String url = fbService.getAuthorizationUrl(null);
		facebookLoginButton.setHref(url);
		VaadinSession.getCurrent().addRequestHandler(this);
	}

	private JBUserEntity newBean() {
		JBUserEntity user=new JBUserEntity();
		user.setLastName("");
		user.setUsername("");
		user.setEmailAboutEntityChanges(Arrays.asList(JibbyConfig.emailAboutEntityChangesTypes).indexOf("Hourly"));
		user.setNewsletterActive(JibbyConfig.checkItemStatus[0]);
		user.setType(Arrays.asList(JibbyConfig.userTypes).indexOf("notActivated"));
		user.setDateFormat("yyyy.MM.dd. HH:mm");
		user.setFirstDayOfWeek(1);
		user.setUnitNames(String.join("\n",JibbyConfig.defaultUnitNames));
		user.setCostNames(String.join("\n",JibbyConfig.defaultCostNames));
		user.setRegistrationDate(new Date());
		user.setActivationCode(getActivationCode());
		user.setHelper("");
		user.setReminderEntityUnfinished(Arrays.asList(JibbyConfig.reminderEntityUnfinished).indexOf("Off"));
		user.setReminderEntityStartFinish(-1);
		user.setDecimalSeparator(".");
		user.setTimeZone("UTC");
		user.setActiveUI(rs.getString("DEFAULT_ACTIVE_VIEW"));
		user.setExpiredDate(Utils.addDaysStartTime(new Date(),JibbyConfig.trialLength));
		user.setIsInvitedUser(0);
		user.setGuid(UUID.randomUUID().toString());
		return user;
	}

	private void setBinder() {
		binder.forField(txtfullname).asRequired("Name shouldn't be empty!").bind(JBUserEntity::getFirstName, JBUserEntity::setFirstName);
		binder.forField(txtemail).asRequired("Email shouldn't be empty!").withValidator(new EmailValidator(
				"This doesn't look like a valid email address!")).withValidator(email->isExistMail()==null,"Email address is already in use!")
		.bind(JBUserEntity::getEmail, JBUserEntity::setEmail);
		binder.forField(txtpassword).asRequired("Password shouldn't be empty!")
		.bind(user->{String pass="";return user.getPassword();},(user,pass)->{
			user.setPassword(Utils.sha1(txtpassword.getValue()));
		});
		binder.forField(rePassword).asRequired("Re-type Password shouldn't be empty!")
		.withValidator(repass -> isMatch(),"Password doesn't match!")
		.bind(user->{String pass="";return user.getPassword();},(user,pass)->{
			user.setPassword(Utils.sha1(txtpassword.getValue()));
		});
	}

	public void write(JBUserEntity entity) throws ValidationException {
		binder.writeBean(entity);
	}

	public boolean writeEntity() {
		if(binder!=null) {
			try {
				write(binder.getBean());
				return true;
			} catch (ValidationException e) {
				logger.error("A ValidationException thrown in SignUp WriteEntity",e);
				return false;
			} catch (NullPointerException e) {
				logger.error("A NullPointerException thrown in SignUp WriteEntity",e);
				return false;
			}
		}else
			return false;
	}

	private void signupCheck() {
		if(writeEntity()) {
			sendMailToUser();
		}
	}

	private JBCustomerEntity createNewCustomer(Integer userId,String name) {
		JBCustomerEntity customer=new JBCustomerEntity();
		customer.setName(name);
		customer.setSubscriptionguid(UUID.randomUUID().toString());
		customer.setCreatedate(new Date());
		customer = dbservice.saveCustomer(customer);

		JBCustomerUsersEntity customerUser=new JBCustomerUsersEntity();
		customerUser.setCustomerid(customer.getId());
		customerUser.setUserid(userId);
		dbservice.saveCustomerUser(customerUser);

		return customer;
	}

	private void successMsg() {
		String message = " Congratulations, you have successfully signed up for Jibby!"
				+"We have just sent you an email to confirm your account. Please click the confirmation link in the email to finish activating your account.";
		//+"If you have any questions or didn't get the confirmation email, please contact <a href='https://app.jibby.com/support'>Jibby Support</a>";
		VerticalLayout vl = new VerticalLayout();
		Label msg = new Label(message);
		msg.getElement().getStyle().set("font-size", "14px");
		Button okButton = new Button("Cancel");
		okButton.addThemeName("primary");
		okButton.getElement().getStyle().set("background-color", "#dc4242");
		Dialog dl= new Dialog();
		vl.add(msg,okButton);
		dl.add(vl);
		okButton.addClickListener(e->{
			dl.close();
		});
		dl.open();
	}

	private JBUserEntity isExistMail() {
		JBUserEntity user=null;
		try {
			user = dbservice.getUserByEmail(txtemail.getValue());
		}catch (Exception e) {
			logger.error("An exception thrown in SignUp isExistMail",e);
		}

		return user;
	}

	private boolean isMatch() {
		if(txtpassword.getValue().equals(rePassword.getValue()))
			return true;
		return false;
	}

	private String getActivationCode() {
		UUID uuid = UUID.randomUUID();
		String UUIDString = uuid.toString();
		UUIDString = UUIDString.replaceAll("-","");
		return UUIDString;
	}

	public interface SignUpModel extends TemplateModel {
	}

	private void sendMailToUser() {
		JBUserEntity user = binder.getBean();
		String fromAddress = "noreply@jibby.com";
		String fromName = Utils.jibbyTitle;
		String toName = user.getFirstName()+" "+user.getLastName();
		String subject = "Welcome to Jibby";
		String cc = "";
		String bcc = "";
		String text = "";
		String strHtml="";
		String cal = null;

		try {
			String url=Utils.getFilterUrl(appModel.getPropertiesModel().getMobileAppUrl());
			List<String> invitationEmailList = new ArrayList<>();
			invitationEmailList.add(user.getEmail());

			String activationUrl = url+"activate?jbacid="+URLEncoder.encode(Utils.encrypt(user.getActivationCode()), "UTF-8");;
			activationUrl = activationUrl.replaceAll("(?<!http:|https:)//", "/");

			strHtml = IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("signupMail/signupMail.html"), "UTF-8");
			strHtml = strHtml.replace("#name#",toName);
			strHtml = strHtml.replace("#email#",user.getEmail());
			strHtml = strHtml.replace("#password#",txtpassword.getValue());
			strHtml = strHtml.replace("#activationUrl#",activationUrl);
			SMTPTransport trans = mailService.sendSMTPMail(fromAddress, fromName, invitationEmailList, toName, subject, cc, bcc, text, strHtml,cal,null);

			if(trans!=null){
				JBUserEntity newUser= createUser(binder.getBean());
				if(newUser!=null) {
					successMsg();
					binder.setBean(newBean());
					sendMailToAdmin(newUser);
				}else {
					logger.info("Jibby new user sign-up {} failed",user.getEmail());
					Utils.showWarning("We were unable to creat user email to "+user.getEmail()+". \n Please try again later or contact Jibby support if you are not able to sign up.");
				}

			}else {
				logger.info("Jibby new user sign-up confirmation mail to {} sending faild.",user.getEmail());
				Utils.showWarning("We were unable to send a confirmation email to "+user.
						getEmail()
				+". \n Please try again later or contact Jibby support if you are not able to sign up."
						); }

		} catch (Exception e) {
			logger.info("Jibby new user sign-up confirmation mail to {} sending faild.",user.getEmail());
			logger.error("An exception thrown in SignUp sendMailToUser",e);
			Utils.showWarning("We were unable to send a confirmation email to "+user.getEmail()+". \n Please try again later or contact Jibby support if you are not able to sign up.");
		}
	}

	private JBUserEntity createUser(JBUserEntity user) {
		user=dbservice.saveUser(user);
		if(null!=user) {
			JBCustomerEntity customer= createNewCustomer(user.getId(),"Default customer 1");
			iSignupNewEntity.createNewDefaultProject(user,customer);

		}
		return user;
	}

	private void sendMailToAdmin(JBUserEntity user) {
		String fromAddress = "noreply@jibby.com";
		String fromName = Utils.jibbyTitle;
		String toName = "Jibby Team";
		String subject = "New Jibby user sign-up";
		String cc = "";
		String bcc = "";
		String text = "";
		String strHtml="";
		String cal = null;

		try {

			List<String> invitationEmailList = new ArrayList<>();
			invitationEmailList.add("admin@jibby.com");

			strHtml = IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("signupMail/signupMailToAdmin.html"), "UTF-8");
			strHtml = strHtml.replace("#name#",toName);
			strHtml = strHtml.replace("#title#","New Jibby user sign-up!");
			strHtml = strHtml.replace("#email#",user.getEmail());
			strHtml = strHtml.replace("#userName#",user.getFirstName()+" "+user.getLastName());

			SMTPTransport transPort = mailService.sendSMTPMail(fromAddress, fromName, invitationEmailList, toName, subject, cc, bcc, text, strHtml,cal,null);

		} catch (Exception e) {
			logger.info(" Jibby new user sign-up mail to \"admin@jibby.com\" sending faild. \n ",e);
		}
	}

	@Override
	public boolean handleRequest(VaadinSession session, VaadinRequest request, VaadinResponse response)
			throws IOException {
		if (request.getParameter("code") != null) {
			String code = request.getParameter("code");
			Verifier v = new Verifier(code);

			if(request.getParameter("auth").equalsIgnoreCase("fb")){
				createJavaCertifiedSSL();
				Token t = fbService.getAccessToken(Token.empty(), v);
				OAuthRequest r=new OAuthRequest(Verb.GET, "https://graph.facebook.com/me?fields=id,email,first_name,gender,last_name");
				fbService.signRequest(t, r);
				Response resp = r.send();
				FacebookAnswer answer = new Gson().fromJson(resp.getBody(),FacebookAnswer.class);

				logger.info("Handle request in mobile signup",resp.getBody()+" "+answer.id+" "+answer.email+" "+answer.gender+" "+answer.first_name+" "+answer.last_name);
				mailLogin(answer.email,answer.first_name);
				((VaadinServletResponse) response).getHttpServletResponse().
				sendRedirect(Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl()));

			}else{
				createJavaCertifiedSSL();
				Token t = service.getAccessToken(Token.empty(), v);
				OAuthRequest r = new OAuthRequest(Verb.GET,	"https://openidconnect.googleapis.com/v1/userinfo");
				service.signRequest(t, r);
				Response resp = r.send();

				JSONObject json = new JSONObject(resp.getBody());
				String email = json.getString("email");
				String name = json.getString("name");
				VaadinSession.getCurrent().removeRequestHandler(this);
				mailLogin(email,name);
				((VaadinServletResponse) response).getHttpServletResponse().
				sendRedirect(Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl()));

			}

			return true;
		}

		return false;
	}

	private void mailLogin(String email,String name){
		JBUserEntity user;
		try {
			user = dbservice.getUserByEmail(email);
			if (null != user) {
				loginAuth(user);
			}
			else {
				user=createUser(getUser(email,name,""));
				if (null != user)
					loginAuth(user);
			}
		}
		catch (Exception e) {
			logger.error("An exception thrown in SignUp mailLogin",e);
		}
	}

	private JBUserEntity getUser(String email,String name,String password) {
		JBUserEntity user = new JBUserEntity();
		user.setFirstName(name);
		user.setEmail(email);
		user.setPassword(password);
		user.setLastName("");
		user.setUsername("");
		user.setEmailAboutEntityChanges(Arrays.asList(JibbyConfig.emailAboutEntityChangesTypes).indexOf("Hourly"));
		user.setNewsletterActive(JibbyConfig.checkItemStatus[0]);
		user.setType(password==""?1:Arrays.asList(JibbyConfig.userTypes).indexOf("notActivated"));
		user.setDateFormat("yyyy.MM.dd. HH:mm");
		user.setFirstDayOfWeek(1);
		user.setUnitNames(String.join("\n",JibbyConfig.defaultUnitNames));
		user.setCostNames(String.join("\n",JibbyConfig.defaultCostNames));
		user.setRegistrationDate(new Date());
		user.setActivationCode(getActivationCode());
		user.setHelper("");
		user.setReminderEntityUnfinished(Arrays.asList(JibbyConfig.reminderEntityUnfinished).indexOf("Off"));
		user.setReminderEntityStartFinish(-1);
		user.setDecimalSeparator(".");
		user.setTimeZone("UTC");
		user.setActiveUI(rs.getString("DEFAULT_ACTIVE_VIEW"));
		user.setExpiredDate(Utils.addDaysStartTime(new Date(),JibbyConfig.trialLength));
		user.setGuid(UUID.randomUUID().toString());
		return user;
	}

	private void loginAuth(JBUserEntity user){
		List<JBCustomerEntity> customers = dbservice.getCustomersByUser(user.getId());
		sessionModel.setUser(user);
		sessionModel.setCustomer(customers.get(0));
		sessionModel.setLoggedIn(true);
		setActiveView(user);
	}

	private void setActiveView(JBUserEntity user){

		if(user.getActiveUI()!=null){
			try{
				ObjectMapper objectMapper=new ObjectMapper();
				ActiveViewDTO activeView=objectMapper.readValue(user.getActiveUI(),ActiveViewDTO.class);
				sessionModel.setActiveViewDTO(activeView);
			}catch(Exception e){
				logger.error("An exception thrown in mobile setActiveView",e);
			}
		}
	}

	private void createJavaCertifiedSSL() {
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}};

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			logger.error("An exception thrown in signup createJavaCertifiedSSL",e);
		}
	}
}
