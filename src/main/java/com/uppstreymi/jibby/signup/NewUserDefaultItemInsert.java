package com.uppstreymi.jibby.signup;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBChatMessageEntity;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;



@UIScoped
public class NewUserDefaultItemInsert implements ISignUpNewEntity{
	@Inject private DbService dbservice;
	@Inject private AppModel appModel;
	private JBUserEntity newUser;
	private JBCustomerEntity customer;

	private JBEntity parentProject;
	Logger logger=LogManager.getLogger(NewUserDefaultItemInsert.class);
	public NewUserDefaultItemInsert(){

	}

	@PostConstruct
	public void init(){

	}

	@Override
	public void createNewDefaultProject(JBUserEntity user, JBCustomerEntity customer) {
		this.newUser = user;
		this.customer = customer;
		if(appModel.getPropertiesModel()!=null) {
			JBEntity sourceEntity = dbservice.getEntity(appModel.getPropertiesModel().getMyfirstjibbyProjectId());
			if(sourceEntity!=null)
				copyItemwritetoDB(sourceEntity,null);
		}
	}

	private void copyItemwritetoDB(JBEntity sourceEntity,JBEntity targetEntity){
		Integer sourceEntityId=sourceEntity.getId();
		sourceEntity.setId(null);
		sourceEntity.setParentId(targetEntity==null?null:targetEntity.getId());
		sourceEntity.setParentProjectId(targetEntity==null?null:(targetEntity.getParentProjectId()==null?targetEntity.getId():targetEntity.getParentProjectId()));
		sourceEntity.setCreationDate(new Date());
		sourceEntity.setLastModificationDate(new Date());
		sourceEntity.setCreator(newUser.getId());
		sourceEntity.setIsPublic(0);
		sourceEntity=dbservice.saveEntity(sourceEntity);

		JBUserRoleEntity userRole = new JBUserRoleEntity();
		userRole.setEntityId(sourceEntity.getId());
		userRole.setUserRole(Arrays.asList(JibbyConfig.userRoles).indexOf("owner")+1);
		userRole.setUser(newUser);
		userRole.setCustomerid(customer==null?null:customer.getId());
		dbservice.saveUserRoleEntity(userRole);

		// Copy all Checklist item
		List<JBChecklistItemEntity>checklistitems= dbservice.getChecklistByEntity(sourceEntityId);
		for(JBChecklistItemEntity checkItem:checklistitems){
			JBChecklistItemEntity newcheckItem=checkItem;
			newcheckItem.setId(null);
			newcheckItem.setEntity(sourceEntity.getId());
			dbservice.saveChecklistItems(newcheckItem);
		}

		// Copy all Chat message item
		List<JBChatMessageEntity>chatitems= dbservice.getChatlistByEntity(sourceEntityId);
		for(JBChatMessageEntity chatitem:chatitems){
			JBChatMessageEntity newChatItem=chatitem;
			newChatItem.setId(null);
			newChatItem.setEntity(sourceEntity.getId());
			dbservice.saveChatAndComments(newChatItem);
		}

		// Attachment CopyWrite
		writeAttachment(sourceEntity,sourceEntityId);

		// copy all child from source entity recursivly
		List<JBEntity> entitieslist= dbservice.getAllChildByEntityId(sourceEntityId);
		for(JBEntity entity:entitieslist){
			copyItemwritetoDB(entity,sourceEntity);
		}
	}

	private void writeAttachment(JBEntity entity,Integer sourceId){
		List<JBAttachmentEntity> attachments= dbservice.getAttachmentByEntity(sourceId);
		for(JBAttachmentEntity attachment:attachments){
			if(fileMove(attachment)){
				JBAttachmentEntity attachmentEntity=new JBAttachmentEntity();
				attachmentEntity.setEntity(entity.getId());
				attachmentEntity.setUploaderUser(newUser.getId());
				attachmentEntity.setuploadDate(new Date());
				attachmentEntity.setName(attachment.getName());
				attachmentEntity.setType(attachment.getType());
				attachmentEntity.setSize(attachment.getSize().intValue());
				attachmentEntity.setDescription("");
				attachmentEntity.setSortOrder(attachment.getSortOrder());
				dbservice.saveAttachment(attachmentEntity);
				File file =new File(Utils.imagePath + "temp_userdefult_attachment_file0");
				file.renameTo(new File(Utils.imagePath + attachmentEntity.getId()));
				Utils.deleteFile(Utils.imagePath + "temp_userdefult_attachment_file0");

			}
		}
	}

	private boolean fileMove(JBAttachmentEntity attachment){
		File sourecFile=new File(Utils.imagePath+""+attachment.getId());
		logger.info("sourecFile exists In NewUserDefaultItemInsert. {}",sourecFile.exists());
		if(sourecFile.exists()) {
			File destinationFile=new File(Utils.imagePath+"temp_userdefult_attachment_file0");

			File fileFolder = new File(Utils.imagePath);
			if (!fileFolder.exists()) {
				try{
					if (fileFolder.mkdirs()) {
						FileUtils.copyFile(sourecFile, destinationFile);
						logger.info("DestinationFile Exits In NewUserDefaultItemInsert. {}",destinationFile.exists());
						return true;
					}
				}catch(IOException ioe){
					logger.error("An exception thrown in fileMove fileFolder NewUserDefaultItemInsert",ioe);
					File file =new File(Utils.imagePath + "temp_userdefult_attachment_file0");
					if(file.exists())
						file.delete();
					return false;
				}
			}else{
				try{
					FileUtils.copyFile(sourecFile, destinationFile);
					return true;

				}catch(IOException ioe){
					logger.error("An exception thrown in fileMove copyfile NewUserDefaultItemInsert",ioe);
					File file =new File(Utils.imagePath + "temp_userdefult_attachment_file0");
					if(file.exists())
						file.delete();
					return false;
				}
			}
		}
		return false;
	}
}
