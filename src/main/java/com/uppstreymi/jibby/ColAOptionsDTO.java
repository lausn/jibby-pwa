package com.uppstreymi.jibby;

import java.util.Date;
import java.util.List;

public class ColAOptionsDTO {
	
	private Integer itemStatus;
	private List<Integer> rules;
	private Integer startType;
	private Integer finishType;
	private Date startDate;
	private Date finishDate;
	private Integer projectType;
	private Integer onHold;
	
	public ColAOptionsDTO(){}
	
	public ColAOptionsDTO(Integer itemStatus,List<Integer> rules,Integer startType,Integer finishType,
			Date startDate,Date finishDate,Integer projectType,Integer onHold){
		
		this.itemStatus=itemStatus;
		this.rules=rules;
		this.startType=startType;
		this.finishType=finishType;
		this.startDate=startDate;
		this.finishDate=finishDate;
		this.projectType=projectType;
		this.onHold=onHold;
		
	}
	
	public void setItemStatus(Integer itemStatus){
		this.itemStatus=itemStatus;
	}
	public Integer getItemStatus(){
		return this.itemStatus;
	}
	
	public void setRules(List<Integer> rules){
		this.rules=rules;
	}
	public List<Integer> getRules(){
		return this.rules;
	}
	
	public void setStartType(Integer startType){
		this.startType=startType;
	}
	public Integer getStartType(){
		return this.startType;
	}
	
	public void setFinishType(Integer finishType){
		this.finishType=finishType;
	}
	public Integer getFinishType(){
		return this.finishType;
	}
	
	public void setStartDate(Date startDate){
		this.startDate=startDate;
	}
	public Date getStartDate(){
		return this.startDate;
	}
	
	public void setFinishDate(Date finishDate){
		this.finishDate=finishDate;
	}
	public Date getFinishDate(){
		return this.finishDate;
	}
	
	public void setProjectType(Integer projectType){
		this.projectType=projectType;
	}
	public Integer getProjectType(){
		return this.projectType;
	}

	public Integer getOnHold() {
		return onHold;
	}

	public void setOnHold(Integer onHold) {
		this.onHold = onHold;
	}
	
}
