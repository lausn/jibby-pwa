package com.uppstreymi.jibby.user;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.imageio.ImageIO;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.item.events.UserEvent;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.WebBrowser;
import com.vaadin.flow.templatemodel.TemplateModel;

import elemental.json.Json;

@Tag("user-settings-popup-page")
@JsModule("./src/views/user/user-settings-popup-page.js")
@UIScoped
public class UserSettingsPopupPage extends PolymerTemplate<UserSettingsPopupPage.UserSettingsPopupPageModel> {

	@Id("divEmailAboutItemChangeSelect")
	private Div divEmailAboutItemChangeSelect;
	@Id("divEmailAboutUnfinishedSelect")
	private Div divEmailAboutUnfinishedSelect;
	@Id("divDateFormat")
	private Div divDateFormat;

	@Id("tfFirstName")
	private TextField tfFirstName;
	@Id("tfLastName")
	private TextField tfLastName;
	@Id("tfEmail")
	private TextField tfEmail;
	@Id("btnSave")
	private Button btnSave;
	@Id("btnCancel")
	private Button btnCancel;
	
	@Inject private DbService dbService;
	@Inject private Event<UserEvent> userEvent;
	
	private Select<String> nsEmailAboutItemChangeSelect = new Select<String>();
	private Select<String> nsEmailAboutUnfinishedSelect = new Select<String>();
	private Select<String> nsDateFormat = new Select<String>();
	
	private JBUserEntity userEntity;
	
	@Id("uploadLayout")
	private Div uploadLayout;
	Upload uploader;
	MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
	SucceededEvent succeedEvent;
	Button uploadButton = new Button("");
	private Image image = new Image();
	Logger logger=LogManager.getLogger(UserSettingsPopupPage.class);
	
    public UserSettingsPopupPage() {
    	initSelect();
    	image.getElement().setAttribute("style", "width:110px;height:110px;border-radius: 100%;");
    }
    
    @PostConstruct
    private void init() {
    	fileUploaderInit();
    	
    	btnSave.addClickListener(e -> {
			modifyUserProfile();
		});

		btnCancel.addClickListener(e -> {
			Dialog parent = (Dialog) this.getParent().get();
			parent.close();
		});
    }
    
    private void fileUploaderInit() {
		uploader = new Upload(buffer);
		uploader.setMaxFiles(1);
		uploader.setDropAllowed(false);
		uploader.setAcceptedFileTypes("image/*");
		uploader.getElement().removeProperty("capture");
		uploader.setUploadButton(image);

		uploader.addSucceededListener(event -> {
			succeedEvent = event;
			saveUploadedImage();
			setUserImage();
		});
		uploader.getElement().addEventListener("file-abort", remove -> {
			succeedEvent = null;
		});
		uploadLayout.add(uploader);
	}
    
    private void saveUploadedImage() {
		if (succeedEvent != null && userEntity != null) {
			addData();
			succeedEvent = null;
			Notification.show("Image saved successfully.",2000, Position.MIDDLE);
			userEvent.fire(new UserEvent(userEntity,false));
		}
		uploader.getElement().setPropertyJson("files", Json.createArray());
	}
	
	public void addData() {
		
			fileMovedForIOS();
					
	}
	
	private void fileMoved() {
		File fileFolder = new File(Utils.userImagePath);
		String fileName = String.valueOf(userEntity.getId());

		try {
			if (!fileFolder.exists())
				fileFolder.mkdirs();

			byte[] buf = new byte[(int) succeedEvent.getContentLength()];
			InputStream is = buffer.getInputStream(succeedEvent.getFileName());
			is.read(buf);

			File targetFile = new File(Utils.userImagePath + fileName);
			OutputStream outStream = new FileOutputStream(targetFile);
			outStream.write(buf);

			outStream.flush();
			outStream.close();
		} catch (Exception e) {
			logger.error("An exception thrown in fileMoved in UserSettings",e);
		}
	}
	
	private void fileMovedForIOS() {
		try {
			File fileFolder = new File(Utils.userImagePath);
			if (!fileFolder.exists())
				fileFolder.mkdirs();

			File file = createTempFile();
			String orientation="";
			Metadata mata = ImageMetadataReader.readMetadata(file);
			for (Directory directory : mata.getDirectories()) {

				for (com.drew.metadata.Tag tag : directory.getTags()) {
					if(tag.getTagName().equalsIgnoreCase("Orientation")){
						orientation=tag.getDescription();
						break;
					}
				}
			}
			
			String fileType = succeedEvent.getMIMEType();
			String fileName = String.valueOf(userEntity.getId());

			BufferedImage img = ImageIO.read(file);

			fileType = fileType.split("\\/")[1];
			File targetFile = new File(Utils.userImagePath + fileName);
			if(orientation.equalsIgnoreCase("Right side, top (Rotate 90 CW)"))
				ImageIO.write(Utils.rotateImage(img), fileType, targetFile);
			else
				ImageIO.write(img, fileType, targetFile);
			file.delete();
		} catch (Exception e) {
			logger.error("An exception thrown in fileMovedForIOS in UserSettings",e);
		}
	}
	
	private void setUserImage() {
		StreamResource resource = Utils.getStream(Utils.userImagePath, userEntity.getId() + "");
		if (resource != null) 
			image.setSrc(resource);
		else 
			image.setSrc("./images/avatar.png");
	}
	
	private File createTempFile() {
		try {
			File file = File.createTempFile(succeedEvent.getFileName(),"");
			
			byte[] buf = new byte[(int) succeedEvent.getContentLength()];
			InputStream is = buffer.getInputStream(succeedEvent.getFileName());
			is.read(buf);

			OutputStream outStream = new FileOutputStream(file);
			outStream.write(buf);

			outStream.flush();
			outStream.close();
			return file;
		} catch (IOException ex) {
			logger.error("An exception thrown in createTempFile in UserSettings",ex);
		}
		return null;
	}
	
	public static boolean isIPhone() {
		WebBrowser browser = UI.getCurrent().getSession().getBrowser();
		return browser.isIPhone() ? true : false;
	}
    
    private void initSelect() {
    	List<String> emailAboutItemChangeSelectList = new ArrayList<String>(
				Arrays.asList(JibbyConfig.emailAboutEntityChangesTypes));
		nsEmailAboutItemChangeSelect.setItems(emailAboutItemChangeSelectList);
		nsEmailAboutItemChangeSelect.getElement().setAttribute("style", "width: 100%;");
		nsEmailAboutItemChangeSelect.getElement().setAttribute("theme", "small");
		divEmailAboutItemChangeSelect.add(nsEmailAboutItemChangeSelect);

		List<String> emailAboutUnfinishedSelectList = new ArrayList<String>(
				Arrays.asList(JibbyConfig.reminderEntityUnfinished));
		nsEmailAboutUnfinishedSelect.setItems(emailAboutUnfinishedSelectList);
		nsEmailAboutUnfinishedSelect.getElement().setAttribute("style", "width: 100%;");
		nsEmailAboutUnfinishedSelect.getElement().setAttribute("theme", "small");
		divEmailAboutUnfinishedSelect.add(nsEmailAboutUnfinishedSelect);
		
		List<String> dateFormatList = new ArrayList<String>(Arrays.asList(JibbyConfig.dateFormatValue));
		nsDateFormat.setItems(dateFormatList);
		nsDateFormat.setItemLabelGenerator(item ->{
			return JibbyConfig.dateFormatLabel[dateFormatList.indexOf(item)];
		});
		nsDateFormat.getElement().setAttribute("style", "width: 100%;");
		nsDateFormat.getElement().setAttribute("theme", "small");
		divDateFormat.add(nsDateFormat);
    }

    private void modifyUserProfile() {
    	userEntity.setFirstName(tfFirstName.getValue());
		userEntity.setLastName(tfLastName.getValue());
		Integer indexOfEmailItemChange = Arrays.asList(JibbyConfig.emailAboutEntityChangesTypes)
				.indexOf(nsEmailAboutItemChangeSelect.getValue());
		userEntity.setEmailAboutEntityChanges(
				nsEmailAboutItemChangeSelect.getValue() == null ? null : indexOfEmailItemChange);
		Integer indexOfEmailEntityUnfinished = Arrays.asList(JibbyConfig.reminderEntityUnfinished)
				.indexOf(nsEmailAboutUnfinishedSelect.getValue());
		userEntity.setReminderEntityUnfinished(
				nsEmailAboutUnfinishedSelect.getValue() == null ? null : indexOfEmailEntityUnfinished);
		userEntity.setDateFormat(nsDateFormat.getValue());
		dbService.saveUser(userEntity);
		Dialog parent = (Dialog) this.getParent().get();
		parent.close();
		Notification.show("Profile Updated Successfully",2000,Position.BOTTOM_CENTER);
	}

	public interface UserSettingsPopupPageModel extends TemplateModel {
    }

	public void setup(JBUserEntity userEntity) {
		clear();
		this.userEntity=userEntity;
		setUserImage();
		tfFirstName.setValue(userEntity.getFirstName() == null ? "" : userEntity.getFirstName());
		tfLastName.setValue(userEntity.getLastName() == null ? "" : userEntity.getLastName());
		tfEmail.setValue(userEntity.getEmail());
		nsEmailAboutItemChangeSelect.setValue(userEntity.getEmailAboutEntityChanges() == null ? null
				: JibbyConfig.emailAboutEntityChangesTypes[userEntity.getEmailAboutEntityChanges()]);
		nsEmailAboutUnfinishedSelect.setValue(userEntity.getReminderEntityUnfinished() == null ? null
				: JibbyConfig.reminderEntityUnfinished[userEntity.getReminderEntityUnfinished()]);
		nsDateFormat.setValue(userEntity.getDateFormat());
	}
	
	private void clear() {
		tfFirstName.setValue("");
		tfLastName.setValue("");
		tfEmail.setValue("");
		nsEmailAboutItemChangeSelect.setEmptySelectionAllowed(false);
		nsEmailAboutUnfinishedSelect.setEmptySelectionAllowed(false);
		nsDateFormat.setEmptySelectionAllowed(false);
		nsEmailAboutItemChangeSelect.clear();
		nsEmailAboutUnfinishedSelect.clear();
	}
}
