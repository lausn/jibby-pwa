package com.uppstreymi.jibby.myaction;

import com.vaadin.flow.templatemodel.TemplateModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.item.events.AssignToOthersDataChangeEvent;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.TextRenderer;


@Tag("assigntoothers-popup-view")
@JsModule("./src/views/myaction/assigntoothers-popup-view.js")
@UIScoped
public class AssigntoothersPopupView extends PolymerTemplate<AssigntoothersPopupView.AssigntoothersPopupViewModel> {

	@Id("btnSave")
	private Button btnSave;

	@Id("btnCancel")
	private Button btnCancel;

	@Id("statusWrapper")
	private Div statusWrapper;

	@Id("assignmentCritria")
	private Div assignmentCritria;

	@Inject private SessionModel sessionModel;
	@Inject private DbService service;
	@Inject private ProjectViewModel model;
	@Inject
	private EntityUserAccess entityUserAccess;

	RadioButtonGroup<Integer> radioDoneNotDone = new RadioButtonGroup<>();
	RadioButtonGroup<Integer> radioAssignment= new RadioButtonGroup<>();
	@Inject Event<AssignToOthersDataChangeEvent> assignToOthersDataChangeEvent;

	private Logger logger=LogManager.getLogger(AssigntoothersPopupView.class);

	public AssigntoothersPopupView() {
		radioButtonGroupCriteria();
	}

	@PostConstruct
	private void init() {
		cmpEvent();
	}

	private void cmpEvent() {
		btnSave.addClickListener(e->{
			setAssignToOthersData();
		});
		btnCancel.addClickListener(e->{
			closeWindow();
		});
	}

	private void setAssignToOthersData() {
		sessionModel.getActiveViewDTO().setActionListDoneEnable(radioDoneNotDone.getValue().equals(2)? 1 : 0);
		if (radioAssignment.getValue() != null) {
			sessionModel.getActiveViewDTO().setAssignToOthersFilter(radioAssignment.getValue()==null ? 2 : radioAssignment.getValue());
		}
		saveActiveUI();
		assignToOthersDataChangeEvent.fire(new AssignToOthersDataChangeEvent(true));
		closeWindow();
	}

	private boolean isFilterNeeded(Integer filterType) {
		ItemBean colAProject = model.getItemBean();
		JBEntity entity = entityUserAccess.getAllUsersAccessUser(colAProject.getEntity());
		boolean filterNeeded = true;
		if (colAProject != null && filterType == 3) {
			filterNeeded = entity.getOwnerlist().contains(sessionModel.getUser());
		}
		return filterNeeded;
	}

	private void radioButtonGroupCriteria() {
		List<Integer> doneNotDone=new ArrayList<>();
		doneNotDone.add(2);
		doneNotDone.add(4);

		List<Integer> assignments=new ArrayList<>();
		assignments.add(1);
		assignments.add(2);
		assignments.add(3);
		List<String> statusCaption=new ArrayList<>();
		statusCaption.add("Not Done");
		statusCaption.add("Done");

		List<String> assignmntCaption=new ArrayList<>();
		assignmntCaption.add("My assignments to others in the selected project");
		assignmntCaption.add("My assignments to others in all projects");
		assignmntCaption.add("All assignments in the selected project");
		radioDoneNotDone.setItems(doneNotDone);
		radioAssignment.setItems(assignments);

		radioDoneNotDone.setRenderer(new TextRenderer<Integer>(s->{return statusCaption.get(doneNotDone.indexOf(s));}));
		radioAssignment.setRenderer(new TextRenderer<Integer>(s->{return assignmntCaption.get(assignments.indexOf(s));}));

		statusWrapper.add(radioDoneNotDone);
		assignmentCritria.add(radioAssignment);

	}
	
	private boolean toplabelOwner() {
		ItemBean colAProject = model.getItemBean();
		JBEntity entity = entityUserAccess.getAllUsersAccessUser(colAProject.getEntity());
		boolean topLabelOwner = true;
		if(colAProject!=null) {
			topLabelOwner = entity.getOwnerlist().contains(sessionModel.getUser());
		}
		return topLabelOwner;
	}

	public void setup() {
		selectRadioProperties();
	}

	private void closeWindow() {
		Dialog parent = (Dialog) this.getParent().get();
		parent.close();
	}

	private void selectRadioProperties() {
		radioAssignment.setItemEnabledProvider(e->{
			return isFilterNeeded(e);
		});

		radioDoneNotDone.setValue(sessionModel.getActiveViewDTO().getActionListDoneEnable()==1?JibbyConfig.status[1]:JibbyConfig.status[0]);//2 is not done 4 is done
		if (sessionModel.getActiveViewDTO().getAssignToOthersFilter() == null) {
			sessionModel.getActiveViewDTO().setAssignToOthersFilter(2);
			saveActiveUI();
		}
		else if(sessionModel.getActiveViewDTO().getAssignToOthersFilter().equals(3) && !toplabelOwner()) {
			sessionModel.getActiveViewDTO().setAssignToOthersFilter(1);
			saveActiveUI();
		}
		radioAssignment.setValue(sessionModel.getActiveViewDTO().getAssignToOthersFilter());
	}

	private void saveActiveUI(){
		String json = Utils.jsonBuilder(sessionModel.getActiveViewDTO());
		if(json !=null){
			sessionModel.getUser().setActiveUI(json);
			service.saveUser(sessionModel.getUser());
		}
	}

	public interface AssigntoothersPopupViewModel extends TemplateModel {

	}
}
