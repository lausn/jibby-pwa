package com.uppstreymi.jibby.myaction;


import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.item.events.AssignToOthersDataChangeEvent;
import com.uppstreymi.jibby.item.events.SettingItemEvent;
import com.uppstreymi.jibby.item.events.TittleEvent;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.MainLayout;
import com.uppstreymi.jibby.ui.components.navigation.bar.MyactionDataLoadEvent;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("myaction-page")
@JsModule("./src/views/myaction/myaction-page.js")
@Route(value="item/myaction", layout = MainLayout.class)
@UIScoped
public class MyactionPage extends PolymerTemplate<MyactionPage.MyactionPageModel> implements AfterNavigationObserver {

	@Inject Event<SettingItemEvent> settingEvent;
	@Inject private Event<TittleEvent> tittleEvent;

	@Inject private MyitemPage myitem;
	@Inject private AssignedtomePage assigntome;
	@Inject private AssigntoothersPage assigntoothers;
	@Inject private AssigntoothersPopupView assigntoothersPopupView;

	String type="items";

	@Id("actiontab")
	private Div actiontab;

	@Id("tabs")
	private Div tablayout;

	@Id("btnMenu")
	private Button btnMenu;

	@Id("btnAssignToOthersMenu")
	private Button btnAssignToOthersMenu;

	@Inject private ProjectViewModel model;

	Tabs tabs = new Tabs();
	ContextMenu contextMenu;
	String selectedTab = "";
	@Inject private SessionModel sessionModel;
	@Inject
	private EntityUserAccess entityUserAccess;
	@Inject private DbService service;

	private Logger logger=LogManager.getLogger(MyactionPage.class);

	public MyactionPage() {
		tabInit();
		initMenu();
	}

	private void tabInit() {
		Tab tab1 = new Tab(JibbyConfig.myActionlistTabCaption[0]);
		Tab tab2 = new Tab(JibbyConfig.myActionlistTabCaption[1]);
		Tab tab3 = new Tab(JibbyConfig.myActionlistTabCaption[2]);
		tabs.add(tab1, tab2, tab3);
		tablayout.add(tabs);
	}

	private void initMenu() {
		contextMenu = new ContextMenu(btnMenu);
		contextMenu.setOpenOnClick(true);
		menuButtonCriteria(false);
		loadContextMenu(0);
	}

	private void loadTabData() {
		loadData(0);
		loadData(1);
		loadData(2);
	}

	public void viewChange(@Observes  MyactionDataLoadEvent event) {
		loadTabData();
	}

	@PostConstruct
	private void init() {

		actiontab.add(myitem);
		type="mytoday";
		myitem.loadData(type);
		tabs.addSelectedChangeListener(e->{
			selectedTab = e.getSelectedTab().getLabel();
			actiontab.removeAll();
			if(selectedTab.equals(JibbyConfig.myActionlistTabCaption[0])) {
				actiontab.add(myitem);
				loadData(0);
				loadContextMenu(0);
				menuButtonCriteria(false);
			}
			else if(selectedTab.equals(JibbyConfig.myActionlistTabCaption[1])) {
				actiontab.add(assigntome);
				loadData(1);
				loadContextMenu(1);
				menuButtonCriteria(false);
			}
			else if(selectedTab.equals(JibbyConfig.myActionlistTabCaption[2])) {
				actiontab.add(assigntoothers);
				loadData(2);
				menuButtonCriteria(true);
			}
		});

		getModel().setPage("1");

		btnAssignToOthersMenu.addClickListener(e->{
			if(selectedTab.equals("Assigned to others")) {
				Dialog dialog = new Dialog();
				dialog.add(assigntoothersPopupView);
				dialog.setWidth("100%");
				dialog.setHeight("100%");
				assigntoothersPopupView.setup();
				dialog.open();
				dialog.setCloseOnOutsideClick(true);
			}
		});
	}

	private void menuButtonCriteria(boolean isAssignToOthers) {
		if(isAssignToOthers) {
			btnAssignToOthersMenu.removeClassName("hide");
			btnMenu.addClassName("hide");
		}
		else {
			btnAssignToOthersMenu.addClassName("hide");
			btnMenu.removeClassName("hide");
		}
	}

	private void loadContextMenu(Integer tab) {
		contextMenu.removeAll();
		if(tab==0) {
			contextMenu.addItem("My Today", e->{
				type = "mytoday";
				myitem.loadData(type);
			});
			contextMenu.addItem("My Tomorrow", e->{
				type = "mytomorrow"; 
				myitem.loadData(type);
			});

			contextMenu.addItem("My Past", e->{
				type = "mypast"; 
				myitem.loadData(type);
			});

			contextMenu.addItem("Items", e->{
				type = "items"; 
				myitem.loadData(type);
			});
			contextMenu.addItem("Check List", e->{
				type = "checklist"; 
				myitem.loadData(type);
			});

		}
		else {

			contextMenu.addItem("Done Item", e->{
				type="items";
				if(tab==1)
					assigntome.loadData(type,JibbyConfig.status[0]);
			});

			contextMenu.addItem("Not Done Item", e->{
				type="items";
				if(tab==1)
					assigntome.loadData(type,JibbyConfig.status[1]);
			});
		}
	}

	public void loadData(int tab) {
		if(tab==0) {
			type="mytoday";
			myitem.loadData(type);
		}
		else if(tab==1) {
			type="items";
			assigntome.loadData(type,JibbyConfig.status[1]);
		}
		else if(tab==2) {
			assignToOthersDataLoad();
		}
	}

	private void assignToOthersDataLoad() {
		type="items";
		if(sessionModel.getActiveViewDTO().getAssignToOthersFilter().equals(3) && !toplabelOwner()) {
			sessionModel.getActiveViewDTO().setAssignToOthersFilter(1);
			saveActiveUI();
		}
		Integer filterType = sessionModel.getActiveViewDTO().getAssignToOthersFilter()==null ? 2 : sessionModel.getActiveViewDTO().getAssignToOthersFilter();
		Integer status = sessionModel.getActiveViewDTO().getActionListDoneEnable()==1 ? JibbyConfig.status[1]: JibbyConfig.status[0];

		assigntoothers.loadData(type,filterType,status,model.getItemBean()==null ? null : model.getItemBean().getId());
	}

	private void saveActiveUI(){
		String json = Utils.jsonBuilder(sessionModel.getActiveViewDTO());
		if(json !=null){
			sessionModel.getUser().setActiveUI(json);
			service.saveUser(sessionModel.getUser());
		}
	}

	private boolean toplabelOwner() {
		ItemBean colAProject = model.getItemBean();
		JBEntity entity = entityUserAccess.getAllUsersAccessUser(colAProject.getEntity());
		boolean topLabelOwner = true;
		if(colAProject!=null) {
			topLabelOwner = entity.getOwnerlist().contains(sessionModel.getUser());
		}
		return topLabelOwner;
	}

	public void assignToOtherDataChange(@Observes  AssignToOthersDataChangeEvent event) {
		if(event.isChange())
			assignToOthersDataLoad();
	}

	public interface MyactionPageModel extends TemplateModel {
		void setPage(String str);
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		TittleEvent te=new TittleEvent();
		te.setTittle("My Action list");
		tittleEvent.fire(te);
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		MainLayout.get().getAppBar().setContextMenuVisibility(true, false);
	}
}
