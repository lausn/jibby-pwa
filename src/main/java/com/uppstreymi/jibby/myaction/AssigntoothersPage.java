package com.uppstreymi.jibby.myaction;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.flow.helper.AsyncManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.DataChangeModelListener;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.ModelUpdateEvent;
import com.uppstreymi.jibby.backend.sync.manager.SynchronizeManager;
import com.uppstreymi.jibby.backend.sync.process.ProcessData;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.item.events.SyncModelGenerateEvent;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("assigntoothers-page")
@JsModule("./src/views/myaction/assigntoothers-page.js")
@UIScoped
public class AssigntoothersPage extends PolymerTemplate<AssigntoothersPage.AssigntoothersPageModel>
implements DataChangeModelListener {

	@Inject
	DbService service;
	@Inject
	SessionModel sessionModel;
	@Inject
	private AppModel appModel;
	@Inject
	EntityUserAccess entityUserAccess;
	@Inject
	SyncUserEvent userEvent;
	@Inject
	SelectedItemView selectedItemView;
	private SyncDataChangeModel dataChangemodel;
	@Inject
	SynchronizeManager syncManager;
	List<MyItemBean> userlist;
	Integer status = JibbyConfig.status[1];
	Integer filterType=2;
	Integer selectedProjectId;
	List<Integer> hasuserToplist;
	private UI ui;
	private Logger logger=LogManager.getLogger(AssigntoothersPage.class);

	public AssigntoothersPage() {
	}

	public void loadData(String type,Integer filterType, Integer status,Integer selectedProjectId) {
		this.status = status;
		this.filterType = filterType;
		this.selectedProjectId = selectedProjectId;
		userlist = new ArrayList<>();
		hasuserToplist = new ArrayList<>();
		List<Object[]> assignedToMeItems;
		if (type.equals("items"))
			assignedToMeItems = service.getAssignedToOtherItem(sessionModel.getUser().getId(), status,filterType,selectedProjectId); // JibbyConfig.status[1]
		else
			assignedToMeItems = service.getAssignedToOtherCheckItem(sessionModel.getUser().getId(), null); // JibbyConfig.checkItemStatus[1]

		for (Object[] item : assignedToMeItems) {
			if (!hasuserToplist.contains(Integer.parseInt(item[9].toString()))) {
				MyItemBean userbean = new MyItemBean();
				userbean.setUserName(String.valueOf(item[10]));
				userbean.setUserPic(userPic(Integer.parseInt(item[9].toString())));
				hasuserToplist.add(Integer.parseInt(item[9].toString()));
				userlist.add(userbean);
			}

			List<Integer> parentlist = ((MyItemBean) userlist
					.get(hasuserToplist.indexOf(Integer.parseInt(item[9].toString())))).getParentidlist();
			if (!parentlist.contains(Integer.parseInt(item[8].toString()))) {
				parentlist.add(Integer.parseInt(item[8].toString()));
				MyItemBean topbean = new MyItemBean();
				topbean.setName(String.valueOf(item[11]));
				topbean.setEntityType(Integer.parseInt(item[12].toString()));
				((MyItemBean) userlist.get(hasuserToplist.indexOf(Integer.parseInt(item[9].toString())))).getMyItems()
				.add(topbean);

			}

			MyItemBean childbean = new MyItemBean();
			childbean.setId(Integer.parseInt(item[0].toString()));
			childbean.setItemType(type);
			childbean.setEntityType(Integer.parseInt(item[2].toString()));
			childbean.setItemId(Integer.parseInt(item[3].toString()));
			childbean.setName(String.valueOf(item[6]));

			if (type.equals("checklist")) {
				childbean.setStatus(Boolean.valueOf(item[7].toString()));
				childbean.setCheckEntity(Integer.parseInt(item[13].toString()));
				childbean.setSortOrder(Integer.parseInt(item[14].toString()));
			} else {
				childbean.setStatus(item[7].equals(4) ? true : false);
			}

			((MyItemBean) userlist.get(hasuserToplist.indexOf(Integer.parseInt(item[9].toString())))).getMyItems()
			.get(parentlist.indexOf(Integer.parseInt(item[8].toString()))).getMyItems().add(childbean);

		}
		getModel().setTopItems(buildJson());
	}

	@ClientCallable
	private void assigntoothersChanged(String items, boolean isdone) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			MyItemBean myitem = objectMapper.readValue(items, MyItemBean.class);
			if (myitem.getItemType().equals("items"))
				setItems(myitem, isdone);
			else
				setCheckItems(myitem, isdone);
			loadData(myitem.getItemType(), this.filterType,this.status,this.selectedProjectId);
		} catch (Exception e) {
			logger.error("An exception thrown assigntoothersChanged in AssignToOthersPage",e);
		}
	}

	private void setItems(MyItemBean myitem, boolean isdone) {
		JBEntity entity = service.getEntity(myitem.getItemId());
		JBEntity oldEntity = new JBEntity();
		oldEntity.copyEntity(entity);
		entity.setStatus(isdone ? JibbyConfig.status[0] : JibbyConfig.status[1]);
		entity.setProgress(isdone ? 100 : 0);
		service.saveEntity(entity);

		entity = entityUserAccess.getAllUsersAccessUser(entity);
		userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
		userEvent.setOldEntity(oldEntity);

		Object[] attachmentOrChecklist = service.entityAttachmentOrChecklistCount(entity.getId());
		userEvent.setHasAttachment(Integer.parseInt(attachmentOrChecklist[2] + "") > 0 ? true : false);
		userEvent.setHasCheckItem(Integer.parseInt(attachmentOrChecklist[1] + "") > 0 ? true : false);

		List<JBChecklistItemEntity> checklists = service.getAllDoneOrUndoneChecklistByEntity(myitem.getItemId(),
				isdone ? 0 : 1);
		int changevalue = isdone ? 1 : 0;
		for (JBChecklistItemEntity checklist : checklists) {
			checklist.setIsChecked(changevalue);
			service.saveChecklistItems(checklist);
		}

		if (isdone) {
			service.deleteMyActionListRow(myitem.getId());
			if (sessionModel.getUser().getId() != entity.getCreator())
				notificationToCreator(entity);
		} else {
			Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1;
			markUndone(itemType, myitem.getItemId(), entity.getName());
		}
		addItemModification(entity);

		if (userEvent.getSyncDataReceiver() != null) {
			userEvent.addHistory();
			userEvent.eventProcessor();
		}
	}

	private void setCheckItems(MyItemBean myitem, boolean isdone) {
		JBChecklistItemEntity checkList = new JBChecklistItemEntity();
		checkList.setId(myitem.getItemId());
		checkList.setIsChecked(isdone ? 1 : 0);
		checkList.setSortOrder(myitem.getSortOrder());
		checkList.setName(myitem.getName());
		checkList.setEntity(myitem.getCheckEntity());
		service.saveChecklistItems(checkList);
		JBEntity entity = service.getEntity(myitem.getCheckEntity());
		entity = entityUserAccess.getAllUsersAccessUser(entity);
		if (isdone) {
			insertItemModification(entity,
					Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("donechecklist") + 1);
			service.deleteMyActionListRow(myitem.getId());
		} else {
			insertItemModification(entity,
					Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undonechecklist") + 1);
			Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem") + 1;
			markUndone(itemType, myitem.getItemId(), myitem.getName());
		}
		changeItemProgressStatus(myitem.getCheckEntity());
	}

	public void changeItemProgressStatus(Integer entityid) {
		JBEntity entity = service.getEntity(entityid);
		List<JBChecklistItemEntity> checkListData = service.getChecklistByEntity(entity.getId());

		Integer progress = 0;
		Integer totalchk = 0;
		Integer totalchkList = checkListData.size();

		for (JBChecklistItemEntity checklist : checkListData) {
			if (checklist.getIsChecked() == JibbyConfig.checkItemStatus[0]) {
				totalchk++;
			}
		}
		if (totalchkList > 0)
			progress = Integer
			.parseInt(BigDecimal.valueOf((Double.parseDouble(totalchk.toString()) / totalchkList) * 100)
					.setScale(0, BigDecimal.ROUND_HALF_UP).toString());

		if (progress == 100)
			entity.setStatus(JibbyConfig.status[0]);
		else
			entity.setStatus(JibbyConfig.status[1]);

		entity.setProgress(progress);
		service.saveEntity(entity);
	}

	private void notificationToCreator(JBEntity entity) {
		String userName = sessionModel.getUser().getFirstName() + " " + sessionModel.getUser().getLastName();
		JBActionlistHistory actionlistHistory = new JBActionlistHistory();
		actionlistHistory.setCreated(new Date());
		actionlistHistory.setItemid(entity.getId());
		actionlistHistory.setItemtype(Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1);
		actionlistHistory.setUserid(entity.getCreator());
		actionlistHistory.setMessage(userName + " completed the task  \"" + entity.getName() + "\".");
		actionlistHistory
		.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("markedByAssignee") + 1);
		actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

		actionlistHistory = service.saveActionlistHistory(actionlistHistory);
		userEvent.addToNotifyUserList(entity.getCreator());
		userEvent.setNotification(actionlistHistory);
	}

	private void markUndone(Integer itemType, Integer itemId, String itemName) {
		String userName = sessionModel.getUser().getFirstName() + " " + sessionModel.getUser().getLastName();
		List<JBUserEntity> assigneeList = service.getAllAssigneeUserById(itemId, itemType);
		if (assigneeList.contains(sessionModel.getUser())) {
			assigneeList.remove(sessionModel.getUser());
		}

		for (JBUserEntity assignee : assigneeList) {
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(itemId);
			actionlistHistory.setItemtype(itemType);
			actionlistHistory.setUserid(assignee.getId());
			actionlistHistory.setMessage(userName + "  marked the task \"" + itemName + "\" as Not done.");
			actionlistHistory
			.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser") + 1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

			service.saveActionlistHistory(actionlistHistory);
			userEvent.addToNotifyUserList(assignee.getId());
			userEvent.setNotification(actionlistHistory);
		}
	}

	public void addItemModification(JBEntity entity) {
		Integer modificationId = service.deleteItemModificationByEntityAndUserAndCompleted(entity.getId(),
				sessionModel.getUser().getId());
		if (modificationId == null)
			insertItemModification(entity,
					entity.getStatus().equals(JibbyConfig.status[0])
					? Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("done") + 1
							: Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undone") + 1);
		else {
			userEvent.setItemModificationRemoveId(String.valueOf(modificationId));
			userEvent.setEntity(entity);
			userEvent.setSender(sessionModel.getUser());
			userEvent.syncDataReceiver(entity);
			userEvent.setTopLebelProjectEntity(
					entity.getParentProjectId() == null ? entity
							: service.getEntity(entity.getParentProjectId()));
		}
	}

	public void insertItemModification(JBEntity entity, Integer type) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entity.getId());
		itemModification.setUserId1(sessionModel.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		userEvent.addToHistoryList(itemModification);
		userEvent.setEntity(entity);
		userEvent.setSender(sessionModel.getUser());
		userEvent.syncDataReceiver(entity);

		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity
						: service.getEntity(entity.getParentProjectId()));
	}

	private String buildJson() {
		ObjectMapper map = new ObjectMapper();
		try {
			String json = map.writeValueAsString(userlist);
			return json;
		} catch (JsonProcessingException e) {
			logger.error("A JsonProcessingException thrown buildJson in AssignToOthersPage",e);
		}
		return "{}";
	}

	public interface AssigntoothersPageModel extends TemplateModel {
		void setTopItems(String a);
	}

	private String userPic(Integer userId) {

		String url = Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl());

		try {
			File preFile = new File(Utils.userImagePath + userId);

			if (!preFile.exists())
				return "./images/avatar.png";
			else {
				return url + "jibby/usercontent/avatars/" + userId;
			}
		} catch (Exception e) {
			logger.error("An Exception thrown userPic in AssignToOthersPage",e);
			return "./images/avatar.png";
		}
	}

	@ClientCallable
	private void _onDeleteItem(String items) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			MyItemBean myitem = objectMapper.readValue(items, MyItemBean.class);
			service.deleteMyActionListRow(myitem.getId());
			loadData(myitem.getItemType(), this.filterType,this.status,selectedProjectId);
		} catch (Exception e) {
			logger.error("An Exception thrown onDeleteItem in AssignToOthersPage",e);
		}
	}

	@ClientCallable
	private void _assigntoothersSelectedItem(String items) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			MyItemBean myitem = objectMapper.readValue(items, MyItemBean.class);
			Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(selectedItemView);
			dialog.open();
			selectedItemView.setup(myitem.getItemId(), "Assigned to others");

		} catch (Exception e) {
			logger.error("An Exception thrown assigntoothersSelectedItem in AssignToOthersPage",e);
		}
	}

	@ClientCallable
	private void _onAddmyTodayItem(String items) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			MyItemBean myitem = objectMapper.readValue(items, MyItemBean.class);
			Integer itemType;
			if (myitem.getItemType().equals("items"))
				itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1;
			else
				itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem") + 1;
			Integer todayType = Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday");
			addItemToMyList(todayType, itemType, myitem.getItemId());

		} catch (Exception e) {
			logger.error("An Exception thrown onAddmyTodayItem in AssignToOthersPage",e);
		}
	}

	@ClientCallable
	private void _onAddmyTomorrowItem(String items) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			MyItemBean myitem = objectMapper.readValue(items, MyItemBean.class);
			Integer itemType;
			if (myitem.getItemType().equals("items"))
				itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1;
			else
				itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem") + 1;
			Integer todayType = Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow");

			addItemToMyList(todayType, itemType, myitem.getItemId());

		} catch (Exception e) {
			logger.error("An Exception thrown onAddmyTomorrowItem in AssignToOthersPage",e);
		}
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		this.ui = attachEvent.getUI();
		if (this.ui != null)
			this.ui.getPushConfiguration().setPushMode(PushMode.MANUAL);
		if (dataChangemodel != null) {
			dataChangemodel.addDataChangeListener(this);
		}
	}

	@Override
	protected void onDetach(DetachEvent detachEvent) {
		this.ui = null;
		if (dataChangemodel != null) {
			dataChangemodel.removeDataChangeListener(this);
		}
	}

	@Override
	public void onChanged(ModelUpdateEvent event) {
		updateUI(event.getProcessData());
	}

	private void updateUI(ProcessData data) {

		try {
			if (this.ui != null)
				this.ui.access(() -> {
					AsyncManager.register(this.ui, asyncTask -> {
						asyncTask.push(() -> {
							updateSync(data);
							ui.push();
						});
					});
				});
		} catch (Exception e) {
			logger.error("ui is not refreshed AssignToOthersPage!",e);
		}
	}

	private void firstTimeSyncGenerate(@Observes SyncModelGenerateEvent se) {

		if (se.isChange() && sessionModel.getUser() != null) {
			if (dataChangemodel == null)
				dataChangemodel = syncManager.getModel(sessionModel.getUser().getId());
		}
	}

	private void updateSync(ProcessData data) {
		if (data.getEventType() == SyncEventType.EVENT_ENTITY_CHANGED && data.getEntity() != null
				&& data.getOldEntity() != null) {
			if (data.getOldEntity().getStatus() != data.getEntity().getStatus()) {
				itemStatusChange(data); // item status change
			}
		}
	}

	private void itemStatusChange(ProcessData data) {
		JBEntity entity = data.getEntity();
		List<MyItemBean> projectList = new ArrayList<MyItemBean>();
		List<MyItemBean> userBeanList = new ArrayList<MyItemBean>();
		List<MyItemBean> users = new ArrayList<MyItemBean>();
		for (MyItemBean userBean : userlist) {
			for (MyItemBean project : userBean.getMyItems()) {
				for (MyItemBean childBean : project.getMyItems()) {
					if (childBean.getItemId().equals(entity.getId())) {
						projectList.add(childBean);
					}
				}
				project.getMyItems().removeAll(projectList);
				if (project.getMyItems().size() == 0)
					userBeanList.add(project);
			}
			userBean.getMyItems().removeAll(userBeanList);
			if (userBean.getMyItems().size() == 0)
				users.add(userBean);
		}

		userlist.removeAll(users);
		getModel().setTopItems(buildJson());
	}

	private void addItemToMyList(Integer todayType, Integer itemtype, Integer itemid) {
		if (!service.itemOnActionList(itemid, itemtype, 0, sessionModel.getUser().getId())) { // if
			// item/checklist/today/tomorrow
			// not in the action
			// list
			Date dateTime = null;
			if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday")) {
				dateTime = new Date();
			} else if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow")) {
				Date date = new Date();
				dateTime = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			}

			JBActionlist actionlist = new JBActionlist();
			actionlist.setAddedby(sessionModel.getUser().getId());
			actionlist.setAssignedto(0);
			actionlist.setItemid(itemid);
			actionlist.setItemname("");
			actionlist.setDateadded(new Date());
			actionlist.setItemtype(itemtype);
			actionlist.setItemdatetime(dateTime);
			service.saveActionlist(actionlist);

			// userEvent.setEventType(SyncEventType.EVENT_ENTITY_HISTORY);
			if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday")) {
				if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1) {
					insertItemModification(itemid,
							Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("mytoday") + 1,
							sessionModel.getUser());
				}
			} else {
				if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1) {
					insertItemModification(itemid,
							Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("mytommorrow") + 1,
							sessionModel.getUser());
				}
			}

			Notification.show("Successfully added", 3000, Position.BOTTOM_CENTER);
		} else {
			Notification.show("Item is already there", 4000, Position.BOTTOM_CENTER);
		}
	}

	public void insertItemModification(Integer entityId, Integer type, JBUserEntity assignee) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entityId);
		itemModification.setUserId1(sessionModel.getUser().getId());
		itemModification.setUserId2(assignee.getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

	}
}
