
package com.uppstreymi.jibby.myaction;

import java.util.ArrayList;
import java.util.List;

public class MyItemBean {

	private String name,username,parentName,userPic,itemType;
	private Integer entityType,itemId,id,sortOrder,checkEntity,myItemType;
	private boolean status;
	
	private List<MyItemBean> myitems=new ArrayList<>();
	private List<Integer> parentidlist=new ArrayList<>();
	public  MyItemBean() {
		
	}
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setItemType(String itemType){
		this.itemType=itemType;
	}
	
	public String getItemType(){
		return this.itemType;
	}
	
	public void setMyItemType(Integer myItemType){
		this.myItemType=myItemType;
	}
	
	public Integer getMyItemType(){
		return this.myItemType;
	}
	
	public void setUserName(String username){
		this.username=username;
	}
	
	public String getUserName(){
		return this.username;
	}
	
	public void setUserPic(String userPic){
		this.userPic=userPic;
	}
	
	public String getUserPic(){
		return this.userPic;
	}
	
	public void setParentName(String parentName){
		this.parentName=parentName;
	}
	
	public String getParentName(){
		return this.parentName;
	}
	
	public void setEntityType(Integer entityType) {
		this.entityType=entityType;
	}
	
	public Integer getEntityType() {
		return this.entityType;
	}
	
	public void setItemId(Integer itemId) {
		this.itemId=itemId;
	}
	
	public Integer getItemId() {
		return this.itemId;
	}
	
	public void setStatus(boolean status) {
		this.status=status;
	}
	
	public boolean getStatus() {
		return this.status;
	}
	
	public void setCheckEntity(Integer checkEntity) {
		this.checkEntity=checkEntity;
	}
	
	public Integer getCheckEntity() {
		return this.checkEntity;
	}
	
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder=sortOrder;
	}
	
	public Integer getSortOrder() {
		return this.sortOrder;
	}
	
	public void setMyItems(List<MyItemBean> myitems) {
		this.myitems=myitems;
	}
	public List<MyItemBean> getMyItems(){
		return myitems;
	}
	
	public void setParentidlist(List<Integer> parentidlist) {
		this.parentidlist=parentidlist;
	}
	
	public List<Integer> getParentidlist() {
		return this.parentidlist;
	}
	
}

