package com.uppstreymi.jibby.myaction;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.uppstreymi.jibby.bean.ChatAndHistoryBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.item.ChatInvitationUserPopupPage;
import com.uppstreymi.jibby.item.DescriptionPage;
import com.uppstreymi.jibby.item.HistoryAndCommentsPage;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("selected-item-view")
@JsModule("./src/views/myaction/selected-item-view.js")
@UIScoped
public class SelectedItemView extends PolymerTemplate<SelectedItemView.SelectedItemViewModel> {

	@Id("btnBackNotiDialog")
	private Button btnBackNotiDialog;

	@Id("btnInvite")
	private Button btnInvite;

	@Id("headerCaption")
	private Label headerCaption;

	@Id("collapseDescriptionWrapper")
	private Div collapseDescriptionWrapper;

	@Id("descriptionWrapper")
	private Div descriptionWrapper;

	@Id("item-description")
	private DescriptionPage itemDescription;

	@Id("collapseChatWrapper")
	private Div collapseChatWrapper;

	@Id("historyAndCommentsDetails")
	private Details historyAndCommentsDetails;

	@Id("historyAndCommentsPage")
	private HistoryAndCommentsPage historyAndCommentsPage;

	@Inject
	private DbService service;

	@Inject
	private HistorModel historyModel;

	@Inject
	private EntityUserAccess entityUserAccess;

	@Inject
	private SessionModel session;

	@Inject
	private ChatInvitationUserPopupPage chatInvitationUserPopupPage;

	private JBEntity selectedEntity;
	private String type;
	Logger logger = Logger.getLogger(SelectedItemView.class.getName());

	public SelectedItemView() {

	}

	@PostConstruct
	public void init() {
		cmpEvent();
		historyAndCommentsDetails.addOpenedChangeListener(e -> {
			if (historyAndCommentsDetails.isOpened())
				historyAndCommentsDetailsClick();
		});
	}

	private void historyAndCommentsDetailsClick() {
		historyAndCommentsPage.dataLoad();
	}

	private void cmpEvent() {
		btnBackNotiDialog.addClickListener(e -> {
			closeWindow();
		});
		btnInvite.addClickListener(e -> {
			chatInvitationUserPopup();
		});
	}

	private void chatInvitationUserPopup() {
		Dialog dialog = new Dialog();
		dialog.add(chatInvitationUserPopupPage);
		chatInvitationUserPopupPage.setup();
		dialog.setWidth("90vw");
		dialog.setHeight("75vh");
		dialog.open();
		dialog.setCloseOnOutsideClick(true);
	}

	private void closeWindow() {
		Dialog parent = (Dialog) this.getParent().get();
		parent.close();
	}

	public void setup(Integer myItemId, String type) {
		this.type = type;
		selectedEntity = service.getEntity(myItemId);
		setEntityUserAccessData();
		setProperTies();
		setName();
		setDescription();
	}

	private void setEntityUserAccessData() {
		selectedEntity = entityUserAccess.getAllUsersAccessUser(selectedEntity);
		if (selectedEntity.getOwnerlist().contains(session.getUser())
				|| selectedEntity.getAssigneelist().contains(session.getUser()))
			btnInvite.setVisible(true);
		else
			btnInvite.setVisible(false);
	}

	private void setDescription() {
		itemDescription.getDescriptionModel()
				.setHtml(selectedEntity.getDescription() == null ? "" : selectedEntity.getDescription());
	}

	private void setName() {
		getModel().setItemicon(selectedEntity.getEntityType() + "");
		getModel().setItemname(selectedEntity.getName());
	}

	private void setProperTies() {
		historyModel.setSelectedEntity(selectedEntity);
		historyAndCommentsPage.getHistoryAndCommentsModel().setChatItems(new ArrayList<ChatAndHistoryBean>());
		historyAndCommentsPage.getHistoryAndCommentsModel().setCreatorName("");
		historyAndCommentsPage.getHistoryAndCommentsModel().setEntityCreateDate("");
		headerCaption.setText(type);

	}

	public interface SelectedItemViewModel extends TemplateModel {
		void setItemicon(String src);

		void setItemname(String name);
	}
}
