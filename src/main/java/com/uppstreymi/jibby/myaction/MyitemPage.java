package com.uppstreymi.jibby.myaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("myitem-page")
@JsModule("./src/views/myaction/myitem-page.js")
@UIScoped
public class MyitemPage extends PolymerTemplate<MyitemPage.MyitemPageModel> {

	@Inject DbService service;
	@Inject SessionModel sessionModel;
	@Inject SelectedItemView selectedItemView;
	List<MyItemBean> toplist;
	private String type;
	private Logger logger=LogManager.getLogger(MyitemPage.class);
	public MyitemPage() {
	}

	public void loadData(String type) {
		this.type = type;
		toplist=new ArrayList<>();
		List<Integer> hasToplist=new ArrayList<>();
		List<Object[]> assignedToMeItems =new ArrayList<>();;
		if(type.equals("items")) 
			assignedToMeItems = service.getMyItem(sessionModel.getUser().getId(),null); //JibbyConfig.status[1]
		else if(type.equals("checklist"))
			assignedToMeItems = service.getMyChecklist(sessionModel.getUser().getId(),null); //JibbyConfig.checkItemStatus[1]
		else if(type.equals("mytoday")) {
			Integer myTodayTypes = Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday");
			assignedToMeItems = service.getAllTodayItemByType(sessionModel.getUser().getId(),myTodayTypes,null); //JibbyConfig.status[1]

		}
		else if(type.equals("mytomorrow")) {
			Integer myTodayTypes = Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow");
			assignedToMeItems = service.getAllTodayItemByType(sessionModel.getUser().getId(),myTodayTypes,null); //JibbyConfig.status[1]
		}
		else if(type.equals("mypast")){
			Integer myTodayTypes = Arrays.asList(JibbyConfig.myTodayType).indexOf("myPast");
			assignedToMeItems = service.getAllTodayItemByType(sessionModel.getUser().getId(),myTodayTypes,null); //JibbyConfig.status[1]
		}

		for (Object[] item: assignedToMeItems) {
			if(!hasToplist.contains(Integer.parseInt(item[8].toString()))) {
				MyItemBean topbean= new MyItemBean();
				topbean.setName(String.valueOf(item[9]));
				topbean.setEntityType(Integer.parseInt(item[10].toString()));
				hasToplist.add(Integer.parseInt(item[8].toString()));
				toplist.add(topbean);
			}
			Integer itemtype=Integer.parseInt(item[1].toString());

			MyItemBean childbean= new MyItemBean();
			childbean.setId(Integer.parseInt(item[0].toString()));
			childbean.setItemType(type);
			childbean.setMyItemType(itemtype);
			childbean.setEntityType(Integer.parseInt(item[2].toString()));
			childbean.setItemId(Integer.parseInt(item[3].toString()));
			childbean.setName(String.valueOf(item[6]));


			if(itemtype.equals(0) || itemtype.equals(1) || itemtype.equals(4)) {
				childbean.setStatus(Integer.valueOf(item[7].toString()).equals(4)?true:false);
			}
			else{
				if(item[7] instanceof Boolean)
					childbean.setStatus(Boolean.valueOf(item[7].toString()));
				else
					childbean.setStatus(Integer.valueOf(item[7].toString()).equals(1)?true:false);
				childbean.setCheckEntity(Integer.parseInt(item[11].toString()));
				childbean.setSortOrder(Integer.parseInt(item[12].toString()));
			}


			((MyItemBean)toplist.get(hasToplist.indexOf(Integer.parseInt(item[8].toString())))).getMyItems().add(childbean);
		}


		getModel().setTopItems(buildJson());
	}

	@ClientCallable
	private void myitemChanged(String items,boolean isdone) {
		try{
			ObjectMapper objectMapper=new ObjectMapper();
			MyItemBean myitem=objectMapper.readValue(items,MyItemBean.class);

			if(myitem.getMyItemType().equals(Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1) || 
					myitem.getMyItemType().equals(Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem")+1)) {
				setItems(myitem,isdone);
			}
			else if(myitem.getMyItemType().equals(Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1) || 
					myitem.getMyItemType().equals(Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem")+1)) {
				setCheckItems(myitem,isdone);
			}
			loadData(myitem.getItemType());
		}catch(Exception e){
			logger.error("An exception thrown myitemChanged in MyitemPage",e);
		}
	}

	private void setItems(MyItemBean myitem,boolean isdone) {
		JBEntity entity = service.getEntity(myitem.getItemId());
		entity.setStatus(isdone?JibbyConfig.status[0]:JibbyConfig.status[1]);
		entity.setProgress(isdone?100:0);
		service.saveEntity(entity);

		List<JBChecklistItemEntity> checklists= service.getAllDoneOrUndoneChecklistByEntity(myitem.getItemId(),isdone?0:1);
		int changevalue=isdone?1:0;
		for(JBChecklistItemEntity checklist:checklists) {
			checklist.setIsChecked(changevalue);
			service.saveChecklistItems(checklist);
		}

		if(isdone) {
			if(myitem.getMyItemType().equals(Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1))
				service.deleteMyActionListRow(myitem.getId());
			if(sessionModel.getUser().getId() != entity.getCreator())
				notificationToCreator(entity);
		}

		addItemModification(entity);
	}

	private void setCheckItems(MyItemBean myitem,boolean isdone) {
		JBChecklistItemEntity checkList = new JBChecklistItemEntity();
		checkList.setId(myitem.getItemId());
		checkList.setIsChecked(isdone?1:0);
		checkList.setSortOrder(myitem.getSortOrder());
		checkList.setName(myitem.getName());
		checkList.setEntity(myitem.getCheckEntity());
		service.saveChecklistItems(checkList);

		if(isdone) {
			insertItemModification(myitem.getCheckEntity(),Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("donechecklist")+1);
			if(myitem.getMyItemType().equals(Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1))
				service.deleteMyActionListRow(myitem.getId());
		}else {
			insertItemModification(myitem.getCheckEntity(),Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undonechecklist")+1);
		}
		changeItemProgressStatus(myitem.getCheckEntity());
	}

	public void changeItemProgressStatus(Integer entityid){ 
		JBEntity entity = service.getEntity(entityid);
		List<JBChecklistItemEntity>checkListData= service.getChecklistByEntity(entity.getId());

		Integer progress=0;
		Integer totalchk=0;
		Integer totalchkList=checkListData.size();

		for(JBChecklistItemEntity checklist:checkListData){
			if(checklist.getIsChecked() == JibbyConfig.checkItemStatus[0]){
				totalchk++;
			}	
		}
		if(totalchkList>0)
			progress = Integer.parseInt(BigDecimal.valueOf((Double.parseDouble(totalchk.toString())/totalchkList)*100).setScale(0, BigDecimal.ROUND_HALF_UP).toString());


		if(progress == 100)
			entity.setStatus(JibbyConfig.status[0]);
		else
			entity.setStatus(JibbyConfig.status[1]);

		entity.setProgress(progress);
		service.saveEntity(entity);

	}

	private void notificationToCreator(JBEntity entity) {
		String userName = sessionModel.getUser().getFirstName()+" "+sessionModel.getUser().getLastName();
		JBActionlistHistory actionlistHistory = new JBActionlistHistory();
		actionlistHistory.setCreated(new Date());
		actionlistHistory.setItemid(entity.getId());
		actionlistHistory.setItemtype(Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1);
		actionlistHistory.setUserid(entity.getCreator());
		actionlistHistory.setMessage(userName+" completed the task  \""+entity.getName()+"\".");
		actionlistHistory.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("markedByAssignee")+1);
		actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

		actionlistHistory=service.saveActionlistHistory(actionlistHistory);

	}

	public void addItemModification(JBEntity entity){
		Integer modificationId = service.deleteItemModificationByEntityAndUserAndCompleted(entity.getId(),sessionModel.getUser().getId());
		if(modificationId == null)
			insertItemModification(entity.getId(),entity.getStatus().equals(JibbyConfig.status[0])?Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("done")+1:Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undone")+1);

	}

	public void insertItemModification(Integer entityId,Integer type){
		JBItemModification itemModification=new JBItemModification();
		itemModification.setEntityId(entityId);
		itemModification.setUserId1(sessionModel.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);
	}

	private String buildJson(){
		ObjectMapper map=new ObjectMapper();
		try {
			String json=map.writeValueAsString(toplist);
			return json;
		} catch (JsonProcessingException e) {
			logger.error("A JsonProcessingException thrown buildJson in MyitemPage",e);
		}
		return "{}";
	}

	public interface MyitemPageModel extends TemplateModel {
		// Add setters and getters for template properties here.
		void setTopItems(String a);
	}

	@ClientCallable
	private void _onDeleteItem(String items) {
		try{
			ObjectMapper objectMapper=new ObjectMapper();
			MyItemBean myitem=objectMapper.readValue(items,MyItemBean.class);
			service.deleteMyActionListRow(myitem.getId());
			loadData(myitem.getItemType());
		}catch(Exception e){
			logger.error("An Exception thrown onDeleteItem in MyitemPage",e);
		}
	}

	@ClientCallable
	private void _myitemsSelectedItem(String items) {
		try{
			ObjectMapper objectMapper=new ObjectMapper();
			MyItemBean myitem=objectMapper.readValue(items,MyItemBean.class);
			if(type.equals("items")) {
				Dialog dialog = new Dialog();
				dialog.getElement().setAttribute("theme", "my-custom-dialog");
				dialog.setHeight("100vh");
				dialog.setWidth("100vw");
				dialog.add(selectedItemView);
				dialog.open();
				selectedItemView.setup(myitem.getItemId(),"My Items");
			}

		}catch(Exception e){
			logger.error("An Exception thrown myitemsSelectedItem in MyitemPage",e);
		}
	}
}
