package com.uppstreymi.jibby;

import javax.servlet.annotation.WebFilter;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

@WebFilter(filterName = "UrlRewriteFilter", urlPatterns = { "/*" }, asyncSupported = true)
public class UrlFilter extends UrlRewriteFilter {

}
