package com.uppstreymi.jibby;

import java.util.logging.Logger;

import javax.enterprise.event.Observes;
import com.vaadin.flow.server.ServiceInitEvent;

public class ServiceCustomizer {
	
	public static final String APPENDED_ID = "TEST_ID";
    public static final String APPENDED_TXT = "By Test";
    Logger log = Logger.getLogger(ServiceCustomizer.class.getName());
    private void onServiceInit(@Observes ServiceInitEvent serviceInitEvent) {
    	
        serviceInitEvent.addBootstrapListener(new CustomBootstrapListener());
    }
}
