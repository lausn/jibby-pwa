package com.uppstreymi.jibby.support;

import com.vaadin.flow.templatemodel.TemplateModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.item.ItemDetails;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.HttpMailgunService;
import com.uppstreymi.jibby.service.MailgunService;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;


@Tag("jibby-support")
@JsModule("./src/views/support/jibby-support.js")
@UIScoped
public class JibbySupport extends PolymerTemplate<JibbySupport.JibbySupportModel> {

	@Id("btnBackSupportDialog")
	private Button btnBackSupportDialog;
	
	@Id("uploadLayout")
	private Div uploaderLayout;
	@Id("btnSend")
	private Button btnSend;
	
	@Id("supportView")
	private Div supportView;
	@Id("tfName")
	private TextField tfName;
	@Id("tfEmail")
	private TextField tfEmail;
	@Id("taMessage")
	private TextArea taMessage;
	@Id("lblMessage")
	private Div lblMessage;

	@Inject
	SessionModel sessionModel;
	
	Upload uploader;
	MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
	SucceededEvent succeedEvent;
	Button uploadButton = new Button("");
	private Logger logger=LogManager.getLogger(ItemDetails.class);
	private HttpMailgunService httpMailgunService = new HttpMailgunService();
	
    public JibbySupport() {
        // You can initialise any data required for the connected UI components here.
    }

	@PostConstruct
	private void init() {
		btnBackSupportDialog.addClickListener(e -> {
			Dialog parent = (Dialog) this.getParent().get();
			parent.close();
		});
		
		btnSend.addClickListener(e->{
			if(isCheck())
				sendMessage();
		});
		
		fileUploaderInit();
	}
	
	private void fileUploaderInit() {

		uploader = new Upload(buffer);
		uploader.setWidth("100%");
		uploader.setId("supportUploader");
		uploader.setMaxFiles(1);
		uploader.setDropAllowed(false);
		uploadButton = new Button("Upload File");
		Icon unloadIc = VaadinIcon.UPLOAD.create();
		unloadIc.getElement().setAttribute("style", "fill: #fff;");
		uploadButton.getElement().setAttribute("style", "color: #fff;");
		uploadButton.addThemeName("primary");
		uploadButton.addThemeName("small");
		uploadButton.setIcon(unloadIc);
		uploadButton.setWidth("120px");
		uploadButton.setHeight("30px");
		uploadButton.addClassName("btn-style");
		uploader.setUploadButton(uploadButton);

		uploader.addSucceededListener(event -> {
			succeedEvent = event;
		});

		uploader.getElement().addEventListener("file-abort", remove -> {
			succeedEvent = null;
		});

		uploaderLayout.add(uploader);
	}
	
    public void setUp() {
    	taMessage.clear();
    	lblMessage.removeAll();
		if(sessionModel.getUser() != null){
			String userName = (sessionModel.getUser().getFirstName() == ""?"":sessionModel.getUser().getFirstName())+" "+(sessionModel.getUser().getLastName() == null || sessionModel.getUser().getLastName().isEmpty()?"":sessionModel.getUser().getLastName());
			tfName.setValue(userName);
			tfEmail.setValue(sessionModel.getUser().getEmail());
			tfName.setReadOnly(true);
			tfEmail.setReadOnly(true);
		}
    }
    
	private boolean isCheck() {
		if(!tfEmail.getValue().trim().isEmpty()){
			if(isValidEmail(tfEmail.getValue())){
				if(!taMessage.getValue().trim().isEmpty()){
					return true;
				}else
					Notification.show("Message can not be empty");
			}
		}else
			Notification.show("Please enter Your email");

		return false;
	}
	
	private boolean isValidEmail(String email) {

		boolean result = Utils.isValidateMail(email);
		if (!result)
			Notification.show("Please Enter Valid Email");
		return result;
	}
	
	private void sendMessage() {
		String fromAddress = tfEmail.getValue();
		String fromName = tfName.getValue() == ""?"":tfName.getValue();
		String toName = "Jibby Support";
		String subject = "Message to support via app.jibby.com";
		String cc = "hordur@lausn.is";
		String bcc ="bjaddt@gmail.com";
		String text = "";
		String strHtml=taMessage.getValue().replaceAll("(\r\n|\n)", "<br>");
		String cal = null;
		String attachUrls = "";
		String toAddress = "support@jibby.com";
		String attachUrl = "";
		try {
			if(succeedEvent !=null){
				UUID uuid = UUID.randomUUID();
				String fileName = uuid.toString();
				File fileFolder = new File(Utils.imagePath+File.separator+"supportcontent");
				if (!fileFolder.exists()) {
					fileFolder.mkdirs();
				}
				File des = createTempFile(fileName);
				attachUrl = des.getAbsolutePath();
				attachUrls=attachUrl;

			}
			
			Integer status = httpMailgunService.sendHTTPMail(fromAddress, fromName, toAddress, toName, subject, cc, bcc, text,
					strHtml,attachUrls , null, "", null);
			
			if (status != null && status==200) {
				
				Utils.deleteFile(attachUrl);
				Span supportSpan = new Span();
				supportSpan.addClassName("supportFormTitle");
				supportSpan.setText("Your message has been sent. Jibby support team will be contact with you shortly. Thank you.");
				lblMessage.add(supportSpan);
				//lblMessage.getElement().setProperty("innerHTML", "<span class='supportFormTitle'>Your message has been sent. Jibby support team will be contact with you shortly. Thank you.</span>");
				
			}

		} catch (Exception e) {
			logger.error("An exception thrown in JibbySupport->sendMessage: ",e);
		}
		
	}
	private File createTempFile(String fileName) {
		try {

			File des =new File(Utils.imagePath+File.separator+"supportcontent"+File.separator+fileName+"-"+succeedEvent.getFileName());
			byte[] buf = new byte[(int) succeedEvent.getContentLength()];
			InputStream is = buffer.getInputStream(succeedEvent.getFileName());
			is.read(buf);

			OutputStream outStream = new FileOutputStream(des);
			outStream.write(buf);

			outStream.flush();
			outStream.close();
			return des;
		} catch (IOException ex) {
			logger.error("An exception thrown in JibbySupportDesignImpl->createTempFile: ",ex);
		}
		return null;
	}
	private String getEmailOrName(JBUserEntity user) {
		String userEmailOrName;
		String userFirstName = user.getFirstName();
		String userLastName = user.getLastName();
		if(userFirstName==null || userLastName==null)
			userEmailOrName = user.getEmail();
		else {

			if(userFirstName.trim().isEmpty() ||  userLastName.trim().isEmpty())
				userEmailOrName = user.getEmail();
			else
				userEmailOrName = userFirstName + " " + userLastName;
		}
		return userEmailOrName;
	}
    public interface JibbySupportModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
