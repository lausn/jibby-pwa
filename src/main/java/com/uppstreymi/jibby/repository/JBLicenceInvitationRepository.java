package com.uppstreymi.jibby.repository;


import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import com.uppstreymi.jibby.bean.JBLicenseInvitation;

@Repository
public interface JBLicenceInvitationRepository extends EntityRepository<JBLicenseInvitation, Integer> {

	JBLicenseInvitation findByGuid(String guid);

	JBLicenseInvitation findByLicenseIdAndReceiverId(Integer licenseId,Integer receiverId);

	
}
