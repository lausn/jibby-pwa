package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import com.uppstreymi.jibby.bean.JBCustomerUsersEntity;



@Repository
public interface JBCustomerUsersRepository extends EntityRepository<JBCustomerUsersEntity, Integer> 
{
	public QueryResult<JBCustomerUsersEntity> findByUserid(Integer userId);
	
}
