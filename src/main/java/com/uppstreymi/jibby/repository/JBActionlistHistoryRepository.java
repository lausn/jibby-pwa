package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;


@Repository
public interface JBActionlistHistoryRepository extends EntityRepository<JBActionlistHistory, Integer> 
{

	public QueryResult <JBActionlistHistory> findByUseridAndIsRead(Integer userId, Integer isRead);

	//public QueryResult<JBAttachmentEntity> findByEntity(Integer id);

}
