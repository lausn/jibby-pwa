package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBChecklistItemEntity;


@Repository
public interface JBChecklistItemRepository extends EntityRepository<JBChecklistItemEntity, Integer> {

	public QueryResult<JBChecklistItemEntity> findByEntity(Integer id);
	
	public QueryResult<JBChecklistItemEntity> findByEntityAndIsChecked(Integer id,Integer isChecked);
	
	@Query("SELECT a FROM JBChecklistItemEntity a WHERE a.entity IN :param")
	public QueryResult<JBChecklistItemEntity> findAllChecklistByEntitys(@QueryParam("param")  List<Integer> itemids);
}
