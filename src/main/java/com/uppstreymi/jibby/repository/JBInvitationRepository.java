package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import com.uppstreymi.jibby.bean.JBInvitation;


@Repository
public interface JBInvitationRepository extends EntityRepository<JBInvitation, Integer> {

	

}
