package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBMessages;

@Repository
public interface JBMessagesRepository extends EntityRepository<JBMessages,Integer> {

	@Query("SELECT m FROM JBMessages m WHERE m.recipient=?1 AND m.isRead =0 AND m.isEmailed =0")
	public QueryResult<JBMessages> findByRecipent(Integer recipent);
}
