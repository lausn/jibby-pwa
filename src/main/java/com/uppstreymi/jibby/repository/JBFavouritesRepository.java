package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBFavouritesEntity;

@Repository
public interface JBFavouritesRepository extends EntityRepository<JBFavouritesEntity, Integer>{

	public JBFavouritesEntity findByUserIdAndEntityId(Integer userId,Integer entityId);
	
	public QueryResult<JBFavouritesEntity> findByUserId(Integer userId);
}
