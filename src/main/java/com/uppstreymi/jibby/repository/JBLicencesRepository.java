package com.uppstreymi.jibby.repository;

import java.util.Date;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBLicencesEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;

@Repository
public interface JBLicencesRepository extends EntityRepository<JBLicencesEntity, Integer> {

	QueryResult<JBLicencesEntity> findByBuyer(JBUserEntity user);
	public JBLicencesEntity findByLicenceCode(String user);
	public JBLicencesEntity findTop1ByUserAndCustomerIdOrderByValidTillDateDesc(JBUserEntity user,Integer customerId);
	
	/*@Query("SELECT a FROM JBLicencesEntity a WHERE a.buyer=?1 and a.orderNumber=?2 and a.isActive=1 ")
	QueryResult<JBLicencesEntity> getMultiActiveLicensesByUserAndOrderNumber(JBUserEntity user,String orderNumber);*/
	
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.buyer=?1 and a.orderNumber=?2 and a.customerId=?3 and a.isActive=1 ")
	QueryResult<JBLicencesEntity> getMultiActiveLicensesByUserAndCustomerAndOrderNumber(JBUserEntity user,String orderNumber,Integer custid);
	
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.user=?1 and a.validTillDate>?2 and a.customerId=?3 order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getValidLicensesByUserAndCustomerId(JBUserEntity user,Date date,Integer custId);
	
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.user=?1 and a.validTillDate>?2 and a.customerId=?3 order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getValidLicensesByUserAndCustomer(JBUserEntity user,Date date,Integer custId);
	
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.orderNumber=?1 order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getAllLicensesByOrderNumber(String orderNumber);
	
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.orderNumber=?1 and a.isActive=1 order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getAllActiveLicensesByOrderNumber(String orderNumber);
	
	@Query("SELECT Distinct a.orderNumber FROM JBLicencesEntity a WHERE a.buyer=?1")
	public QueryResult<String> getAllBuyerOrderNumber(JBUserEntity user);
	
	@Query("SELECT Distinct a.orderNumber FROM JBLicencesEntity a WHERE a.isActive=1 AND a.validTillDate BETWEEN :start AND :end")
	public QueryResult<String> getAllActiveValidLicenseOrderNumberByTodate(@QueryParam("start") Date start, @QueryParam("end") Date end);
	
	/*@Query("SELECT a FROM JBLicencesEntity a WHERE a.user=?1 and a.validTillDate>?2 and a.orderNumber in :param order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getValidLicensesByUserAndCustomer(JBUserEntity user,Date date,@QueryParam("param") List<String> orders);
	*/
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.user=?1 and a.validTillDate>?2 and a.orderNumber in :param and a.customerId=?3 order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getValidLicensesByUserAndCustomer(JBUserEntity user,Date date,@QueryParam("param") List<String> orders,Integer custid);
	
	/*@Query("SELECT a FROM JBLicencesEntity a WHERE a.user=?1 and a.orderNumber in :param order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getLicensesByUserAndCustomer(JBUserEntity user,@QueryParam("param") List<String> orders);*/
	
	@Query("SELECT a FROM JBLicencesEntity a WHERE a.user=?1 and a.orderNumber in :param and a.customerId=?3 order by a.validTillDate desc")
	public QueryResult<JBLicencesEntity> getLicensesByUserAndCustomer(JBUserEntity user,@QueryParam("param") List<String> orders,Integer custId);

}
