package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBChatMessageEntity;



@Repository
public interface JBChatMessageRepository extends EntityRepository<JBChatMessageEntity, Integer> {

	public QueryResult<JBChatMessageEntity> findByEntity(Integer id);

}
