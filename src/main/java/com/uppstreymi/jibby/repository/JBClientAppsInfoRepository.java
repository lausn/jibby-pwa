package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBClientAppsInfoEntity;

@Repository
public interface JBClientAppsInfoRepository extends EntityRepository<JBClientAppsInfoEntity, Integer>{
	
	public JBClientAppsInfoEntity findByGuidAndEnable(String guid, Integer isEnable);
}
