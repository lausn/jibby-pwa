package com.uppstreymi.jibby.repository;

import java.util.List;
import java.util.Set;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;



@Repository
public interface JBUserRoleRepository extends EntityRepository<JBUserRoleEntity, Integer> 
{
	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.entityId in :entities and u.userRole<3")
	public QueryResult<JBUserRoleEntity> findAllUserbyEntity(@QueryParam("entities") List<Integer> parentArray);
	
	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.entityId in :entities order by u.userRole")
	public QueryResult<JBUserRoleEntity> findByEntityIdOrderByUserRole(@QueryParam("entities") List<Integer> parentArray);
	
	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.entityId in :arrentity and u.user =?1")
	public QueryResult<JBUserRoleEntity> findByEntityIdAndUserId(JBUserEntity user,@QueryParam("arrentity") List<Integer> array);
	
	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.entityId =?1 and u.user=?2 and u.userRole in :roles order by u.userRole")
	public QueryResult<JBUserRoleEntity> findByEntityIdAndUserWithRolesOrderByUserRole(Integer entityId,JBUserEntity user,@QueryParam("roles") List<Integer> roles);
	
	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.user=?1 and u.userRole in :roles order by u.userRole")
	public QueryResult<JBUserRoleEntity> findByUserWithRolesOrderByUserRole(JBUserEntity user,@QueryParam("roles") List<Integer> roles);
	
	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.entityId =?1 and u.userRole=?2 and u.user in :users")
	public QueryResult<JBUserRoleEntity> findByEntityIdAndUsersAndRoles(Integer entityId,Integer roles,@QueryParam("users") Set<JBUserEntity> user);
	
//	@Query("SELECT u FROM JBUserRoleEntity u WHERE u.entityId in :entities order by u.userRole")
	public QueryResult<JBUserRoleEntity> findByEntityId(Integer entityId);
	
	public JBUserRoleEntity findTop1ByEntityIdAndUserRole(Integer entityId,Integer userRole);
}
