package com.uppstreymi.jibby.repository;


import java.util.Date;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBSubscriptionsEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;

@Repository
public interface JBSubscriptionsRepository extends EntityRepository<JBSubscriptionsEntity, Integer>{

	//QueryResult<JBSubscriptionsEntity> findByUserId(JBUserEntity user);

	public JBSubscriptionsEntity findTop1ByuserIdOrderByIdDesc(JBUserEntity user);
	
	public JBSubscriptionsEntity findByOrderNumber(String orderNumber);
	
	@Query("SELECT Distinct a.orderNumber FROM JBSubscriptionsEntity a WHERE a.isActive=1 AND a.validTillDate BETWEEN :start AND :end")
	public QueryResult<String> getAllActiveValidSubscriptionOrderNumberByTodate(@QueryParam("start") Date start, @QueryParam("end") Date end);
	
	
	@Query("SELECT a FROM JBSubscriptionsEntity a WHERE a.userId=?1 AND a.orderNumber in :param order by a.id desc")
	public QueryResult<JBSubscriptionsEntity> findByuserIdAndCustomerOrderByIdDesc(JBUserEntity user,@QueryParam("param") List<String> orders);
	

}
