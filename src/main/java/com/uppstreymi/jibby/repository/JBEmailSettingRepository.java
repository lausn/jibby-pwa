package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import com.uppstreymi.jibby.bean.JBEmailSetting;



@Repository
public interface JBEmailSettingRepository extends EntityRepository<JBEmailSetting, Integer> 
{
	
	public JBEmailSetting findByUser(Integer userId);
	
}
