package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;
import com.uppstreymi.jibby.bean.JBUserEntity;


@Repository
public interface JBUserEntityRepository extends EntityRepository<JBUserEntity, Integer> 
{
	public JBUserEntity findByEmailAndPassword(String userName,String password);
	
	public JBUserEntity findByEmail(String email);
	
	@Query("SELECT u FROM JBUserEntity u WHERE  u.emailAboutEntityChanges IN :param AND u.id IN "
			+ "(SELECT DISTINCT m.recipient FROM JBMessages m WHERE m.recipient>0 AND m.isRead=0 AND m.isEmailed=0)")
	public QueryResult<JBUserEntity> findByMessageRecipent(@QueryParam("param")  List<Integer> emailAbout);

	public JBUserEntity findByActivationCodeAndType(String acid,Integer type);
	
	public JBUserEntity findByEmailAndTempguid(String userName,String guid);
	
	public JBUserEntity findByGuid(String guid);
	
	@Query("SELECT u FROM JBUserEntity u WHERE u.id=?1 AND u.reminderEntityUnfinished IN :param")
	public JBUserEntity findByUserIdAndUnfinishedActivities(Integer id, @QueryParam("param") List<Integer> unfinishedActivities);
}
