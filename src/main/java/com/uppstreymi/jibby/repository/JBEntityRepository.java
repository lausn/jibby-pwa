package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;


@Repository
public interface JBEntityRepository extends EntityRepository<JBEntity, Integer> {

	public QueryResult<JBEntity> findByhierarchyLike(String hierarchy);
	public QueryResult<JBEntity> findByparentProjectId(Integer id);

	public QueryResult<JBEntity> findByIdOrHierarchyLike(Integer id,String hierarchy);
	
	@Query("SELECT a,(SELECT count(b.id) FROM JBEntity b  WHERE b.parentId=a.id) ischild, "
			+ "(SELECT count(c.id) FROM JBChecklistItemEntity c WHERE c.entity=a.id) isChecklist, "
			+"(SELECT count(d.id) FROM JBAttachmentEntity d WHERE d.entity=a.id) isAttachment"
			+"FROM JBEntity a WHERE a.parentId = ?1")
	public QueryResult<Object[]> findByAllParentId(Integer parentId);
	
	public QueryResult<JBEntity> findByParentId(Integer parentId);
	
	@Query("SELECT a FROM JBEntity a WHERE a.parentId in :entities")
	public QueryResult<Object[]> findByParentIds(@QueryParam List<Integer> entities);
	
	@Query("SELECT a FROM JBEntity a WHERE a.id IN :param")
	public QueryResult<JBEntity> findByEntityId(@QueryParam("param")  List<Integer> arrayid);
	
	@Query("SELECT a FROM JBEntity a WHERE a.id IN :param and a.status IN :param2")
	public QueryResult<JBEntity> findByEntityIdAndStatus(@QueryParam("param")  List<Integer> arrayid,@QueryParam("param2") List<Integer> status);
	
	@Query("SELECT a FROM JBEntity a WHERE a.id IN :param and a.parentProjectId = ?1")
	public QueryResult<JBEntity> findByEntityIdAndParentProjectId(@QueryParam("param")  List<Integer> arrayid,Integer parentProjectId);
	
	@Query("SELECT a,0 isChild, "
			+ "(SELECT count(c.id) FROM JBChecklistItemEntity c WHERE c.entity=a.id) isChecklist, "
			+"(SELECT count(d.id) FROM JBAttachmentEntity d WHERE d.entity=a.id) isAttachment"
			+" FROM JBEntity a WHERE a.parentId = ?1")
	public QueryResult<Object[]> findByParentIdAndStatusOrderBy(Integer parentId);
	
	
	public QueryResult<JBEntity> findByCreatorAndParentIdIsNullOrderByNameAsc(Integer id);
	
	
	/*@Query("SELECT a FROM JBEntity a WHERE (a.parentId is null and a.creator=?2 and a.isPublic=?3) or a.id in "
			+ "(SELECT distinct c.parentProjectId from JBEntity c where c.isPublic=?3 and c.id in "
			+ "(SELECT b.entityId FROM JBUserRoleEntity b where b.user=?1))")*/
	
	/*-------edited by Asad 24-11-2017----------*/
	@Query("SELECT a FROM JBEntity a WHERE a.isPublic in :paramispublic AND (a.isDelete = 0 OR a.isDelete IS NULL) AND a.id in "
			+ "(SELECT DISTINCT COALESCE(c.parentProjectId,c.id) FROM JBEntity c WHERE (c.isDelete = 0 OR c.isDelete IS NULL) AND c.isPublic in :paramispublic AND c.id in"
			+ "(SELECT b.entityId FROM JBUserRoleEntity b WHERE b.user=?1 AND b.userRole in :param AND b.customerid not in :customerparam))")
	public QueryResult<JBEntity> findColAProject(JBUserEntity user,@QueryParam("paramispublic") List<Integer> publicroles,@QueryParam("param") List<Integer> roles,@QueryParam("customerparam") List<Integer> customerIds);
	
	@Query("SELECT a FROM JBEntity a WHERE a.parentProjectId is null AND a.isDelete = 1 AND a.id in "
			+ "(SELECT b.entityId FROM JBUserRoleEntity b WHERE b.user = ?1 AND b.userRole = ?2 AND b.customerid = ?3)")
	public QueryResult<JBEntity> findColATrashProject(JBUserEntity user, Integer roles, Integer customerIds);
	
	@Query("SELECT count(b.id) ischild,(SELECT count(c.id) FROM JBChecklistItemEntity c WHERE c.entity=?1) isChecklist,"
			+ "(SELECT count(d.id) FROM JBAttachmentEntity d WHERE d.entity=?1) isAttachment FROM JBEntity b  WHERE b.parentId=?1")
	public Object[] getIsChild(Integer parentId);
	
	@Query("SELECT count(b.id) ischild,(SELECT count(c.id) FROM JBChecklistItemEntity c WHERE c.entity=?1) isChecklist,"
			+ "(SELECT count(d.id) FROM JBAttachmentEntity d WHERE d.entity=?1) isAttachment FROM JBEntity b  WHERE b.id=?1")
	public Object[] entityAttachmentOrChecklistCount(Integer entityId);

	
	//search
	@Query("SELECT a FROM JBEntity a WHERE  a.parentProjectId in :param")
	public QueryResult<JBEntity> findByParentProjectId(@QueryParam("param")  List<Integer> parentProjectIds);
	
	@Query("SELECT a FROM JBEntity a WHERE a.parentId = ?1")
	public QueryResult<JBEntity> findByParentIdOrderBy(Integer parentId);
	
	@Query("SELECT a.name FROM JBEntity a  WHERE a.id in (SELECT b.entityId FROM JBGoogleDriveEntity b WHERE b.userId=?1 AND b.isUploaded=?2)")
	public QueryResult<Object> googleDriveProjectNames(Integer userId,Integer isUploaded);
	
	@Query("SELECT a.name FROM JBEntity a WHERE a.id = ?1")
	public QueryResult<Object> projectNames(Integer entity);
	
	
}
