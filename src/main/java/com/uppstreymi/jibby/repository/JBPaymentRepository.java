package com.uppstreymi.jibby.repository;

import java.util.Date;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBPayment;

@Repository
public interface JBPaymentRepository extends EntityRepository<JBPayment, Integer> {

	public JBPayment findTop1ByOrderNumberOrderByIdDesc(String orderNumber);
	public JBPayment findByOrderNumber(String orderNumber);
	
	@Query("SELECT a FROM JBPayment a WHERE a.creditcard is not null AND a.paymentProvider= :provider AND a.orderNumber in :order")
	public QueryResult<JBPayment> getAllPaymentByProviderAndOrderNumber(@QueryParam("provider") Integer provider, @QueryParam("order") List<String> order);

}
