package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBKanbanBoardColumns;



@Repository
public interface JBKanbanBoardColumnsRepository extends EntityRepository<JBKanbanBoardColumns, Integer> 
{
	public QueryResult<JBKanbanBoardColumns> findByEntityId(Integer id);
}
