package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBPaidUserEntity;


@Repository
public interface JBPaidUserEntityRepository extends EntityRepository<JBPaidUserEntity, Integer> 
{

	public JBPaidUserEntity findByEmail(String email);
}
