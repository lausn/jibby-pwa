package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import com.uppstreymi.jibby.bean.JBGoogleDriveEntity;



@Repository
public interface JBGoogleDriveRepository extends EntityRepository<JBGoogleDriveEntity, Integer> 
{

	public QueryResult<JBGoogleDriveEntity> findAllOrderByCreatedateAsc();
	
	public QueryResult<JBGoogleDriveEntity> findByIsUploadedOrderByUserId(Integer isUploaded);
	
	public JBGoogleDriveEntity findByUserIdAndEntityId(Integer userId,Integer entityId);
	
}
