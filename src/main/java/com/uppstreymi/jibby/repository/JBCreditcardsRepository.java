package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBCreditcardsEntity;


@Repository
public interface JBCreditcardsRepository extends EntityRepository<JBCreditcardsEntity, Integer> 
{

	public JBCreditcardsEntity findByUserId(Integer userId);
	
	@Query("SELECT a FROM JBCreditcardsEntity a WHERE a.expiryDate=?1 ")
	public QueryResult<JBCreditcardsEntity> getAllExpiredCreditCards(String expiryDate);

}
