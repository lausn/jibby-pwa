package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBInvoicesEntity;


@Repository
public interface JBInvoicesRepository extends EntityRepository<JBInvoicesEntity, Integer> 
{

	

}
