package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;


@Repository
public interface JBActionlistRepository extends EntityRepository<JBActionlist, Integer> 
{
	public QueryResult<JBActionlist> findByid(Integer id);
	
	
	@Query("Select a from JBUserEntity a where a.id in( "
			+ "Select distinct b.assignedto from JBActionlist b WHERE b.itemid =?1 "
			+ "and b.itemtype=?2)")
	public QueryResult<JBUserEntity> findAllAssigneeById(Integer itemid,Integer itemtype);
	
	public QueryResult<JBActionlist> findByItemidAndItemtypeAndAddedby(Integer itemid,Integer itemtype,Integer addedby);

}
