package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBAttachmentEntity;


@Repository
public interface JBAttachmentEntityRepository extends EntityRepository<JBAttachmentEntity, Integer> {

	public QueryResult<JBAttachmentEntity> findByEntity(Integer id);
	
	@Query("SELECT a FROM JBAttachmentEntity a WHERE a.entity IN :param")
	public QueryResult<JBAttachmentEntity> findByEntityId(@QueryParam("param")  List<Integer> arrayid);

}
