package com.uppstreymi.jibby.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBItemModification;


@Repository
public interface JBItemModificationRepository extends EntityRepository<JBItemModification,Integer> {

}
