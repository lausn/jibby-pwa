package com.uppstreymi.jibby.repository;

import java.util.Date;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;

import com.uppstreymi.jibby.bean.JBLicencesEntity;
import com.uppstreymi.jibby.bean.JBLicencesHistory;
import com.uppstreymi.jibby.bean.JBUserEntity;

@Repository
public interface JBLicencesHistoryRepository extends EntityRepository<JBLicencesHistory, Integer> {

	@Query("SELECT a FROM JBLicencesHistory a WHERE a.licenceCode IN :param order by a.date desc")
	public QueryResult<JBLicencesHistory> findHistoryOrderByDateDesc(@QueryParam("param")  List<String> codes);
	
	//public JBLicencesHistory findTop1ByUserIdAndTextOrderByDateDesc(JBUserEntity user,String status);
	public JBLicencesHistory findTop1ByUserIdAndCustomerIdAndTextOrderByDateDesc(JBUserEntity user,Integer customerId,String status);
	
	@Query("SELECT a FROM JBLicencesHistory a WHERE a.userId=?1 and a.text=?2 and a.licenceCode in :param order by a.date desc")
	public QueryResult<JBLicencesHistory> getLicensesHistorByUserAndCustomer(JBUserEntity user,String status,@QueryParam("param") List<String> licenceCode);

}
