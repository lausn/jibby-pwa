package com.uppstreymi.jibby.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.QueryResult;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import com.uppstreymi.jibby.bean.JBCustomerEntity;



@Repository
public interface JBCustomerRepository extends EntityRepository<JBCustomerEntity, Integer> 
{
	@Query("SELECT a FROM JBCustomerEntity a WHERE  a.id in :param AND a.subscriptionguid is null" )
	public QueryResult<JBCustomerEntity> findByCustomerAccess(@QueryParam("param") List<Integer> arrayid);
	
	@Query("SELECT a FROM JBCustomerEntity a WHERE  a.id in (Select b.customerid from JBCustomerUsersEntity b where b.userid=?1) order by a.createdate" )
	public QueryResult<JBCustomerEntity> findByCustomerByUser(Integer userId);
	
	public JBCustomerEntity findBySubscriptionguid(String guid);
}
