package com.uppstreymi.jibby.bean;


import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_kanbanboardcolumns")
@Cacheable(false)
public class JBKanbanBoardColumns extends AbstractEntity{

	@Column(name = "name")
	private String name;
	
	@Column(name = "wipLimit")
	private Integer wipLimit;
	
	@Column(name = "entityId")
	private Integer entityId;
	
	@Column(name = "sortOrder")
	private Integer sortOrder;
	
	@Column(name = "containedEntityIds")
	private String containedEntityIds;
	
	
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	
	public void setWipLimit(Integer wipLimit){
		this.wipLimit=wipLimit;
	}
	public Integer getWipLimit(){
		return this.wipLimit;
	}
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return this.entityId;
	}
	public void setSortOrder(Integer sortOrder){
		this.sortOrder=sortOrder;
	}
	public Integer getSortOrder(){
		return sortOrder;
	}
	public void setContainedEntityIds(String containedEntityIds){
		this.containedEntityIds=containedEntityIds;
	}
	public String getContainedEntityIds(){
		return containedEntityIds;
	}
	
}
