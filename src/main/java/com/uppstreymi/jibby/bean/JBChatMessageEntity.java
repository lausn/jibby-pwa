package com.uppstreymi.jibby.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.deltaspike.data.api.Query;


@Entity
@Table(name = "jb_chatmessages")
@Cacheable(false)
public class JBChatMessageEntity extends AbstractEntity{
	
	@Column(name = "sender")
	private Integer sender;
	
	@Column(name = "entity")
	private Integer entity;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "date")
	private Date date;
	
	public void setSender(Integer sender){
		this.sender=sender;
	}
	public Integer getSender(){
		return sender;
	}
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	public void setText(String text){
		this.text=text;
	}
	public String getText(){
		return this.text;
	}
	public void setDate(Date date){
		this.date=date;
	}
	public Date getDate(){
		return date;
	}
}
