package com.uppstreymi.jibby.bean;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


@SuppressWarnings("serial")
@Entity
@Table(name = "jb_creditcards")
@Cacheable(false)
public class JBCreditcardsEntity extends AbstractEntity {

	@Column(name = "userId")
	private Integer userId;
	
	@Column(name = "createdate")
	private Date createDate;

	@Column(name = "cardholdername")
	private String cardholderName;

	@Column(name = "maskedcardnumber")
	private String maskedcardnumber;

	@Column(name = "expirydate")
	private String expiryDate;
	
	@Column(name = "virtualcardnumber")
	private String virtualCardnumber;
	
	@Transient
	private String cardNumber;
	
	@Transient
	private String cvcNumber;
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber=cardNumber;
	}
	
	public String getCardNumber() {
		return this.cardNumber;
	}
	
	public void setCvcNumber(String cvcNumber) {
		this.cvcNumber=cvcNumber;
	}
	
	public String getCvcNumber() {
		return this.cvcNumber;
	}
	
	public Integer getUserId() {
		return userId;
	}
	
	public void setUserId(Integer userId) {
		this.userId=userId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCardholderName() {
		return cardholderName;
	}

	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}

	public String getMaskedcardnumber() {
		return maskedcardnumber;
	}

	public void setMaskedcardnumber(String maskedcardnumber) {
		this.maskedcardnumber = maskedcardnumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String getVirtualCardnumber() {
		return virtualCardnumber;
	}

	public void setVirtualCardnumber(String virtualCardnumber) {
		this.virtualCardnumber = virtualCardnumber;
	}
}
