package com.uppstreymi.jibby.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ItemBean {

	private JBEntity entity;
	private boolean isChild;
	private String name;
	private Integer entityType;
	private Integer id;
	private Integer status;
	private Date estimatedStartDate;
	private Date estimatedFinishDate;
	private Date creationDate;
	private Integer parentProjectId;
	private Integer parentId;
	private boolean isRoles;
	private String keyword;
	private boolean isAttachment,isChecklist;
	
	
	public ItemBean(){}
	public ItemBean(JBEntity entity){
		this.entity=entity;
		this.name = entity.getName();
		this.entityType = entity.getEntityType();
		this.id = entity.getId();
		this.parentProjectId = entity.getParentProjectId();
		this.parentId = entity.getParentId();
		this.status=entity.getStatus();
		this.estimatedStartDate=entity.getEstimatedStartDate();
		this.estimatedFinishDate=entity.getEstimatedFinishDate();
		this.creationDate=entity.getCreationDate();
		this.keyword=entity.getKeywords();
		
	}
	public void setEntity(JBEntity entity){
		this.entity=entity;
		this.name = entity.getName();
		this.entityType = entity.getEntityType();
		this.id = entity.getId();
		this.parentProjectId = entity.getParentProjectId();
		this.parentId = entity.getParentId();
		this.status=entity.getStatus();
		this.estimatedStartDate=entity.getEstimatedStartDate();
		this.estimatedFinishDate=entity.getEstimatedFinishDate();
		this.creationDate=entity.getCreationDate();
		this.keyword=entity.getKeywords();
	}

	
	public JBEntity getEntity(){
		return this.entity;
	}
	public void setId(Integer id){
		this.id=id;
	}
	
	public Integer getId(){
		return this.id;
	}
	
	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}
	
	public Integer getParentId(){
		return this.parentId;
	}
	
	public void setParentProjectId(Integer parentProjectId){
		this.parentProjectId=parentProjectId;
	}
	
	public Integer getParentProjectId(){
		return this.parentProjectId;
	}
	public void setEntityType(Integer type){
		this.entityType=type;
	}
	
	public Integer getEntityType(){
		return this.entityType;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setIsChild(boolean isChild){
		this.isChild=isChild;
	}
	public boolean getIsChild(){
		return this.isChild;
	}
	
	public void setIsAttachment(boolean isAttachment){
		this.isAttachment=isAttachment;
	}
	public boolean getIsAttachment(){
		return this.isAttachment;
	}
	
	public void setIsChecklist(boolean isChecklist){
		this.isChecklist=isChecklist;
	}
	public boolean getIsChecklist(){
		return this.isChecklist;
	}
	
	public void setIsRoles(boolean isRoles) {
		this.isRoles=isRoles;
	}
	
	public boolean getIsRoles() {
		return this.isRoles;
	}
	
	public void setStatus(Integer status) {
		this.status=status;
	}
	
	public Integer getStatus() {
		return this.status;
	}
	
	public void setEstimatedStartDate(Date estimatedStartDate){
		this.estimatedStartDate=estimatedStartDate;
	}
	public Date getEstimatedStartDate(){
		return this.estimatedStartDate;
	}
	
	public void setEstimatedFinishDate(Date estimatedFinishDate){
		this.estimatedFinishDate=estimatedFinishDate;
	}
	public Date getEstimatedFinishDate(){
		return this.estimatedFinishDate;
	}
	
	public void setCreationDate(Date creationDate){
		this.creationDate=creationDate;
	}
	public Date getCreationDate(){
		return this.creationDate;
	}
	
	public void setKeywords(String keyword) {
		this.keyword=keyword;
	}
	
	public String getKeywords() {
		return this.keyword;
	}
}
