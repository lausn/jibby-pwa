package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_actionlist")
@Cacheable(false)
public class JBActionlist extends AbstractEntity{

	@Column(name = "itemtype")
	private Integer itemtype;
	
	@Column(name = "itemid")
	private Integer itemid;
	
	@Column(name = "dateadded")
	private Date dateadded;
	
	
	@Column(name = "addedby")
	private Integer addedby;
	
	@Column(name = "itemname")
	private String itemname;
	
	@Column(name = "assignedto")
	private Integer assignedto;
	
	@Column(name = "itemdatetime")
	private Date itemdatetime;
	
	
	public void setItemtype(Integer itemtype){
		this.itemtype=itemtype;
	}
	public Integer getItemtype(){
		return this.itemtype;
	}
	
	public void setItemid(Integer itemid){
		this.itemid=itemid;
	}
	public Integer getItemid(){
		return this.itemid;
	}
	public void setDateadded(Date dateadded){
		this.dateadded=dateadded;
	}
	public Date getDateadded(){
		return this.dateadded;
	}
	public void setAddedby(Integer addedby){
		this.addedby=addedby;
	}
	public Integer getAddedby(){
		return addedby;
	}
	public void setItemname(String itemname){
		this.itemname=itemname;
	}
	public String getItemname(){
		return itemname;
	}
	public void setAssignedto(Integer assignedto){
		this.assignedto=assignedto;
	}
	public Integer getAssignedto(){
		return assignedto;
	}
	public void setItemdatetime(Date itemdatetime){
		this.itemdatetime=itemdatetime;
	}
	public Date getItemdatetime(){
		return itemdatetime;
	}
	
	
	
}
