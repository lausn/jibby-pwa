package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_favourites")
@Cacheable(false)
public class JBFavouritesEntity extends AbstractEntity{
		
	@Column(name = "createdate")
	private Date createDate;
	
	@Column(name = "user_id")
	private Integer userId;
	
	@Column(name = "entity_id")
	private Integer entityId;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}
	

}
