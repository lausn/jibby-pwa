package com.uppstreymi.jibby.bean;

public class StatusCriteria {

	Integer id;
	String caption;

	
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setCaption(String caption){
		this.caption=caption;
	}
	public String getCaption(){
		return caption;
	}
	
}
