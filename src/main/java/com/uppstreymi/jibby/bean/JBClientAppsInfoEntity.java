package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_clientappsinfo")
@Cacheable(false)
public class JBClientAppsInfoEntity extends AbstractEntity {

	@Column(name = "appName")
	private String appName;

	@Column(name = "guid")
	private String guid;

	@Column(name = "createDate")
	private Date createDate;

	@Column(name = "enable")
	private Integer enable;

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppName() {
		return this.appName;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	public Integer getEnable() {
		return this.enable;
	}
}
