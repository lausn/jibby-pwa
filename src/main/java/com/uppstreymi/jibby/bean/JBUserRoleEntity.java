package com.uppstreymi.jibby.bean;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "jb_userroletoentity")
@Cacheable(false)
public class JBUserRoleEntity extends AbstractEntity{
	
	@Column(name = "entityId")
	private Integer entityId;
	
	
	@OneToOne(optional=false)
	@JoinColumn(name="userId", unique=true, nullable=false, updatable=true)
	private JBUserEntity user;
	
	
	@Column(name = "userRole")
	private Integer userRole;
	
	@Column(name = "customer_id")
	private Integer customerid;
	
	
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return entityId;
	}
	
	public void setUser(JBUserEntity user){
		this.user=user;
	}
	public JBUserEntity getUser(){
		return this.user;
	}
	public void setUserRole(Integer userRole){
		this.userRole=userRole;
	}
	public Integer getUserRole(){
		return userRole;
	}
	
	public void setCustomerid(Integer customerid) {
		this.customerid=customerid;
	}
	public Integer getCustomerid() {
		return this.customerid;
	}
	
}
