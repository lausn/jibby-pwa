package com.uppstreymi.jibby.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Table;


@Entity
@Table(name = "jb_googleDrive")
@Cacheable(false)
public class JBGoogleDriveEntity extends AbstractEntity{
	
	@Column(name = "entityId")
	private Integer entityId;
	
	@Column(name="userId")
	private Integer userId;
	
	@Column(name="isUploaded")
	private Integer isUploaded;
	
	@Column(name = "createdate")
	private Date createdate;
	
	
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return entityId;
	}
	
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	public Integer getUserId(){
		return this.userId;
	}
	
	public void setIsUploaded(Integer isUploaded){
		this.isUploaded=isUploaded;
	}
	public Integer getIsUploaded(){
		return this.isUploaded;
	}
	
	public void setCreatedate(Date createdate){
		this.createdate=createdate;
	}
	public Date getCreatedate(){
		return createdate;
	}
	
}
