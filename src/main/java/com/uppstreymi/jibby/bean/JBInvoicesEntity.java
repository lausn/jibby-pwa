package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name = "jb_invoices")
@Cacheable(false)
public class JBInvoicesEntity extends AbstractEntity {

	@Column(name = "payment_id")
	private Integer paymentId;
	
	@Column(name = "createdate")
	private Date createDate;

	@Column(name = "transection_no")
	private String transectionNo;

	@Column(name = "authcode")
	private String authcode;

	@Column(name = "payername")
	private String payername;

	@Column(name = "payeremail")
	private String payeremail;
	
	public Integer getPaymentId() {
		return this.paymentId;
	}
	
	public void setPaymentId(Integer paymentId) {
		this.paymentId=paymentId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getTransectionNo() {
		return transectionNo;
	}

	public void setTransectionNo(String transectionNo) {
		this.transectionNo = transectionNo;
	}

	public String getAuthcode() {
		return authcode;
	}

	public void setAuthcode(String authcode) {
		this.authcode = authcode;
	}

	public String getPayername() {
		return payername;
	}

	public void setPayername(String payername) {
		this.payername = payername;
	}

	public String getPayeremail() {
		return payeremail;
	}

	public void setPayeremail(String payeremail) {
		this.payeremail = payeremail;
	}
}
