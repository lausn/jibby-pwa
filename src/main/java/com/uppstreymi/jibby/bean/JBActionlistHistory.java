package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_actionlisthistory")
@Cacheable(false)
public class JBActionlistHistory extends AbstractEntity{

	@Column(name = "created")
	private Date created;
	
	@Column(name = "itemid")
	private Integer itemid;
	
	@Column(name = "itemtype")
	private Integer itemtype;
	
	
	@Column(name = "userid")
	private Integer userid;
	
	@Column(name = "messagetype")
	private Integer messagetype;
	
	@Column(name = "message")
	private String message;
	
	@Column(name = "isRead")
	private Integer isRead;
	
	
	public void setCreated(Date created){
		this.created=created;
	}
	public Date getCreated(){
		return this.created;
	}
	
	public void setItemid(Integer itemid){
		this.itemid=itemid;
	}
	public Integer getItemid(){
		return this.itemid;
	}
	public void setItemtype(Integer itemtype){
		this.itemtype=itemtype;
	}
	public Integer getItemtype(){
		return this.itemtype;
	}
	public void setUserid(Integer userid){
		this.userid=userid;
	}
	public Integer getUserid(){
		return userid;
	}
	public void setMessagetype(Integer messagetype){
		this.messagetype=messagetype;
	}
	public Integer getMessagetype(){
		return messagetype;
	}
	public void setMessage(String message){
		this.message=message;
	}
	public String getMessage(){
		return message;
	}
	
	public void setIsRead(Integer isRead){
		this.isRead=isRead;
	}
	public Integer getIsRead(){
		return this.isRead;
	}
	
}
