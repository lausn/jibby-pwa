package com.uppstreymi.jibby.bean;

public class ColorBean {
	
	private String colorCode;
	private String colorName;
	private String colorIcon;
	
	
	public void setColorCode(String colorCode){
		this.colorCode=colorCode;
	}
	public String getColorCode(){
		return this.colorCode;
	}
	
	public void setColorName(String colorName){
		this.colorName=colorName;
	}
	public String getColorName(){
		return this.colorName;
	}
	
	public void setColorIcon(String colorIcon){
		this.colorIcon=colorIcon;
	}
	public String getColorIcon(){
		return this.colorIcon;
	}

}
