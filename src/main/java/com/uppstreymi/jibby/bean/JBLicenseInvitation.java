package com.uppstreymi.jibby.bean;

import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_license_invitation")
@Cacheable(false)
public class JBLicenseInvitation extends AbstractEntity {

	@Column(name = "license_id")
	private Integer licenseId;

	@Column(name = "receiver_id")
	private Integer receiverId;
	
	@Column(name = "createdate")
	private Date createdate;
	
	@Column(name = "guid")
	private String guid;

	public Integer getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Integer licenseId) {
		this.licenseId = licenseId;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

}
