package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jb_payments")
@Cacheable(false)
public class JBPayment extends AbstractEntity {
	
	@Column(name = "userId")
	private Integer userId;
	
	@Column(name = "custEmail")
	private String custEmail;
	
	@Column(name = "custName")
	private String custName;
	
	@Column(name = "paymentProvider")
	private Integer paymentProvider;
	
	@Column(name = "itemType")
	private Integer itemType;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "orderNumber")
	private String orderNumber;
	
	@Column(name = "rebillAfter")
	private Integer rebillAfter;
	
	@Column(name = "amount")
	private Double amount;
	
	@OneToOne(optional=false)
	@JoinColumn(name="creditcard_id", unique=true, nullable=false, updatable=true)
	private JBCreditcardsEntity creditcard;
	
	@Column(name = "customer_id")
	private Integer customerid;
	
	public Integer getUserId(){
		return this.userId;
	}
	
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	
	public String getCustEmail(){
		return this.custEmail;
	}
	
	public void setCustEmail(String custEmail){
		this.custEmail=custEmail;
	}
	
	public String getCustName(){
		return this.custName;
	}
	
	public void setCustName(String custName){
		this.custName=custName;
	}
	
	public String getOrderNumber(){
		return this.orderNumber;
	}
	
	public void setOrderNumber(String orderNumber){
		this.orderNumber=orderNumber;
	}
	
	public Integer getPaymentProvider(){
		return this.paymentProvider;
	}
	
	public void setPaymentProvider(Integer paymentProvider){
		this.paymentProvider=paymentProvider;
	}
	
	public Integer getItemType(){
		return this.itemType;
	}
	
	public void setItemType(Integer itemType){
		this.itemType=itemType;
	}
	
	public Integer getRebillAfter(){
		return this.rebillAfter;
	}
	
	public void setRebillAfter(Integer rebillAfter){
		this.rebillAfter=rebillAfter;
	}
	
	public Double getAmount(){
		return this.amount;
	}
	
	public void setAmount(Double amount){
		this.amount=amount;
	}
	
	public Date getDate(){
		return this.date;
	}
	
	public void setDate(Date date){
		this.date=date;
	}
	
	public void setCreditcard(JBCreditcardsEntity creditcard) {
		this.creditcard=creditcard;
	}
	
	public JBCreditcardsEntity getCreditcard() {
		return this.creditcard;
	}
	
	public void setCustomerid(Integer customerid){
		this.customerid=customerid;
	}
	public Integer getCustomerid(){
		return customerid;
	}
}
