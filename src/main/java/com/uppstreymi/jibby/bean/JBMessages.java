package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_messages")
@Cacheable(false)
public class JBMessages extends AbstractEntity{
	@Column(name = "type")
	private Integer type;
	
	@Column(name = "recipient")
	private Integer recipient;
	
	@Column(name = "entity")
	private Integer entity;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "isRead")
	private Integer isRead;
	
	@Column(name = "isEmailed")
	private Integer isEmailed;
	
	public void setType(Integer type){
		this.type=type;
	}
	public Integer getType(){
		return this.type;
	}
	
	public void setRecipient(Integer recipient){
		this.recipient=recipient;
	}
	public Integer getRecipient(){
		return this.recipient;
	}
	
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	
	public void setText(String text){
		this.text=text;
	}
	public String getText(){
		return this.text;
	}
	
	public void setDate(Date date){
		this.date=date;
	}
	public Date getDate(){
		return this.date;
	}
	
	public void setIsRead(Integer isRead){
		this.isRead=isRead;
	}
	public Integer getIsRead(){
		return this.isRead;
	}
	
	public void setIsEmailed(Integer isEmailed){
		this.isEmailed=isEmailed;
	}
	public Integer getIsEmailed(){
		return this.isEmailed;
	}
	
}
