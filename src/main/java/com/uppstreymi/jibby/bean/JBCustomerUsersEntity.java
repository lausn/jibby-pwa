package com.uppstreymi.jibby.bean;


import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "jb_customer_user")
@Cacheable(false)
public class JBCustomerUsersEntity extends AbstractEntity{
	
	@Column(name="customer_id")
	private Integer customerid;
	
	@Column(name="user_id")
	private Integer userid;
	
	
	public void setCustomerid(Integer customerid){
		this.customerid=customerid;
	}
	public Integer getCustomerid(){
		return customerid;
	}
	
	public void setUserid(Integer userid){
		this.userid=userid;
	}
	public Integer getUserid(){
		return userid;
	}
	
}
