package com.uppstreymi.jibby.bean;



import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "jb_licences")
@Cacheable(false)
public class JBLicencesEntity extends AbstractEntity {

	@Column(name = "licenceCode")
	private String licenceCode;
	
	
	@OneToOne(optional=false)
	@JoinColumn(name="buyerUserId", unique=true, nullable=false, updatable=true)
	private JBUserEntity buyer;
	
	@OneToOne(optional=false)
	@JoinColumn(name="userUserId", unique=true, nullable=false, updatable=true)
	private JBUserEntity user;
	
	@Column(name = "orderNumber")
	private String orderNumber;
	
	@Column(name = "validTillDate")
	private Date validTillDate;
	
	@Column(name = "isActive")
	private Integer isActive;
	
	@Column(name = "customerId")
	private Integer customerId;

	
	public String getLicencesCode(){
		return this.licenceCode;
	}
	
	public void setLicencesCode(String licenceCode){
		this.licenceCode=licenceCode;
	}

	public JBUserEntity getBuyer(){
		return this.buyer;
	}
	
	public void setBuyer(JBUserEntity buyer){
		this.buyer=buyer;
	}
	
	public JBUserEntity getUser(){
		return this.user;
	}
	
	public void setUser(JBUserEntity user){
		this.user=user;
	}
	
	public String getOrderNumber(){
		return this.orderNumber;
	}
	
	public void setOrderNumber(String orderNumber){
		this.orderNumber=orderNumber;
	}
	
	public Date getValidTillDate(){
		return this.validTillDate;
	}
	
	public void setValidTillDate(Date validTillDate){
		this.validTillDate=validTillDate;
	}
	
	public Integer getIsActive(){
		return this.isActive;
	}
	
	public void setIsActive(Integer isActive){
		this.isActive=isActive;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
}
