package com.uppstreymi.jibby.bean;

public class ChatAndHistoryBean {

	private Integer id;
	private Integer senderId;
	private Integer entityId;
	private String text;
	private String date;
	private String mimetype;
	private Integer types;
	private Integer assignee;
	private String username;
	private String assigneeName;
	private Boolean isHistory;
	private Boolean isChat;
	private Boolean isAttachment;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setSender(Integer senderId) {
		this.senderId = senderId;
	}

	public Integer getSender() {
		return this.senderId;
	}

	public void setEntity(Integer entityId) {
		this.entityId = entityId;
	}

	public Integer getEntity() {
		return this.entityId;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public String getMimetype() {
		return this.mimetype;
	}

	public void setTypes(Integer types) {
		this.types = types;
	}

	public Integer getTypes() {
		return this.types;
	}

	public void setAssignee(Integer assignee) {
		this.assignee = assignee;
	}

	public Integer getAssignee() {
		return this.assignee;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getAssigneeName() {
		return this.assigneeName;
	}

	public void setIsChat(boolean isChat) {
		this.isChat = isChat;
	}

	public boolean getIsChat() {
		return this.isChat;
	}

	public void setIsHistory(Boolean isHistory) {
		this.isHistory = isHistory;
	}

	public Boolean getIsHistory() {
		return this.isHistory;
	}

	public void setIsAttachment(Boolean isAttachment) {
		this.isAttachment = isAttachment;
	}

	public Boolean getIsAttachment() {
		return this.isAttachment;
	}

}
