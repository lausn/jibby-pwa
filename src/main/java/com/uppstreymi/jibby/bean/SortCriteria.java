package com.uppstreymi.jibby.bean;

public class SortCriteria {

	Integer id;
	String caption;
	String criteria;
	boolean sort;
	
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setCaption(String caption){
		this.caption=caption;
	}
	public String getCaption(){
		return caption;
	}
	public void setCriteria(String criteria){
		this.criteria=criteria;
	}
	public String getCriteria(){
		return criteria;
	}
	public void setAsc(boolean sort){
		this.sort=sort;
	}
	public boolean isAsc(){
		return sort;
	}
}
