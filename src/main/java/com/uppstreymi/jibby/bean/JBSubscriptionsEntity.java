package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "jb_subscriptions")
@Cacheable(false)
public class JBSubscriptionsEntity extends AbstractEntity {
	
	@OneToOne(optional=false)
	@JoinColumn(name="userId", unique=true, nullable=false, updatable=true)
	private JBUserEntity userId;
	
	@Column(name = "orderNumber")
	private String orderNumber;
	
	@Column(name = "validTillDate")
	private Date validTillDate;
	
	@Column(name = "isActive")
	private Integer isActive;
	
	@Column(name = "customerId")
	private Integer customerId;
	
	public JBUserEntity getUser(){
		return this.userId;
	}
	
	public void setUser(JBUserEntity userId){
		this.userId=userId;
	}
	
	public String getOrderNumber(){
		return this.orderNumber;
	}
	
	public void setOrderNumber(String orderNumber){
		this.orderNumber=orderNumber;
	}
	
	public Date getValidTillDate(){
		return this.validTillDate;
	}
	
	public void setValidTillDate(Date validTillDate){
		this.validTillDate=validTillDate;
	}
	
	public Integer getIsActive(){
		return this.isActive;
	}
	
	public void setIsActive(Integer isActive){
		this.isActive=isActive;
	}
	
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
}
