package com.uppstreymi.jibby.bean;

import java.math.BigInteger;

public class JBEmailAttachmentBean {
	
	String type,name,data,id,messageid;
	private String contentId;
	BigInteger size;
	
	public JBEmailAttachmentBean(){}
	
	public void setContentId(String contentId){
		this.contentId=contentId;
	}
	public String getContentId(){
		return this.contentId;
	}
	public void setId(String id){
		this.id=id;
	}
	public String getId(){
		return this.id;
	}
	
	public void setmessageId(String messageid){
		this.messageid=messageid;
	}
	public String getmessageId(){
		return this.messageid;
	}
	
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	
	public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return this.type;
	}
	
	public void setData(String data){
		this.data=data;
	}
	public String getData(){
		return this.data;
	}
	
	public void setSize(BigInteger size){
		this.size=size;
	}
	public BigInteger getSize(){
		return this.size;
	}
	
	
	
}
