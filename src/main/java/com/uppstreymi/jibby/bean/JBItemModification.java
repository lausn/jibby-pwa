package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_itemmodification")
@Cacheable(false)
public class JBItemModification extends AbstractEntity {
	@Column(name = "entityId")
	private Integer entityId;
	
	@Column(name = "userId1")
	private Integer userId1;
	
	@Column(name = "userId2")
	private Integer userId2;
	
	@Column(name = "modificationtype")
	private Integer modificationtype;
	
	@Column(name = "modificationDate")
	private Date modificationDate;
	
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return this.entityId;
	}
	
	public void setUserId1(Integer userId1){
		this.userId1=userId1;
	}
	public Integer getUserId1(){
		return this.userId1;
	}
	
	public void setUserId2(Integer userId2){
		this.userId2=userId2;
	}
	public Integer getUserId2(){
		return this.userId2;
	}
	
	public void setModificationType(Integer modificationtype){
		this.modificationtype=modificationtype;
	}
	public Integer getModificationType(){
		return this.modificationtype;
	}
	
	public void setModificationDate(Date modificationDate){
		this.modificationDate=modificationDate;
	}
	public Date getModificationDate(){
		return this.modificationDate;
	}
}
