package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_invitations")
@Cacheable(false)
public class JBInvitation extends AbstractEntity{
	
	@Column(name = "sender")
	private Integer sender;
	
	@Column(name = "recipient")
	private Integer recipient;
	
	@Column(name = "invitedatetime")
	private Date invitedatetime;
	
	@Column(name = "entity")
	private Integer entity;
	
	public void setSender(Integer sender){
		this.sender=sender;
	}
	public Integer getSender(){
		return this.sender;
	}
	
	public void setRecipient(Integer recipient){
		this.recipient=recipient;
	}
	public Integer getRecipient(){
		return this.recipient;
	}
	
	public void setInvitedatetime(Date invitedatetime){
		this.invitedatetime=invitedatetime;
	}
	public Date getInvitedatetime(){
		return this.invitedatetime;
	}
	
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	
	
}
