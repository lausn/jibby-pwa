package com.uppstreymi.jibby.bean;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jb_emailsettings")
@Cacheable(false)
public class JBEmailSetting extends AbstractEntity{
	@Column(name = "user")
	private Integer user;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "provider")
	private Integer provider;
	
	@Column(name = "emailsToShow")
	private Integer emailsToShow;
	
	@Column(name = "data")
	private String data;
	
	@Column(name = "isActiveEmail")
	private Integer isActiveEmail;
	
	@Column(name = "isActiveGoogleDrive")
	private Integer isActiveGoogleDrive;
	
	public void setUser(Integer user){
		this.user=user;
	}
	public Integer getUser(){
		return this.user;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	public String getEmail(){
		return this.email;
	}
	public void setProvider(Integer provider){
		this.provider=provider;
	}
	public Integer getProvider(){
		return this.provider;
	}
	public void setEmailsToShow(Integer emailsToShow){
		this.emailsToShow=emailsToShow;
	}
	public Integer getEmailsToShow(){
		return this.emailsToShow;
	}
	public void setData(String data){
		this.data=data;
	}
	public String getData(){
		return this.data;
	}
	
	public void setIsActiveEmail(Integer isActiveEmail) {
		this.isActiveEmail=isActiveEmail;
	}
	public Integer getIsActiveEmail() {
		return this.isActiveEmail;
	}
	
	public void setIsActiveGoogleDrive(Integer isActiveGoogleDrive) {
		this.isActiveGoogleDrive=isActiveGoogleDrive;
	}
	public Integer getIsActiveGoogleDrive() {
		return this.isActiveGoogleDrive;
	}
}
