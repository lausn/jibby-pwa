package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "jb_attachments")
@Cacheable(false)
public class JBAttachmentEntity extends AbstractEntity{
	
	@Column(name = "entity")
	private Integer entity;
	
	@Column(name = "uploaderUser")
	private Integer uploaderUser;
	
	@Column(name = "uploadDate")
	private Date uploadDate;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "size")
	private Integer size;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "sortOrder")
	private Integer sortOrder;
	
	
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	public void setUploaderUser(Integer uploaderUser){
		this.uploaderUser=uploaderUser;
	}
	public Integer getUploaderUser(){
		return this.uploaderUser;
	}
	
	public void setuploadDate(Date uploadDate){
		this.uploadDate=uploadDate;
	}
	public Date getUploadDate(){
		return this.uploadDate;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return this.type;
	}
	public void setSize(Integer size){
		this.size=size;
	}
	public Integer getSize(){
		return this.size;
	}
	public void setDescription(String description){
		this.description=description;
	}
	public String getDescription(){
		return this.description;
	}
	public void setSortOrder(Integer sortOrder){
		this.sortOrder=sortOrder;
	}
	public Integer getSortOrder(){
		return this.sortOrder;
	}
	
	
}
