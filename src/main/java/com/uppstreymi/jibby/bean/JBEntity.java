package com.uppstreymi.jibby.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.uppstreymi.jibby.utils.JibbyConfig;

@Entity
@Table(name = "jb_entities")
@Cacheable(false)
public class JBEntity extends AbstractEntity {

	@Column(name = "hierarchy")
	private String hierarchy;

	@Column(name = "entityType")
	private Integer entityType;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "status")
	private Integer status;

	@Column(name = "progress")
	private Integer progress;

	@Column(name = "_DEL_isTimeBased")
	private Integer delIsTimeBased;

	@Column(name = "estimatedStartDate")
	private Date estimatedStartDate;

	@Column(name = "estimatedFinishDate")
	private Date estimatedFinishDate;

	@Column(name = "recurrenceUnit")
	private Integer recurrenceUnit;

	@Column(name = "recurrenceScale")
	private Integer recurrenceScale;

	@Column(name = "recurrenceDuration")
	private Integer recurrenceDuration;

	@Column(name = "recurringUntilDate")
	private Date recurringUnitlDate;

	@Column(name = "worksheetType")
	private Integer worksheetType;

	@Column(name = "unitName")
	private String unitName;

	@Column(name = "unitQuantity")
	private Double unitQuantity;

	@Column(name = "costName")
	private String costName;

	@Column(name = "costPerUnit")
	private Integer costPerUnit;

	@Column(name = "keywords")
	private String keywords;

	@Column(name = "priority")
	private Integer priority;

	@Column(name = "creator")
	private Integer creator;

	@Column(name = "creationDate")
	private Date creationDate;

	@Column(name = "lastModificationDate")
	private Date lastModificationDate;

	@Column(name = "color")
	private String color;

	@Column(name = "isPublic")
	private Integer isPublic;

	@Column(name = "onHold")
	private Integer onHold;

	@Column(name = "chatEnabled")
	private Integer chatEnabled;

	@Column(name = "parentid")
	private Integer parentId;

	@Column(name = "parentProjectId")
	private Integer parentProjectId;

	@Column(name = "kanbanColor")
	private String kanbanColor;

	@Column(name = "isDelete")
	private Integer isDelete;

	@Column(name = "deletedate")
	private Date deleteDate;

	@Transient
	private Integer indentValue;

	@Transient
	private List<JBUserEntity> ownerlist = new ArrayList<JBUserEntity>();
	@Transient
	private List<JBUserEntity> assigneelist = new ArrayList<JBUserEntity>();
	@Transient
	private List<JBUserEntity> followerlist = new ArrayList<JBUserEntity>();
	@Transient
	private List<JBUserEntity> connectionlist = new ArrayList<JBUserEntity>();

	@Transient
	private List<JBUserEntity> directOwnerlist = new ArrayList<JBUserEntity>();
	@Transient
	private List<JBUserEntity> directAssigneelist = new ArrayList<JBUserEntity>();
	@Transient
	private List<JBUserEntity> directFollowerlist = new ArrayList<JBUserEntity>();
	@Transient
	private List<JBUserEntity> directConnectionlist = new ArrayList<JBUserEntity>();

	@Transient
	private List<Integer> parentArray = new ArrayList<Integer>();

	@PrePersist
	void preInserts() {
		this.kanbanColor = (this.kanbanColor == null) ? JibbyConfig.defaultKanbanColor : this.kanbanColor;
	}

	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="parentid", unique=true, nullable=false, updatable=true)
	 * private JBEntity parentEntity;
	 */

	/*
	 * @OneToMany(mappedBy="parentEntity", cascade=CascadeType.ALL,
	 * fetch=FetchType.LAZY ) private List<JBEntity> childEntities;
	 */

	public void setIndentValue(Integer indentValue) {
		this.indentValue = indentValue;
	}

	@Transient
	public Integer getIndentValue() {
		return indentValue;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentProjectId(Integer parentProjectId) {
		this.parentProjectId = parentProjectId;
	}

	public Integer getParentProjectId() {
		return this.parentProjectId;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}

	public Integer getEntityType() {
		return this.entityType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setCostName(String costName) {
		this.costName = costName;
	}

	public String getCostName() {
		return costName;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setUnitQuantity(Double unitQuantity) {
		this.unitQuantity = unitQuantity;
	}

	public Double getUnitQuantity() {
		return unitQuantity;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setCostPerUnit(Integer costPerUnit) {
		this.costPerUnit = costPerUnit;
	}

	public Integer getCostPerUnit() {
		return costPerUnit;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setEstimatedStartDate(Date estimatedStartDate) {
		this.estimatedStartDate = estimatedStartDate;
	}

	public Date getEstimatedStartDate() {
		return this.estimatedStartDate;
	}

	public void setEstimatedFinishDate(Date estimatedFinishDate) {
		this.estimatedFinishDate = estimatedFinishDate;
	}

	public Date getEstimatedFinishDate() {
		return this.estimatedFinishDate;
	}

	public void setChatEnabled(Integer chatEnabled) {
		this.chatEnabled = chatEnabled;
	}

	public Integer getChatEnabled() {
		return this.chatEnabled;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public Integer getIsPublic() {
		return this.isPublic;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setWorksheetType(Integer worksheetType) {
		this.worksheetType = worksheetType;
	}

	public Integer getWorksheetType() {
		return worksheetType;
	}

	public void setLastModificationDate(Date lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	public Date getLastModificationDate() {
		return lastModificationDate;
	}

	public void setRecurrenceUnit(Integer recurrenceUnit) {
		this.recurrenceUnit = recurrenceUnit;
	}

	public Integer getRecurrenceUnit() {
		return recurrenceUnit;
	}

	public void setRecurrenceScale(Integer recurrenceScale) {
		this.recurrenceScale = recurrenceScale;
	}

	public Integer getRecurrenceScale() {
		return recurrenceScale;
	}

	public void setRecurrenceDuration(Integer recurrenceDuration) {
		this.recurrenceDuration = recurrenceDuration;
	}

	public Integer getRecurrenceDuration() {
		return recurrenceDuration;
	}

	public void setRecurrenceUntilDate(Date recurringUnitlDate) {
		this.recurringUnitlDate = recurringUnitlDate;
	}

	public Date getRecurrenceUntilDate() {
		return recurringUnitlDate;
	}

	public void setDelIsTimeBased(Integer delIsTimeBased) {
		this.delIsTimeBased = delIsTimeBased;
	}

	public Integer getDelIsTimeBased() {
		return delIsTimeBased;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setKanbanColor(String kanbanColor) {
		this.kanbanColor = kanbanColor;
	}

	public String getKanbanColor() {
		return kanbanColor;
	}

	public void copyEntity(JBEntity entity) {
		this.name = entity.getName();
		this.description = entity.getDescription() == null ? "" : entity.getDescription();
		this.estimatedStartDate = entity.getEstimatedStartDate();
		this.estimatedFinishDate = entity.getEstimatedFinishDate();
		this.entityType = entity.getEntityType();
		this.unitQuantity = entity.getUnitQuantity();
		this.unitName = entity.getUnitName();
		this.color = entity.getColor();
		this.costName = entity.getCostName();
		this.costPerUnit = entity.getCostPerUnit();
		this.creationDate = entity.getCreationDate();
		this.creator = entity.getCreator();
		this.delIsTimeBased = entity.getDelIsTimeBased();
		this.hierarchy = entity.getHierarchy();
		this.chatEnabled = entity.getChatEnabled();
		this.isPublic = entity.getIsPublic();
		this.keywords = entity.getKeywords();
		this.lastModificationDate = entity.getLastModificationDate();
		this.parentId = entity.getParentId();
		this.parentProjectId = entity.getParentProjectId();
		this.priority = entity.getPriority();
		this.progress = entity.getProgress();
		this.recurrenceDuration = entity.getRecurrenceDuration();
		this.recurrenceScale = entity.getRecurrenceScale();
		this.recurrenceUnit = entity.getRecurrenceUnit();
		this.recurringUnitlDate = entity.getRecurrenceUntilDate();
		this.status = entity.getStatus();
		this.worksheetType = entity.getWorksheetType();

		this.ownerlist = entity.getOwnerlist();
		this.assigneelist = entity.getAssigneelist();
		this.followerlist = entity.getFollowerlist();
		this.connectionlist = entity.getConnectionlist();
		this.directOwnerlist = entity.getDirectOwnerlist();
		this.directAssigneelist = entity.getDirectAssigneelist();
		this.directFollowerlist = entity.getDirectFollowerlist();
		this.directConnectionlist = entity.getDirectConnectionlist();

		this.parentArray = entity.getParentArray();
		this.indentValue = entity.getIndentValue();
	}

	/*
	 * public void setChildEntities(List<JBEntity> childEntities){
	 * this.childEntities=childEntities; }
	 * 
	 * public List<JBEntity> getChildEntities(){ return this.childEntities; }
	 */

	public void setOwnerlist(List<JBUserEntity> ownerlist) {
		this.ownerlist = ownerlist;
	}

	public List<JBUserEntity> getOwnerlist() {
		return ownerlist;
	}

	public void setAssigneelist(List<JBUserEntity> assigneelist) {
		this.assigneelist = assigneelist;
	}

	public List<JBUserEntity> getAssigneelist() {
		return assigneelist;
	}

	public void setFollowerlist(List<JBUserEntity> followerlist) {
		this.followerlist = followerlist;
	}

	public List<JBUserEntity> getFollowerlist() {
		return followerlist;
	}

	public void setConnectionlist(List<JBUserEntity> connectionlist) {
		this.connectionlist = connectionlist;
	}

	public List<JBUserEntity> getConnectionlist() {
		return connectionlist;
	}

	public void setDirectOwnerlist(List<JBUserEntity> directOwnerlist) {
		this.directOwnerlist = directOwnerlist;
	}

	public List<JBUserEntity> getDirectOwnerlist() {
		return directOwnerlist;
	}

	public void setDirectAssigneelist(List<JBUserEntity> directAssigneelist) {
		this.directAssigneelist = directAssigneelist;
	}

	public List<JBUserEntity> getDirectAssigneelist() {
		return directAssigneelist;
	}

	public void setDirectFollowerlist(List<JBUserEntity> directFollowerlist) {
		this.directFollowerlist = directFollowerlist;
	}

	public List<JBUserEntity> getDirectFollowerlist() {
		return directFollowerlist;
	}

	public void setDirectConnectionlist(List<JBUserEntity> directConnectionlist) {
		this.directConnectionlist = directConnectionlist;
	}

	public List<JBUserEntity> getDirectConnectionlist() {
		return directConnectionlist;
	}

	public void setParentArray(List<Integer> parentArray) {
		this.parentArray = parentArray;
	}

	public List<Integer> getParentArray() {
		return parentArray;
	}

	public Integer getOnHold() {
		return onHold;
	}

	public void setOnHold(Integer onHold) {
		this.onHold = onHold;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}
}
