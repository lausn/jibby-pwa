package com.uppstreymi.jibby.bean;

import java.util.Date;

public class NotificationBean {
	
	
	private Integer id;
	private String created;
	private Integer itemid;
	private Integer itemtype;
	private Integer userid;
	private Integer messagetype;
	private String message;
	private Integer toplabelid;
	private String toplabelname;
	private Integer toplabeltype;
	private Integer entity;
	
	
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return this.id;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getCreated() {
		return this.created;
	}
	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}
	public Integer getItemid() {
		return this.itemid;
	}
	public void setItemtype(Integer itemtype) {
		this.itemtype = itemtype;
	}
	public Integer getItemtype() {
		return this.itemtype;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public Integer getUserid() {
		return this.userid;
	}
	public void setMessagetype(Integer messagetype) {
		this.messagetype = messagetype;
	}
	public Integer getMessagetype() {
		return this.messagetype;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return this.message;
	}
	public void setToplabelid(Integer toplabelid) {
		this.toplabelid = toplabelid;
	}
	public Integer getToplabelid() {
		return this.toplabelid;
	}
	public void setToplabelname(String toplabelname) {
		this.toplabelname = toplabelname;
	}
	public String getToplabelname() {
		return this.toplabelname;
	}
	public void setToplabeltype(Integer toplabeltype) {
		this.toplabeltype = toplabeltype;
	}
	public Integer getToplabeltype() {
		return this.toplabeltype;
	}
	public void setEntity(Integer entity) {
		this.entity = entity;
	}
	public Integer getEntity() {
		return this.entity;
	}
	
}
