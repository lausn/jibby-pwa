package com.uppstreymi.jibby.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "jb_customer")
@Cacheable(false)
public class JBCustomerEntity extends AbstractEntity{
	
	@Column(name = "name")
	private String name;
	
	@Column(name="subscriptionguid")
	private String subscriptionguid;
	
	@Column(name = "createdate")
	private Date createdate;
	
	@Transient
	private String subscriptionServiceGuid;
	
	
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	
	public void setSubscriptionguid(String subscriptionguid){
		this.subscriptionguid=subscriptionguid;
	}
	public String getSubscriptionguid(){
		return this.subscriptionguid;
	}
	
	public void setCreatedate(Date createdate){
		this.createdate=createdate;
	}
	public Date getCreatedate(){
		return createdate;
	}
	
	public String getSubscriptionServiceGuid() {
		return this.subscriptionServiceGuid;
	}
	
	public void setSubscriptionServiceGuid(String subscriptionServiceGuid) {
		this.subscriptionServiceGuid = subscriptionServiceGuid;
	}
	
}
