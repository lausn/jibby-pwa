package com.uppstreymi.jibby.bean;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "jb_users")
@Cacheable(false)
public class JBUserEntity extends AbstractEntity {
	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(name = "company")
	private String company;

	@Column(name = "username")
	private String username;

	@Column(name = "homeNumber")
	private String homeNumber;

	@Column(name = "mobileNumber")
	private String mobileNumber;

	@Column(name = "emailAboutEntityChanges")
	private Integer emailAboutEntityChanges;

	@Column(name = "reminderEntityUnfinished")
	private Integer reminderEntityUnfinished;

	@Column(name = "reminderEntityStartFinish")
	private Integer reminderEntityStartFinish;

	@Column(name = "newsletterActive")
	private Integer newsletterActive;

	@Column(name = "type")
	private Integer type;

	@Column(name = "dateFormat")
	private String dateFormat;

	@Column(name = "timeZone")
	private String timeZone;

	@Column(name = "firstDayOfWeek")
	private Integer firstDayOfWeek;

	@Column(name = "decimalSeparator")
	private String decimalSeparator;

	@Column(name = "thousandSeparator")
	private String thousandSeparator;

	@Column(name = "unitNames")
	private String unitNames;

	@Column(name = "costNames")
	private String costNames;

	@Column(name = "activeView")
	private String activeView;

	@Column(name = "inboxView")
	private Integer inboxView;

	@Column(name = "outboxView")
	private Integer outboxView;

	@Column(name = "registrationDate")
	private Date registrationDate;

	@Column(name = "firstLoginDate")
	private Date firstLoginDate;

	@Column(name = "lastLoginDate")
	private Date lastLoginDate;

	@Column(name = "savedAtDate")
	private Date savedAtDate;

	@Column(name = "showAdvancedSettings")
	private Integer showAdvancedSettings;

	@Column(name = "activationCode")
	private String activationCode;

	@Column(name = "entityGeneratorValues")
	private String entityGeneratorValues;

	@Column(name = "helper")
	private String helper;

	@Column(name = "activeUI")
	private String activeUI;

	@Column(name = "tempguid")
	private String tempguid;

	@Column(name = "expiredDate")
	private Date expiredDate;

	@Column(name = "isInvitedUser")
	private Integer isInvitedUser;
	
	@Column(name ="guid")
	private String guid;
	
	@Transient
	private String fullName;
	
	@Transient
	private boolean isExitingUser;
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@PrePersist
	void preInsert() {
		this.isInvitedUser=(this.isInvitedUser==null)?0:this.isInvitedUser;
	}

	public void setFirstName(String firstName){
		this.firstName=firstName;
	}
	public String getFirstName(){
		return firstName;
	}
	public void setLastName(String lastName){
		this.lastName=lastName;
	}
	public String getLastName(){
		return lastName;
	}

	public void setEmail(String email){
		this.email=email;
	}
	public String getEmail(){
		return this.email;
	}
	public void setPassword(String password){
		this.password=password;
	}
	public String getPassword(){
		return password;
	}

	public void setCompany(String company){
		this.company=company;
	}

	public String getCompany(){
		return this.company;
	}

	public void setUsername(String username){
		this.username=username;
	}
	public String getUsername(){
		return this.username;
	}

	public void setHomeNumber(String homeNumber){
		this.homeNumber=homeNumber;
	}
	public String getHomeNumber(){
		return homeNumber;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber=mobileNumber;
	}
	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setEmailAboutEntityChanges(Integer emailAboutEntityChanges){
		this.emailAboutEntityChanges=emailAboutEntityChanges;
	}
	public Integer getEmailAboutEntityChanges(){
		return emailAboutEntityChanges;
	}

	public void setReminderEntityUnfinished(Integer reminderEntityUnfinished){
		this.reminderEntityUnfinished=reminderEntityUnfinished;
	}
	public Integer getReminderEntityUnfinished(){
		return reminderEntityUnfinished;
	}

	public void setReminderEntityStartFinish(Integer reminderEntityStartFinish){
		this.reminderEntityStartFinish=reminderEntityStartFinish;
	}
	public Integer getReminderEntityStartFinish(){
		return reminderEntityStartFinish;
	}

	public void setNewsletterActive(Integer newsletterActive){
		this.newsletterActive=newsletterActive;
	}
	public Integer getNewsletterActive(){
		return this.newsletterActive;
	}

	public void setType(Integer type){
		this.type=type;
	}
	public Integer getType(){
		return this.type;
	}

	public void setDateFormat(String dateFormat){
		this.dateFormat=dateFormat;
	}
	public String getDateFormat(){
		return dateFormat;
	}

	public void setTimeZone(String timeZone){
		this.timeZone=timeZone;
	}
	public String getTimeZone(){
		return timeZone;
	}

	public void setFirstDayOfWeek(Integer firstDayOfWeek){
		this.firstDayOfWeek=firstDayOfWeek;
	}
	public Integer getFirstDayOfWeek(){
		return firstDayOfWeek;
	}

	public void setDecimalSeparator(String decimalSeparator){
		this.decimalSeparator=decimalSeparator;
	}
	public String getDecimalSeparator(){
		return decimalSeparator;
	}

	public void setThousandSeparator(String thousandSeparator){
		this.thousandSeparator=thousandSeparator;
	}
	public String getThousandSeparator(){
		return thousandSeparator;
	}

	public void setUnitNames(String unitNames){
		this.unitNames=unitNames;
	}
	public String getUnitNames(){
		return this.unitNames;
	}

	public void setCostNames(String costNames){
		this.costNames=costNames;
	}
	public String getCostNames(){
		return this.costNames;
	}
	public void setActiveView(String activeView){
		this.activeView=activeView;
	}
	public String getActiveView(){
		return activeView;
	}

	public void setInboxView(Integer inboxView){
		this.inboxView=inboxView;
	}

	public Integer getInboxView(){
		return this.inboxView;
	}

	public void setOutboxView(Integer outboxView){
		this.outboxView=outboxView;
	}
	public Integer getOutboxView(){
		return this.outboxView;
	}

	public void setRegistrationDate(Date registrationDate){
		this.registrationDate=registrationDate;
	}
	public Date getRegistrationDate(){
		return registrationDate;
	}

	public void setFirstLoginDate(Date firstLoginDate){
		this.firstLoginDate=firstLoginDate;
	}
	public Date getFirstLoginDate(){
		return firstLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate){
		this.lastLoginDate=lastLoginDate;
	}
	public Date getLastLoginDate(){
		return lastLoginDate;
	}

	public void setSavedAtDate(Date savedAtDate){
		this.savedAtDate=savedAtDate;
	}
	public Date getSavedAtDate(){
		return savedAtDate;
	}

	public void setShowAdvancedSettings(Integer showAdvancedSettings){
		this.showAdvancedSettings=showAdvancedSettings;
	}
	public Integer getShowAdvancedSettings(){
		return showAdvancedSettings;
	}

	public void setActivationCode(String activationCode){
		this.activationCode=activationCode;
	}
	public String getActivationCode(){
		return this.activationCode;
	}

	public void setEntityGeneratorValues(String entityGeneratorValues){
		this.entityGeneratorValues=entityGeneratorValues;
	}
	public String getEntityGeneratorValues(){
		return this.entityGeneratorValues;
	}
	public void setHelper(String helper){
		this.helper=helper;
	}
	public String getHelper(){
		return this.helper;
	}
	public void setActiveUI(String activeUI){
		this.activeUI=activeUI;
	}
	public String getActiveUI(){
		return this.activeUI;
	}

	public void setTempguid(String tempguid){
		this.tempguid=tempguid;
	}
	public String getTempguid(){
		return this.tempguid;
	}

	public void setExpiredDate(Date expiredDate){
		this.expiredDate=expiredDate;
	}
	public Date getExpiredDate(){
		return this.expiredDate;
	}

	public void setIsInvitedUser(Integer isInvitedUser){
		this.isInvitedUser=isInvitedUser;
	}
	public Integer getIsInvitedUser(){
		return isInvitedUser;
	}
	
	public void setGuid(String guid) {
		this.guid=guid;
	}
	public String getGuid() {
		return this.guid;
	}
	
	public boolean isExitingUser() {
		return isExitingUser;
	}

	public void setExitingUser(boolean isExitingUser) {
		this.isExitingUser = isExitingUser;
	}
}
