package com.uppstreymi.jibby.bean;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "jb_newuserdefaultdata")
@Cacheable(false)
public class JBUserDefaultEntity extends AbstractEntity{
	
	@Column(name = "uniqueID")
	private Integer uniqueID;
	
	@Column(name = "taskName")
	private String taskName;
	
	@Column(name = "duration")
	private String duration;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "outlineLevel")
	private Integer outlineLevel;
	
	@Column(name = "percentComplete")
	private Integer percentComplete;
	
	@Column(name = "priority")
	private Integer priority;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "jibbyType")
	private Integer jibbyType;
	

	@ManyToOne
	@JoinColumn(name="parentId", unique=true, nullable=false, updatable=true)
	private JBUserDefaultEntity parentEntity;

	@OneToMany(mappedBy="parentEntity", cascade=CascadeType.ALL, fetch=FetchType.EAGER )
    @OrderBy("id ASC")
	private List<JBUserDefaultEntity> childList;
	
	
	public void setUniqueID(Integer uniqueID){
		this.uniqueID=uniqueID;
	}
	public Integer getUniqueID(){
		return uniqueID;
	}
	
	public void setTaskName(String taskName){
		this.taskName=taskName;
	}
	public String getTaskName(){
		return taskName;
	}
	
	public void setDuration(String duration){
		this.duration=duration;
	}
	public String getDuration(){
		return duration;
	}
	public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return type;
	}
	public void setOutlineLevel(Integer outlineLevel){
		this.outlineLevel=outlineLevel;
	}
	public Integer getOutlineLevel(){
		return outlineLevel;
	}
	public void setPercentComplete(Integer percentComplete){
		this.percentComplete=percentComplete;
	}
	public Integer getPercentComplete(){
		return percentComplete;
	}
	
	public void setPriority(Integer priority){
		this.priority=priority;
	}
	public Integer getPriority(){
		return priority;
	}
	public void setNote(String note){
		this.note=note;
	}
	public String getNote(){
		return note;
	}
	public void setJibbyType(Integer jibbyType){
		this.jibbyType=jibbyType;
	}
	public Integer getJibbyType(){
		return jibbyType;
	}
	/*public void setParentId(Integer parentId){
		this.parentId=parentId;
	}
	public Integer getParentId(){
		return parentId;
	}*/
	public void setChildList(List<JBUserDefaultEntity> childList) {
		this.childList = childList;
	}
	public List<JBUserDefaultEntity> getChildList() {
		return childList;
	}
	
	
	
}
