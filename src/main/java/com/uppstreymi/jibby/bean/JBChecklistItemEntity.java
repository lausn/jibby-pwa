package com.uppstreymi.jibby.bean;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "jb_checklistitems")
@Cacheable(false)
public class JBChecklistItemEntity extends AbstractEntity{
	
	@Column(name = "entity")
	private Integer entity;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "isChecked")
	private Integer isChecked;
	
	@Column(name = "sortOrder")
	private Integer sortOrder;
	
	
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	public void setIsChecked(Integer isChecked){
		this.isChecked=isChecked;
	}
	public Integer getIsChecked(){
		return isChecked;
	}
	public void setSortOrder(Integer sortOrder){
		this.sortOrder=sortOrder;
	}
	public Integer getSortOrder(){
		return sortOrder;
	}
}
