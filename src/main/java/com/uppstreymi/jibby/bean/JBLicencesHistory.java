package com.uppstreymi.jibby.bean;



import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jb_licenceshistory")
@Cacheable(false)
public class JBLicencesHistory extends AbstractEntity {

	@Column(name = "licenceCode")
	private String licenceCode;
	
	
	@OneToOne(optional=false)
	@JoinColumn(name="userId", unique=true, nullable=false, updatable=true)
	private JBUserEntity userId;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "customerId")
	private Integer customerId;
	
	public String getLicencesCode(){
		return this.licenceCode;
	}
	
	public void setLicencesCode(String licenceCode){
		this.licenceCode=licenceCode;
	}
	
	public JBUserEntity getUser(){
		return this.userId;
	}
	
	public void setUser(JBUserEntity userId){
		this.userId=userId;
	}
	
	public String getStatus(){
		return this.text;
	}
	
	public void setStatus(String text){
		this.text=text;
	}
	
	public Date getDate(){
		return this.date;
	}
	
	public void setDate(Date date){
		this.date=date;
	}
	
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
}
