package com.uppstreymi.jibby;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.mail.smtp.SMTPTransport;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.service.MailgunService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;


@WebServlet(urlPatterns = {"/activate","/activate/*"})

public class Activate extends HttpServlet {

	@Inject private SessionModel sessionModel;
	@Inject private DbService service;
	@Inject private AppModel appModel;

	private static final long serialVersionUID = 1L;

	private SMTPTransport trans;
	private MailgunService mailService = new MailgunService();

	Logger logger=LogManager.getLogger(Activate.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String acid=request.getParameter("jbacid");
		try{
			if(acid !=null){
				acid= Utils.decrypt(acid);
				JBUserEntity user = service.getUserByActiveCodeAndType(acid,0);
				if(user !=null ){
					if(user.getType() == Arrays.asList(JibbyConfig.userTypes).indexOf("notActivated")){
						long registrationtime= user.getRegistrationDate().getTime();
						long newTime= new Date().getTime();
						long exeedtime= JibbyConfig.activationExpiredTimeMinute*60*1000;
						if((newTime-registrationtime)>exeedtime) {
							try {
								String html= IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("htmlpage/regconfirmationExpirationError.html"), "UTF-8");
								response.setContentType("text/html");
								PrintWriter writer = response.getWriter();
								writer.println(html);
								writer.flush();
							} catch (Exception e) {
								logger.error("File reading error in activate class",e);
							}

						}else {
							user.setType(Arrays.asList(JibbyConfig.userTypes).indexOf("user"));
							service.saveUser(user);
							
							List<JBCustomerEntity> customers = service.getCustomersByUser(user.getId());
							if(customers.size()==1) {
								sessionModel.setUser(user);
								setActiveView(user);
								sessionModel.setLoggedIn(true);
								sessionModel.setCustomer(customers.get(0));
							}
							sendMailToAdmin(user);
							response.sendRedirect(appModel.getPropertiesModel().getMobileAppUrl());
						}
					}else
						response.sendRedirect(appModel.getPropertiesModel().getMobileAppUrl());
				}else{
					String html= IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("htmlpage/regconfirmationerror.html"), "UTF-8");
					response.setContentType("text/html");
					PrintWriter writer = response.getWriter();
					writer.println(html);
					writer.flush();

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void setActiveView(JBUserEntity user){
		if(user.getActiveUI()!=null){
			try{
				ObjectMapper objectMapper=new ObjectMapper();
				ActiveViewDTO activeView=objectMapper.readValue(user.getActiveUI(),ActiveViewDTO.class);
				sessionModel.setActiveViewDTO(activeView);
			}catch(Exception e){
				logger.error("Active view set error from activate page",e);
			}
		}
	}

	private void sendMailToAdmin(JBUserEntity user) {
		// TODO Auto-generated method stub
		String fromAddress = "noreply@jibby.com";
		String fromName = "Jibby - Get Organized";//Props.UITitle;
		String toName = "Jibby Team";
		String subject = "New Jibby user sign-up confirmation";
		String cc = "";
		String bcc = "";
		String text = "";
		String strHtml="";
		String cal = null;

		try {

			List<String> invitationEmailList = new ArrayList<>();
			invitationEmailList.add("admin@jibby.com");

			strHtml = IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("signupMail/signupMailConfirmationToAdmin.html"), "UTF-8");
			strHtml = strHtml.replace("#name#",toName);
			strHtml = strHtml.replace("#email#",user.getEmail());
			strHtml = strHtml.replace("#userName#",user.getFirstName()+" "+user.getLastName());
			trans = mailService.sendSMTPMail(fromAddress, fromName, invitationEmailList, toName, subject, cc, bcc, text, strHtml,cal,null);
			/*Integer status = new HttpMailgunService().sendHTTPMail(fromAddress,fromName, 
					"raasaddev@gmail.com",toName, subject, "", "", "", strHtml, "",invitationEmailList,"",null);*/

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Jibby new user sign-up mail confirmation to \"admin@jibby.com\" sending faild. \n",e.getMessage());
		}
	}


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	@Override
	public void init(ServletConfig config) throws ServletException {

	}

	@Override
	public void destroy() {

	}

}