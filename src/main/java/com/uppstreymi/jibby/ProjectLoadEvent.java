package com.uppstreymi.jibby;

import com.uppstreymi.jibby.bean.JBUserEntity;

public class ProjectLoadEvent {

	private JBUserEntity user;
	private boolean loadsetting, sorting;
	private boolean isFiltering = false;

	public ProjectLoadEvent() {

	}

	public ProjectLoadEvent(boolean loadsetting) {
		this.loadsetting = loadsetting;
	}

	public JBUserEntity getUser() {
		return this.user;
	}

	public void setLoadSettings(boolean loadsetting) {
		this.loadsetting = loadsetting;
	}

	public boolean isLoadSettings() {
		return this.loadsetting;
	}

	public void setSorting(boolean sorting) {
		this.sorting = sorting;
	}

	public boolean isSorting() {
		return this.sorting;
	}

	public boolean isFiltering() {
		return isFiltering;
	}

	public void setFiltering(boolean isFiltering) {
		this.isFiltering = isFiltering;
	}
}
