package com.uppstreymi.jibby.backend.sync.bean;

import java.util.Date;

public class JBActionlistHistory{
	public Integer id;
	public Date created;
	public Integer itemid;
	public Integer itemtype;
	public Integer userid;
	public Integer messagetype;
	public String message;
	public Integer isRead;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setCreated(Date created){
		this.created=created;
	}
	public Date getCreated(){
		return this.created;
	}
	
	public void setItemid(Integer itemid){
		this.itemid=itemid;
	}
	public Integer getItemid(){
		return this.itemid;
	}
	public void setItemtype(Integer itemtype){
		this.itemtype=itemtype;
	}
	public Integer getItemtype(){
		return this.itemtype;
	}
	public void setUserid(Integer userid){
		this.userid=userid;
	}
	public Integer getUserid(){
		return userid;
	}
	public void setMessagetype(Integer messagetype){
		this.messagetype=messagetype;
	}
	public Integer getMessagetype(){
		return messagetype;
	}
	public void setMessage(String message){
		this.message=message;
	}
	public String getMessage(){
		return message;
	}
	
	public void setIsRead(Integer isRead){
		this.isRead=isRead;
	}
	public Integer getIsRead(){
		return this.isRead;
	}
	
}
