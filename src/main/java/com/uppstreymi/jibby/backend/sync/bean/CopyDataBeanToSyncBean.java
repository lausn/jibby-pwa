package com.uppstreymi.jibby.backend.sync.bean;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.cdi.annotation.UIScoped;

@UIScoped
public class CopyDataBeanToSyncBean {

	public JBEntity getSyncEntity(com.uppstreymi.jibby.bean.JBEntity entity) {

		if(entity!=null) {
			JBEntity syncEntity = new JBEntity();
			syncEntity.id=entity.getId();
			syncEntity.name=entity.getName();
			syncEntity.description=entity.getDescription()==null?"":entity.getDescription();
			syncEntity.estimatedStartDate=entity.getEstimatedStartDate();
			syncEntity.estimatedFinishDate=entity.getEstimatedFinishDate();
			syncEntity.entityType=entity.getEntityType();
			syncEntity.unitQuantity=entity.getUnitQuantity();
			syncEntity.unitName=entity.getUnitName();
			syncEntity.color=entity.getColor();
			syncEntity.costName=entity.getCostName();
			syncEntity.costPerUnit=entity.getCostPerUnit();
			syncEntity.creationDate=entity.getCreationDate();
			syncEntity.creator=entity.getCreator();
			syncEntity.delIsTimeBased=entity.getDelIsTimeBased();
			syncEntity.hierarchy=entity.getHierarchy();
			syncEntity.chatEnabled=entity.getChatEnabled();
			syncEntity.isPublic=entity.getIsPublic();
			syncEntity.keywords=entity.getKeywords();
			syncEntity.lastModificationDate=entity.getLastModificationDate();
			syncEntity.parentId=entity.getParentId();
			syncEntity.parentProjectId=entity.getParentProjectId();
			syncEntity.priority=entity.getPriority();
			syncEntity.progress=entity.getProgress();
			syncEntity.recurrenceDuration=entity.getRecurrenceDuration();
			syncEntity.recurrenceScale=entity.getRecurrenceScale();
			syncEntity.recurrenceUnit=entity.getRecurrenceUnit();
			syncEntity.recurringUnitlDate=entity.getRecurrenceUntilDate();
			syncEntity.status=entity.getStatus();
			syncEntity.worksheetType=entity.getWorksheetType();

			syncEntity.ownerlist = getSyncUserList(entity.getOwnerlist());
			syncEntity.assigneelist= getSyncUserList(entity.getAssigneelist());
			syncEntity.followerlist= getSyncUserList(entity.getFollowerlist());
			syncEntity.connectionlist=getSyncUserList(entity.getConnectionlist());
			syncEntity.directOwnerlist=getSyncUserList(entity.getDirectOwnerlist());
			syncEntity.directAssigneelist=getSyncUserList(entity.getDirectAssigneelist());
			syncEntity.directFollowerlist=getSyncUserList(entity.getDirectFollowerlist());
			syncEntity.directConnectionlist=getSyncUserList(entity.getDirectConnectionlist());

			syncEntity.parentArray=entity.getParentArray();
			syncEntity.indentValue=entity.getIndentValue();
			return  syncEntity;
		}

		return  null;
	}

	public JBActionlist getSyncJBActionlist(com.uppstreymi.jibby.bean.JBActionlist actionlist) {
		if(actionlist!=null) {
			JBActionlist syncActionlist = new JBActionlist();
			syncActionlist.id=actionlist.getId();
			syncActionlist.itemtype=actionlist.getItemtype();
			syncActionlist.itemid=actionlist.getItemid();
			syncActionlist.dateadded=actionlist.getDateadded();
			syncActionlist.addedby=actionlist.getAddedby();
			syncActionlist.itemname=actionlist.getItemname();
			syncActionlist.assignedto=actionlist.getAssignedto();
			syncActionlist.itemdatetime=actionlist.getItemdatetime();
			return  syncActionlist;
		}
		return  null;
	}

	public JBActionlistHistory getSyncJBActonlistHistory(com.uppstreymi.jibby.bean.JBActionlistHistory actionlistHistory) {
		if(actionlistHistory!=null) {
			JBActionlistHistory syncJBActionlistHistory = new JBActionlistHistory();
			syncJBActionlistHistory.id=actionlistHistory.getId();
			syncJBActionlistHistory.created=actionlistHistory.getCreated();
			syncJBActionlistHistory.itemid=actionlistHistory.getItemid();
			syncJBActionlistHistory.itemtype=actionlistHistory.getItemtype();
			syncJBActionlistHistory.userid=actionlistHistory.getUserid();
			syncJBActionlistHistory.messagetype=actionlistHistory.getMessagetype();
			syncJBActionlistHistory.message=actionlistHistory.getMessage();
			syncJBActionlistHistory.isRead=actionlistHistory.getIsRead();
			return  syncJBActionlistHistory;
		}
		return  null;
	}

	public JBAttachmentEntity getSyncJBAttachmentEntity(com.uppstreymi.jibby.bean.JBAttachmentEntity attachmentEntity) {
		if(attachmentEntity!=null) {
			JBAttachmentEntity syncJBAttachmentEntity = new JBAttachmentEntity();
			syncJBAttachmentEntity.id=attachmentEntity.getId();
			syncJBAttachmentEntity.entity=attachmentEntity.getEntity();
			syncJBAttachmentEntity.uploaderUser=attachmentEntity.getUploaderUser();
			syncJBAttachmentEntity.uploadDate=attachmentEntity.getUploadDate();
			syncJBAttachmentEntity.name=attachmentEntity.getName();
			syncJBAttachmentEntity.type=attachmentEntity.getType();
			syncJBAttachmentEntity.size=attachmentEntity.getSize();
			syncJBAttachmentEntity.description=attachmentEntity.getDescription();
			syncJBAttachmentEntity.sortOrder=attachmentEntity.getSortOrder();
			return  syncJBAttachmentEntity;
		}
		return  null;
	}

	public JBChatMessageEntity getSyncJBChatMessageEntity(com.uppstreymi.jibby.bean.JBChatMessageEntity chatMessageEntity) {
		if(chatMessageEntity!=null) {
			JBChatMessageEntity syncJBChatMessageEntity = new JBChatMessageEntity();
			syncJBChatMessageEntity.id=chatMessageEntity.getId();
			syncJBChatMessageEntity.sender=chatMessageEntity.getSender();
			syncJBChatMessageEntity.entity=chatMessageEntity.getEntity();
			syncJBChatMessageEntity.text=chatMessageEntity.getText();
			syncJBChatMessageEntity.date=chatMessageEntity.getDate();
			return  syncJBChatMessageEntity;
		}
		return  null;
	}

	public JBChecklistItemEntity getSyncJBChecklistItemEntity(com.uppstreymi.jibby.bean.JBChecklistItemEntity checklistItemEntity) {
		if(checklistItemEntity!=null) {
			JBChecklistItemEntity syncJBChecklistItemEntity = new JBChecklistItemEntity();
			syncJBChecklistItemEntity.id=checklistItemEntity.getId();
			syncJBChecklistItemEntity.entity=checklistItemEntity.getEntity();
			syncJBChecklistItemEntity.name=checklistItemEntity.getName();
			syncJBChecklistItemEntity.isChecked=checklistItemEntity.getIsChecked();
			syncJBChecklistItemEntity.sortOrder=checklistItemEntity.getSortOrder();
			return  syncJBChecklistItemEntity;
		}
		return  null;
	}

	public JBItemModification getSyncJBItemModification(com.uppstreymi.jibby.bean.JBItemModification itemModification) {
		if(itemModification!=null) {
			JBItemModification syncJBItemModification = new JBItemModification();
			syncJBItemModification.id=itemModification.getId();
			syncJBItemModification.entityId=itemModification.getEntityId();
			syncJBItemModification.userId1=itemModification.getUserId1();
			syncJBItemModification.userId2=itemModification.getUserId2();
			syncJBItemModification.modificationtype=itemModification.getModificationType();
			syncJBItemModification.modificationDate=itemModification.getModificationDate();
			return  syncJBItemModification;
		}
		return  null;
	}

	public JBKanbanBoardColumns getSyncJBKanbanBoardColumns(com.uppstreymi.jibby.bean.JBKanbanBoardColumns kanbanBoardColumns) {
		if(kanbanBoardColumns!=null) {
			JBKanbanBoardColumns syncJBKanbanBoardColumns = new JBKanbanBoardColumns();
			syncJBKanbanBoardColumns.id=kanbanBoardColumns.getId();
			syncJBKanbanBoardColumns.name=kanbanBoardColumns.getName();
			syncJBKanbanBoardColumns.wipLimit=kanbanBoardColumns.getWipLimit();
			syncJBKanbanBoardColumns.entityId=kanbanBoardColumns.getEntityId();
			syncJBKanbanBoardColumns.sortOrder=kanbanBoardColumns.getSortOrder();
			syncJBKanbanBoardColumns.containedEntityIds=kanbanBoardColumns.getContainedEntityIds();
			return  syncJBKanbanBoardColumns;
		}
		return  null;
	}

	public JBUserEntity getSyncJBUserEntity(com.uppstreymi.jibby.bean.JBUserEntity userEntity) {
		if(userEntity!=null) {
			JBUserEntity syncJBUserEntity = new JBUserEntity();
			syncJBUserEntity.id=userEntity.getId();
			syncJBUserEntity.firstName=userEntity.getFirstName();
			syncJBUserEntity.lastName=userEntity.getLastName();
			syncJBUserEntity.email=userEntity.getEmail();
			syncJBUserEntity.dateFormat=userEntity.getDateFormat();
			syncJBUserEntity.timeZone=userEntity.getTimeZone();
			
			return  syncJBUserEntity;
		}
		return  null;
	}

	public JBUserRoleEntity getSyncJBUserRoleEntity(com.uppstreymi.jibby.bean.JBUserRoleEntity userRoleEntity) {
		if(userRoleEntity!=null) {
			JBUserRoleEntity syncJBUserRoleEntity = new JBUserRoleEntity();
			syncJBUserRoleEntity.id=userRoleEntity.getId();
			syncJBUserRoleEntity.entityId=userRoleEntity.getEntityId();
			syncJBUserRoleEntity.userId=userRoleEntity.getUser().getId();
			syncJBUserRoleEntity.userRole=userRoleEntity.getUserRole();
			syncJBUserRoleEntity.firstName=userRoleEntity.getUser().getFirstName();
			syncJBUserRoleEntity.lastName=userRoleEntity.getUser().getLastName();
			syncJBUserRoleEntity.email=userRoleEntity.getUser().getEmail();
			syncJBUserRoleEntity.dateFormat=userRoleEntity.getUser().getDateFormat();
			syncJBUserRoleEntity.timeZone=userRoleEntity.getUser().getTimeZone();
			return  syncJBUserRoleEntity;
		}
		return  null;
	}

	public List<JBUserEntity> getSyncUserList(List<com.uppstreymi.jibby.bean.JBUserEntity> userlists){
		if(userlists!=null) {
			List<JBUserEntity> userlist= new ArrayList<JBUserEntity>();
			for(com.uppstreymi.jibby.bean.JBUserEntity user: userlists) {
				userlist.add(getSyncJBUserEntity(user));
			}
			return userlist;
		}
		return null;
	}

	public List<JBUserRoleEntity> getSyncJBUserRoleEntityList(List<com.uppstreymi.jibby.bean.JBUserRoleEntity> userRolelists){
		if(userRolelists!=null) {
			List<JBUserRoleEntity> userRolelist= new ArrayList<JBUserRoleEntity>();
			for(com.uppstreymi.jibby.bean.JBUserRoleEntity userRole: userRolelists) {
				userRolelist.add(getSyncJBUserRoleEntity(userRole));
			}
			return userRolelist;
		}
		return null;
	}

	public List<JBChecklistItemEntity> getSyncJBChecklistItemEntityList(List<com.uppstreymi.jibby.bean.JBChecklistItemEntity> checklistItemEntitylists){
		if(checklistItemEntitylists!=null) {
			List<JBChecklistItemEntity> checklistItemEntitylist= new ArrayList<JBChecklistItemEntity>();
			for(com.uppstreymi.jibby.bean.JBChecklistItemEntity checklistItemEntity: checklistItemEntitylists) {
				checklistItemEntitylist.add(getSyncJBChecklistItemEntity(checklistItemEntity));
			}
			return checklistItemEntitylist;
		}
		return null;
	}

	public List<JBActionlist> getSyncJBActionlistList(List<com.uppstreymi.jibby.bean.JBActionlist> actionlistlists){
		if(actionlistlists!=null) {
			List<JBActionlist> actionlists= new ArrayList<JBActionlist>();
			for(com.uppstreymi.jibby.bean.JBActionlist actionlist: actionlistlists) {
				actionlists.add(getSyncJBActionlist(actionlist));
			}
			return actionlists;
		}
		return null;
	}

	public List<JBItemModification> getSyncJBItemModificationList(List<com.uppstreymi.jibby.bean.JBItemModification> itemModifications){
		if(itemModifications!=null) {
			List<JBItemModification> itemModificationlist= new ArrayList<JBItemModification>();
			for(com.uppstreymi.jibby.bean.JBItemModification itemModification: itemModifications) {
				itemModificationlist.add(getSyncJBItemModification(itemModification));
			}
			return itemModificationlist;
		}
		return null;
	}

	public List<JBEntity> getSyncJBEntityList(List<com.uppstreymi.jibby.bean.JBEntity> entitys){
		if(entitys!=null) {
			List<JBEntity> entitylist= new ArrayList<JBEntity>();
			for(com.uppstreymi.jibby.bean.JBEntity entity: entitys) {
				entitylist.add(getSyncEntity(entity));
			}
			return entitylist;
		}
		return null;
	}

	public List<JBKanbanBoardColumns> getSyncJBKanbanBoardColumnsList(List<com.uppstreymi.jibby.bean.JBKanbanBoardColumns> kanbancolumns){
		if(kanbancolumns!=null) {
			List<JBKanbanBoardColumns> kanbancolumnlist= new ArrayList<JBKanbanBoardColumns>();
			for(com.uppstreymi.jibby.bean.JBKanbanBoardColumns kanbancolumn: kanbancolumns) {
				kanbancolumnlist.add(getSyncJBKanbanBoardColumns(kanbancolumn));
			}
			return kanbancolumnlist;
		}
		return null;
	}
}
