package com.uppstreymi.jibby.backend.sync.manager;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.backend.sync.bean.CopySyncBeanToDataBean;
import com.uppstreymi.jibby.backend.sync.process.ProcessData;
import com.uppstreymi.jibby.backend.sync.process.SeparateSyncProcessData;

@LocalBean
@Singleton
@Startup
public class SyncProcessor {

	private Queue<String>userEvents =new LinkedList<>();
	Logger logger=LogManager.getLogger(SyncProcessor.class);
	private static final String JSON_UTF8 = "application/json; charset=UTF-8";

	@Inject CopySyncBeanToDataBean copySyncBeanToDataBean;
	@Inject SynchronizeManager syncManager;

	public void addEvent(String json){
		userEvents.add(json);
	}


	@Schedule(second = "*/06", minute = "*", hour = "*", persistent = false)
	public synchronized void processData(){
		//logger.info("Start proceed sync JIBBY-PWA data...");
		if(!userEvents.isEmpty()){
			
			String json=userEvents.peek();
			logger.info("user event peak from JIBBY-PWA queue." );
			
			try {

				ObjectMapper objectMapper= new ObjectMapper();
				SeparateSyncUserEvent userEvent=objectMapper.readValue(json, SeparateSyncUserEvent.class);

				for(Integer userId:userEvent.getSyncData().keySet()){

					if(syncManager.getModel(userId)!=null){
						SeparateSyncProcessData syncProcessData=userEvent.getSyncData().get(userId);
						syncProcessData.setReceiverId(userId);
						ProcessData processData=convertSyncProcessToDataProcess(syncProcessData);
						syncManager.updateModel(processData);
					}
				}
				userEvents.remove();
			}catch(Exception e) {
				
				logger.error("An exception thrown in syncprocessor",e);
			}

		}
	}

	private ProcessData convertSyncProcessToDataProcess(SeparateSyncProcessData syncProcessData) {

		ProcessData processData=new ProcessData();

		processData.setUserId(syncProcessData.getUserId());
		processData.setReceiverId(syncProcessData.getReceiverId());
		processData.setSourceParentId(syncProcessData.getSourceParentId());
		processData.setAssingedUserId(syncProcessData.getAssingedUserId());
		processData.setItemModificationRemoveId(syncProcessData.getItemModificationRemoveId());
		processData.setEventType(syncProcessData.getEventType());

		processData.setIsRemoveUserRole(syncProcessData.getIsRemoveUserRole());
		processData.setIsAttachmentIcon(syncProcessData.getIsAttachmentIcon());
		processData.setIsNewProject(syncProcessData.getIsNewProject());
		processData.setColAProjectRemove(syncProcessData.getColAProjectRemove());
		processData.setColAProjectAdd(syncProcessData.getColAProjectAdd());
		processData.setIsTopLabelProjectRemove(syncProcessData.getIsTopLabelProjectRemove());
		processData.setHasAttachment(syncProcessData.getHasAttachment());
		processData.setHasChild(syncProcessData.getHasChild());
		processData.setHasCheckItem(syncProcessData.getHasCheckItem());
		processData.setIsMoveItem(syncProcessData.getIsMoveItem());
		processData.setIsColumnAdd(syncProcessData.getIsColumnAdd());
		processData.setIsColumnDelete(syncProcessData.getIsColumnDelete());

		processData.setReceiver(copySyncBeanToDataBean.getDataJBUserEntity(syncProcessData.getReceiver()));
		processData.setSender(copySyncBeanToDataBean.getDataJBUserEntity(syncProcessData.getSender()));
		processData.setSourceEntity(copySyncBeanToDataBean.getDataEntity(syncProcessData.getSourceEntity()));
		processData.setOldEntity(copySyncBeanToDataBean.getDataEntity(syncProcessData.getOldEntity()));
		processData.setKanbanCardEntity(copySyncBeanToDataBean.getDataEntity(syncProcessData.getKanbanCardEntity()));
		processData.setEntity(copySyncBeanToDataBean.getDataEntity(syncProcessData.getEntity()));
		processData.setTopLebelProjectEntity(copySyncBeanToDataBean.getDataEntity(syncProcessData.getTopLebelProjectEntity()));
		processData.setNotification(copySyncBeanToDataBean.getDataJBActonlistHistory(syncProcessData.getNotification()));
		processData.setComments(copySyncBeanToDataBean.getDataJBChatMessageEntity(syncProcessData.getComments()));
		processData.setAttachments(copySyncBeanToDataBean.getDataJBAttachmentEntity(syncProcessData.getAttachments()));
		processData.setKanbanSourceColumn(copySyncBeanToDataBean.getDataJBKanbanBoardColumns(syncProcessData.getKanbanSourceColumn()));

		processData.setEntityReceiver(copySyncBeanToDataBean.getDataUserList(syncProcessData.getEntityReceiver()));
		processData.setAssigneeList(copySyncBeanToDataBean.getDataUserList(syncProcessData.getAssigneeList()));
		processData.setDirectUsers(copySyncBeanToDataBean.getDataUserList(syncProcessData.getDirectUsers()));
		processData.setHistorylist(copySyncBeanToDataBean.getDataJBItemModificationList(syncProcessData.getHistorylist()));
		processData.setChecklists(copySyncBeanToDataBean.getDataJBChecklistItemEntityList(syncProcessData.getChecklists()));
		processData.setUserRolelist(copySyncBeanToDataBean.getDataJBUserRoleEntityList(syncProcessData.getUserRolelist()));
		processData.setKanbanColumnList(copySyncBeanToDataBean.getDataJBKanbanBoardColumnsList(syncProcessData.getKanbanColumnList()));
		processData.setColumnEntitiesSortList(copySyncBeanToDataBean.getDataJBEntityList(syncProcessData.getColumnEntitiesSortList()));
		processData.setActionlists(copySyncBeanToDataBean.getDataJBActionlistList(syncProcessData.getActionlists()));

		processData.setUnsubscribeEntitylist(syncProcessData.getUnsubscribeEntitylist());


		return processData;
	}
}
