package com.uppstreymi.jibby.backend.sync.manager;

import java.util.HashMap;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.uppstreymi.jibby.backend.sync.process.ProcessData;

@Startup
@Singleton
public class SynchronizeManager{
	private HashMap<Integer, SyncDataChangeModel> syncModels=new HashMap<Integer, SyncDataChangeModel>();
	
	public SyncDataChangeModel getModel(Integer userid) {
		return syncModels.get(userid);
	}
	
	public void updateModel(ProcessData data) {
		SyncDataChangeModel tanksPumpsModel = syncModels.get(data.getUserId());
		tanksPumpsModel.setProcessData(data);
	}
	
	public void addUserDataChangeModel(Integer userId){
		syncModels.put(userId, new SyncDataChangeModel());
	}
	
	public void removeUserDataChangeModel(Integer user){
		if(syncModels.containsKey(user))
			syncModels.remove(user);
	}

	
	
}
