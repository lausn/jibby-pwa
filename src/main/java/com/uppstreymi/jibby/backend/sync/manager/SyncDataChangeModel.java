package com.uppstreymi.jibby.backend.sync.manager;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.uppstreymi.jibby.backend.sync.process.ProcessData;

public class SyncDataChangeModel {

	private Set<DataChangeModelListener> syncModelListeners = new HashSet<DataChangeModelListener>();
	private ProcessData data;
	
	public SyncDataChangeModel(){}
	
	public interface DataChangeModelListener extends Serializable {
		void onChanged(ModelUpdateEvent event);
		
	}
	
	public void setProcessData(ProcessData data){
		this.data=data;
		fireDataChangeModelUpdate(data);
	}
	public ProcessData getProcessData(){
		return this.data;
	}

	public class ModelUpdateEvent {
		ProcessData data;
		public ModelUpdateEvent(ProcessData data) {
			this.data=data;
		}
		public ProcessData getProcessData(){
			return this.data;
		}
	}
	
	private void fireDataChangeModelUpdate(ProcessData data) {
		ModelUpdateEvent updateEvent=new ModelUpdateEvent(data);
		for (DataChangeModelListener listener : Collections.unmodifiableCollection(syncModelListeners)) {
			listener.onChanged(updateEvent);
		}
	}

	public void addDataChangeListener(DataChangeModelListener listener) {
		
		syncModelListeners.add(listener);
		//logger.info("addlistener "+syncModelListeners);
	}

	public void removeDataChangeListener(DataChangeModelListener listener) {
		syncModelListeners.remove(listener);
		//logger.info("removelistener "+syncModelListeners);
	}

}