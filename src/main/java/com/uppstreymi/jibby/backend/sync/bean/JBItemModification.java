package com.uppstreymi.jibby.backend.sync.bean;

import java.util.Date;


public class JBItemModification{
	public Integer id;
	public Integer entityId;
	public Integer userId1;
	public Integer userId2;
	public Integer modificationtype;
	public Date modificationDate;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return this.entityId;
	}
	
	public void setUserId1(Integer userId1){
		this.userId1=userId1;
	}
	public Integer getUserId1(){
		return this.userId1;
	}
	
	public void setUserId2(Integer userId2){
		this.userId2=userId2;
	}
	public Integer getUserId2(){
		return this.userId2;
	}
	
	public void setModificationType(Integer modificationtype){
		this.modificationtype=modificationtype;
	}
	public Integer getModificationType(){
		return this.modificationtype;
	}
	
	public void setModificationDate(Date modificationDate){
		this.modificationDate=modificationDate;
	}
	public Date getModificationDate(){
		return this.modificationDate;
	}
}
