package com.uppstreymi.jibby.backend.sync.manager;


import javax.ejb.LocalBean;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.net.ssl.SSLContext;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.utils.Utils;

@LocalBean
@Startup
public class EventProcessor {

	@Inject private AppModel appModel;
	Logger logger=LogManager.getLogger(EventProcessor.class);
	private static final String JSON_UTF8 = "application/json; charset=UTF-8";



	public void addEvent(SyncUserEvent userEvent){
		callRestService(userEvent);
	}

	private synchronized void callRestService(SyncUserEvent userEvent) {
		try {
			
			Client client = createSSLClient();
			client.register(String.class);
			SeparateSyncUserEvent separateSyncData=new SeparateSyncUserEvent();
			separateSyncData.setSyncData(userEvent.getSyncData());
			
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(separateSyncData);
			//logger.info(json);
			
			String url= Utils.getFilterUrl(appModel.getPropertiesModel().getSyncAppUrl());
			WebTarget target = client.target(url).path("rest/restservice/sync");
			String resp = target.request().post(Entity.entity(json, JSON_UTF8), String.class);

		}catch(NotFoundException e) {
			logger.error("Jibby-Sync server not found!",e);
		}catch(Exception e) {
			logger.error("Jibby-pwa event exception: ",e);
		}

	}

	private Client createSSLClient() {
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			@Override
			public boolean isTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub
				return true;
			}
		};

		SSLContext sslContext = null;
		try {
			sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		} catch (Exception e) {
			logger.error("SSl exception : ",e.getMessage());
		}
		return ClientBuilder.newBuilder().sslContext(sslContext)
				.hostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();
	}
}
