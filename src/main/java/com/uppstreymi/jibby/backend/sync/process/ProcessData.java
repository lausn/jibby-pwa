package com.uppstreymi.jibby.backend.sync.process;

import java.util.List;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBChatMessageEntity;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBKanbanBoardColumns;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;

public class ProcessData{

	private Integer userId;
	private Integer receiverid;
	private Integer sourceParentId;
	private Integer assignedUserid;
	
	private String modificationId;
	
	private boolean isRemoveUserRole;
	private boolean isAttachmentIcon,isNewProject,colAProjectRemove,colAProjectAdd,isTopLebelProject,hasAttachment,hasChild,hasCheckItem,isMoveItem;
	private boolean isColumnAdd,isColumnDelete; 

	private JBUserEntity receiverEntity;
	private JBUserEntity sender;
	
	private List<JBUserEntity> receiver;
	private List<JBUserEntity> assigneeList,directUsers;

	private JBEntity sourceEntity,oldEntity,cardEntity;
	private JBEntity entity,topLebelEntity;
	private SyncEventType eventType;

	private JBActionlistHistory notification;
	private JBChatMessageEntity message;
	private JBAttachmentEntity attachment;
	private JBKanbanBoardColumns kanbanSrcColumn;
	
	private List<JBItemModification> historyList;
	private List<JBChecklistItemEntity> checklists;
	private List<JBUserRoleEntity> userRoleList;
	private List<Integer> unsubscribeEntitylist;
	private List<JBKanbanBoardColumns> kanbanColumnList;
	private List<JBEntity> sortEntities;
	private List<JBActionlist> actionlists;


	public ProcessData(){}
	
	public void setDirectUsers(List<JBUserEntity> directUsers){
		this.directUsers=directUsers;
	}
	
	public List<JBUserEntity> getDirectUsers(){
		return directUsers;
	} 
	
	public void setHasChild(boolean hasChild){
		this.hasChild=hasChild;
	}
	
	public boolean getHasChild(){
		return hasChild;
	}
	
	public void setHasCheckItem(boolean hasCheckItem){
		this.hasCheckItem=hasCheckItem;
	}
	
	public boolean getHasCheckItem(){
		return hasCheckItem;
	}
	
	public void setHasAttachment(boolean hasAttachment){
		this.hasAttachment=hasAttachment;
	}
	
	public boolean getHasAttachment(){
		return hasAttachment;
	}
	
	public void setIsMoveItem(boolean isMoveItem){
		this.isMoveItem=isMoveItem;
	}
	
	public boolean isMoveItem(){
		return isMoveItem;
	}
	
	public void setSourceParentId(Integer sourceParentId){
		this.sourceParentId=sourceParentId;
	}
	
	public Integer getSourceParentId(){
		return sourceParentId;
	}
	
	public void setIsNewProject(boolean isNewProject) {
		this.isNewProject = isNewProject;
	}
	
	public boolean getIsNewProject() {
		return this.isNewProject;
	}
	
	public void setIsTopLabelProjectRemove(boolean isTopLebelProject) {
		this.isTopLebelProject = isTopLebelProject;
	}
	
	public boolean getIsTopLabelProjectRemove() {
		return this.isTopLebelProject;
	}
	
	public void setColAProjectRemove(boolean colAProjectRemove) {
		this.colAProjectRemove = colAProjectRemove;
	}
	
	public boolean getColAProjectRemove() {
		return this.colAProjectRemove;
	}
	
	public void setColAProjectAdd(boolean colAProjectAdd) {
		this.colAProjectAdd = colAProjectAdd;
	}
	
	public boolean getColAProjectAdd() {
		return this.colAProjectAdd;
	}

	public void setReceiver(JBUserEntity receiverEntity){
		this.receiverEntity=receiverEntity;
	}
	public JBUserEntity getReceiver(){
		return this.receiverEntity;
	}

	public void setReceiverId(Integer receiverid){
		this.receiverid=receiverid;
	}
	public Integer getReceiverId(){
		return this.receiverid;
	}

	public void setSender(JBUserEntity sender){
		this.sender=sender;
	}
	public JBUserEntity getSender(){
		return this.sender;
	}

	public void setEventType(SyncEventType eventType){
		this.eventType=eventType;
	}

	public SyncEventType getEventType(){
		return this.eventType;
	}

	public void setUserId(Integer userId){
		this.userId=userId;
	}
	public Integer getUserId(){
		return this.userId;
	}

	public void setNotification(JBActionlistHistory notification){
		this.notification=notification;
	}

	public JBActionlistHistory getNotification(){
		return this.notification;
	}

	public void setEntity(JBEntity entity){
		this.entity=entity;
	}

	public JBEntity getEntity(){
		return this.entity;
	}

	public void setComments(JBChatMessageEntity message){
		this.message=message;
	}

	public JBChatMessageEntity getComments(){
		return this.message;
	}

	public void setEntityReceiver(List<JBUserEntity> receiver){
		this.receiver=receiver;
	}

	public List<JBUserEntity> getEntityReceiver(){
		return this.receiver;
	}

	public void setHistorylist(List<JBItemModification> historyList) {
		this.historyList = historyList;
	}
	public List<JBItemModification> getHistorylist() {
		return this.historyList;
	}

	public void setAssigneeList(List<JBUserEntity> assigneeLists) {
		this.assigneeList = assigneeLists;

	}
	public List<JBUserEntity> getAssigneeList() {
		return this.assigneeList;

	}

	public void setAttachments(JBAttachmentEntity attachment){
		this.attachment=attachment;
	}

	public JBAttachmentEntity getAttachments(){
		return this.attachment;
	}

	public void setItemModificationRemoveId(String modificationId) {
		this.modificationId = modificationId;
	}
	public String getItemModificationRemoveId() {
		return this.modificationId;
	}

	public void setChecklists(List<JBChecklistItemEntity> checklists) {
		this.checklists = checklists;
	}

	public List<JBChecklistItemEntity> getChecklists() {
		return this.checklists;
	}

	public void setSourceEntity(JBEntity sourceEntity){
		this.sourceEntity=sourceEntity;
	}

	public JBEntity getSourceEntity(){
		return this.sourceEntity;
	}

	public void setIsAttachmentIcon(boolean isAttachmentIcon) {
		// TODO Auto-generated method stub
		this.isAttachmentIcon = isAttachmentIcon;
	}

	public boolean getIsAttachmentIcon() {
		// TODO Auto-generated method stub
		return this.isAttachmentIcon;
	}
	
	public void setOldEntity(JBEntity oldEntity){
		this.oldEntity=oldEntity;
	}
	
	public JBEntity getOldEntity(){
		return this.oldEntity;
	}
	
	public void setUserRolelist(List<JBUserRoleEntity> userRoleList) {
		this.userRoleList = userRoleList;
	}
	public List<JBUserRoleEntity> getUserRolelist() {
		return this.userRoleList;
	}
	
	public void setIsRemoveUserRole(boolean isRemoveUserRole) {
		this.isRemoveUserRole = isRemoveUserRole;
	}
	
	public boolean getIsRemoveUserRole() {
		return this.isRemoveUserRole;
	}
	
	public void setUnsubscribeEntitylist(List<Integer> unsubscribeEntitylist){
		this.unsubscribeEntitylist = unsubscribeEntitylist;
	}
	
	public List<Integer> getUnsubscribeEntitylist() {
		return this.unsubscribeEntitylist;
	}
	
	public void setKanbanColumnList(List<JBKanbanBoardColumns> kanbanColumnList){
		this.kanbanColumnList=kanbanColumnList;
	}

	public List<JBKanbanBoardColumns> getKanbanColumnList(){
		return this.kanbanColumnList;
	}
	
	public void setKanbanCardEntity(JBEntity cardEntity) {
		this.cardEntity = cardEntity;
	}
	
	public JBEntity getKanbanCardEntity() {
		return this.cardEntity;
	}
	
	public void setKanbanSourceColumn(JBKanbanBoardColumns kanbanSrcColumn) {
		this.kanbanSrcColumn = kanbanSrcColumn;
	}
	
	public JBKanbanBoardColumns getKanbanSourceColumn() {
		return this.kanbanSrcColumn;
	}
	public void setIsColumnAdd(boolean isColumnAdd) {
		this.isColumnAdd = isColumnAdd;
	}
	public boolean getIsColumnAdd() {
		return this.isColumnAdd;	
	}
	public void setIsColumnDelete(boolean isColumnDelete) {
		this.isColumnDelete = isColumnDelete;
	}
	public boolean getIsColumnDelete() {
		return this.isColumnDelete;	
	}

	public void setColumnEntitiesSortList(List<JBEntity> sortEntities) {
		this.sortEntities = sortEntities;
	}
	public List<JBEntity> getColumnEntitiesSortList() {
		return this.sortEntities;
	}

	public void setActionlists(List<JBActionlist> actionLists) {
		this.actionlists = actionLists;
	}
	public List<JBActionlist> getActionlists() {
		return this.actionlists;
	}
	
	public void setTopLebelProjectEntity(JBEntity topLebelEntity) {
		this.topLebelEntity = topLebelEntity;
	}
	public JBEntity getTopLebelProjectEntity() {
		return this.topLebelEntity;
	}
	public void setAssingedUserId(Integer assignedUserid) {
		this.assignedUserid = assignedUserid;
	}
	public Integer getAssingedUserId() {
		return this.assignedUserid;
	}

}