package com.uppstreymi.jibby.backend.sync.bean;



public class JBChecklistItemEntity{
	public Integer id;
	public Integer entity;
	public String name;
	public Integer isChecked;
	public Integer sortOrder;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	public void setIsChecked(Integer isChecked){
		this.isChecked=isChecked;
	}
	public Integer getIsChecked(){
		return isChecked;
	}
	public void setSortOrder(Integer sortOrder){
		this.sortOrder=sortOrder;
	}
	public Integer getSortOrder(){
		return sortOrder;
	}
}
