package com.uppstreymi.jibby.backend.sync.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;


@Stateless
@LocalBean
public class CopySyncBeanToDataBean {

	public com.uppstreymi.jibby.bean.JBEntity getDataEntity(JBEntity entity) {
		if(entity!=null) {
			com.uppstreymi.jibby.bean.JBEntity dataEntity = new com.uppstreymi.jibby.bean.JBEntity();
			dataEntity.setId(entity.getId());
			dataEntity.setName(entity.getName());
			dataEntity.setDescription(entity.getDescription()==null?"":entity.getDescription());
			dataEntity.setEstimatedStartDate(entity.getEstimatedStartDate());
			dataEntity.setEstimatedFinishDate(entity.getEstimatedFinishDate());
			dataEntity.setEntityType(entity.getEntityType());
			dataEntity.setUnitQuantity(entity.getUnitQuantity());
			dataEntity.setUnitName(entity.getUnitName());
			dataEntity.setColor(entity.getColor());
			dataEntity.setCostName(entity.getCostName());
			dataEntity.setCostPerUnit(entity.getCostPerUnit());
			dataEntity.setCreationDate(entity.getCreationDate());
			dataEntity.setCreator(entity.getCreator());
			dataEntity.setDelIsTimeBased(entity.getDelIsTimeBased());
			dataEntity.setHierarchy(entity.getHierarchy());
			dataEntity.setChatEnabled(entity.getChatEnabled());
			dataEntity.setIsPublic(entity.getIsPublic());
			dataEntity.setKeywords(entity.getKeywords());
			dataEntity.setLastModificationDate(entity.getLastModificationDate());
			dataEntity.setParentId(entity.getParentId());
			dataEntity.setParentProjectId(entity.getParentProjectId());
			dataEntity.setPriority(entity.getPriority());
			dataEntity.setProgress(entity.getProgress());
			dataEntity.setRecurrenceDuration(entity.getRecurrenceDuration());
			dataEntity.setRecurrenceScale(entity.getRecurrenceScale());
			dataEntity.setRecurrenceUnit(entity.getRecurrenceUnit());
			dataEntity.setRecurrenceUntilDate(entity.getRecurrenceUntilDate());
			dataEntity.setStatus(entity.getStatus());
			dataEntity.setWorksheetType(entity.getWorksheetType());

			dataEntity.setOwnerlist(getDataUserList(entity.getOwnerlist()));
			dataEntity.setAssigneelist(getDataUserList(entity.getAssigneelist()));
			dataEntity.setFollowerlist(getDataUserList(entity.getFollowerlist()));
			dataEntity.setConnectionlist(getDataUserList(entity.getConnectionlist()));
			dataEntity.setDirectOwnerlist(getDataUserList(entity.getDirectOwnerlist()));
			dataEntity.setDirectAssigneelist(getDataUserList(entity.getDirectAssigneelist()));
			dataEntity.setDirectFollowerlist(getDataUserList(entity.getDirectFollowerlist()));
			dataEntity.setDirectConnectionlist(getDataUserList(entity.getDirectConnectionlist()));

			dataEntity.setParentArray(entity.getParentArray());
			dataEntity.setIndentValue(entity.getIndentValue());
			return  dataEntity;
		}

		return null;
	}

	public com.uppstreymi.jibby.bean.JBActionlist getDataJBActionlist(JBActionlist actionlist) {
		if(actionlist!=null) {
			com.uppstreymi.jibby.bean.JBActionlist dataActionlist = new com.uppstreymi.jibby.bean.JBActionlist();
			dataActionlist.setId(actionlist.getId());
			dataActionlist.setItemtype(actionlist.getItemtype());
			dataActionlist.setItemid(actionlist.getItemid());
			dataActionlist.setDateadded(actionlist.getDateadded());
			dataActionlist.setAddedby(actionlist.getAddedby());
			dataActionlist.setItemname(actionlist.getItemname());
			dataActionlist.setAssignedto(actionlist.getAssignedto());
			dataActionlist.setItemdatetime(actionlist.getItemdatetime());
			return  dataActionlist;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBActionlistHistory getDataJBActonlistHistory(JBActionlistHistory actionlistHistory) {
		if(actionlistHistory!=null) {
			com.uppstreymi.jibby.bean.JBActionlistHistory dataJBActionlistHistory = new com.uppstreymi.jibby.bean.JBActionlistHistory();
			dataJBActionlistHistory.setId(actionlistHistory.getId());
			dataJBActionlistHistory.setCreated(actionlistHistory.getCreated());
			dataJBActionlistHistory.setItemid(actionlistHistory.getItemid());
			dataJBActionlistHistory.setItemtype(actionlistHistory.getItemtype());
			dataJBActionlistHistory.setUserid(actionlistHistory.getUserid());
			dataJBActionlistHistory.setMessagetype(actionlistHistory.getMessagetype());
			dataJBActionlistHistory.setMessage(actionlistHistory.getMessage());
			dataJBActionlistHistory.setIsRead(actionlistHistory.getIsRead());
			return  dataJBActionlistHistory;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBAttachmentEntity getDataJBAttachmentEntity(JBAttachmentEntity attachmentEntity) {
		if(attachmentEntity!=null) {
			com.uppstreymi.jibby.bean.JBAttachmentEntity dataJBAttachmentEntity = new com.uppstreymi.jibby.bean.JBAttachmentEntity();
			dataJBAttachmentEntity.setId(attachmentEntity.getId());
			dataJBAttachmentEntity.setEntity(attachmentEntity.getEntity());
			dataJBAttachmentEntity.setUploaderUser(attachmentEntity.getUploaderUser());
			dataJBAttachmentEntity.setuploadDate(attachmentEntity.getUploadDate());
			dataJBAttachmentEntity.setName(attachmentEntity.getName());
			dataJBAttachmentEntity.setType(attachmentEntity.getType());
			dataJBAttachmentEntity.setSize(attachmentEntity.getSize());
			dataJBAttachmentEntity.setDescription(attachmentEntity.getDescription());
			dataJBAttachmentEntity.setSortOrder(attachmentEntity.getSortOrder());
			return  dataJBAttachmentEntity;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBChatMessageEntity getDataJBChatMessageEntity(JBChatMessageEntity chatMessageEntity) {
		if(chatMessageEntity!=null) {
			com.uppstreymi.jibby.bean.JBChatMessageEntity dataJBChatMessageEntity = new com.uppstreymi.jibby.bean.JBChatMessageEntity();
			dataJBChatMessageEntity.setId(chatMessageEntity.getId());
			dataJBChatMessageEntity.setSender(chatMessageEntity.getSender());
			dataJBChatMessageEntity.setEntity(chatMessageEntity.getEntity());
			dataJBChatMessageEntity.setText(chatMessageEntity.getText());
			dataJBChatMessageEntity.setDate(chatMessageEntity.getDate());
			return  dataJBChatMessageEntity;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBChecklistItemEntity getDataJBChecklistItemEntity(JBChecklistItemEntity checklistItemEntity) {
		if(checklistItemEntity!=null) {
			com.uppstreymi.jibby.bean.JBChecklistItemEntity dataJBChecklistItemEntity = new com.uppstreymi.jibby.bean.JBChecklistItemEntity();
			dataJBChecklistItemEntity.setId(checklistItemEntity.getId());
			dataJBChecklistItemEntity.setEntity(checklistItemEntity.getEntity());
			dataJBChecklistItemEntity.setName(checklistItemEntity.getName());
			dataJBChecklistItemEntity.setIsChecked(checklistItemEntity.getIsChecked());
			dataJBChecklistItemEntity.setSortOrder(checklistItemEntity.getSortOrder());
			return  dataJBChecklistItemEntity;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBItemModification getDataJBItemModification(JBItemModification itemModification) {
		if(itemModification!=null) {
			com.uppstreymi.jibby.bean.JBItemModification dataJBItemModification = new com.uppstreymi.jibby.bean.JBItemModification();
			dataJBItemModification.setId(itemModification.getId());
			dataJBItemModification.setEntityId(itemModification.getEntityId());
			dataJBItemModification.setUserId1(itemModification.getUserId1());
			dataJBItemModification.setUserId2(itemModification.getUserId2());
			dataJBItemModification.setModificationType(itemModification.getModificationType());
			dataJBItemModification.setModificationDate(itemModification.getModificationDate());
			return  dataJBItemModification;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBKanbanBoardColumns getDataJBKanbanBoardColumns(JBKanbanBoardColumns kanbanBoardColumns) {
		if(kanbanBoardColumns!=null) {
			com.uppstreymi.jibby.bean.JBKanbanBoardColumns dataJBKanbanBoardColumns = new com.uppstreymi.jibby.bean.JBKanbanBoardColumns();
			dataJBKanbanBoardColumns.setId(kanbanBoardColumns.getId());
			dataJBKanbanBoardColumns.setName(kanbanBoardColumns.getName());
			dataJBKanbanBoardColumns.setWipLimit(kanbanBoardColumns.getWipLimit());
			dataJBKanbanBoardColumns.setEntityId(kanbanBoardColumns.getEntityId());
			dataJBKanbanBoardColumns.setSortOrder(kanbanBoardColumns.getSortOrder());
			dataJBKanbanBoardColumns.setContainedEntityIds(kanbanBoardColumns.getContainedEntityIds());
			return  dataJBKanbanBoardColumns;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBUserEntity getDataJBUserEntity(JBUserEntity userEntity) {
		if(userEntity!=null) {
			com.uppstreymi.jibby.bean.JBUserEntity dataJBUserEntity = new com.uppstreymi.jibby.bean.JBUserEntity();
			dataJBUserEntity.setId(userEntity.getId());
			dataJBUserEntity.setFirstName(userEntity.getFirstName());
			dataJBUserEntity.setLastName(userEntity.getLastName());
			dataJBUserEntity.setEmail(userEntity.getEmail());
			dataJBUserEntity.setDateFormat(userEntity.getDateFormat());
			dataJBUserEntity.setTimeZone(userEntity.getTimeZone());
			return  dataJBUserEntity;
		}
		return null;
	}

	public com.uppstreymi.jibby.bean.JBUserRoleEntity getDataJBUserRoleEntity(JBUserRoleEntity userRoleEntity) {
		if(userRoleEntity!=null) {
			com.uppstreymi.jibby.bean.JBUserRoleEntity dataJBUserRoleEntity = new com.uppstreymi.jibby.bean.JBUserRoleEntity();
			com.uppstreymi.jibby.bean.JBUserEntity user=new com.uppstreymi.jibby.bean.JBUserEntity();
			user.setId(userRoleEntity.getUserId());
			user.setFirstName(userRoleEntity.getFirstName());
			user.setLastName(userRoleEntity.getLastName());
			user.setEmail(userRoleEntity.getEmail());
			user.setDateFormat(userRoleEntity.getDateFormat());
			user.setTimeZone(userRoleEntity.getTimeZone());
			dataJBUserRoleEntity.setId(userRoleEntity.getId());
			dataJBUserRoleEntity.setEntityId(userRoleEntity.getEntityId());
			dataJBUserRoleEntity.setUser(user);
			dataJBUserRoleEntity.setUserRole(userRoleEntity.getUserRole());
			return  dataJBUserRoleEntity;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBUserEntity> getDataUserList(List<JBUserEntity> userlists){
		if(userlists!=null) {
			List<com.uppstreymi.jibby.bean.JBUserEntity> userlist= new ArrayList<com.uppstreymi.jibby.bean.JBUserEntity>();
			for(JBUserEntity user: userlists) {
				userlist.add(getDataJBUserEntity(user));
			}
			return userlist;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBUserRoleEntity> getDataJBUserRoleEntityList(List<JBUserRoleEntity> userRolelists){
		if(userRolelists!=null) {
			List<com.uppstreymi.jibby.bean.JBUserRoleEntity> userRolelist= new ArrayList<com.uppstreymi.jibby.bean.JBUserRoleEntity>();
			for(JBUserRoleEntity userRole: userRolelists) {
				userRolelist.add(getDataJBUserRoleEntity(userRole));
			}
			return userRolelist;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBChecklistItemEntity> getDataJBChecklistItemEntityList(List<JBChecklistItemEntity> checklistItemEntitylists){
		if(checklistItemEntitylists!=null) {
			List<com.uppstreymi.jibby.bean.JBChecklistItemEntity> checklistItemEntitylist= new ArrayList<com.uppstreymi.jibby.bean.JBChecklistItemEntity>();
			for(JBChecklistItemEntity checklistItemEntity: checklistItemEntitylists) {
				checklistItemEntitylist.add(getDataJBChecklistItemEntity(checklistItemEntity));
			}
			return checklistItemEntitylist;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBActionlist> getDataJBActionlistList(List<JBActionlist> actionlistlists){
		if(actionlistlists!=null) {
			List<com.uppstreymi.jibby.bean.JBActionlist> actionlists= new ArrayList<com.uppstreymi.jibby.bean.JBActionlist>();
			for(JBActionlist actionlist: actionlistlists) {
				actionlists.add(getDataJBActionlist(actionlist));
			}
			return actionlists;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBItemModification> getDataJBItemModificationList(List<JBItemModification> itemModifications){
		if(itemModifications!=null) {
			List<com.uppstreymi.jibby.bean.JBItemModification> itemModificationlist= new ArrayList<com.uppstreymi.jibby.bean.JBItemModification>();
			for(JBItemModification itemModification: itemModifications) {
				itemModificationlist.add(getDataJBItemModification(itemModification));
			}
			return itemModificationlist;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBEntity> getDataJBEntityList(List<JBEntity> entitys){
		if(entitys!=null) {
			List<com.uppstreymi.jibby.bean.JBEntity> entitylist= new ArrayList<com.uppstreymi.jibby.bean.JBEntity>();
			for(JBEntity entity: entitys) {
				entitylist.add(getDataEntity(entity));
			}
			return entitylist;
		}
		return null;
	}

	public List<com.uppstreymi.jibby.bean.JBKanbanBoardColumns> getDataJBKanbanBoardColumnsList(List<JBKanbanBoardColumns> kanbancolumns){
		if(kanbancolumns!=null) {
			List<com.uppstreymi.jibby.bean.JBKanbanBoardColumns> kanbancolumnlist= new ArrayList<com.uppstreymi.jibby.bean.JBKanbanBoardColumns>();
			for(JBKanbanBoardColumns kanbancolumn: kanbancolumns) {
				kanbancolumnlist.add(getDataJBKanbanBoardColumns(kanbancolumn));
			}
			return kanbancolumnlist;
		}
		return null;
	}
}
