package com.uppstreymi.jibby.backend.sync.bean;



public class JBKanbanBoardColumns{
	public Integer id;
	public String name;
	public Integer wipLimit;
	public Integer entityId;
	public Integer sortOrder;
	public String containedEntityIds;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	
	public void setWipLimit(Integer wipLimit){
		this.wipLimit=wipLimit;
	}
	public Integer getWipLimit(){
		return this.wipLimit;
	}
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return this.entityId;
	}
	public void setSortOrder(Integer sortOrder){
		this.sortOrder=sortOrder;
	}
	public Integer getSortOrder(){
		return sortOrder;
	}
	public void setContainedEntityIds(String containedEntityIds){
		this.containedEntityIds=containedEntityIds;
	}
	public String getContainedEntityIds(){
		return containedEntityIds;
	}
	
}
