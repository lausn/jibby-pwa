package com.uppstreymi.jibby.backend.sync.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.uppstreymi.jibby.backend.sync.bean.CopyDataBeanToSyncBean;
import com.uppstreymi.jibby.backend.sync.manager.EventProcessor;
import com.uppstreymi.jibby.backend.sync.process.SeparateSyncProcessData;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBChatMessageEntity;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBKanbanBoardColumns;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;

@RequestScoped
public class SyncUserEvent {

	private JBEntity entity;
	private JBUserEntity sender;

	private SyncEventType eventType;
	private JBChatMessageEntity message;
	private List<JBUserEntity> entityUsers;
	private List<JBUserEntity> directUsers;
	private HashMap<Integer,SeparateSyncProcessData> syncData=new HashMap<Integer,SeparateSyncProcessData>();
	private List<JBItemModification> historyList = new ArrayList<>();
	private List<JBUserEntity> assigneeList = new ArrayList<>();
	private JBActionlistHistory notification;
	private List<Integer> notifyUserList = new ArrayList<>();
	private List<JBUserRoleEntity> userRoleList  = new ArrayList<>();
	private JBAttachmentEntity attachment;
	private String modificationId;
	private List<JBChecklistItemEntity> checklists = new ArrayList<>();
	private JBEntity sourceEntity,oldEntity,cardEntity,topLebelEntity;
	private boolean isAttachmentIcon=true,hasAttachment,hasChild,hasCheckItem,isMoveItem;
	private boolean isNewProject=false,isTopLebelProject = false,isRemoveUserRole = false;
	private boolean isColumnAdd = false,isColumnDelete = false;
	private List<Integer> removeProjectUserList = new ArrayList<>();
	private List<Integer> projectAddUserList = new ArrayList<>();
	private Integer sourceParentId;
	private List<Integer> unsubscribeEntityList = new ArrayList<>();
	private List<JBKanbanBoardColumns> kanbanColumnList = new ArrayList<>();
	private JBKanbanBoardColumns kanbanSrcColumn;
	private List<JBEntity> sortEntities = new ArrayList<>();
	private List<JBActionlist> actionLists = new ArrayList<>();
	private Integer assignedUserid;
	
	private @Inject EventProcessor eventProcess;
	private @Inject CopyDataBeanToSyncBean copyDataBeanToSyncBean;

	private Logger logger=Logger.getLogger(SyncUserEvent.class.getName());
	
	public void eventProcessor(){
		eventProcess.addEvent(this);
	}
	
	public void setEventType(SyncEventType eventType){
		this.eventType=eventType;
	}
	
	public void setHasChild(boolean hasChild){
		this.hasChild=hasChild;
	}
	
	public boolean getHasChild(){
		return hasChild;
	}
	
	public void setHasCheckItem(boolean hasCheckItem){
		this.hasCheckItem=hasCheckItem;
	}
	
	public boolean getHasCheckItem(){
		return hasCheckItem;
	}
	
	public void setHasAttachment(boolean hasAttachment){
		this.hasAttachment=hasAttachment;
	}
	
	public boolean getHasAttachment(){
		return hasAttachment;
	}
	
	public void setIsMoveItem(boolean isMoveItem){
		this.isMoveItem=isMoveItem;
	}
	
	public boolean isMoveItem(){
		return isMoveItem;
	}
	
	public void setSourceParentId(Integer sourceParentId){
		this.sourceParentId=sourceParentId;
	}
	
	public Integer getSourceParentId(){
		return sourceParentId;
	}
	
	public void setEntity(JBEntity entity){
		this.entity=entity;
	}

	public JBEntity getEntity(){
		return this.entity;
	}
	
	public void addToUserRoleList(JBUserRoleEntity userRole){
		this.userRoleList.add(userRole);
	}

	public List<JBUserRoleEntity> getUserRoleList(){
		return this.userRoleList;
	}
	
	public void setSourceEntity(JBEntity sourceEntity){
		this.sourceEntity=sourceEntity;
	}

	public JBEntity getSourceEntity(){
		return this.sourceEntity;
	}
	
	public void setIsNewProject(boolean isNewProject) {
		this.isNewProject = isNewProject;
	}
	
	public boolean getIsNewProject() {
		return this.isNewProject;
	}
	
	public void setIsTopLabelProjectRemove(boolean isTopLebelProject) {
		this.isTopLebelProject = isTopLebelProject;
	}
	
	public boolean getIsTopLabelProjectRemove() {
		return this.isTopLebelProject;
	}
	
	public void addToRemoveProjectUserList(Integer userId){
		this.removeProjectUserList.add(userId);
	}

	public List<Integer> getRemoveProjectUserList(){
		return this.removeProjectUserList;
	}
	
	public void addToProjectAddUserList(Integer userId){
		this.projectAddUserList.add(userId);
	}

	public List<Integer> getProjectAddUserList(){
		return this.projectAddUserList;
	}
	
	public void addToChecklist(JBChecklistItemEntity checklist) {
		this.checklists.add(checklist);
	}
	
	public List<JBChecklistItemEntity> getChecklists() {
		return this.checklists;
	}

	public void setSender(JBUserEntity sender){
		this.sender=sender;
	}

	public JBUserEntity getSender(){
		return this.sender;
	}
	
	public void setOldEntity(JBEntity oldEntity){
		this.oldEntity=oldEntity;
	}
	
	public JBEntity getOldEntity(){
		return this.oldEntity;
	}

	public void setComments(JBChatMessageEntity message){
		this.message=message;
	}

	public JBChatMessageEntity getComments(){
		return this.message;
	}
	
	public void addToHistoryList(JBItemModification history){
		historyList.add(history);
	}

	public List<JBItemModification> getHistoryList(){
		return historyList;
	}
	
	public void addToAssigneeList(JBUserEntity assignee){
		this.assigneeList.add(assignee);
	}

	public List<JBUserEntity> getAssigneeList(){
		return this.assigneeList;
	}
	
	public void setNotification(JBActionlistHistory notification){
		this.notification=notification;
	}

	public JBActionlistHistory getNotification(){
		return this.notification;
	}
	
	public void setIsAttachmentIcon(boolean isAttachmentIcon) {
		this.isAttachmentIcon = isAttachmentIcon;
	}
	
	public boolean getIsAttachmentIcon() {
		return this.isAttachmentIcon;
	}
	
	public void setIsRemoveUserRole(boolean isRemoveUserRole) {
		this.isRemoveUserRole = isRemoveUserRole;
	}
	
	public boolean getIsRemoveUserRole() {
		return this.isRemoveUserRole;
	}
	
	public void addToNotifyUserList(Integer userId){
		this.notifyUserList.add(userId);
	}

	public List<Integer> getNotifyUserList(){
		return this.notifyUserList;
	}
	
	public void addToUnsubscribeEntityList(Integer entityId) {
		this.unsubscribeEntityList.add(entityId);
	}
	
	public List<Integer> getUnsubscribeEntityList(){
		return this.unsubscribeEntityList.stream().distinct().collect(Collectors.toList());
	}
	
	public void setAttachments(JBAttachmentEntity attachment){
		this.attachment=attachment;
	}

	public JBAttachmentEntity getAttachments(){
		return this.attachment;
	}
	
	public void syncDataReceiver(JBEntity entity){
		entityUsers =Stream.of(entity.getAssigneelist(), entity.getFollowerlist(), entity.getOwnerlist()).flatMap(Collection::stream).distinct().collect(Collectors.toList());
		entityUsers.remove(sender);
	}
	
	public void replyEmailEntitySyncDataReceiver(JBEntity entity){
		entityUsers =Stream.of(entity.getAssigneelist(), entity.getFollowerlist(), entity.getOwnerlist()).flatMap(Collection::stream).distinct().collect(Collectors.toList());
	}
	
	public void restApiCallEntitySyncDataReceiver(JBEntity entity) {
		entityUsers =Stream.of(entity.getAssigneelist(), entity.getFollowerlist(), entity.getOwnerlist()).flatMap(Collection::stream).distinct().collect(Collectors.toList());
	}
	
	public void setItemModificationRemoveId(String modificationId) {
		this.modificationId = modificationId;
	}
	public String getItemModificationRemoveId() {
		return this.modificationId;
	}

	public List<JBUserEntity> getSyncDataReceiver(){
		return this.entityUsers;
	}
	
	public void syncDirectDataReceiver(JBEntity entity){
		entityUsers =Stream.of(entity.getDirectAssigneelist(), entity.getDirectFollowerlist(), entity.getDirectOwnerlist()).flatMap(Collection::stream).distinct().collect(Collectors.toList());
		entityUsers.remove(sender);
	}
	
	public void setDirectUsers(JBEntity entity){
		directUsers =Stream.of(entity.getDirectAssigneelist(), entity.getDirectFollowerlist(), entity.getDirectOwnerlist()).flatMap(Collection::stream).distinct().collect(Collectors.toList());
		directUsers.remove(sender);
	}
	
	public List<JBUserEntity> getDirectUsers(){
		return directUsers;
	}
	
	public void addToKanbanColumnList(JBKanbanBoardColumns kanbanColumn){
		this.kanbanColumnList.add(kanbanColumn);
	}

	public List<JBKanbanBoardColumns> getKanbanColumnList(){
		return this.kanbanColumnList;
	}
	
	public void setKanbanCardEntity(JBEntity cardEntity) {
		this.cardEntity = cardEntity;
	}
	
	public JBEntity getKanbanCardEntity() {
		return this.cardEntity;
	}
	
	public void setKanbanSourceColumn(JBKanbanBoardColumns kanbanSrcColumn) {
		this.kanbanSrcColumn = kanbanSrcColumn;
	}
	
	public JBKanbanBoardColumns getKanbanSourceColumn() {
		return this.kanbanSrcColumn;
	}
	
	public void setIsColumnAdd(boolean isColumnAdd) {
		this.isColumnAdd = isColumnAdd;
	}
	public boolean getIsColumnAdd() {
		return this.isColumnAdd;	
	}
	public void setIsColumnDelete(boolean isColumnDelete) {
		this.isColumnDelete = isColumnDelete;
	}
	public boolean getIsColumnDelete() {
		return this.isColumnDelete;	
	}
	
	public void setColumnEntitiesSortList(List<JBEntity> sortEntities) {
		this.sortEntities = sortEntities;
	}
	public List<JBEntity> getColumnEntitiesSortList() {
		return this.sortEntities;
	}
	public void addToActionList(JBActionlist actionList){
		actionLists.add(actionList);
	}

	public List<JBActionlist> getActionLists(){
		return actionLists;
	}
	public void setTopLebelProjectEntity(JBEntity topLebelEntity) {
		this.topLebelEntity = topLebelEntity;
	}
	public JBEntity getTopLebelProjectEntity() {
		return this.topLebelEntity;
	}
	
	public void setAssingedUserId(Integer assignedUserid) {
		this.assignedUserid = assignedUserid;
	}
	public Integer getAssingedUserId() {
		return this.assignedUserid;
	}
	
	
	public synchronized void addHistoryNotification(JBActionlistHistory notification){
		SeparateSyncProcessData data = new SeparateSyncProcessData();
		data.setNotification(copyDataBeanToSyncBean.getSyncJBActonlistHistory(notification));
		data.setUserId(notification.getUserid());
		data.setEventType(eventType);
		data.setComments(copyDataBeanToSyncBean.getSyncJBChatMessageEntity(this.message));
		data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
		data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
		data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
		
		syncData.put(notification.getUserid(), data);
	}
	
	public synchronized void addComments(){
		
		entityUsers.forEach(e->{
			SeparateSyncProcessData data = new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setComments(copyDataBeanToSyncBean.getSyncJBChatMessageEntity(this.message));
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			syncData.put(e.getId(), data);
		});
		
	}
	
	public synchronized void removeComments(int id){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data = new SeparateSyncProcessData();
			com.uppstreymi.jibby.backend.sync.bean.JBChatMessageEntity chatmessage=new com.uppstreymi.jibby.backend.sync.bean.JBChatMessageEntity();
			chatmessage.setId(id);
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setComments(chatmessage);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void addHistory(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data = new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setHistorylist(copyDataBeanToSyncBean.getSyncJBItemModificationList(getHistoryList()));
			if(getNotifyUserList().size()>0 && getNotifyUserList().contains(e.getId())){
				data.setNotification(copyDataBeanToSyncBean.getSyncJBActonlistHistory(getNotification()));
			}
			if(getRemoveProjectUserList().size()>0 && getRemoveProjectUserList().contains(e.getId())) {
				data.setColAProjectRemove(true);
			}
			if(getProjectAddUserList().size()>0 && getProjectAddUserList().contains(e.getId())) {
				data.setColAProjectAdd(true);
			}
			data.setIsMoveItem(isMoveItem);
			data.setSourceParentId(sourceParentId);
			data.setHasAttachment(hasAttachment);
			data.setHasChild(hasChild);
			data.setHasCheckItem(hasCheckItem);
			data.setItemModificationRemoveId(getItemModificationRemoveId());
			data.setAssigneeList(copyDataBeanToSyncBean.getSyncUserList(getAssigneeList()));
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			if(getUserRoleList().size()>0){
				data.setUserRolelist(copyDataBeanToSyncBean.getSyncJBUserRoleEntityList(getUserRoleList()));
			}
			data.setIsRemoveUserRole(getIsRemoveUserRole());
			data.setOldEntity(copyDataBeanToSyncBean.getSyncEntity(this.oldEntity));
			data.setIsAttachmentIcon(getIsAttachmentIcon());
			data.setSourceEntity(copyDataBeanToSyncBean.getSyncEntity(this.sourceEntity));
			data.setChecklists(copyDataBeanToSyncBean.getSyncJBChecklistItemEntityList(getChecklists()));
			data.setActionlists(copyDataBeanToSyncBean.getSyncJBActionlistList(getActionLists()));
			data.setTopLebelProjectEntity(copyDataBeanToSyncBean.getSyncEntity(getTopLebelProjectEntity()));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
	
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void addAttachment(){
		
		entityUsers.forEach(e->{
			SeparateSyncProcessData data = new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setAttachments(copyDataBeanToSyncBean.getSyncJBAttachmentEntity(this.attachment));
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setIsAttachmentIcon(getIsAttachmentIcon());
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			data.setItemModificationRemoveId(getItemModificationRemoveId());
			data.setSourceEntity(copyDataBeanToSyncBean.getSyncEntity(sourceEntity));
			
			syncData.put(e.getId(), data);
		});
		
	}
	
	public synchronized void addNewItem(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data = new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			if(getNotifyUserList().size()>0 && getNotifyUserList().contains(e.getId())){
				data.setNotification(copyDataBeanToSyncBean.getSyncJBActonlistHistory(getNotification()));
			}
			data.setHasAttachment(hasAttachment);
			data.setHasChild(hasChild);
			data.setHasCheckItem(hasCheckItem);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			data.setIsNewProject(this.isNewProject);
			data.setDirectUsers(copyDataBeanToSyncBean.getSyncUserList(directUsers));
			data.setIsMoveItem(isMoveItem);
			data.setActionlists(copyDataBeanToSyncBean.getSyncJBActionlistList(getActionLists()));
			data.setTopLebelProjectEntity(copyDataBeanToSyncBean.getSyncEntity(getTopLebelProjectEntity()));
			if(isMoveItem){
				data.setSourceParentId(sourceParentId);
			}
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void deleteEntityItem(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data = new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setIsTopLabelProjectRemove(getIsTopLabelProjectRemove());
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void unsubscribeFromEntity(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data=new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setHistorylist(copyDataBeanToSyncBean.getSyncJBItemModificationList(getHistoryList()));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			data.setUnsubscribeEntitylist(getUnsubscribeEntityList());
			
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void changeKanbanColumn(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data=new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			data.setKanbanColumnList(copyDataBeanToSyncBean.getSyncJBKanbanBoardColumnsList(getKanbanColumnList()));
			data.setIsColumnAdd(getIsColumnAdd());
			data.setIsColumnDelete(getIsColumnDelete());
			data.setColumnEntitiesSortList(copyDataBeanToSyncBean.getSyncJBEntityList(getColumnEntitiesSortList()));
			
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void addKanbanCard(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data=new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			data.setKanbanCardEntity(copyDataBeanToSyncBean.getSyncEntity(getKanbanCardEntity()));
			data.setKanbanColumnList(copyDataBeanToSyncBean.getSyncJBKanbanBoardColumnsList(getKanbanColumnList()));
			data.setKanbanSourceColumn(copyDataBeanToSyncBean.getSyncJBKanbanBoardColumns(getKanbanSourceColumn()));
			
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void deleteKanbanCard(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data=new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
			data.setKanbanCardEntity(copyDataBeanToSyncBean.getSyncEntity(getKanbanCardEntity()));
			data.setKanbanColumnList(copyDataBeanToSyncBean.getSyncJBKanbanBoardColumnsList(getKanbanColumnList()));
			
			syncData.put(e.getId(), data);
		});
	}

	public HashMap<Integer,SeparateSyncProcessData> getSyncData(){
		return syncData;
	}
	
	public synchronized void checklistAssigned(){
		entityUsers.forEach(e->{
			SeparateSyncProcessData data=new SeparateSyncProcessData();
			data.setUserId(e.getId());
			data.setEventType(eventType);
			if(getNotifyUserList().size()>0 && getNotifyUserList().contains(e.getId())){
				data.setNotification(copyDataBeanToSyncBean.getSyncJBActonlistHistory(getNotification()));
			}
			data.setAssigneeList(copyDataBeanToSyncBean.getSyncUserList(getAssigneeList()));
			data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
			data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
			data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
			data.setChecklists(copyDataBeanToSyncBean.getSyncJBChecklistItemEntityList(getChecklists()));
			data.setActionlists(copyDataBeanToSyncBean.getSyncJBActionlistList(getActionLists()));
			data.setTopLebelProjectEntity(copyDataBeanToSyncBean.getSyncEntity(getTopLebelProjectEntity()));
			data.setReceiver(copyDataBeanToSyncBean.getSyncJBUserEntity(e));
	
			syncData.put(e.getId(), data);
		});
	}
	
	public synchronized void deleteAssignedItem(){
		
		SeparateSyncProcessData data=new SeparateSyncProcessData();
		data.setUserId(getAssingedUserId());
		data.setEventType(eventType);
		data.setEntityReceiver(copyDataBeanToSyncBean.getSyncUserList(entityUsers));
		data.setSender(copyDataBeanToSyncBean.getSyncJBUserEntity(sender));
		data.setEntity(copyDataBeanToSyncBean.getSyncEntity(entity));
		data.setChecklists(copyDataBeanToSyncBean.getSyncJBChecklistItemEntityList(getChecklists()));
		data.setAssingedUserId(getAssingedUserId());
		
		syncData.put(getAssingedUserId(), data);
	}

}
