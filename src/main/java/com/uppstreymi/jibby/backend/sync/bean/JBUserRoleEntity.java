package com.uppstreymi.jibby.backend.sync.bean;

import java.util.Date;
import java.util.List;


public class JBUserRoleEntity{
	public Integer id;
	public Integer entityId;
	public Integer userId;
	public Integer userRole;
	public String firstName;
	public String lastName;
	public String email;
	public String dateFormat;
	public String timeZone;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public void setEntityId(Integer entityId){
		this.entityId=entityId;
	}
	public Integer getEntityId(){
		return entityId;
	}
	
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	public Integer getUserId(){
		return this.userId;
	}
	public void setUserRole(Integer userRole){
		this.userRole=userRole;
	}
	public Integer getUserRole(){
		return userRole;
	}
	
	public void setFirstName(String firstName){
		this.firstName=firstName;
	}
	public String getFirstName(){
		return firstName;
	}
	public void setLastName(String lastName){
		this.lastName=lastName;
	}
	public String getLastName(){
		return lastName;
	}

	public void setEmail(String email){
		this.email=email;
	}
	public String getEmail(){
		return this.email;
	}
	
	public void setDateFormat(String dateFormat){
		this.dateFormat=dateFormat;
	}
	public String getDateFormat(){
		return dateFormat;
	}

	public void setTimeZone(String timeZone){
		this.timeZone=timeZone;
	}
	public String getTimeZone(){
		return timeZone;
	}
}
