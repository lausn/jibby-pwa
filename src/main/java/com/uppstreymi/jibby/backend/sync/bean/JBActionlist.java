package com.uppstreymi.jibby.backend.sync.bean;

import java.util.Date;


public class JBActionlist {

	public Integer id;
	public Integer itemtype;
	public Integer itemid;
	public Date dateadded;
	public Integer addedby;
	public String itemname;
	public Integer assignedto;
	public Date itemdatetime;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setItemtype(Integer itemtype){
		this.itemtype=itemtype;
	}
	public Integer getItemtype(){
		return this.itemtype;
	}
	
	public void setItemid(Integer itemid){
		this.itemid=itemid;
	}
	public Integer getItemid(){
		return this.itemid;
	}
	public void setDateadded(Date dateadded){
		this.dateadded=dateadded;
	}
	public Date getDateadded(){
		return this.dateadded;
	}
	public void setAddedby(Integer addedby){
		this.addedby=addedby;
	}
	public Integer getAddedby(){
		return addedby;
	}
	public void setItemname(String itemname){
		this.itemname=itemname;
	}
	public String getItemname(){
		return itemname;
	}
	public void setAssignedto(Integer assignedto){
		this.assignedto=assignedto;
	}
	public Integer getAssignedto(){
		return assignedto;
	}
	public void setItemdatetime(Date itemdatetime){
		this.itemdatetime=itemdatetime;
	}
	public Date getItemdatetime(){
		return itemdatetime;
	}
	
	
	
}
