package com.uppstreymi.jibby.backend.sync.manager;

import java.util.HashMap;

import com.uppstreymi.jibby.backend.sync.process.SeparateSyncProcessData;


public class SeparateSyncUserEvent {

	private HashMap<Integer,SeparateSyncProcessData> syncData=new HashMap<Integer,SeparateSyncProcessData>();
	
	public void setSyncData(HashMap<Integer,SeparateSyncProcessData> syncData) {
		this.syncData=syncData;
	}
	public HashMap<Integer,SeparateSyncProcessData> getSyncData(){
		return syncData;
	}
}
