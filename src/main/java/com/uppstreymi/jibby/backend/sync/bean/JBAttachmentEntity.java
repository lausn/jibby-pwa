package com.uppstreymi.jibby.backend.sync.bean;

import java.util.Date;


public class JBAttachmentEntity{
	public Integer id;
	public Integer entity;
	public Integer uploaderUser;
	public Date uploadDate;
	public String name;
	public String type;
	public Integer size;
	public String description;
	public Integer sortOrder;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	public void setUploaderUser(Integer uploaderUser){
		this.uploaderUser=uploaderUser;
	}
	public Integer getUploaderUser(){
		return this.uploaderUser;
	}
	
	public void setuploadDate(Date uploadDate){
		this.uploadDate=uploadDate;
	}
	public Date getUploadDate(){
		return this.uploadDate;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return this.type;
	}
	public void setSize(Integer size){
		this.size=size;
	}
	public Integer getSize(){
		return this.size;
	}
	public void setdescription(String description){
		this.description=description;
	}
	public String getDescription(){
		return this.description;
	}
	public void setSortOrder(Integer sortOrder){
		this.sortOrder=sortOrder;
	}
	public Integer getSortOrder(){
		return this.sortOrder;
	}
	
	
}
