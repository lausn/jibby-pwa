package com.uppstreymi.jibby.backend.sync.bean;

import java.util.Date;




public class JBChatMessageEntity{
	public Integer id;
	public Integer sender;
	public Integer entity;
	public String text;
	public Date date;
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setSender(Integer sender){
		this.sender=sender;
	}
	public Integer getSender(){
		return sender;
	}
	public void setEntity(Integer entity){
		this.entity=entity;
	}
	public Integer getEntity(){
		return this.entity;
	}
	public void setText(String text){
		this.text=text;
	}
	public String getText(){
		return this.text;
	}
	public void setDate(Date date){
		this.date=date;
	}
	public Date getDate(){
		return date;
	}
}
