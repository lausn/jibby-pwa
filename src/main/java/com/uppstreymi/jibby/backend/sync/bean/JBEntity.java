package com.uppstreymi.jibby.backend.sync.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;




public class JBEntity{
	
	public Integer id;
	public String hierarchy;
	public Integer entityType;
	public String name;
	public String description;
	public Integer status;
	public Integer progress;
	public Integer delIsTimeBased;
	public Date estimatedStartDate;
	public Date estimatedFinishDate;
	public Integer recurrenceUnit;
	public Integer recurrenceScale;
	public Integer recurrenceDuration;
	public Date recurringUnitlDate;
	public Integer worksheetType;
	public String unitName;
	public Double unitQuantity;
	public String costName;
	public Integer costPerUnit;
	public String keywords;
	public Integer priority;
	public Integer creator;
	public Date creationDate;
	public Date lastModificationDate;
	public String color;
	public Integer isPublic;
	public Integer chatEnabled;
	public Integer parentId;
	public Integer parentProjectId;
	public String kanbanColor;
	public Integer indentValue;
	
	
	public List<JBUserEntity> ownerlist = new ArrayList<JBUserEntity>();

	public List<JBUserEntity> assigneelist = new ArrayList<JBUserEntity>();

	public List<JBUserEntity> followerlist = new ArrayList<JBUserEntity>();

	public List<JBUserEntity> connectionlist = new ArrayList<JBUserEntity>();
	

	public List<JBUserEntity> directOwnerlist = new ArrayList<JBUserEntity>();

	public List<JBUserEntity> directAssigneelist = new ArrayList<JBUserEntity>();

	public List<JBUserEntity> directFollowerlist = new ArrayList<JBUserEntity>();

	public List<JBUserEntity> directConnectionlist = new ArrayList<JBUserEntity>();
	

	public List<Integer> parentArray = new ArrayList<Integer>();
	

	public void setIndentValue(Integer indentValue){
		this.indentValue=indentValue;
	}
	
	
	public Integer getIndentValue(){
		return indentValue;
	}
	
	public void setId(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}
	public Integer getParentId(){
		return this.parentId;
	}
	
	public void setParentProjectId(Integer parentProjectId){
		this.parentProjectId=parentProjectId;
	}
	public Integer getParentProjectId(){
		return this.parentProjectId;
	}
	
	public void setHierarchy(String hierarchy){
		this.hierarchy=hierarchy;
	}
	public String getHierarchy(){
		return hierarchy;
	}
	public void setEntityType(Integer entityType){
		this.entityType=entityType;
	}
	public Integer getEntityType(){
		return this.entityType;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	public void setProgress(Integer progress){
		this.progress=progress;
	}
	public Integer getProgress(){
		return progress;
	}

	public void setDescription(String description){
		this.description=description;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	
	
	public void setCreationDate(Date creationDate){
		this.creationDate=creationDate;
	}
	public Date getCreationDate(){
		return this.creationDate;
	}
	
	public void setUnitName(String unitName){
		this.unitName=unitName;
	}
	public String getUnitName(){
		return unitName;
	}
	
	public void setCostName(String costName){
		this.costName=costName;
	}
	public String getCostName(){
		return costName;
	}
	
	public void setKeywords(String keywords){
		this.keywords=keywords;
	}
	public String getKeywords(){
		return keywords;
	}
	
	public void setUnitQuantity(Double unitQuantity){
		this.unitQuantity=unitQuantity;
	}
	public Double getUnitQuantity(){
		return unitQuantity;
	}
	
	public void setColor(String color){
		this.color=color;
	}
	public String getColor(){
		return color;
	}
	
	public void setCostPerUnit(Integer costPerUnit){
		this.costPerUnit=costPerUnit;
	}
	public Integer getCostPerUnit(){
		return costPerUnit;
	}
	
	public void setPriority(Integer priority){
		this.priority=priority;
	}
	public Integer getPriority(){
		return priority;
	}
	
	public void setEstimatedStartDate(Date estimatedStartDate){
		this.estimatedStartDate=estimatedStartDate;
	}
	public Date getEstimatedStartDate(){
		return this.estimatedStartDate;
	}
	
	public void setEstimatedFinishDate(Date estimatedFinishDate){
		this.estimatedFinishDate=estimatedFinishDate;
	}
	public Date getEstimatedFinishDate(){
		return this.estimatedFinishDate;
	}
	
	public void setChatEnabled(Integer chatEnabled){
		this.chatEnabled=chatEnabled;
	}
	public Integer getChatEnabled(){
		return this.chatEnabled;
	}
	
	public void setIsPublic(Integer isPublic){
		this.isPublic=isPublic;
	}
	public Integer getIsPublic(){
		return this.isPublic;
	}
	
	public void setCreator(Integer creator){
		this.creator=creator;
	}
	
	public Integer getCreator(){
		return creator;
	}
	
	public void setWorksheetType(Integer worksheetType){
		this.worksheetType=worksheetType;
	}
	
	public Integer getWorksheetType(){
		return worksheetType;
	}
	
	public void setLastModificationDate(Date lastModificationDate){
		this.lastModificationDate=lastModificationDate;
	}
	public Date getLastModificationDate(){
		return lastModificationDate;
	}
	
	public void setRecurrenceUnit(Integer recurrenceUnit){
		this.recurrenceUnit=recurrenceUnit;
	}
	public Integer getRecurrenceUnit(){
		return recurrenceUnit;
	}
	
	public void setRecurrenceScale(Integer recurrenceScale){
		this.recurrenceScale=recurrenceScale;
	}
	public Integer getRecurrenceScale(){
		return recurrenceScale;
	}
	
	public void setRecurrenceDuration(Integer recurrenceDuration){
		this.recurrenceDuration=recurrenceDuration;
	}
	public Integer getRecurrenceDuration(){
		return recurrenceDuration;
	}
	
	public void setRecurrenceUntilDate(Date recurringUnitlDate){
		this.recurringUnitlDate=recurringUnitlDate;
	}
	public Date getRecurrenceUntilDate(){
		return recurringUnitlDate;
	}
	
	public void setDelIsTimeBased(Integer delIsTimeBased){
		this.delIsTimeBased=delIsTimeBased;
	}
	public Integer getDelIsTimeBased(){
		return delIsTimeBased;
	}
	
	public void setStatus(Integer status){
		this.status=status;
	}
	public Integer getStatus(){
		return status;
	}
	
	public void setKanbanColor(String kanbanColor){
		this.kanbanColor=kanbanColor;
		this.kanbanColor=(this.kanbanColor==null)?"#9dc4d4":this.kanbanColor;
	}
	public String getKanbanColor(){
		return this.kanbanColor=(this.kanbanColor==null)?"#9dc4d4":this.kanbanColor;
	}
	
	public void setOwnerlist(List<JBUserEntity> ownerlist){
		this.ownerlist = ownerlist;
	}
	
	public List<JBUserEntity> getOwnerlist(){
		return ownerlist;
	}
	public void setAssigneelist(List<JBUserEntity> assigneelist){
		this.assigneelist = assigneelist;
	}
	
	public List<JBUserEntity> getAssigneelist(){
		return assigneelist;
	}
	public void setFollowerlist(List<JBUserEntity> followerlist){
		this.followerlist = followerlist;
	}
	public List<JBUserEntity> getFollowerlist(){
		return followerlist;
	}
	public void setConnectionlist(List<JBUserEntity> connectionlist){
		this.connectionlist = connectionlist;
	}
	
	public List<JBUserEntity> getConnectionlist(){
		return connectionlist;
	}
	
	public void setDirectOwnerlist(List<JBUserEntity> directOwnerlist){
		this.directOwnerlist = directOwnerlist;
	}
	
	public List<JBUserEntity> getDirectOwnerlist(){
		return directOwnerlist;
	}
	public void setDirectAssigneelist(List<JBUserEntity> directAssigneelist){
		this.directAssigneelist = directAssigneelist;
	}
	
	public List<JBUserEntity> getDirectAssigneelist(){
		return directAssigneelist;
	}
	public void setDirectFollowerlist(List<JBUserEntity> directFollowerlist){
		this.directFollowerlist = directFollowerlist;
	}
	public List<JBUserEntity> getDirectFollowerlist(){
		return directFollowerlist;
	}
	public void setDirectConnectionlist(List<JBUserEntity> directConnectionlist){
		this.directConnectionlist = directConnectionlist;
	}
	
	public List<JBUserEntity> getDirectConnectionlist(){
		return directConnectionlist;
	}
	
	public void setParentArray(List<Integer> parentArray){
		this.parentArray = parentArray;
	}
	public List<Integer> getParentArray(){
		return parentArray;
	}
}
