package com.uppstreymi.jibby;

public class CEvent {

	private boolean isBack;
	
	public CEvent(boolean isBack){
		this.isBack=isBack;
	}
	
	public boolean isBack(){
		return this.isBack;
	}
}
