package com.uppstreymi.jibby.item;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.flow.helper.AsyncManager;

import com.uppstreymi.jibby.CEvent;
import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.DataChangeModelListener;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.ModelUpdateEvent;
import com.uppstreymi.jibby.backend.sync.manager.SynchronizeManager;
import com.uppstreymi.jibby.backend.sync.process.ProcessData;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.SortCriteria;
import com.uppstreymi.jibby.item.events.ItemRowAddEvent;
import com.uppstreymi.jibby.item.events.ItemSortEvent;
import com.uppstreymi.jibby.item.events.SettingItemEvent;
import com.uppstreymi.jibby.item.events.SyncModelGenerateEvent;
import com.uppstreymi.jibby.item.events.TittleEvent;
import com.uppstreymi.jibby.item.events.TopEditOrDeleteButtonEvent;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.components.navigation.bar.AppBar;
import com.uppstreymi.jibby.ui.page.ItemView;
import com.uppstreymi.jibby.utils.HasLogger;
import com.uppstreymi.jibby.utils.JibbyColBFilter;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.ModelItem;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.templatemodel.Include;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("item-page")
@JsModule("./src/views/item/item-page.js")
@UIScoped
public class ItemPage extends PolymerTemplate<ItemPage.ItemPageModel> implements HasLogger, DataChangeModelListener {

	@Id("childItems")
	Div childItems;

	@Id("addItem")
	private Button addItem;

	@Id("dialog")
	private Dialog dialog;

	@Inject
	private Event<CEvent> event;
	@Inject
	private Event<TittleEvent> tittleEvent;
	@Inject
	Event<SettingItemEvent> settingEvent;
	@Inject
	private DbService dbService;
	@Inject
	Event<TopEditOrDeleteButtonEvent> deleteOrEditButtonEvent;
	@Inject
	private HistorModel historyModel;
	@Inject
	private ProjectViewModel projectModel;
	@Inject
	private SessionModel session;
	@Inject
	private JibbyColBFilter filter;
	@Inject
	SynchronizeManager syncManager;
	@Inject
	AppBar appBar;

	private ItemBean selectedItem = null;
	private boolean isTopLabelProject = false;

	List<ItemBean> projectList;
	Integer parentId;
	List<Long> acessAllEntities = null;

	Integer ownerId = Arrays.asList(JibbyConfig.userRoles).indexOf("owner") + 1;
	Integer assigneeId = Arrays.asList(JibbyConfig.userRoles).indexOf("assignee") + 1;
	Integer followersId = Arrays.asList(JibbyConfig.userRoles).indexOf("followers") + 1;
	Integer connectionId = Arrays.asList(JibbyConfig.userRoles).indexOf("connections") + 1;

	private SyncDataChangeModel dataChangemodel;
	private UI ui;

	private Logger logger = LogManager.getLogger(ItemPage.class);

	public ItemPage() {
	}

	@PostConstruct
	private void init() {
		addItem.addClickListener(e -> {
			if (parentId != null) {
				loadItemList(parentId);
				historyModel.setAddItemParentId(parentId);
				dialog.open();
			}
		});

		dialog.addOpenedChangeListener(e -> {
			UI.getCurrent().getPage().executeJs(
					" var x = document.getElementById(\"overlay\");\n" + " x.classList.add(\"item-list-popup\");");
		});

		appBar.getTitleDiv().addClickListener(e -> {
			ItemBean item = projectModel.getItemBean();
			if (item.getIsRoles()) {
				historyModel.setSelectedItemHasChild(item.getIsChild());
				event.fire(new CEvent(true));
				getUI().ifPresent(nameevent -> nameevent.navigate(ItemDetails.class, item.getId()));

			} else {
				Notification.show("You do not have rights to view the content of this item.", 2000,
						Position.BOTTOM_CENTER);
			}
		});
	}

	public interface ItemPageModel extends TemplateModel {
		@Include({ "name", "id", "entityType", "parentId", "parentProjectId", "status", "isChild", "isRoles",
				"isAttachment", "isChecklist" })
		void setItems(List<ItemBean> items);

		List<ItemBean> getItems();

		@Include({ "name", "id", "entityType", "parentId", "parentProjectId", "status", "isChild", "isRoles" })
		void setItemList(List<ItemBean> items); // for dialog

		List<ItemBean> getItemList();// for dialog
	}

	public void setParameter(BeforeEvent arg0, Integer param) {

		if (session.getUser() == null) {
			arg0.rerouteTo(LoginPage.class);
		} else {
			deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false, false));
			if (param != null) {

				parentId = param;

				if (!historyModel.getStages().contains(param)) {
					historyModel.getParentArray().add(parentId); // add the value to parent array when forward
					event.fire(new CEvent(true));
					historyModel.getStages().add(param);

					ItemBean parentItem = historyModel.getItemBean().get(param);
					// parentItem will be null on browser refresh
					if (parentItem == null) {
						getAllAccessUser();
						JBEntity parentEntity = dbService.getEntity(param);
						parentItem = new ItemBean(parentEntity);
						parentItem.setIsRoles(
								filter.isAccess(acessAllEntities, parentEntity.getId(), parentEntity.getParentId()));
						historyModel.getItemBean().put(parentItem.getId(), parentItem);
					}

					if (setProjectList(historyModel.getItemBean().get(param))) {
						historyModel.setEntitiesByKey(param, projectList);
						ItemRowAddEvent itemRow = new ItemRowAddEvent();
						itemRow.setItemBean(historyModel.getItemBean().get(param));
						itemRow.setEntityList(projectList);
						setRow(itemRow);
					}
				} else {
					if (historyModel.getParentArray().size() > 0)
						historyModel.getParentArray().remove(historyModel.getParentArray().size() - 1); // last index
																										// remove
					// when backword
					if (historyModel.getEntitiesByKey(param) != null) {
						event.fire(new CEvent(true));
						sorting(param);
						ItemRowAddEvent itemRow = new ItemRowAddEvent();
						itemRow.setItemBean(historyModel.getItemBean().get(param));
						itemRow.setEntityList(historyModel.getEntitiesByKey(param));
						setRow(itemRow);

					}

					Integer id = historyModel.getStages().peek();
					if (!id.equals(parentId)) {
						historyModel.getStages().remove(id);
						historyModel.removeEntitiesKey(id);
					}
				}
			} else {
				if (projectModel.getDrawerActiveProject() != null) {
					Integer projectId = projectModel.getDrawerActiveProject().getId();
					parentId = projectId;
					historyModel.getStages().clear();
					if (historyModel.getEntitiesByKey(projectId) == null) {
						event.fire(new CEvent(false));
						getAllAccessUser();
						if (setProjectList(projectModel.getItemBean())) {
							ItemRowAddEvent itemRow = new ItemRowAddEvent();
							itemRow.setItemBean(projectModel.getItemBean());
							itemRow.setEntityList(projectList);
							setRow(itemRow);
							historyModel.setEntitiesByKey(projectId, projectList);
						}
					} else {
						event.fire(new CEvent(false));
						sorting(parentId);
						ItemRowAddEvent itemRow = new ItemRowAddEvent();
						itemRow.setItemBean(projectModel.getItemBean());
						itemRow.setEntityList(historyModel.getEntitiesByKey(projectId));
						setRow(itemRow);
					}
					historyModel.getParentArray().clear(); // initial the parent array
					historyModel.getParentArray().add(parentId);
				} else {
					parentId = null;
					event.fire(new CEvent(false));
					ItemRowAddEvent itemRow = new ItemRowAddEvent();
					itemRow.setItemBean(null);
					itemRow.setEntityList(new ArrayList<ItemBean>());
					setRow(itemRow);

				}
			}
		}
	}

	public void setRow(@Observes ItemRowAddEvent addevent) {

		getModel().setItems(addevent.getEntityList());
		TittleEvent te = new TittleEvent();
		te.setTittle(addevent.getItemBean() == null ? "" : addevent.getItemBean().getName());
		te.setIsProjectItem(
				addevent.getItemBean() == null ? false : addevent.getItemBean().getParentId() == null ? true : false);
		tittleEvent.fire(te);
		SettingItemEvent myevent = new SettingItemEvent(false, true);
		settingEvent.fire(myevent);
	}

	public List<JBEntity> getProjectList(JBEntity entityParent) {
		return dbService.getAllChildByEntityId(entityParent.getId());
	}

	@EventHandler
	private void _nextView(@ModelItem ItemBean item) {
		historyModel.getItemBean().put(item.getId(), item);
		getUI().ifPresent(ee -> ee.navigate(ItemView.class, item.getId()));
	}

	public boolean setProjectList(ItemBean itemBean) {
		projectList = new ArrayList<ItemBean>();
		try {
			List<Object[]> immediateItem = dbService.getAllEntitiesByParentAndStatusOrderBy(itemBean.getId(),
					session.getUser(), projectModel.getSortOption().getCriteria(),
					projectModel.getSortOption().isAsc());

			List<Object[]> allEntities = (List<Object[]>) dbService.getAllItemByProjectId(
					itemBean.getParentProjectId() == null ? itemBean.getId() : itemBean.getParentProjectId(),
					session.getUser().getId());
			List<Long> acessEntities = null;

			if (!itemBean.getIsRoles()) {
				acessEntities = acessAllEntities;
			}

			for (Object[] entityObjects : immediateItem) {
				JBEntity entity = (JBEntity) entityObjects[0];
				ItemBean item = new ItemBean(entity);
				// return true For show items in tree OR not show;
				boolean entityProperties = getItem(entity, entity.getId(), allEntities, acessEntities,
						itemBean.getIsRoles());

				if (entityProperties) {
					item.setIsChild(true);
					item.setIsRoles(itemBean.getIsRoles() ? true
							: filter.isAccess(acessAllEntities, entity.getId(), entity.getParentId()));
					projectList.add(item);
				} else if (!entityProperties && isValidFilter(entity, acessEntities, itemBean.getIsRoles())) {
					item.setIsChild(false);
					item.setIsRoles(itemBean.getIsRoles() ? true
							: filter.isAccess(acessAllEntities, entity.getId(), entity.getParentId()));
					projectList.add(item);
				}

				if (Integer.parseInt(entityObjects[2] + "") > 0) {
					item.setIsChecklist(true);
				}

				if (Integer.parseInt(entityObjects[3] + "") > 0) {
					item.setIsAttachment(true);
				}
			}
			return true;

		} catch (Exception e) {
			logger.error("An exception thrown in mobile item load", e);
			return true;
		}

	}

	public boolean getItem(JBEntity entity, int id, List<Object[]> entities2, List<Long> accessEntities,
			boolean isUserRole) {
		for (Object[] en : entities2) {
			long pid = (long) en[1];
			if (pid == id) {
				if (isValidFilter(en, accessEntities, entity, isUserRole)) {
					return true;
				} else {
					BigInteger itemid = (BigInteger) en[0];
					boolean value = getItem(entity, itemid.intValue(), entities2, accessEntities, isUserRole);
					if (value) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean isValidFilter(JBEntity entity, List<Long> acessEntities, boolean isUserRole) {
		return filter.isValidEntity(entity, acessEntities, isUserRole);
	}

	public boolean isValidFilter(Object[] entity, List<Long> acessEntities, JBEntity parentEntity, boolean isUserRole) {
		return filter.isValidEntity(entity, acessEntities, parentEntity, isUserRole);
	}

	public void getAllAccessUser() {
		String userRoles = "";
		for (Integer userRole : projectModel.getUserRole()) {
			if (!userRoles.isEmpty())
				userRoles += ',';
			userRoles += userRole;
		}
		acessAllEntities = dbService.getAllAcessEntity(session.getUser().getId(), userRoles,
				projectModel.getDrawerActiveProject().getId());
		acessAllEntities = acessAllEntities == null ? new ArrayList<Long>() : acessAllEntities;

		if (projectModel.getItemBean() != null)
			projectModel.getItemBean()
					.setIsRoles(filter.isAccess(acessAllEntities, projectModel.getItemBean().getId()));
	}

	public void loadProjectFirstTime(@Observes ItemSortEvent itemSortEvent) {
		if (itemSortEvent.isSorting()) {
			saveActiveUI();
			itemSort();
		}
	}

	private void saveActiveUI() {
		String json = Utils.jsonBuilder(session.getActiveViewDTO());
		if (json != null) {
			session.getUser().setActiveUI(json);
			dbService.saveUser(session.getUser());
		}
	}

	public void itemSort() {
		if (parentId != null) {
			sorting(parentId);
			getModel().setItems(historyModel.getEntitiesByKey(parentId));
		}
	}

	@EventHandler
	private void _showdetails(@ModelItem ItemBean item) {
		if (item.getIsRoles()) {
			historyModel.setSelectedItemHasChild(item.getIsChild());
			event.fire(new CEvent(true));
			getUI().ifPresent(nameevent -> nameevent.navigate(ItemDetails.class, item.getId()));

		} else {
			Notification.show("You do not have rights to view the content of this item.", 2000, Position.BOTTOM_CENTER);
		}
	}

	private void sorting(Integer param) {

		SortCriteria sc = projectModel.getSortOption();

		Collections.sort(historyModel.getEntitiesByKey(param), new Comparator<ItemBean>() {

			@Override
			public int compare(ItemBean o1, ItemBean o2) {

				if (sc.getCriteria().equalsIgnoreCase("a.name"))
					return sc.isAsc() ? o1.getName().compareToIgnoreCase(o2.getName())
							: o2.getName().compareToIgnoreCase(o1.getName());
				else if (sc.getCriteria().equalsIgnoreCase("a.creationDate") && o1.getCreationDate() != null
						&& o2.getCreationDate() != null)
					return sc.isAsc() ? o1.getCreationDate().compareTo(o2.getCreationDate())
							: o2.getCreationDate().compareTo(o1.getCreationDate());
				else if (sc.getCriteria().equalsIgnoreCase("a.estimatedStartDate") && o1.getEstimatedStartDate() != null
						&& o2.getEstimatedStartDate() != null)
					return sc.isAsc() ? o1.getEstimatedStartDate().compareTo(o2.getEstimatedStartDate())
							: o2.getEstimatedStartDate().compareTo(o1.getEstimatedStartDate());
				else if (sc.getCriteria().equalsIgnoreCase("a.estimatedFinishDate")
						&& o1.getEstimatedFinishDate() != null && o2.getEstimatedFinishDate() != null)
					return sc.isAsc() ? o1.getEstimatedFinishDate().compareTo(o2.getEstimatedFinishDate())
							: o2.getEstimatedFinishDate().compareTo(o1.getEstimatedFinishDate());
				else if (sc.getCriteria().equalsIgnoreCase("a.keywords"))
					return sc.isAsc() ? o1.getKeywords().compareToIgnoreCase(o2.getKeywords())
							: o2.getKeywords().compareToIgnoreCase(o1.getKeywords());
				else
					return 0;
			}
		});

	}

	public void loadItemList(Integer parentId) {
		ItemRowAddEvent itemRow = new ItemRowAddEvent();
		itemRow.setItemBean(historyModel.getItemBean().get(parentId));
		itemRow.setEntityList(historyModel.getEntitiesByKey(parentId));

		setDialogItemRows(itemRow); // for dialog
	}

	public void setDialogItemRows(ItemRowAddEvent itemRow) {
		List<ItemBean> entityList = new ArrayList<>();
		entityList.addAll(itemRow.getEntityList());

		// parent item add before all child list
		entityList.add(0, itemRow.getItemBean() == null ? projectModel.getItemBean() : itemRow.getItemBean());
		getModel().setItemList(entityList);
	}

	@EventHandler
	private void _selectItem(@ModelItem ItemBean item) {
		selectedItem = item;
		isTopLabelProject = false;
	}

	@EventHandler
	private void _makeTopLabelProject() {
		isTopLabelProject = true;
		selectedItem = null;
	}

	@EventHandler
	private void _btnDialogOk() {
		if (selectedItem == null && isTopLabelProject) {
			TittleEvent te = new TittleEvent();
			te.setTittle("Make new top label project");
			tittleEvent.fire(te);
			dialog.close();
			getUI().ifPresent(ee -> ee.navigate(AddItem.class, -1));
			isTopLabelProject = false;
			selectedItem = null;
		} else if (selectedItem != null && !isTopLabelProject) {
			// change the title name to selected
			TittleEvent te = new TittleEvent();
			te.setTittle(selectedItem.getName());
			tittleEvent.fire(te);
			dialog.close();
			getUI().ifPresent(ee -> ee.navigate(AddItem.class, selectedItem.getId()));
			selectedItem = null;
			isTopLabelProject = false;
		} else
			Notification.show("select a item first");
	}

	@EventHandler
	private void _btnDialogCancel() {

		UI.getCurrent().getPage().executeJs(
				"var selectedItem = document.getElementById(\"overlay\").shadowRoot.querySelector('#content').shadowRoot.querySelector('.active');\n"
						+ "	\n" + "		if(selectedItem != null){\n"
						+ "	     	selectedItem.classList.remove(\"active\");\n" + "} ");

		dialog.close();
		selectedItem = null;
	}

	private void firstTimeSyncGenerate(@Observes SyncModelGenerateEvent se) {

		if (se.isChange() && session.getUser() != null) {
			if (dataChangemodel == null)
				dataChangemodel = syncManager.getModel(session.getUser().getId());
		}
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		this.ui = attachEvent.getUI();
		if (this.ui != null)
			this.ui.getPushConfiguration().setPushMode(PushMode.MANUAL);
		if (dataChangemodel != null) {
			dataChangemodel.addDataChangeListener(this);
		}
	}

	@Override
	protected void onDetach(DetachEvent detachEvent) {
		this.ui = null;
		if (dataChangemodel != null) {
			dataChangemodel.removeDataChangeListener(this);
		}
	}

	@Override
	public void onChanged(ModelUpdateEvent event) {
		updateUI(event.getProcessData());
	}

	private void updateUI(ProcessData data) {

		try {
			if (this.ui != null)
				this.ui.access(() -> {
					AsyncManager.register(this.ui, asyncTask -> {
						asyncTask.push(() -> {
							updateSyncTree(data);
							ui.push();
						});
					});
				});
		} catch (Exception e) {
			logger.error("An exception thrown in itempage updateUI", e);
		}
	}

	private void updateSyncTree(ProcessData data) {
		if (data.getEventType() == SyncEventType.EVENT_ENTITY_CHANGED && data.getEntity() != null && parentId != null) {
			if (data.isMoveItem()) {

			} else {
				itemChangeSyncInTree(data);
			}
		} else if (data.getEventType() == SyncEventType.EVENT_NEW_ITEM_ADD && !data.getIsNewProject()
				&& data.getReceiver() != null && data.getEntity() != null && parentId != null && itemFilter(data)) {
			/* new item add */
			syncItemAdd(data);
		} else if (data.getEventType() == SyncEventType.EVENT_ENTITY_ATTACHMENT_ADD && data.getEntity() != null
				&& data.getReceiver() != null) {

			if (historyModel.getEntitiesByKey(data.getEntity().getParentId()) != null) {
				List<ItemBean> items = historyModel.getEntitiesByKey(data.getEntity().getParentId());
				ItemBean bean = items.stream().filter(x -> x.getId().equals(data.getEntity().getId())).findAny()
						.orElse(null);
				if (bean != null) {
					bean.setIsAttachment(true);
					ItemBean itembean = getModel().getItems().stream()
							.filter(x -> x.getId().equals(data.getEntity().getId())).findAny().orElse(null);
					if (itembean != null)
						itembean.setIsAttachment(true);
				}
			}

		} else if (data.getEventType() == SyncEventType.EVENT_ENTITY_ATTACHMENT_REMOVE && data.getEntity() != null
				&& data.getItemModificationRemoveId() != null && data.getReceiver() != null) {

		}

		else if (data.getEventType() == SyncEventType.EVENT_ENTITY_REMOVE && data.getEntity() != null) {
			removeItemFromItemPage(data);
		} else if (data.getEventType() == SyncEventType.EVENT_ENTITY_UNSUBSCRIBE && data.getEntity() != null
				&& data.getUnsubscribeEntitylist() != null && data.getUnsubscribeEntitylist().size() > 0) {
			unsubscribeFromEntity(data);
		}
	}

	private void removeItemFromItemPage(ProcessData data) {
		List<ItemBean> items = historyModel.getEntitiesByKey(data.getEntity().getParentId());
		items.removeIf(e -> e.getEntity().getId().equals(data.getEntity().getId()));
		getModel().setItems(items);
	}

	private void unsubscribeFromEntity(ProcessData data) {
		JBEntity entity = data.getEntity();
		for (Integer entityId : data.getUnsubscribeEntitylist()) {
			if (historyModel.getItemBean().get(entityId) != null) {
				ItemBean itemBean = (ItemBean) historyModel.getItemBean().get(entityId);
				itemBean.getEntity().getDirectOwnerlist().remove(data.getSender());
				itemBean.getEntity().getDirectAssigneelist().remove(data.getSender());
				itemBean.getEntity().getDirectFollowerlist().remove(data.getSender());
				itemBean.getEntity().getDirectConnectionlist().remove(data.getSender());

				if (projectModel.getItemBean() != null
						&& projectModel.getItemBean().getEntity().getId().equals(entityId)) {
					projectModel.getItemBean().setEntity(entity);
					projectModel.setItemBean(itemBean);
				}
			}
		}
	}

	private void itemChangeSyncInTree(ProcessData data) {
		JBEntity entity = data.getEntity();
		Integer status = entity.getStatus();
		if (parentId.equals(data.getEntity().getParentId()) && historyModel.getEntitiesByKey(parentId) != null) {
			List<ItemBean> items = historyModel.getEntitiesByKey(parentId);
			ItemBean itembean = items.stream().filter(item -> item.getId().equals(data.getEntity().getId())).findAny()
					.orElse(null);
			if (itembean != null) {
				itembean.setEntity(entity);

				if (data.getOldEntity() != null && data.getOldEntity().getStatus() != entity.getStatus()) {
					if (projectModel.getStatus().size() <= 1 && !projectModel.getStatus().contains(status)
							&& !itembean.getIsChild()) {
						items.remove(itembean);
					}
				}
				getModel().setItems(items);
			} else {
				if (itemFilter(data)) {
					addNewSynData(data);
				}
			}
		} else if (parentId.equals(data.getEntity().getId())) {
			TittleEvent te = new TittleEvent();
			te.setTittle(data.getEntity().getName());
			tittleEvent.fire(te);
		}
	}

	private void syncItemAdd(ProcessData data) {

		if (parentId.equals(data.getEntity().getParentId()) && historyModel.getEntitiesByKey(parentId) != null) {
			ItemBean itembean = getModel().getItems().stream()
					.filter(item -> item.getId().equals(data.getEntity().getId())).findAny().orElse(null);
			if (itembean == null) {
				addNewSynData(data);
			}
		} else {
			List<ItemBean> items = historyModel.getEntitiesByKey(parentId);
			ItemBean itembean = items.stream().filter(item -> item.getId().equals(data.getEntity().getParentId()))
					.findAny().orElse(null);
			if (itembean != null) {
				itembean.setIsChild(true);
				getModel().setItems(items);
			}

		}
	}

	private void addNewSynData(ProcessData data) {
		// ItemBean
		ItemBean item = new ItemBean(data.getEntity());
		item.setIsChild(data.getHasChild());
		item.setIsRoles(true);
		item.setIsChecklist(false);
		item.setIsAttachment(data.getHasAttachment());
		historyModel.getEntitiesByKey(parentId).add(item);
		itemSort();
	}

	private boolean itemFilter(ProcessData data) {
		// -5: not found
		Integer ownerId = data.getEntity().getOwnerlist().contains(data.getReceiver()) ? 1 : -5;
		Integer assignerId = data.getEntity().getAssigneelist().contains(data.getReceiver()) ? 2 : -5;
		Integer followerId = data.getEntity().getFollowerlist().contains(data.getReceiver()) ? 3 : -5;

		if ((projectModel.getUserRole().contains(ownerId) || projectModel.getUserRole().contains(assignerId)
				|| projectModel.getUserRole().contains(followerId))
				&& projectModel.isPublic() == data.getEntity().getIsPublic()) {
			return filter.isValidEntity(data.getEntity(), null, true);
		} else
			return false;

	}
}
