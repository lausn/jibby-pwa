package com.uppstreymi.jibby.item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.item.events.ChangeCheckListEvent;
import com.uppstreymi.jibby.item.events.MyActionlistAssignedEvent;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.templatemodel.Include;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("checklist-page")
@JsModule("./src/views/item/checklist-page.js")
@UIScoped
public class ChecklistPage extends PolymerTemplate<ChecklistPage.ChecklistPageModel> {

	@Id("tfChecklist")
	private TextArea tfChecklist;
	
	private JBEntity entity;
	ContextMenu contextMenu;

	@Inject
	private SessionModel session;
	@Inject
	private DbService service;
	@Inject
	private HistorModel historyModel;
	@Inject
	private MyactionlistpopupPage myactionlistpopup;
	@Inject
	private Event<ChangeCheckListEvent> checklistChangeEvent;
	@Inject
	private Event<MyActionlistAssignedEvent> myActionEvent;
	@Inject
	private SyncUserEvent userEvent;

	private Logger logger=LogManager.getLogger(ChecklistPage.class);

	JBChecklistItemEntity checkitem = null;
	Dialog dl = new Dialog();
	boolean isEnter;

	public ChecklistPage() {
		getModel().setIsAssignee(false);
		tfChecklist.setValueChangeMode(ValueChangeMode.EAGER);
	}

	public ChecklistPageModel getChecklistModel() {
		return getModel();
	}

	@PostConstruct
	private void init() {
		tfChecklist.addKeyPressListener(Key.ENTER, e -> {
			saveCheckList();
		});
	}

	private void saveCheckList() {
		if (!tfChecklist.getValue().trim().isEmpty()) {

			if (this.checkitem != null) {

				JBChecklistItemEntity ckItem = new JBChecklistItemEntity();
				ckItem.setId(checkitem.getId());
				ckItem.setEntity(checkitem.getEntity());
				ckItem.setIsChecked(checkitem.getIsChecked());
				ckItem.setName(tfChecklist.getValue().trim());
				ckItem.setSortOrder(checkitem.getSortOrder());

				service.saveChecklistItems(ckItem);
				userEvent.setEventType(SyncEventType.EVENT_CHECKLIST_EDIT);
				insertItemModification(entity,
						Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("editchecklist") + 1, ckItem);

				userEvent.addHistory();
				userEvent.eventProcessor();

				JBChecklistItemEntity cklist = getModel().getChecklists().stream()
						.filter(c -> c.getId().equals(ckItem.getId())).findAny().orElse(null);
				if (cklist != null) {
					cklist.setName(tfChecklist.getValue().trim());
				}
				Notification.show("Edit successfully", 2000, Position.BOTTOM_CENTER);
			} else {
				userEvent.setEventType(SyncEventType.EVENT_CHECKLIST_ADD);
				JBEntity oldEntity = new JBEntity();
				oldEntity.copyEntity(entity);
				userEvent.setOldEntity(oldEntity);

				JBChecklistItemEntity checklist = new JBChecklistItemEntity();
				checklist.setEntity(entity.getId());
				checklist.setIsChecked(JibbyConfig.checkItemStatus[1]);
				checklist.setName(tfChecklist.getValue().trim());
				checklist.setSortOrder(0);
				service.saveChecklistItems(checklist);

				insertItemModification(entity,
						Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("addchecklist") + 1, checklist);
				addChecklistToWrapper(checklist);

				userEvent.setHasCheckItem(true);
				userEvent.addHistory();
				userEvent.eventProcessor();

				Notification.show("Added successfully", 2000, Position.BOTTOM_CENTER);
				changeItemProgressStatus();
			}
			this.checkitem = null;
			tfChecklist.clear();

		}
	}

	private void addChecklistToWrapper(JBChecklistItemEntity checklist) {
		List<JBChecklistItemEntity> checklists = getModel().getChecklists();
		checklists.add(checklist);
		getModel().setChecklists(checklists);
	}

	public interface ChecklistPageModel extends TemplateModel {

		@Include({ "name", "id", "isChecked", "sortOrder", "entity" })
		void setChecklists(List<JBChecklistItemEntity> items);

		void setIsAssignee(Boolean isAssignee);

		List<JBChecklistItemEntity> getChecklists();

	}

	public void setup() {
		getModel().setChecklists(new ArrayList<>());
		getModel().setIsAssignee(false);
		tfChecklist.clear();
		this.checkitem = null;
		this.entity = historyModel.getSelectedEntity();
	}

	private void insertItemModification(JBEntity entity, Integer type, JBChecklistItemEntity checklist) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entity.getId());
		itemModification.setUserId1(session.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		userEvent.addToHistoryList(itemModification);
		userEvent.setEntity(entity);
		userEvent.addToChecklist(checklist);
		userEvent.setSender(session.getUser());
		userEvent.syncDataReceiver(entity);

		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity
						: service.getEntity(entity.getParentProjectId()));
	}

	@ClientCallable
	private void checklistChange(JBChecklistItemEntity checkitem) {

		JBChecklistItemEntity chk = new JBChecklistItemEntity();
		chk.setId(checkitem.getId());
		chk.setIsChecked(checkitem.getIsChecked() == 1 ? 0 : 1);
		chk.setEntity(checkitem.getEntity());
		chk.setName(checkitem.getName());
		chk.setSortOrder(checkitem.getSortOrder());
		service.saveChecklistItems(chk);
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem") + 1;
		if (chk.getIsChecked().equals(1)) {
			// for notification if user is assignee for this checklist
			taskCompletedByAssignee(chk.getId(), itemtype, chk.getName());
			insertItemModification(entity,
					Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("donechecklist") + 1, chk);
			removeCheckItemFromMyactionList(chk.getId());
		} else {
			// for notification if user is assiger of this item
			markUndone(itemtype, chk.getId(), chk.getName());
			insertItemModification(entity,
					Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undonechecklist") + 1, chk);
		}

		JBChecklistItemEntity cklist = getModel().getChecklists().stream()
				.filter(c -> c.getId().equals(checkitem.getId())).findAny().orElse(null);
		if (cklist != null) {
			cklist.setIsChecked(chk.getIsChecked());
		}
		changeItemProgressStatus();
	}

	public void removeCheckItemFromMyactionList(Integer itemid) {
		List<JBActionlist> actionlists = service.getActionlistByEntityIdAndTypeAndAddedBy(itemid,
				Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem") + 1, session.getUser().getId());
		for (JBActionlist actionlist : actionlists) {
			service.deleteMyActionListRow(actionlist.getId());
		}
	}

	public void changeItemProgressStatus() {
		List<JBChecklistItemEntity> checkListData = service.getChecklistByEntity(entity.getId());

		Integer progress = 0;
		Integer totalchk = 0;
		Integer totalchkList = checkListData.size();

		for (JBChecklistItemEntity checklist : checkListData) {
			if (checklist.getIsChecked() == JibbyConfig.checkItemStatus[0]) {
				totalchk++;
			}
		}
		if (totalchkList > 0)
			progress = Integer
			.parseInt(BigDecimal.valueOf((Double.parseDouble(totalchk.toString()) / totalchkList) * 100)
					.setScale(0, BigDecimal.ROUND_HALF_UP).toString());

		if (progress == 100)
			entity.setStatus(JibbyConfig.status[0]);
		else
			entity.setStatus(JibbyConfig.status[1]);

		entity.setProgress(progress);
		service.saveEntity(entity);
		checklistChangeEvent.fire(new ChangeCheckListEvent(true));

	}

	private void taskCompletedByAssignee(Integer itemId, Integer itemType, String itemName) {

		List<?> assignersData = service.getAllAssignerByEntity(itemId, itemType, session.getUser().getId());

		for (Object assigner : assignersData) {
			String userName = session.getUser().getFirstName() + " " + session.getUser().getLastName();
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(itemId);
			actionlistHistory.setItemtype(itemType);
			actionlistHistory.setUserid(Integer.parseInt(assigner.toString()));
			actionlistHistory.setMessage(userName + " completed the task  \"" + itemName + "\".");
			actionlistHistory
			.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("markedByAssignee") + 1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);
			service.saveActionlistHistory(actionlistHistory);

		}
	}

	private void markUndone(Integer itemType, Integer itemId, String itemName) {
		String userName = session.getUser().getFirstName() + " " + session.getUser().getLastName();
		List<JBUserEntity> assigneeList = service.getAllAssigneeUserById(itemId, itemType);
		if (assigneeList.contains(session.getUser())) {
			assigneeList.remove(session.getUser());
		}

		for (JBUserEntity assignee : assigneeList) {
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(itemId);
			actionlistHistory.setItemtype(itemType);
			actionlistHistory.setUserid(assignee.getId());
			actionlistHistory.setMessage(userName + "  marked the task \"" + itemName + "\" as Not done.");
			actionlistHistory
			.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser") + 1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);
			service.saveActionlistHistory(actionlistHistory);

		}
	}

	@ClientCallable
	private void _onCheckListBtnClick(JBChecklistItemEntity checkitem) {
		this.checkitem = checkitem;
	}

	@ClientCallable
	private void _onEditItem(JBChecklistItemEntity checkitem) {
		this.checkitem = checkitem;
		tfChecklist.setValue(checkitem.getName());
		tfChecklist.focus();
	}

	@ClientCallable
	private void _onDeleteItem(JBChecklistItemEntity checkitem) {
		getModel().getChecklists().removeIf(filter -> filter.getId().equals(checkitem.getId()));
		service.deleteCheckItem(checkitem);
		userEvent.setEventType(SyncEventType.EVENT_CHECKLIST_REMOVE);
		insertItemModification(entity, Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removechecklist") + 1,
				checkitem);
		userEvent.addHistory();
		userEvent.eventProcessor();
		tfChecklist.clear();
		this.checkitem = null;
		changeItemProgressStatus();
	}

	@ClientCallable
	private void _onAddItem(JBChecklistItemEntity checkitem) {
		convertChecklistToItem(checkitem);
	}

	private void convertChecklistToItem(JBChecklistItemEntity checkList) {

		Integer maxChar = 255;
		String name = checkList.getName().length() > maxChar ? checkList.getName().substring(0, maxChar)
				: checkList.getName();
		Integer parentProjectId = entity.getParentProjectId() == null ? entity.getId() : entity.getParentProjectId();

		JBEntity entity = new JBEntity();
		entity.setName(name);
		entity.setParentId(checkList.getEntity());
		entity.setParentProjectId(parentProjectId);
		entity.setEstimatedStartDate(new Date());
		entity.setEstimatedFinishDate(Utils.addDaysStartTime(new Date(), 1));
		entity.setUnitQuantity(Double.valueOf("0.00"));
		entity.setChatEnabled(1);
		entity.setIsPublic(0);
		entity.setDescription("");
		Integer index = Arrays.asList(JibbyConfig.entityTypes).indexOf("Task");
		entity.setEntityType(index + 1);
		entity.setColor("#000000");
		entity.setKanbanColor(JibbyConfig.defaultKanbanColor);
		entity.setUnitName("");
		entity.setCostName("");
		entity.setCostPerUnit(0);
		entity.setKeywords("");
		Integer defaultPriority = Arrays.asList(JibbyConfig.priorities).indexOf(JibbyConfig.priorities[1]) + 1; // default item = medium

		entity.setPriority(defaultPriority);
		entity.setLastModificationDate(new Date());
		entity.setCreationDate(new Date());
		entity.setCreator(session.getUser().getId());

		entity.setProgress(0);
		entity.setStatus(JibbyConfig.status[1]);
		entity.setHierarchy("");
		entity.setDelIsTimeBased(0);
		Integer defaultRecurrenceScale = Arrays.asList(JibbyConfig.recurrences).indexOf(JibbyConfig.recurrences[0]) + 1; // default item = not recurring

		entity.setRecurrenceScale(defaultRecurrenceScale);
		entity.setRecurrenceUnit(1);
		entity.setWorksheetType(1);

		entity = service.saveEntity(entity);

		JBUserRoleEntity userRoles = new JBUserRoleEntity();
		userRoles.setEntityId(entity.getId());
		userRoles.setUser(session.getUser());
		userRoles.setUserRole(Arrays.asList(JibbyConfig.userRoles).indexOf("owner") + 1);
		userRoles.setCustomerid(session.getCustomer() != null ? session.getCustomer().getId() : null);
		service.saveUserRoleEntity(userRoles);

		ItemBean item = new ItemBean(entity);
		if (historyModel.getEntitiesByKey(this.entity.getId()) != null) // add item directly to parent
			historyModel.getEntitiesByKey(this.entity.getId()).add(item);
		else { // show the child icon for the parent
			List<ItemBean> items = historyModel.getEntitiesByKey(
					this.entity.getParentId() == null ? this.entity.getId() : this.entity.getParentId());

			List<ItemBean> itemsList =  items.stream().filter(x -> x.getId().equals(this.entity.getId()))
					.collect(Collectors.toList());
			if(itemsList.size()>0) {
				ItemBean bean = itemsList.get(0);
				bean.setIsChild(true);
			}
		}
		Notification.show("Item create successfully", 2000, Position.BOTTOM_CENTER);
	}

	@ClientCallable
	private void _onAddmyItem(JBChecklistItemEntity checkitem) {
		showMyActionlistView(checkitem);
	}

	public void showMyActionlistView(JBChecklistItemEntity checkitem) {
		Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem") + 1;
		MyActionlistAssignedEvent openmyactionevent = new MyActionlistAssignedEvent(false, false, true);
		openmyactionevent.setItemid(checkitem.getId());
		openmyactionevent.setItemType(itemType);
		openmyactionevent.setItemName(checkitem.getName());
		openmyactionevent.setCheckItem(checkitem);
		myActionEvent.fire(openmyactionevent);
		dl.removeAll();
		dl.add(myactionlistpopup);
		dl.open();
		dl.setWidth("330px");
		dl.setCloseOnOutsideClick(true);
	}
}
