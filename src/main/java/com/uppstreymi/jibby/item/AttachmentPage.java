package com.uppstreymi.jibby.item;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.item.events.AttachmentDescriptionEvent;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.components.ConfirmationDialog;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.templatemodel.Include;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("attachment-page")
@JsModule("./src/views/item/attachment-page.js")
@UIScoped
public class AttachmentPage extends PolymerTemplate<AttachmentPage.AttachmentPageModel> {

	String fileExtensions[] = { "application/pdf", "image/jpeg", "image/jpg", "image/png", "image/gif" };

	SucceededEvent succeedEvent;

	MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
	Upload upload;

	Dialog attachmentDescriptionDialog = new Dialog();

	@Inject
	AttachmentDescriptionPopupPage attachmentDescriptionPopupPage;
	@Inject
	private SessionModel session;
	@Inject
	private DbService service;
	@Inject
	private HistorModel historyModel;
	@Inject
	private AppModel appModel;
	@Inject
	private SyncUserEvent userEvent;
	@Inject
	private ProjectViewModel projectModel;
	@Id("uploadWrapper")
	private Div uploadWrapper;
	private List<JBAttachmentEntity> attachments = new ArrayList<JBAttachmentEntity>();
	Logger logger=LogManager.getLogger(AttachmentPage.class);

	public AttachmentPage() {
	}

	@PostConstruct
	private void init() {
		getModel().setAttachLink(Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl()));
	}

	public void addAttachmentInWrapper(JBAttachmentEntity attachment) {
		// TODO Auto-generated method stub
		List<JBAttachmentEntity> attachments = getModel().getAttachments();
		attachments.add(attachment);
		getModel().setAttachments(attachments);
	}

	public void setAttachmentList(List<JBAttachmentEntity> attachments) {
		this.attachments = attachments;
		getModel().setAttachments(attachments);
	}

	public AttachmentPageModel getAttachmentModel() {
		return getModel();
	}

	/**
	 * This model binds properties between AttachmentPage and attachment-page.html
	 */
	public interface AttachmentPageModel extends TemplateModel {
		// Add setters and getters for template properties here.

		@Include({ "name", "id", "entity", "uploaderUser", "type", "size", "description", "sortOrder" })
		void setAttachments(List<JBAttachmentEntity> items);

		void setAttachLink(String url);

		List<JBAttachmentEntity> getAttachments();

		void setHtml(String str);

	}

	public void clear() {
		// TODO Auto-generated method stub
		getModel().setAttachments(new ArrayList<>());
	}

	@ClientCallable
	private void _filePreview(JBAttachmentEntity item) {
		if (item != null) {
			if (Arrays.asList(fileExtensions).contains(item.getType())) {
				showOriginalFile(item);
			}
		}
	}

	@ClientCallable
	private void _deleteAttachment(JBAttachmentEntity item) {
		ConfirmationDialog<Integer> confirmationDialog = new ConfirmationDialog<Integer>();
		String title = "Please Confirm:";
		String message = "Do you really want to delete this Attachment?";
		String okText = "Yes,I am";
		String cancelText = "Cancel";
		confirmationDialog.open(title, message, "", okText, cancelText, true, 0, (es) -> {
			getModel().getAttachments().removeIf(filter -> filter.getId().equals(item.getId()));
			attachmentRemoved();
			
			userEvent.setEventType(SyncEventType.EVENT_ENTITY_ATTACHMENT_REMOVE);
			userEvent.setItemModificationRemoveId(item.getId()+"-"+item);
			userEvent.setEntity(historyModel.getSelectedEntity());
			userEvent.setSender(session.getUser());
			userEvent.syncDataReceiver(historyModel.getSelectedEntity());
			
			userEvent.setTopLebelProjectEntity(
					historyModel.getSelectedEntity().getParentProjectId() == null ? historyModel.getSelectedEntity()
							: service.getEntity(historyModel.getSelectedEntity().getParentProjectId()));
			
			userEvent.addHistory();
			userEvent.eventProcessor();
			
			service.deleteAttachment(item);
		}, () -> {
		});
	}

	private void attachmentRemoved() {
		
		if(historyModel.getSelectedEntity().getParentId()!=null) {
			List<ItemBean> items = historyModel.getEntitiesByKey(historyModel.getSelectedEntity().getParentId());
			ItemBean itemBean = items.stream().filter(e -> e.getEntity().getId().equals(historyModel.getSelectedEntity().getId())).findAny()
					.orElse(null);
			itemBean.setIsAttachment(false);
		}
		else {
			ItemBean itemBean=projectModel.getItemBean();
			itemBean.setIsAttachment(false);
		}
	}

	@ClientCallable
	private void _attachmentDescription(JBAttachmentEntity item) {
		JBAttachmentEntity entity = this.attachments.stream().filter(e -> e.getId().equals(item.getId())).findAny()
				.orElse(null);
		attachmentDescriptionPopupPage.setup(entity);
		attachmentDescriptionDialog.add(attachmentDescriptionPopupPage);
		attachmentDescriptionDialog.open();
		attachmentDescriptionDialog.setHeight("-1");
	}

	public void showOriginalFile(JBAttachmentEntity docEntity) {
		try {
			String fileName = Utils.imagePath + docEntity.getId();
			String orginalFilename = docEntity.getName();
			InputStream inputInstream = getStream(fileName);
			StreamResource streamResource = new StreamResource(orginalFilename, () -> inputInstream);
			Element object = new Element("object");
			object.setAttribute("type", docEntity.getType());
			object.setAttribute("data", streamResource);
			object.setAttribute("width", "100%");
			object.setAttribute("height", "300px");
			object.getStyle().set("display", "block");
			// object.getStyle().set("overflow", "auto");
			showFile(object);
		} catch (Exception e) {
			logger.error("File preview exception on attachment page",e);
		}


	}

	public void showFile(Element ele) {
		Dialog dl = new Dialog();
		dl.setCloseOnEsc(true);
		dl.setCloseOnOutsideClick(true);

		VerticalLayout vl = new VerticalLayout();
		vl.setWidth("100%");
		vl.setHeight("100%");
		vl.addClassName("overflow");
		vl.getElement().appendChild(ele);
		// vl.setId("filepreviewcontent");

		Button cancel = new Button("Cancel");
		cancel.setClassName("jibbyCancel");
		cancel.setWidth("90%");

		Div div = new Div();
		div.add(cancel);
		div.getElement().setAttribute("style", "width: 100%;text-align: center;margin-bottom: 15px;");

		dl.add(vl, div);
		dl.open();
		dl.addAttachListener(event -> {
			UI.getCurrent().getPage().executeJavaScript(
					" var x = document.getElementById('overlay');\n" + " x.classList.add('attachmment-popup');");
		});

		cancel.addClickListener(e -> {
			dl.close();
		});
	}

	public java.io.InputStream getStream(String fileName) {
		try {
			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);
			return fis;
		} catch (Exception e) {
			logger.error("Getstream exception on attachment page",e);
		}
		return null;
	}

	private void changeAttachmentDescription(@Observes AttachmentDescriptionEvent event) {
		getModel().setHtml(event.getAttachmentEntity().getDescription());
	}
}
