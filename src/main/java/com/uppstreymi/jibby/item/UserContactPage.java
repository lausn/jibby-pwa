package com.uppstreymi.jibby.item;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("user-contact-page")
@JsModule("./src/views/item/user-contact-page.js")
@UIScoped
public class UserContactPage extends PolymerTemplate<UserContactPage.UserContactPageModel> {

	@Id("btnAddOwner")
	private Button btnAddOwner;
	
	@Id("btnAddEditor")
	private Button btnAddEditor;
	
	@Id("btnAddReader")
	private Button btnAddReader;
	
	@Id("btnConnection")
	private Button btnConnection;
	
	@Inject
	private UserAccessView userAccessView;
	
	
    public UserContactPage() {
    }
    
    @PostConstruct
    public void init() {
    	btnAddOwner.addClickListener(e->{
			Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(userAccessView);
			dialog.open();
			userAccessView.setup("owner");
    	});
    	
    	btnAddEditor.addClickListener(e->{
    		Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(userAccessView);
			dialog.open();
			userAccessView.setup("editors");
    	});
    	
    	btnAddReader.addClickListener(e->{
    		Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(userAccessView);
			dialog.open();
			userAccessView.setup("reader");
    	});
    	
    	btnConnection.addClickListener(e->{
    		Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(userAccessView);
			dialog.open();
			userAccessView.setup("connection");
    	});
    }
    
    public UserContactPageModel getUserContactModel() {
    	return getModel();
    }

    public interface UserContactPageModel extends TemplateModel {
    	void setHtml(String str);
    	String getHtml();
    	void setShowBtn(boolean show);
    }
}
