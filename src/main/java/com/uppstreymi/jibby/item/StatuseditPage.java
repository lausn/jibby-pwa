package com.uppstreymi.jibby.item;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.PriorityCriteria;
import com.uppstreymi.jibby.bean.StatusCriteria;
import com.uppstreymi.jibby.item.events.StatusChangeEvent;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.myaction.AssigntoothersPage;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("statusedit-page")
@JsModule("./src/views/item/statusedit-page.js")
@UIScoped
public class StatuseditPage extends PolymerTemplate<StatuseditPage.StatuseditPageModel> {

	@Inject
	HistorModel historyModel;
	@Inject
	SessionModel sessionModel;
	@Inject
	DbService service;
	@Inject
	Event<StatusChangeEvent> statusChangeEvent;
	@Inject
	SyncUserEvent userEvent;
	SimpleDateFormat dateFormat;
	boolean isChange;
	@Id("btnSave")
	private Button btnSave;
	@Id("btnCancel")
	private Button btnCancel;

	@Id("selectStatusWrapper")
	private Div selectStatusWrapper;
	@Id("selectPriorityWrapper")
	private Div selectPriorityWrapper;
	@Id("dpStart")
	private DatePicker dpStart;
	@Id("dpFinish")
	private DatePicker dpFinish;

	private Select<StatusCriteria> selectStatus = new Select<StatusCriteria>();
	private Select<PriorityCriteria> selectPriority = new Select<PriorityCriteria>();
	List<StatusCriteria> listStatusCriteria = new ArrayList<>();
	List<PriorityCriteria> listPriorityCriteria = new ArrayList<>();

	private JBEntity entity;
	private Logger logger = LogManager.getLogger(AssigntoothersPage.class);

	public StatuseditPage() {
		initData();
	}

	private void initData() {
		for (Integer i : JibbyConfig.status) {
			StatusCriteria sc = new StatusCriteria();
			sc.setId(i);
			if (i == JibbyConfig.status[0])
				sc.setCaption("Done");
			else
				sc.setCaption("Not Done");
			listStatusCriteria.add(sc);
		}
		selectStatusWrapper.add(selectStatus);
		selectStatus.setItems(listStatusCriteria);
		selectStatus.setItemLabelGenerator(StatusCriteria::getCaption);
		selectStatus.setValue(listStatusCriteria.get(1));
		selectStatus.getElement().setAttribute("style", "width:100%");

		int i = 0;
		for (String priority : JibbyConfig.priorities) {
			PriorityCriteria pc = new PriorityCriteria();
			pc.setId(i + 1);
			pc.setCaption(priority);
			listPriorityCriteria.add(pc);
			i++;
		}
		selectPriorityWrapper.add(selectPriority);
		selectPriority.setItems(listPriorityCriteria);
		selectPriority.setItemLabelGenerator(PriorityCriteria::getCaption);
		selectPriority.setValue(listPriorityCriteria.get(0));
		selectPriority.getElement().setAttribute("style", "width:100%");
	}

	@PostConstruct
	private void init() {

		btnSave.addClickListener(e -> {

			StatusChangeEvent statusEvent = new StatusChangeEvent(false, false);
			if (!entity.getStatus().equals(selectStatus.getValue().getId())) {
				JBEntity oldentity = new JBEntity();
				oldentity.copyEntity(entity);
				statusEvent.setOthersChange(true);
				statusEvent.setStatusChange(true);
				entity.setStatus(selectStatus.getValue().getId());
				entity.setProgress(selectStatus.getValue().getId().equals(JibbyConfig.status[0]) ? 100 : 0);
				service.saveEntity(entity);
				getAllUsersAccessUser(oldentity, entity);
				changeChecklistStatus(entity);

				userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
				userEvent.setOldEntity(oldentity);

				List<ItemBean> items = entity.getParentId() == null ? historyModel.getEntitiesByKey(entity.getId())
						: historyModel.getEntitiesByKey(entity.getParentId());
				ItemBean item = getItem(items, entity.getId());
				if (item != null) {
					userEvent.setHasAttachment(item.getIsAttachment());
					userEvent.setHasCheckItem(item.getIsChecklist());
				} else {
					userEvent.setHasAttachment(false);
					userEvent.setHasCheckItem(false);
				}

				Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1;
				if (selectStatus.getValue().getId().equals(JibbyConfig.status[0])) {
					removeItemFromMyactionList(entity.getId());
					// for notification if user is assigee of this item
					taskCompletedByAssignee(entity.getId(), itemtype, entity.getName(), entity.getCreator());
				} else {
					// for notification if user is assigner of this item
					markUndone(itemtype, entity.getId(), entity.getName());
				}
				userEvent.addHistory();
				userEvent.eventProcessor();
			}
			if (!entity.getPriority().equals(selectPriority.getValue().getId())) {
				JBEntity oldentity = new JBEntity();
				oldentity.copyEntity(entity);
				statusEvent.setOthersChange(true);
				entity.setPriority(selectPriority.getValue().getId());
				service.saveEntity(entity);
				getAllUsersAccessUser(oldentity, entity);
				userEvent.setOldEntity(oldentity);
				insertItemModification(entity, Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("edit") + 1);
				userEvent.addHistory();
				userEvent.eventProcessor();
			}

			if (dpStart.getValue() != null && dpFinish.getValue() != null) {
				if (Utils.localDateToDate(dpFinish.getValue())
						.compareTo(Utils.localDateToDate(dpStart.getValue())) > 0) {
					if (!Utils.localDateToDate(dpStart.getValue()).equals(entity.getEstimatedStartDate())
							|| !Utils.localDateToDate(dpFinish.getValue()).equals(entity.getEstimatedFinishDate())) {
						JBEntity oldentity = new JBEntity();
						oldentity.copyEntity(entity);
						statusEvent.setOthersChange(true);
						entity.setEstimatedStartDate(Utils.localDateToDate(dpStart.getValue()));
						entity.setEstimatedFinishDate(Utils.localDateToDate(dpFinish.getValue()));
						service.saveEntity(entity);
						getAllUsersAccessUser(oldentity, entity);
						userEvent.setOldEntity(oldentity);
						insertItemModification(entity,
								Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("edit") + 1);
						userEvent.addHistory();
						userEvent.eventProcessor();
					}
				}

			}

			statusChangeEvent.fire(statusEvent);
			closeWindow();
		});

		btnCancel.addClickListener(e -> {
			closeWindow();
		});
	}

	private void closeWindow() {
		Dialog dl = (Dialog) getParent().get();
		dl.close();
	}

	public void setup() {
		isChange = false;
		entity = historyModel.getSelectedEntity();
		logger.info("entity name--> " + entity.getName());
		dateFormat = new SimpleDateFormat(sessionModel.getUser().getDateFormat());
		dateFormat.setTimeZone(TimeZone.getTimeZone(sessionModel.getUser().getTimeZone()));
		selectStatus.setValue(
				entity.getStatus() == JibbyConfig.status[0] ? listStatusCriteria.get(0) : listStatusCriteria.get(1));
		selectPriority.setValue(listPriorityCriteria.get(entity.getPriority() - 1));
		dpStart.setValue(entity.getEstimatedStartDate() == null ? LocalDate.now()
				: Utils.dateToLocalDate(entity.getEstimatedStartDate()));
		dpFinish.setValue(entity.getEstimatedFinishDate() == null ? LocalDate.now()
				: Utils.dateToLocalDate(entity.getEstimatedFinishDate()));

	}

	public interface StatuseditPageModel extends TemplateModel {

	}

	private void removeItemFromMyactionList(Integer itemid) {
		List<JBActionlist> actionlists = service.getActionlistByEntityIdAndTypeAndAddedBy(itemid,
				Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1, sessionModel.getUser().getId());
		for (JBActionlist actionlist : actionlists) {
			service.deleteMyActionListRow(actionlist.getId());
		}
	}

	private void taskCompletedByAssignee(Integer itemId, Integer itemType, String itemName, Integer creator) {

		List<Object> assignersData = (List<Object>) service.getAllAssignerByEntity(itemId, itemType,
				sessionModel.getUser().getId());
		if (!sessionModel.getUser().getId().equals(creator)) { // notification to creator
			if (!assignersData.contains(creator)) {
				assignersData.add(creator);
			}
		}

		for (Object assigner : assignersData) {
			String userName = sessionModel.getUser().getFirstName() + " " + sessionModel.getUser().getLastName();
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(itemId);
			actionlistHistory.setItemtype(itemType);
			actionlistHistory.setUserid(Integer.parseInt(assigner.toString()));
			actionlistHistory.setMessage(userName + " completed the task  \"" + itemName + "\".");
			actionlistHistory
					.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("markedByAssignee") + 1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

			service.saveActionlistHistory(actionlistHistory);
		}

	}

	private void markUndone(Integer itemType, Integer itemId, String itemName) {
		String userName = sessionModel.getUser().getFirstName() + " " + sessionModel.getUser().getLastName();
		List<JBUserEntity> assigneeList = service.getAllAssigneeUserById(itemId, itemType);
		if (assigneeList.contains(sessionModel.getUser())) {
			assigneeList.remove(sessionModel.getUser());
		}
		userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
		for (JBUserEntity assignee : assigneeList) {
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(itemId);
			actionlistHistory.setItemtype(itemType);
			actionlistHistory.setUserid(assignee.getId());
			actionlistHistory.setMessage(userName + "  marked the task \"" + itemName + "\" as Not done.");
			actionlistHistory
					.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser") + 1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

			service.saveActionlistHistory(actionlistHistory);
		}

	}

	public void changeChecklistStatus(JBEntity entity) {

		Integer checkStatus = entity.getStatus().equals(JibbyConfig.status[0]) ? 1 : 0; // done=1,undone=0
		Integer isChecked = checkStatus.equals(1) ? 0 : 1; // if status=0,find done checkitem or if status=1 find undone
															// checkitem

		List<JBChecklistItemEntity> checkListData = service.getAllDoneOrUndoneChecklistByEntity(entity.getId(),
				isChecked);
		for (JBChecklistItemEntity checklist : checkListData) {
			checklist.setIsChecked(checkStatus);
			service.saveChecklistItems(checklist);
		}
		addItemModification(entity);

	}

	public void addItemModification(JBEntity entity) {
		Integer modificationId = service.deleteItemModificationByEntityAndUserAndCompleted(entity.getId(),
				sessionModel.getUser().getId());
		if (modificationId == null)
			insertItemModification(entity,
					entity.getStatus().equals(JibbyConfig.status[0])
							? Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("done") + 1
							: Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undone") + 1);
		else {
			userEvent.setItemModificationRemoveId(String.valueOf(modificationId));
			userEvent.setEntity(entity);
			userEvent.setSender(sessionModel.getUser());
			userEvent.syncDataReceiver(entity);
		}
	}

	public void insertItemModification(JBEntity entity, Integer type) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entity.getId());
		itemModification.setUserId1(sessionModel.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
		userEvent.addToHistoryList(itemModification);
		userEvent.setEntity(entity);
		userEvent.setSender(sessionModel.getUser());
		userEvent.syncDataReceiver(entity);

		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity : service.getEntity(entity.getParentProjectId()));
	}

	private JBEntity getAllUsersAccessUser(JBEntity oldEntity, JBEntity selectedEntity) {

		selectedEntity.setOwnerlist(oldEntity.getOwnerlist());
		selectedEntity.setAssigneelist(oldEntity.getAssigneelist());
		selectedEntity.setFollowerlist(oldEntity.getFollowerlist());
		selectedEntity.setConnectionlist(oldEntity.getConnectionlist());

		selectedEntity.setDirectOwnerlist(oldEntity.getDirectOwnerlist());
		selectedEntity.setDirectAssigneelist(oldEntity.getDirectAssigneelist());
		selectedEntity.setDirectFollowerlist(oldEntity.getDirectFollowerlist());
		selectedEntity.setDirectConnectionlist(oldEntity.getDirectConnectionlist());

		selectedEntity.setParentArray(oldEntity.getParentArray());
		return selectedEntity;
	}

	private ItemBean getItem(List<ItemBean> list, Integer element) {
		for (ItemBean item : list) {
			if (item.getId().equals(element))
				return item;
		}
		return null;
	}
}
