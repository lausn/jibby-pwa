package com.uppstreymi.jibby.item;

import java.io.File;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.enterprise.event.Event;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.customcomponent.AssignedlistPopup;
import com.uppstreymi.jibby.item.events.TopLabelProjectEvent;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.MainLayout;
import com.uppstreymi.jibby.ui.components.navigation.bar.AppBar.NaviMode;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("add-item")
@JsModule("./src/views/item/add-item.js")
@Route(value="item/additem", layout = MainLayout.class)
public class AddItem extends PolymerTemplate<AddItem.AddItemModel> implements HasUrlParameter<Integer> {

	@Id("tfName")
	private TextField tfName;

	@Id("taDescription")
	private TextArea taDescription;

	@Id("btnSave")
	private Button btnSave;
	
	@Id("btnShowAssignedUser")
	private Button btnShowAssignedUser;

	@Id("lblAssignee")
	private Div lblAssignee;
	
	@Inject private ProjectViewModel projectModel;
	@Inject private SessionModel session;
	@Inject private DbService service;
	@Inject private HistorModel historyModel;
	@Inject private AssignedlistPopup assignListPopup;
	@Inject SyncUserEvent userEvent;
	@Inject private EntityUserAccess entityUserAccess;
	@Inject private AppModel appModel;
	@Inject Event<TopLabelProjectEvent> topLabelProjectEvent;

	private Integer parentId;

	private Dialog dialog = new Dialog();

	private Set<JBUserEntity> selectedUsers = new HashSet<>();

	private JBEntity newEntity = new JBEntity();
	Logger logger=LogManager.getLogger(AddItem.class);

	/**
	 * Creates a new AddItem.
	 */
	public AddItem() {
		lblAssignee.setVisible(false);
	}

	@PostConstruct
	private void init() {
		btnSave.addClickListener(e->{
			Integer type = getModel().getEntityType()==null?null:Integer.parseInt(getModel().getEntityType());
			String name = tfName.getValue();
			String description = taDescription.getValue();
			if(isCheck(type,name)) {
				newEntity = saveNewEntity(type,name,description);

				for(JBUserEntity user: selectedUsers){
					assignedToSelectedUser(user);
				}
				newEntity=entityUserAccess.getAllUsersAccessUser(newEntity);
				userEvent.setEventType(SyncEventType.EVENT_NEW_ITEM_ADD);
				userEvent.setSender(session.getUser());
				userEvent.syncDataReceiver(newEntity);
				userEvent.setTopLebelProjectEntity(
						newEntity.getParentProjectId() == null ? newEntity
								: service.getEntity(newEntity.getParentProjectId()));
				
				userEvent.setEntity(newEntity);

				if (userEvent.getSyncDataReceiver() != null) {
					userEvent.addNewItem();
					userEvent.eventProcessor();
				}

				ItemBean item = new ItemBean(newEntity);
				item.setIsRoles(true);
				if(historyModel.getEntitiesByKey(parentId) != null) //add item directly to parent
					historyModel.getEntitiesByKey(parentId).add(item);
				else { // show the child icon for the parent
					if(!parentId.equals(-1)) {
						List<ItemBean> items = historyModel.getEntitiesByKey(historyModel.getAddItemParentId());
						ItemBean bean = items.stream().filter(x -> x.getId().equals(parentId)).collect(Collectors.toList()).get(0);
						bean.setIsChild(true);
					}
				}
				UI.getCurrent().getPage().getHistory().back();
				if(parentId.equals(-1)) {
					TopLabelProjectEvent projectEvent=new TopLabelProjectEvent(true,newEntity);
					topLabelProjectEvent.fire(projectEvent);
					Notification.show("Project Created Successfully");
					logger.info("{} Project Created Successfully", item.getName());
				}
				else {
					Notification.show("Item Created Successfully");
					logger.info("{} Item Created Successfully", item.getName());
				}
					
				assignListPopup.clear();
				selectedUsers = new HashSet<>();
			}
		});

		btnShowAssignedUser.addClickListener(e->{
			dialog = new Dialog();
			dialog.add(assignListPopup);
			dialog.open();
			assignListPopup.loadAssigneduserListByEntity(selectedUsers,parentId);
			assignListPopup.isFromNewItemCreate(true);
			
			dialog.addOpenedChangeListener(f->{
				if(!f.isOpened()) {
					selectedUsers = new HashSet<>();
					selectedUsers = assignListPopup.getSelectedUsers();
					showSelectedAssigneeLbl();
				}
			});
		});
		
	}

	private void showSelectedAssigneeLbl() {
		// TODO Auto-generated method stub
		if(selectedUsers.size()>0) {
			lblAssignee.setVisible(true);
		}else
			lblAssignee.setVisible(false);
		
		lblAssignee.removeAll();
		
		for(JBUserEntity user : selectedUsers) {
			addUserRow(user);
		}
	}

	private void addUserRow(JBUserEntity user) {
		// TODO Auto-generated method stub
		Div wrapper = new Div();
		wrapper.getElement().setAttribute("style","width: 100%;display: inline-flex;padding-top: 2px;padding-bottom: 2px;");
		
		Div content = new Div();
		String assignee = user.getFirstName()+" "+user.getLastName();
		String html = "<div style='margin-right:10px;float:left;'><img class='' style='width:30px;height:30px;border-radius:100%;' alt='' src='"+ userPic(user.getId())+"'></div><div style='width: calc(100% - 70px);margin-top:5px;'>"+assignee+"</div>";
		content.getElement().setProperty("innerHTML",html);
		content.getElement().setAttribute("style","width:100%");
		
		Button btnDelete = new Button (VaadinIcon.CLOSE.create());
		btnDelete.getElement().setAttribute("style","color:red;margin-top: -3px;");
		btnDelete.addClassName("btnRowDelete");
		
		wrapper.add(content,btnDelete);
		
		wrapper.setId(String.valueOf(user.getId()));
		
		lblAssignee.add(wrapper);
		
		btnDelete.addClickListener(e->{
			String userId = e.getSource().getParent().get().getId().get().toString();
			//e.getSource().getParent().get().getElement().getClassList().add("animateDelete");
			lblAssignee.remove(e.getSource().getParent().get());
			
			Set<JBUserEntity> users = new HashSet<>();
			users.addAll(selectedUsers);
			users.removeIf(f->f.getId().equals(Integer.parseInt(userId)));
			
			selectedUsers = users;
		});
	}

	private void assignedToSelectedUser(JBUserEntity user) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1;
		JBActionlist actionlist = new JBActionlist();
		actionlist.setAddedby(session.getUser().getId());
		actionlist.setAssignedto(user.getId());
		actionlist.setItemid(newEntity.getId());
		actionlist.setItemname("");
		actionlist.setDateadded(new Date());
		actionlist.setItemtype(itemtype);
		actionlist.setItemdatetime(null);

		String userName = session.getUser().getFirstName()+" "+session.getUser().getLastName();
		JBActionlistHistory actionlistHistory = new JBActionlistHistory();
		actionlistHistory.setCreated(new Date());
		actionlistHistory.setItemid(newEntity.getId());
		actionlistHistory.setItemtype(itemtype);
		actionlistHistory.setUserid(user.getId());
		actionlistHistory.setMessage(userName+" assigned the task \""+newEntity.getName()+"\" to you.");
		actionlistHistory.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser")+1);
		actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

		service.saveActionlist(actionlist);
		actionlistHistory=service.saveActionlistHistory(actionlistHistory);

		// -----------notifications synchronized-----------
		userEvent.addToNotifyUserList(user.getId());
		userEvent.setNotification(actionlistHistory);
		userEvent.addToActionList(actionlist);
//		userEvent.setTopLebelProjectEntity(service.getEntity(newEntity.getParentProjectId()));
		
		userEvent.setTopLebelProjectEntity(
				newEntity.getParentProjectId() == null ? newEntity
						: service.getEntity(newEntity.getParentProjectId()));

		insertItemModification(newEntity.getId(),Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("assigntoother")+1,user.getId());

	}

	public void insertItemModification(Integer entityId,Integer type,Integer user2){
		JBItemModification itemModification=new JBItemModification();
		itemModification.setEntityId(entityId);
		itemModification.setUserId1(session.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setUserId2(user2);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);
	}
	private boolean isCheck(Integer type, String name) {
		if(type !=null) {
			if(!name.isEmpty()) {
				return true;
			}else
				Notification.show("Please select name");
		}else
			Notification.show("Please select a type");

		return false;
	}

	private void clearField() {
		// TODO Auto-generated method stub
		tfName.clear();
		taDescription.clear();
	}

	/**
	 * This model binds properties between AddItem and add-item.html
	 */
	public interface AddItemModel extends TemplateModel {
		// Add setters and getters for template properties here.
		void setEntityType(String type);
		String getEntityType();

		void setAssignedUser(List<String> users);
		List<String> getAssignedUser();
	}

	@Override
	public void setParameter(BeforeEvent arg0,@OptionalParameter Integer param) {
		// TODO Auto-generated method stub
		if(session.getUser()==null) {
			arg0.rerouteTo(LoginPage.class);
		}else {
			parentId = param;
			if(parentId.equals(-1)) {
				btnShowAssignedUser.setVisible(false);
				btnSave.getElement().getStyle().set("width", "100%");
				getElement().callJsFunction("disableNsTypeSelect");
				getModel().setEntityType("1");
			}
		}
	}

	private JBEntity saveNewEntity(Integer type, String name, String description) {
		// TODO Auto-generated method stub
		JBEntity selectedEntity = new JBEntity();
		if(!parentId.equals(-1)) {
			selectedEntity.setParentProjectId(projectModel.getDrawerActiveProject().getId());
			selectedEntity.setParentId(parentId);
		}
		selectedEntity.setName(name);
		selectedEntity.setEstimatedStartDate(new Date());
		selectedEntity.setEstimatedFinishDate(getFinishDateViaProjectType(type));
		selectedEntity.setUnitQuantity(Double.valueOf("0.00"));
		selectedEntity.setChatEnabled(1);
		selectedEntity.setIsPublic(0);
		selectedEntity.setDescription(description);
		selectedEntity.setEntityType(type);
		selectedEntity.setColor("#000000");
		selectedEntity.setKanbanColor(JibbyConfig.defaultKanbanColor);
		selectedEntity.setUnitName("");
		selectedEntity.setCostName("");
		selectedEntity.setCostPerUnit(0);
		selectedEntity.setKeywords("");
		Integer defaultPriority = Arrays.asList(JibbyConfig.priorities).indexOf(JibbyConfig.priorities[1])+1; // default item = medium
		selectedEntity.setPriority(defaultPriority);
		selectedEntity.setLastModificationDate(new Date());
		selectedEntity.setCreationDate(new Date());
		selectedEntity.setCreator(session.getUser().getId());
		selectedEntity.setProgress(0);
		selectedEntity.setStatus(JibbyConfig.status[1]);
		selectedEntity.setHierarchy("");
		selectedEntity.setDelIsTimeBased(0);
		Integer defaultRecurrenceScale = Arrays.asList(JibbyConfig.recurrences).indexOf(JibbyConfig.recurrences[0])+1; // default item = not recurring
		selectedEntity.setRecurrenceScale(defaultRecurrenceScale);
		selectedEntity.setRecurrenceUnit(1);
		selectedEntity.setWorksheetType(1);

		service.saveEntity(selectedEntity);

		JBUserRoleEntity userRoles= new JBUserRoleEntity();
		userRoles.setEntityId(selectedEntity.getId());
		userRoles.setUser(session.getUser());
		userRoles.setUserRole(Arrays.asList(JibbyConfig.userRoles).indexOf("owner")+1);
		userRoles.setCustomerid(session.getCustomer()!=null?session.getCustomer().getId():null);
		service.saveUserRoleEntity(userRoles);

		return selectedEntity;
	}

	private Date getFinishDateViaProjectType(Integer entityType) {
		// TODO Auto-generated method stub
		//Integer typeIndex=Arrays.asList(JibbyConfig.entityTypes).indexOf(entityType);
		String time = JibbyConfig.duration[entityType-1];

		String numberOfTime = time.substring(0, time.length() - 1);
		char keyWord = time.charAt(time.length()-1);
		Date finishTime;
		switch(keyWord){
		case 'H':
			finishTime = Utils.getAddHoursStartTime(new Date(),Integer.parseInt(numberOfTime));
			break;
		case 'D':
			finishTime = Utils.addDaysStartTime(new Date(),Integer.parseInt(numberOfTime));
			break;
		case 'M':
			finishTime = Utils.getAddMinutesStartTime(new Date(),Integer.parseInt(numberOfTime));
			break;
		default :
			finishTime = null;
		}
		return finishTime;
	}
	
	private String userPic(Integer userId){
		
		String url=Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl());

		try{
			File preFile = new File(Utils.userImagePath + userId);

			if(!preFile.exists())
				return "./images/avatar.png";
			else{
				return url+"jibby/usercontent/avatars/"+userId;
			}
		}catch (Exception e) {
			logger.error("userPic previewfile exception thrown in additem",e);
			return "./images/avatar.png";
		}
	}
	
	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		MainLayout.get().getAppBar().setNaviMode(NaviMode.CONTEXTUAL);
	}
}
