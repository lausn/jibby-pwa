package com.uppstreymi.jibby.item;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("status-page")
@JsModule("./src/views/item/status-page.js")
@UIScoped
public class StatusPage extends PolymerTemplate<StatusPage.StatusPageModel> {

	Dialog dl=new Dialog();
	@Inject  StatuseditPage statusEditpage;

	public StatusPage() {

	}

	@PostConstruct
	public void init() {
		dl.add(statusEditpage);
	}

	@EventHandler
	private void _editStatus() {
		statusEditpage.setup();
		dl.open();
		dl.setHeight("-1");
	}

	public StatusPageModel getStatusModel() {
		return getModel();
	}

	public interface StatusPageModel extends TemplateModel {
		void setDone(String done);
		void setPriority(String priority);
		void setDuration(String duration);
		void setShowedit(boolean show);
	}
}
