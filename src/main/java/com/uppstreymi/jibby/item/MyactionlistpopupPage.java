package com.uppstreymi.jibby.item;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.item.events.MyActionlistAssignedEvent;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("myactionlistpopup-page")
@JsModule("./src/views/item/myactionlistpopup-page.js")
@UIScoped
public class MyactionlistpopupPage extends PolymerTemplate<MyactionlistpopupPage.MyactionlistpopupPageModel> {

	@Inject
	private SessionModel session;
	@Inject
	private DbService service;
	@Inject
	private HistorModel historyModel;
	@Inject
	private Event<MyActionlistAssignedEvent> myactionlistAssingEvent;
	@Inject
	SyncUserEvent userEvent;
	@Inject
	private AppModel appModel;
	private CheckboxGroup<JBUserEntity> checkgroup = new CheckboxGroup<JBUserEntity>();
	JBChecklistItemEntity checkitem;
	@Id("btnMytoday")
	private Button btnMytoday;
	@Id("btnMytomorrow")
	private Button btnMytomorrow;
	@Id("btnMyactionlist")
	private Button btnMyactionlist;
	@Id("btnSelectAssignTo")
	private Button btnSelectAssignTo;
	@Id("btnCancel")
	private Button btnCancel;
	@Id("assigneeWrapper")
	private Div assigneeWrapper;

	Integer itemtype, itemid;
	String itemName;
	
	private Logger logger=LogManager.getLogger(MyactionlistpopupPage.class);

	public MyactionlistpopupPage() {
		checkgroup.addClassName("userCheck");
		assigneeWrapper.add(checkgroup);
	}

	@PostConstruct
	private void init() {
		checkgroup.setItemLabelGenerator(e -> {
			return getUsernameAndPic(e);
		});

		btnSelectAssignTo.addClickListener(e -> {
			assignedToOtherEvent();
		});
		btnCancel.addClickListener(e -> {
			myactionlistAssingEvent.fire(new MyActionlistAssignedEvent(false, false, false));
			closeWindow();
		});
		btnMytoday.addClickListener(e -> {
			if (this.itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1) {
				this.itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1;
			} else {
				this.itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem") + 1;
			}
			Integer todayType = Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday");
			addItemToMyList(todayType);
		});
		btnMytomorrow.addClickListener(e -> {
			if (this.itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1) {
				this.itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1;
			} else {
				this.itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem") + 1;
			}
			Integer todayType = Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow");
			addItemToMyList(todayType);
		});
		btnMyactionlist.addClickListener(e -> {
			Integer todayType = -1; // let -1 for item/check except myToday tools
			addItemToMyList(todayType);
		});
	}

	private String getUsernameAndPic(JBUserEntity e) {
		String name = "";
		if (e.getFirstName().isEmpty() && e.getLastName().isEmpty())
			name = e.getEmail();
		else
			name = e.getFirstName() + " " + e.getLastName();
		String html = "<div style='width: calc(100% - 30px);display: inline-block;font-size: 13px;'>" + name
				+ "</div><div style='display: inline-block;vertical-align: middle;'><img class='' style='width:30px;height:30px;border-radius:100%;float:left;' alt='' src='"
				+ userPic(e.getId()) + "'></div>";
		return html;
	}

	private void setup(@Observes MyActionlistAssignedEvent event) {
		if (event.isSetup() && event.getItemid() != null && event.getItemType() != null
				&& event.getItemName() != null) {
			this.itemtype = event.getItemType();
			this.itemid = event.getItemid();
			this.itemName = event.getItemName();
			this.checkitem = event.getCheckItem();
			loadAssigneduserListByEntity();
		}
	}

	public void loadAssigneduserListByEntity() {
		checkgroup.clear();
		List<JBUserEntity> assignedList = new ArrayList<JBUserEntity>();
		List<Integer> parentArray = historyModel.getParentArray();

		if (!parentArray.contains(historyModel.getSelectedEntity().getId()))
			parentArray.add(historyModel.getSelectedEntity().getId()); // add the current parent

		List<JBUserRoleEntity> assignedUserList = service.getAssignedUserListByEntity(parentArray);
		for (JBUserRoleEntity userRole : assignedUserList) {
			if (!assignedList.contains(userRole.getUser())
					&& !userRole.getUser().getId().equals(session.getUser().getId())) {
				assignedList.add(userRole.getUser());
			}
		}
		checkgroup.setItems(assignedList);

		checkgroup.getChildren().forEach(e -> {
			Checkbox checkBox = (Checkbox) e;
			checkBox.setWidth("100%");
			checkBox.setLabelAsHtml(checkBox.getLabel());
		});
	}

	private void assignedToOtherEvent() {
		boolean isAssignedSuccess = false;
		List<String> unsuccessUser = new ArrayList<>();

		if (checkgroup.getSelectedItems().size() > 0) {

			String appendAssingedToUsers = "";
			for (JBUserEntity user : checkgroup.getSelectedItems()) {
				if (!service.itemOnActionList(itemid, itemtype, user.getId(), session.getUser().getId())) { // if item/checklist not in the action list
		
					assignedToSelectedUser(user);
					appendAssingedToUsers += (appendAssingedToUsers == ""
							? (user.getFirstName() + " " + user.getLastName())
							: ", " + (user.getFirstName() + " " + user.getLastName()));
					isAssignedSuccess = true;
					userEvent.addToNotifyUserList(user.getId());
				} else {
					unsuccessUser.add(user.getFirstName() + " " + user.getLastName());
					String unsuccessUserList = String.join(",", unsuccessUser);
					Notification.show(unsuccessUserList + " are already assigned user", 4000, Position.BOTTOM_CENTER);
				}
			}
			if (isAssignedSuccess) {
				if (historyModel.getSelectedEntity().getParentProjectId() != null) {
					JBEntity topLebelEntity = service.getEntity(historyModel.getSelectedEntity().getParentProjectId());
					userEvent.setTopLebelProjectEntity(topLebelEntity);
				} else {
					userEvent.setTopLebelProjectEntity(historyModel.getSelectedEntity());
				}

				if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1) {
					userEvent.addHistory();
					myactionlistAssingEvent.fire(new MyActionlistAssignedEvent(true, false, false));
					closeWindow();
				} else {
					userEvent.setEventType(SyncEventType.EVENT_CHECKLIST_ASSIGNED);
					userEvent.setEntity(historyModel.getSelectedEntity());
					userEvent.addToChecklist(checkitem);
					userEvent.setSender(session.getUser());
					userEvent.syncDataReceiver(historyModel.getSelectedEntity());
					userEvent.setSender(session.getUser());
					userEvent.checklistAssigned();

					myactionlistAssingEvent.fire(new MyActionlistAssignedEvent(false, true, false));
					closeWindow();
				}
				userEvent.eventProcessor();
				Notification.show("Item assigned to users successfully", 3000, Position.BOTTOM_CENTER);
			}

		}
	}

	private void assignedToSelectedUser(JBUserEntity user) {
		JBActionlist actionlist = new JBActionlist();
		actionlist.setAddedby(session.getUser().getId());
		actionlist.setAssignedto(user.getId());
		actionlist.setItemid(itemid);
		actionlist.setItemname("");
		actionlist.setDateadded(new Date());
		actionlist.setItemtype(itemtype);
		actionlist.setItemdatetime(null);

		String userName = session.getUser().getFirstName() + " " + session.getUser().getLastName();
		JBActionlistHistory actionlistHistory = new JBActionlistHistory();
		actionlistHistory.setCreated(new Date());
		actionlistHistory.setItemid(itemid);
		actionlistHistory.setItemtype(itemtype);
		actionlistHistory.setUserid(user.getId());
		actionlistHistory.setMessage(userName + " assigned the task \"" + itemName + "\" to you.");
		actionlistHistory
				.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser") + 1);
		actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

		service.saveActionlist(actionlist);
		service.saveActionlistHistory(actionlistHistory);

		userEvent.setNotification(actionlistHistory);
		userEvent.addToActionList(actionlist);

		if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1) {
			insertItemModification(itemid,
					Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("assigntoother") + 1, user);
		}

	}

	private void addItemToMyList(Integer todayType) {
		if (!service.itemOnActionList(itemid, itemtype, 0, session.getUser().getId())) { // if
			// item/checklist/today/tomorrow
			// not in the action list
			Date dateTime = null;
			if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday")) {
				dateTime = new Date();
			} else if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow")) {
				Date date = new Date();
				dateTime = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			}

			JBActionlist actionlist = new JBActionlist();
			actionlist.setAddedby(session.getUser().getId());
			actionlist.setAssignedto(0);
			actionlist.setItemid(itemid);
			actionlist.setItemname("");
			actionlist.setDateadded(new Date());
			actionlist.setItemtype(itemtype);
			actionlist.setItemdatetime(dateTime);
			service.saveActionlist(actionlist);

			if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday")) {
				if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1) {
					insertItemModification(itemid,
							Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("mytoday") + 1,
							session.getUser());
				}
			} else if (todayType == Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow")) {
				if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem") + 1) {
					insertItemModification(itemid,
							Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("mytommorrow") + 1,
							session.getUser());
				}
			} else {
				if (itemtype == Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1) {
					insertItemModification(itemid,
							Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("myitem") + 1, session.getUser());
				}
			}

			if (itemtype != Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem") + 1
					&& itemtype != Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem") + 1) {
				userEvent.addHistory();
			}
			userEvent.eventProcessor();

			myactionlistAssingEvent.fire(new MyActionlistAssignedEvent(false, false, false));
			closeWindow();
			Notification.show("Successfully added", 3000, Position.BOTTOM_CENTER);
		} else {
			myactionlistAssingEvent.fire(new MyActionlistAssignedEvent(false, false, false));
			closeWindow();
			Notification.show("Item is already there", 4000, Position.BOTTOM_CENTER);
		}
	}

	private void closeWindow() {
		Dialog dl = (Dialog) getParent().get();
		dl.close();

	}

	public void insertItemModification(Integer entityId, Integer type, JBUserEntity assignee) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entityId);
		itemModification.setUserId1(session.getUser().getId());
		itemModification.setUserId2(assignee.getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		userEvent.setEventType(SyncEventType.EVENT_TASK_ASSIGNED);
		userEvent.addToHistoryList(itemModification);
		userEvent.addToAssigneeList(assignee);
		userEvent.setEntity(historyModel.getSelectedEntity());
		userEvent.setSender(session.getUser());
		userEvent.syncDataReceiver(historyModel.getSelectedEntity());
		
		userEvent.setTopLebelProjectEntity(
				historyModel.getSelectedEntity().getParentProjectId() == null ? historyModel.getSelectedEntity()
						: service.getEntity(historyModel.getSelectedEntity().getParentProjectId()));

	}

	public interface MyactionlistpopupPageModel extends TemplateModel {
		
	}

	private String userPic(Integer userId) {

		String url = Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl());

		try {
			File preFile = new File(Utils.userImagePath + userId);

			if (!preFile.exists())
				return "./images/avatar.png";
			else {
				return url + "jibby/usercontent/avatars/" + userId;
			}
		} catch (Exception e) {
			logger.error("An exception thrown in userPic myactionlistpopup",e);
			return "./images/avatar.png";
		}
	}
}
