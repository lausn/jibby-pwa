package com.uppstreymi.jibby.item;

import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBInvitation;
import com.uppstreymi.jibby.bean.JBMessages;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.service.HttpMailgunService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("chat-invitation-user-popup-page")
@JsModule("./src/views/item/chat-invitation-user-popup-page.js")
@UIScoped
public class ChatInvitationUserPopupPage
		extends PolymerTemplate<ChatInvitationUserPopupPage.ChatInvitationUserPopupPageModel> {

	@Id("gridUser")
	private Grid<JBUserEntity> gridUser;

	@Id("btnCancel")
	private Button btnCancel;
	@Id("btnInvite")
	private Button btnInvite;

	@Inject
	private SessionModel session;

	@Inject
	private DbService dbService;

	@Inject
	SessionModel sessionModel;
	
	@Inject
	private AppModel appModel;

	@Inject
	private HistorModel historyModel;
	
	@Inject
	private EntityUserAccess entityUserAccess;

	@Inject
	SyncUserEvent userEvent;

	private TextField txtFilter = new TextField();
	private HeaderRow header;
	private ListDataProvider<JBUserEntity> dataProvider;

	private SimpleDateFormat dateformat = new SimpleDateFormat();
	private String url = "";
	private HttpMailgunService httpMailgunService = new HttpMailgunService();
	private String invitehtml = "";
	Logger logger=LogManager.getLogger(ChatInvitationUserPopupPage.class);
	public ChatInvitationUserPopupPage() {
		
	}

	@PostConstruct
	public void init() {
		buildUserGrid();
		btnCancel.addClickListener(e -> {
			closeWindow();
		});

		btnInvite.addClickListener(e -> {
			if (!gridUser.getSelectedItems().isEmpty()) {
				Set<JBUserEntity> users = gridUser.getSelectedItems();
				System.out.println("size = "+users.size());
				for(JBUserEntity user:users) {
					if (!historyModel.getSelectedEntity().getAssigneelist().contains(user)
							&& historyModel.getSelectedEntity().getFollowerlist().contains(user)) {
						addUserToFollower(user); // make user as follower
					}
					inviteUserToChat(user);
				}
				closeWindow();
			} else
				Notification.show("Select a user first");
		});
	}

	private void inviteUserToChat(JBUserEntity user) {
		JBInvitation invitation = new JBInvitation();
		invitation.setSender(sessionModel.getUser().getId());
		invitation.setRecipient(user.getId());
		invitation.setInvitedatetime(new Date());
		invitation.setEntity(historyModel.getSelectedEntity().getId());

		dbService.saveInvitation(invitation);

		String userName = sessionModel.getUser().getFirstName() + " " + sessionModel.getUser().getLastName();
		String itemName = historyModel.getSelectedEntity().getName();
		JBActionlistHistory actionlistHistory = new JBActionlistHistory();
		actionlistHistory.setCreated(new Date());
		actionlistHistory.setItemid(historyModel.getSelectedEntity().getId());
		actionlistHistory.setItemtype(Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1);
		actionlistHistory.setUserid(user.getId());
		actionlistHistory.setMessage(userName + " invites you to comments on \"" + itemName + "\".");
		actionlistHistory
				.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser") + 1);
		actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

		actionlistHistory = dbService.saveActionlistHistory(actionlistHistory);
		/*
		 * userEvent.setEventType(SyncEventType.EVENT_ENTITY_INVITATION);
		 * userEvent.addHistoryNotification(actionlistHistory);
		 * userEvent.eventProcessor();
		 */

		JBMessages message = new JBMessages();
		message.setRecipient(user.getId());
		message.setIsRead(JibbyConfig.messageRead[0]);
		message.setIsEmailed(0);
		message.setType(Arrays.asList(JibbyConfig.messageTypes).indexOf("chatInvite") + 1);
		message.setDate(new Date());
		message.setEntity(historyModel.getSelectedEntity().getId());
		message.setText("");

		String subject = "Chat invitation to:" + historyModel.getSelectedEntity().getName();
		String fromAddress = sessionModel.getUser().getEmail();
		String fromName = sessionModel.getUser().getFirstName() + " " + sessionModel.getUser().getLastName();
		String toAddress = user.getEmail();
		String toName = user.getFirstName() + " " + user.getLastName();
		String cc = "";
		String bcc = "";

		try {
			url = Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl());
			String topLebelId = String.valueOf(historyModel.getSelectedEntity().getParentProjectId() == null
					? historyModel.getSelectedEntity().getId()
					: historyModel.getSelectedEntity().getParentProjectId());
			String entityId = String.valueOf(historyModel.getSelectedEntity().getId());

			String id = Utils.encrypt(topLebelId + "#" + entityId);
			String entityPermalink = url + "?jbrid=" + URLEncoder.encode(id, "UTF-8");

			invitehtml = IOUtils.toString((InputStream) getClass().getClassLoader()
					.getResourceAsStream("invitationMail/chatInvitationMail.html"), "UTF-8");
			invitehtml = invitehtml.replace("#recipientName#", toName);
			invitehtml = invitehtml.replace("#senderName#", fromName);
			invitehtml = invitehtml.replace("#senderEmail#", fromAddress);
			invitehtml = invitehtml.replace("#entityName#",
					StringEscapeUtils.escapeHtml(historyModel.getSelectedEntity().getName()));
			invitehtml = invitehtml.replace("#entityPermalink#", entityPermalink);

			loadChatListData(historyModel.getSelectedEntity());

		} catch (Exception e) {
			logger.error("invitationMail/chatInvitationMail.html File read exception on chatinvitationuserpopuppage",e);
			
		}

		List<String> invitationEmailList = new ArrayList<>();
		invitationEmailList.add(toAddress);
		Integer status = httpMailgunService.sendHTTPMail(fromAddress, fromName, toAddress, toName, subject, cc, bcc, "",
				invitehtml, "", null, "", null);

		if (status != null && status == 200) {
			Notification.show(toName + " has been invited to this chat via email.");
			message.setIsEmailed(1);
		} else
			Notification.show("mail not send successfully");

		dbService.saveMessages(message);

	}

	private void loadChatListData(JBEntity entity) {
		List<Object[]> chatlists = new ArrayList<>();
		chatlists = dbService.getAllChatsByEntity(entity.getId().toString());
		String html = "";
		String chatHTML = "";
		for (Object[] chatlist : chatlists) {
			// Add chat
			html += addChat(chatlist);
		}
		try {
			if (chatlists.size() > 0) {
				chatHTML = IOUtils.toString(
						(InputStream) getClass().getClassLoader().getResourceAsStream("report/chat.html"), "UTF-8");
				chatHTML = chatHTML.replace("#head#", "Comments");
				chatHTML = chatHTML.replace("#chat#", html);
				chatHTML = "Previous comments are: <br />" + chatHTML;
			}
			invitehtml = invitehtml.replace("#chat#", chatHTML);

		} catch (Exception e) {
			logger.error("report/chat.html File read exception on chatinvitationuserpopuppage",e);
		}

	}

	public String addChat(Object[] chatlist) {
		String chatHtml = "";
		String chat = chatlist[3].toString().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		String innerchatHtml = "<div style = 'padding-top: 5px;padding-bottom: 5px;padding-left: 5px;display:inline-block;float: left; width: 100%;background-color: #f0f0f0;'><div style='display:inline-block;position: relative;width:100%;'>"
				+ "<div style='display:inline;width:86%;font-weight: 400;color: black;'>" + chatlist[8] + "</div>"
				+ "<div style='display:inline;float:right;color: #8a8585;vertical-align: top;'>"
				+ dateformat.format(chatlist[4]) + "</div></div>" + "<div style='display: inline-block;width: 100%;'>"
				+ chat.replaceAll("\n", "<br>") + "</div></div>";

		chatHtml += "<table style='width: 100%;'>" + "<tr>" + "<td colspan='1' width='18px' valign='top'>"
				+ "<img style='width: 25px;height: 25px;border-radius: 15px;border: 1px solid #f1f1f1;' src='"
				+ userPic(Integer.parseInt(String.valueOf(chatlist[1]))) + "'></td>" + "<td colspan='1' valign='top'>"
				+ innerchatHtml + "</td>" + "</tr>" + "</table>";
		return chatHtml;

	}

	public String userPic(Integer userId) {
		try {
			File preFile = new File(Utils.userImagePath + userId);

			if (!preFile.exists())
				return "images/avatar.png";
			else {
				String id = Utils.encrypt(userId + ""); // record id
				return "./previewuserpic" + "?image=" + URLEncoder.encode(id, "UTF-8");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("previewuserpic exception on chatinvitationuserpopuppage",e);
			return "images/loginUser.png";
		}
	}

	private void addUserToFollower(JBUserEntity user) {
		JBUserRoleEntity userRoles = new JBUserRoleEntity();
		userRoles.setEntityId(historyModel.getSelectedEntity().getId());
		userRoles.setUser(user);
		userRoles.setUserRole(Arrays.asList(JibbyConfig.userRoles).indexOf("followers") + 1);
		JBUserRoleEntity userRole = dbService.getTop1ByUserRolesAndEntity(historyModel.getSelectedEntity().getId());
		userRoles.setCustomerid(userRole.getCustomerid());
		dbService.saveUserRoleEntity(userRoles);
	}

	private void closeWindow() {
		Dialog dia = (Dialog) getParent().get();
		dia.close();
	}

	private void buildUserGrid() {
		gridUser.setSelectionMode(SelectionMode.MULTI);
		Column columnName = gridUser.addColumn(e->{
			if(e.getFirstName() == "" || e.getLastName() == ""){
				return e.getEmail();
			}
			else{
				return e.getFirstName()+" "+e.getLastName();
			}
		});
		headerRowfilterName(columnName);
	}
	
	private void headerRowfilterName(Column columnName) {
		// TODO Auto-generated method stub
		header = gridUser.appendHeaderRow();
		txtFilter.setPlaceholder("Search here...");
		txtFilter.addClassName("filter-text-field");
		txtFilter.setValueChangeMode(ValueChangeMode.EAGER);
		txtFilter.setWidth("100%");
		header.getCell(columnName).setComponent(txtFilter);

		txtFilter.addValueChangeListener(e->{
			if(e.getValue()!=null) {
				ListDataProvider<JBUserEntity> dataProvider = (ListDataProvider<JBUserEntity>) gridUser.getDataProvider();
				dataProvider.setFilter(f->{

					if(f.getFirstName() == "" || f.getLastName() == ""){
						return f.getEmail();
					}
					else{
						return f.getFirstName()+" "+f.getLastName();
					}
				}, s -> caseInsensitiveContains(s, e.getValue()));
			}
		});
	}
    
    private Boolean caseInsensitiveContains(String where, String what) {
		return where.toLowerCase().contains(what.toLowerCase());
	}

	public void setup() {
		clearText();
		loadInviteToUserlist();
		dateformat = new SimpleDateFormat(sessionModel.getUser().getDateFormat());
		dateformat.setTimeZone(TimeZone.getTimeZone(sessionModel.getUser().getTimeZone()));
	}

	private void loadInviteToUserlist() {
		gridUser.getDataProvider().refreshAll();
		List<JBUserEntity> userList = new ArrayList<JBUserEntity>();
		
		JBEntity entity = historyModel.getSelectedEntity();
		entity = entityUserAccess.getAllUsersAccessUser(entity);
		
		userList.addAll(entity.getOwnerlist());
		userList.addAll(entity.getAssigneelist());
		userList.addAll(entity.getFollowerlist());
		
		/*userList.addAll(historyModel.getSelectedEntity().getOwnerlist());
		userList.addAll(historyModel.getSelectedEntity().getAssigneelist());
		userList.addAll(historyModel.getSelectedEntity().getFollowerlist());*/

		userList.remove(session.getUser());
		dataProvider = new ListDataProvider<JBUserEntity>(userList);
		gridUser.setDataProvider(dataProvider);
	}
	
	private void clearText() {
		txtFilter.setValue("");
	}
	public interface ChatInvitationUserPopupPageModel extends TemplateModel {
	}
}
