package com.uppstreymi.jibby.item;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("cost-page")
@JsModule("./src/views/item/cost-page.js")
@UIScoped
public class CostPage extends PolymerTemplate<CostPage.CostPageModel> {

	
	@Inject CosteditPage costpage;

	Dialog dl=new Dialog();
    public CostPage() {
    }
    
    public CostPageModel getCostModel() {
    	return getModel();
    }
    
    @PostConstruct
	public void init() {
    	dl.add(costpage);
	}
    
    @EventHandler
    private void _editCostAndQuantities() {
    	costpage.setData();
    	dl.open();
    	dl.setWidth("100%");
    }

    public interface CostPageModel extends TemplateModel {
    	void setSummary(String str);
    	void setQuantity(String str);
    	void setCost(String str);
    	void setShowedit(boolean show);
    }
}
