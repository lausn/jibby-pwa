package com.uppstreymi.jibby.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.item.events.UserAndContactChangeEvent;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("user-access-view")
@JsModule("./src/views/item/user-access-view.js")
@UIScoped
public class UserAccessView extends PolymerTemplate<UserAccessView.UserAccessViewModel> {

	@Id("btnBackNotiDialog")
	private Button btnBackNotiDialog;

	@Id("btnSave")
	private Button btnSave;

	@Id("headerCaption")
	private Label headerCaption;

	@Id("gridUser")
	private Grid<JBUserEntity> gridUser;

	private Column columnName;

	@Inject
	private SessionModel session;

	@Inject
	private DbService service;

	@Inject
	SessionModel sessionModel;

	@Inject
	private HistorModel historyModel;

	@Inject
	private EntityUserAccess entityUserAccess;

	@Inject SyncUserEvent userEvent;

	@Inject Event<UserAndContactChangeEvent> userAndContactEvent;


	private HashMap<String, TextField> hmFilterableTextField = new HashMap<>();
	private ListDataProvider<JBUserEntity> dataProvider;
	private Integer role;
	private String userType;
	private Integer customerid;
	private JBEntity entity;
	private List<JBUserRoleEntity> userRoleList;
	private JBEntity oldEntity;

	TextField txtFilter = new TextField();

	public UserAccessView() {

	}

	@PostConstruct
	public void init() {
		buildUserGrid();
		cmpEvent();

	}

	private void setProperties() {
		entity = historyModel.getSelectedEntity();
		entity = entityUserAccess.getAllUsersAccessUser(entity);
		JBUserRoleEntity userRoleForCustomer= service.getTop1ByUserRolesAndEntity(entity.getId());
		customerid = userRoleForCustomer.getCustomerid();
		userRoleList = new ArrayList<>();

		if(userType.equals("owner")) {
			headerCaption.setText("Select user for add owner");
			if(entity.getDirectOwnerlist().size()>0) {
				for(JBUserEntity user : entity.getDirectOwnerlist()) {
					gridUser.select(user);
				}
			}
		}

		else if(userType.equals("editors")) {
			headerCaption.setText("Select user for add editor");
			if(entity.getDirectAssigneelist().size()>0) {
				for(JBUserEntity user : entity.getDirectAssigneelist()) {
					gridUser.select(user);
				}
			}
		}
		else if(userType.equals("reader")) {
			headerCaption.setText("Select user for add follower");
			if(entity.getDirectFollowerlist().size()>0) {
				for(JBUserEntity user : entity.getDirectFollowerlist()) {
					gridUser.select(user);
				}
			}
		}
		else {
			headerCaption.setText("Select user for add connection");
			if(entity.getDirectConnectionlist().size()>0) {
				for(JBUserEntity user : entity.getDirectConnectionlist()) {
					gridUser.select(user);
				}
			}
		}
	}

	private void cmpEvent() {
		btnBackNotiDialog.addClickListener(e -> {
			closeWindow();
		});

		btnSave.addClickListener(e->{
			if(userType.equals("owner"))
				saveOwner();
			else if(userType.equals("editors"))
				saveEditors();
			else if(userType.equals("reader"))
				saveReader();
			else
				saveConnections();

		});
	}

	private void closeWindow() {
		Dialog parent = (Dialog) this.getParent().get();
		parent.close();
	}


	private void saveConnections() {
		Set<JBUserEntity> users = gridUser.getSelectedItems();
		List<JBUserEntity> connectionlist = entity.getDirectConnectionlist();
		List<JBUserEntity> addConnectionlist = new ArrayList<JBUserEntity>();
		List<JBUserEntity> checkConnectionList = new ArrayList<JBUserEntity>();
		List<JBUserEntity> removeConnectionList = new ArrayList<JBUserEntity>();

		for(JBUserEntity user : users) {
			if(!connectionlist.contains(user))
				addConnectionlist.add(user);
			else
				checkConnectionList.add(user);
		}

		for(JBUserEntity user : connectionlist) {
			if(!checkConnectionList.contains(user))
				removeConnectionList.add(user);
		}

		if(removeConnectionList.size()>0) {
			userRoleList = service.getAllUserRolesByEntityId(historyModel.getParentArray());
			for(JBUserEntity romoveConnectionUser : removeConnectionList) {
				JBUserRoleEntity removeUserRole = userRoleList.stream().filter(e-> e.getUser().equals(romoveConnectionUser) && e.getUserRole().equals(4) && e.getEntityId().equals(entity.getId())).findAny().orElse(null);
				service.removerUserRoleEntity(removeUserRole);
				insertItemModification(entity,  Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removeconnection")+1, romoveConnectionUser, null);
			}
		}

		if(addConnectionlist.size()>0) {
			for(JBUserEntity user: addConnectionlist) {
				if (!connectionlist.contains(user)) {
					connectionlist.add(user);
					entity.getConnectionlist().add(user);
					role = Arrays.asList(JibbyConfig.userRoles).indexOf(JibbyConfig.userRoles[3]) + 1;
					addUserListToEntity(role, "addconnection",user);
				} 
			}
		}
		userAndContactEvent.fire(new UserAndContactChangeEvent(true));
		closeWindow();
	}

	private void saveReader() {
		Set<JBUserEntity> users = gridUser.getSelectedItems();
		List<JBUserEntity> followerlist = entity.getDirectFollowerlist();
		List<JBUserEntity> addFollowerlist = new ArrayList<JBUserEntity>();
		List<JBUserEntity> checkFollowerList = new ArrayList<JBUserEntity>();
		List<JBUserEntity> removeFollowerList = new ArrayList<JBUserEntity>();

		for(JBUserEntity user : users) {
			if(!followerlist.contains(user))
				addFollowerlist.add(user);
			else
				checkFollowerList.add(user);
		}

		for(JBUserEntity user : followerlist) {
			if(!checkFollowerList.contains(user))
				removeFollowerList.add(user);
		}

		if(removeFollowerList.size()>0) {
			userRoleList = service.getAllUserRolesByEntityId(historyModel.getParentArray());
			for(JBUserEntity romoveFollower : removeFollowerList) {
				JBUserRoleEntity removeUserRole = userRoleList.stream().filter(e-> e.getUser().equals(romoveFollower) && e.getUserRole().equals(3) && e.getEntityId().equals(entity.getId())).findAny().orElse(null);
				service.removerUserRoleEntity(removeUserRole);
				insertItemModification(entity,  Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removefollower")+1, romoveFollower, null);
			}
		}

		if(addFollowerlist.size()>0) {
			for(JBUserEntity user: addFollowerlist) {
				if (!followerlist.contains(user)) {
					followerlist.add(user);
					entity.getFollowerlist().add(user);
					role = Arrays.asList(JibbyConfig.userRoles).indexOf(JibbyConfig.userRoles[2]) + 1;
					addUserListToEntity(role, "addfollower",user);
				} 
			}
		}
		userAndContactEvent.fire(new UserAndContactChangeEvent(true));
		closeWindow();
	}

	private void saveEditors() {
		Set<JBUserEntity> users = gridUser.getSelectedItems();
		List<JBUserEntity> assignerlist = entity.getDirectAssigneelist();
		List<JBUserEntity> addAssignerlist = new ArrayList<JBUserEntity>();
		List<JBUserEntity> checkAssignerList = new ArrayList<JBUserEntity>();
		List<JBUserEntity> removeAssignerList = new ArrayList<JBUserEntity>();

		for(JBUserEntity user : users) {
			if(!assignerlist.contains(user))
				addAssignerlist.add(user);
			else
				checkAssignerList.add(user);
		}

		for(JBUserEntity user : assignerlist) {
			if(!checkAssignerList.contains(user))
				removeAssignerList.add(user);
		}

		if(removeAssignerList.size()>0) {
			userRoleList = service.getAllUserRolesByEntityId(historyModel.getParentArray());
			for(JBUserEntity romoveAssigner : removeAssignerList) {
				JBUserRoleEntity removeUserRole = userRoleList.stream().filter(e-> e.getUser().equals(romoveAssigner) && e.getUserRole().equals(2) && e.getEntityId().equals(entity.getId())).findAny().orElse(null);
				service.removerUserRoleEntity(removeUserRole);
				insertItemModification(entity,  Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removeassigner")+1, romoveAssigner, null);
			}
		}

		if(addAssignerlist.size()>0) {
			for(JBUserEntity user: addAssignerlist) {
				if (!assignerlist.contains(user)) {
					assignerlist.add(user);
					entity.getAssigneelist().add(user);
					role = Arrays.asList(JibbyConfig.userRoles).indexOf(JibbyConfig.userRoles[1]) + 1;
					addUserListToEntity(role, "addassigner",user);
				} 
			}
		}
		userAndContactEvent.fire(new UserAndContactChangeEvent(true));
		closeWindow();
	}

	private void saveOwner() {
		if (entity.getOwnerlist().contains(session.getUser())) {
			Set<JBUserEntity> users = gridUser.getSelectedItems();
			List<JBUserEntity> ownerlist = entity.getDirectOwnerlist();
			List<JBUserEntity> addOwnerList = new ArrayList<JBUserEntity>();
			List<JBUserEntity> checkOwnerList = new ArrayList<JBUserEntity>();
			List<JBUserEntity> removeOwnerList = new ArrayList<JBUserEntity>();
			for(JBUserEntity user: users) {
				if (!ownerlist.contains(user)) 
					addOwnerList.add(user);
				else
					checkOwnerList.add(user);
			}

			for(JBUserEntity user: ownerlist) {
				if (!checkOwnerList.contains(user)) 
					removeOwnerList.add(user);
			}

			if(removeOwnerList.size()>0) {
				userRoleList = service.getAllUserRolesByEntityId(historyModel.getParentArray());
				for(JBUserEntity romoveUser : removeOwnerList) {
					JBUserRoleEntity removeUserRole = userRoleList.stream().filter(e-> e.getUser().equals(romoveUser) && e.getUserRole().equals(1) && e.getEntityId().equals(entity.getId())).findAny().orElse(null);
					service.removerUserRoleEntity(removeUserRole);
					insertItemModification(entity,  Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removeowner")+1, romoveUser, null);
				}
			}

			if(addOwnerList.size()>0) {
				for(JBUserEntity user: addOwnerList) {
					if (!ownerlist.contains(user)) {
						ownerlist.add(user);
						entity.getOwnerlist().add(user);
						role = Arrays.asList(JibbyConfig.userRoles).indexOf(JibbyConfig.userRoles[0]) + 1;
						addUserListToEntity(role, "addowner",user);
					} 
				}
			}
		} else
			Notification.show("You do not have rights to create owner", 1000, Position.MIDDLE);

		userAndContactEvent.fire(new UserAndContactChangeEvent(true));
		closeWindow();
	}

	private void addUserListToEntity(Integer userRole, String str,JBUserEntity user) {

		JBUserRoleEntity userRoleEntity = new JBUserRoleEntity();
		userRoleEntity.setEntityId(entity.getId());
		userRoleEntity.setUser(user);
		userRoleEntity.setUserRole(userRole);
		userRoleEntity.setCustomerid(customerid);
		service.saveUserRoleEntity(userRoleEntity);

		userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
		userEvent.addToUserRoleList(userRoleEntity);


		insertItemModification(entity,Arrays.asList(JibbyConfig.itemModificationStatus).indexOf(str)+1,user,null);
		if(entity.getParentProjectId()==null && user!=null) {
			userEvent.addToProjectAddUserList(user.getId());
		}
		userEvent.addHistory();
		userEvent.eventProcessor();
	}

	public void insertItemModification(JBEntity entity,Integer type,JBUserEntity user2,JBChecklistItemEntity checklist){
		JBItemModification itemModification=new JBItemModification();
		itemModification.setEntityId(entity.getId());
		itemModification.setUserId1(session.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setUserId2(user2 == null?null:user2.getId());
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		Integer removeOwner = Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removeowner")+1;
		Integer removeAssigner = Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removeassigner")+1;
		Integer removeFollower = Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removefollower")+1;
		Integer removeConnection = Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("removeconnection")+1;

		if(entity.getParentProjectId()==null && user2!=null && 
				(type.equals(removeOwner) || type.equals(removeAssigner) || 
						type.equals(removeFollower) || type.equals(removeConnection))) {
			Integer isMultiple = entity.getOwnerlist().contains(user2)?1:0;
			isMultiple += entity.getAssigneelist().contains(user2)?1:0;
			isMultiple += entity.getFollowerlist().contains(user2)?1:0;
			isMultiple += entity.getConnectionlist().contains(user2)?1:0;
			if(isMultiple.equals(1))
				userEvent.addToRemoveProjectUserList(user2.getId());
		}
		userEvent.addToHistoryList(itemModification);
		if(user2 != null)
			userEvent.addToAssigneeList(user2);
		userEvent.setEntity(entity);
		if(checklist != null)
			userEvent.addToChecklist(checklist);
		userEvent.setSender(session.getUser());
		userEvent.setOldEntity(oldEntity);
		userEvent.syncDataReceiver(entity);

		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity
						: service.getEntity(entity.getParentProjectId()));
	}

	private void buildUserGrid() {

		gridUser.setSelectionMode(SelectionMode.MULTI);
		columnName = gridUser.addColumn(e -> {

			if (e.getFirstName().equals("") || e.getLastName().equals("")) {
				return e.getEmail();
			} else {
				return e.getFirstName() + " " + e.getLastName();
			}

		}).setKey("username");

		addFilterableField();
	}

	public void setup(String userType) {
		txtFilter.clear();
		gridUser.getDataProvider().refreshAll();
		this.userType = userType;
		oldEntity=new JBEntity();
		oldEntity.copyEntity(historyModel.getSelectedEntity());
		loadUserGridData();
		setProperties();
	}

	private void loadUserGridData() {
		List<JBUserEntity> userList = new ArrayList<JBUserEntity>();
		userList = service.getAllUser();
		userList.remove(session.getUser());
		dataProvider = new ListDataProvider<JBUserEntity>(userList);
		gridUser.setDataProvider(dataProvider);
	}

	private void addFilterableField() {

		hmFilterableTextField.clear();
		HeaderRow headerRow = gridUser.appendHeaderRow();
		headerRow.getCell(columnName).setComponent(getFilterableTextField("username"));
	}

	private TextField getFilterableTextField(String key) {


		txtFilter.getElement().getStyle().set("width", "100%");
		txtFilter.getElement().getStyle().set("font-size", "14px");
		txtFilter.setPlaceholder("Search here...");
		txtFilter.getElement().getThemeList().set("small", true);

		txtFilter.setValueChangeMode(ValueChangeMode.EAGER);
		txtFilter.addValueChangeListener(event -> {
			dataProvider.setFilter(user -> compare(user));
		});
		hmFilterableTextField.put(key, txtFilter);
		return txtFilter;
	}

	private boolean compare(JBUserEntity user) {

		String username = hmFilterableTextField.get("username").getValue();
		return StringUtils
				.containsIgnoreCase((user.getFirstName().equals("") || user.getLastName().equals("")) ? user.getEmail()
						: user.getFirstName() + " " + user.getLastName(), username);
	}

	public interface UserAccessViewModel extends TemplateModel {

	}
}
