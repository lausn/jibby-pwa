package com.uppstreymi.jibby.item;

import com.vaadin.flow.templatemodel.TemplateModel;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

/**
 * A Designer generated component for the assigned-history-page.html template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("assigned-history-page")
@JsModule("./src/views/item/assigned-history-page.js")
@UIScoped
public class AssignedHistoryPage extends PolymerTemplate<AssignedHistoryPage.AssignedHistoryPageModel> {

    /**
     * Creates a new AssignedHistoryPage.
     */
	
	
    public AssignedHistoryPage() {
        // You can initialise any data required for the connected UI components here.
    }
    
   
    
    public AssignedHistoryPageModel getAssignedHistoryModel() {
    	return getModel();
    }

    /**
     * This model binds properties between AssignedHistoryPage and assigned-history-page.html
     */
    public interface AssignedHistoryPageModel extends TemplateModel {
        // Add setters and getters for template properties here.
    	
    	void setAssignedHistory(String assignedHistory);
    	String getAssignedHistory();
    }
}
