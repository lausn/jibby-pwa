package com.uppstreymi.jibby.item;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.item.events.AttachmentDescriptionEvent;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("attachment-description-popup-page")
@JsModule("./src/views/item/attachment-description-popup-page.js")
@UIScoped
public class AttachmentDescriptionPopupPage
		extends PolymerTemplate<AttachmentDescriptionPopupPage.AttachmentDescriptionPopupPageModel> {

	@Id("taDescription")
	private TextArea taDescription;
	@Id("btnSave")
	private Button btnSave;
	@Id("btnCancel")
	private Button btnCancel;

	@Inject
	private DbService dbService;
	@Inject
	Event<AttachmentDescriptionEvent> attachmentdescriptionEvent;
	@Inject
	SyncUserEvent userEvent;
	@Inject
	private SessionModel session;
	@Inject
	private HistorModel historyModel;

	private JBAttachmentEntity attachmentEntity;

	public AttachmentDescriptionPopupPage() {
	}

	@PostConstruct
	private void init() {
		btnSave.addClickListener(e -> {
			attachmentEntity.setDescription(taDescription.getValue());
			dbService.saveAttachment(attachmentEntity);
			insertItemModification(historyModel.getSelectedEntity(),attachmentEntity);
			closeWindow();
			AttachmentDescriptionEvent event = new AttachmentDescriptionEvent(attachmentEntity);
			attachmentdescriptionEvent.fire(event);
		});

		btnCancel.addClickListener(e -> {
			closeWindow();
		});
	}

	public void insertItemModification(JBEntity entity, JBAttachmentEntity attachment) {
		userEvent.setItemModificationRemoveId(attachment.getId()+"-"+attachment);
		userEvent.setEventType(SyncEventType.EVENT_ENTITY_ATTACHMENT_DESCRIPTION);
		userEvent.setEntity(entity);
		userEvent.setAttachments(attachment);
		userEvent.setSender(session.getUser());
		userEvent.syncDataReceiver(entity);
		userEvent.addAttachment();
		userEvent.eventProcessor();
	}

	private void closeWindow() {
		Dialog dl = (Dialog) getParent().get();
		dl.close();
	}

	public void setup(JBAttachmentEntity attachmentEntity) {
		taDescription.clear();
		this.attachmentEntity = attachmentEntity;
		taDescription.setValue(attachmentEntity.getDescription());
	}

	public interface AttachmentDescriptionPopupPageModel extends TemplateModel {
	}
}
