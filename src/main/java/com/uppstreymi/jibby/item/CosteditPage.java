package com.uppstreymi.jibby.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.item.events.CostChangeEvent;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("costedit-page")
@JsModule("./src/views/item/costedit-page.js")
@UIScoped
public class CosteditPage extends PolymerTemplate<CosteditPage.CosteditPageModel> {

	@Inject DbService service;
	@Inject private SyncUserEvent userEvent;
	@Inject SessionModel sessionModel;
	@Id("typewrapper")
	private Div typewrapper;
	@Id("btnCancel")
	private Button btnCancel;
	@Id("btnSave")
	private Button btnSave;
	RadioButtonGroup<String> typerdo = new RadioButtonGroup<>();
	@Id("unit")
	private TextField unit;
	@Id("cost")
	private TextField cost;
	@Inject private Event<CostChangeEvent> costChangeEvent;
	@Inject private HistorModel historyModel;
	private Logger logger=LogManager.getLogger(CosteditPage.class);

	private JBEntity entity;
	public CosteditPage() {
		unit.setMaxLength(11);
		cost.setMaxLength(9);
		List<String> items=new ArrayList<>();
		items.add("1");
		items.add("2");
		items.add("3");
		typerdo.setItems(items);


		List<String> caption=new ArrayList<>();
		caption.add("Disable");
		caption.add("Worksheet");
		caption.add("Summary");
		typerdo.setRenderer(new TextRenderer<String>(p->{return caption.get(items.indexOf(p));}));

		typewrapper.add(typerdo);
	}
	
	@PostConstruct
	private void init() {
		btnCancel.addClickListener(e->{
			closeWindow();
		});

		btnSave.addClickListener(e->{
			if(!unit.getValue().isEmpty() && !cost.getValue().isEmpty()) {
				try {
				JBEntity oldentity= new JBEntity();
				oldentity.copyEntity(entity);
				entity.setWorksheetType(Integer.valueOf(typerdo.getValue()));
				entity.setUnitQuantity(Double.valueOf(unit.getValue()));
				entity.setCostPerUnit(Integer.valueOf(cost.getValue()));
				service.saveEntity(entity);
				getAllUsersAccessUser(oldentity,entity);
				if(!oldentity.getUnitQuantity().equals(entity.getUnitQuantity()) ||
						!oldentity.getCostPerUnit().equals(entity.getCostPerUnit()) ||
						!oldentity.getWorksheetType().equals(entity.getWorksheetType())){
					userEvent.setOldEntity(oldentity);
					insertItemModification(entity,Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("edit")+1);
					userEvent.addHistory();
					userEvent.eventProcessor();
				}
				costChangeEvent.fire(new CostChangeEvent(true));
				closeWindow();
				}
				catch(Exception ex) {
					logger.error("invalid value:",ex);
				}
			}else {
				Notification.show("Unit Quantity/Cost value can't be empty!",2000,Position.BOTTOM_CENTER);
			}
		});
	}
	
	private void closeWindow() {
		Dialog dl= (Dialog)getParent().get();
		dl.close();
	}

	public void setData() {
		this.entity=historyModel.getSelectedEntity();
		typerdo.setValue(entity.getWorksheetType()+"");
		unit.setValue(entity.getUnitQuantity()+"");
		cost.setValue(entity.getCostPerUnit()+"");
	}

	public interface CosteditPageModel extends TemplateModel {

	}
	
	public void insertItemModification(JBEntity entity,Integer type){
		JBItemModification itemModification=new JBItemModification();
		itemModification.setEntityId(entity.getId());
		itemModification.setUserId1(sessionModel.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
		userEvent.addToHistoryList(itemModification);
		userEvent.setEntity(entity);
		userEvent.setSender(sessionModel.getUser());
		userEvent.syncDataReceiver(entity);
		
		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity
						: service.getEntity(entity.getParentProjectId()));
	}
	
	private JBEntity getAllUsersAccessUser(JBEntity oldEntity,JBEntity selectedEntity){

		selectedEntity.setOwnerlist(oldEntity.getOwnerlist());
		selectedEntity.setAssigneelist(oldEntity.getAssigneelist());
		selectedEntity.setFollowerlist(oldEntity.getFollowerlist());
		selectedEntity.setConnectionlist(oldEntity.getConnectionlist());

		selectedEntity.setDirectOwnerlist(oldEntity.getDirectOwnerlist());
		selectedEntity.setDirectAssigneelist(oldEntity.getDirectAssigneelist());
		selectedEntity.setDirectFollowerlist(oldEntity.getDirectFollowerlist());
		selectedEntity.setDirectConnectionlist(oldEntity.getDirectConnectionlist());

		selectedEntity.setParentArray(oldEntity.getParentArray());
		return selectedEntity;
	}
}
