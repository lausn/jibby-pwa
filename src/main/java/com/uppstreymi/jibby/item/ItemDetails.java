package com.uppstreymi.jibby.item;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.imageio.ImageIO;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.flow.helper.AsyncManager;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.DataChangeModelListener;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.ModelUpdateEvent;
import com.uppstreymi.jibby.backend.sync.manager.SynchronizeManager;
import com.uppstreymi.jibby.backend.sync.process.ProcessData;
import com.uppstreymi.jibby.bean.ChatAndHistoryBean;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.item.events.AttachmentDescriptionEvent;
import com.uppstreymi.jibby.item.events.ChangeCheckListEvent;
import com.uppstreymi.jibby.item.events.CostChangeEvent;
import com.uppstreymi.jibby.item.events.DescriptionEvent;
import com.uppstreymi.jibby.item.events.HistoryAndCommentsSyncEvent;
import com.uppstreymi.jibby.item.events.MyActionlistAssignedEvent;
import com.uppstreymi.jibby.item.events.SettingItemEvent;
import com.uppstreymi.jibby.item.events.StatusChangeEvent;
import com.uppstreymi.jibby.item.events.SyncModelGenerateEvent;
import com.uppstreymi.jibby.item.events.TittleEvent;
import com.uppstreymi.jibby.item.events.TopEditOrDeleteButtonEvent;
import com.uppstreymi.jibby.item.events.UserAndContactChangeEvent;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.MainLayout;
import com.uppstreymi.jibby.ui.components.navigation.bar.AppBar.NaviMode;
import com.uppstreymi.jibby.utils.HasLogger;
import com.uppstreymi.jibby.utils.JibbyColBFilter;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.WebBrowser;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.templatemodel.TemplateModel;

import elemental.json.Json;

@Tag("item-details")
@JsModule("./src/views/item/item-details.js")
@Route(value = "item/details", layout = MainLayout.class)
@UIScoped
public class ItemDetails extends PolymerTemplate<ItemDetails.ItemDetailsModel>
		implements HasLogger, HasUrlParameter<Integer>, DataChangeModelListener {

	@Inject
	private DbService dbService;
	@Inject
	private Event<TittleEvent> tittleEvent;
	@Inject
	Event<SettingItemEvent> settingEvent;
	@Inject
	Event<UserAndContactChangeEvent> userAndContactEvent;
	@Inject
	private SessionModel session;
	@Inject
	private HistorModel historyModel;
	@Inject
	private ProjectViewModel projectModel;
	@Inject
	private JibbyColBFilter filter;
	@Inject
	Event<TopEditOrDeleteButtonEvent> deleteOrEditButtonEvent;
	@Inject
	Event<HistoryAndCommentsSyncEvent> historyAndCommentsEvent;
	@Inject
	private SyncUserEvent userEvent;

	@Inject
	SynchronizeManager syncManager;

	SimpleDateFormat dateformat;
	private SyncDataChangeModel dataChangemodel;
	private List<JBUserRoleEntity> userRoleList = new ArrayList<>();

	List<JBUserEntity> ownerlist = new ArrayList<JBUserEntity>();
	List<JBUserEntity> assignerlist = new ArrayList<JBUserEntity>();
	List<JBUserEntity> followerlist = new ArrayList<JBUserEntity>();
	List<JBUserEntity> connectionlist = new ArrayList<JBUserEntity>();

	List<JBUserEntity> directOwnerlist = new ArrayList<JBUserEntity>();
	List<JBUserEntity> directAssignerlist = new ArrayList<JBUserEntity>();
	List<JBUserEntity> directFollowerlist = new ArrayList<JBUserEntity>();
	List<JBUserEntity> directConnectionlist = new ArrayList<JBUserEntity>();

	Integer ownerId = Arrays.asList(JibbyConfig.userRoles).indexOf("owner");
	Integer assigneeId = Arrays.asList(JibbyConfig.userRoles).indexOf("assignee");
	Integer followersId = Arrays.asList(JibbyConfig.userRoles).indexOf("followers");
	Integer connectionId = Arrays.asList(JibbyConfig.userRoles).indexOf("connections");

	private JBEntity selectedEntity;

	@Id("statusWrapper")
	private Div statusWrapper;

	@Id("descriptionWrapper")
	private Div descriptionWrapper;

	@Id("item-status")
	private StatusPage itemStatus;

	@Id("item-description")
	private DescriptionPage itemDescription;

	@Id("item-cost")
	private CostPage itemCost;

	@Id("checklistWrapper")
	private Div checklistWrapper;

	@Id("checklistPage")
	private ChecklistPage checklistPage;

	@Id("attachmentPage")
	private AttachmentPage attachmentPage;

	@Id("collapseStatusWrapper")
	private Div collapseStatusWrapper;

	@Id("collapseDescriptionWrapper")
	private Div collapseDescriptionWrapper;

	@Id("collapseChatWrapper")
	private Div collapseChatWrapper;

	@Id("collapseCostWrapper")
	private Div collapseCostWrapper;

	@Id("collapseChecklistWrapper")
	private Div collapseChecklistWrapper;

	@Id("collapseAttachmentWrapper")
	private Div collapseAttachmentWrapper;

	@Id("collapseHistoryWrapper")
	private Div collapseHistoryWrapper;

	@Id("collapseUserContactWrapper")
	private Div collapseUserContactWrapper;

	@Id("assignedHistoryPage")
	private AssignedHistoryPage assignedHistoryPage;

	@Id("userContactPage")
	private UserContactPage userContactPage;

	@Id("historyAndCommentsPage")
	private HistoryAndCommentsPage historyAndCommentsPage;

	@Id("uploadLayout")
	private Div uploadLayout;

	@Id("userContactDetail")
	private Details userContactDetail;
	@Id("historyAndCommentsDetails")
	private Details historyAndCommentsDetails;
	@Id("costAndQuantityDetails")
	private Details costAndQuantityDetails;

	Upload uploader;
	MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
	SucceededEvent succeedEvent;
	Button uploadButton = new Button("");
	private Logger logger = LogManager.getLogger(ItemDetails.class);
	private UI ui;

	public ItemDetails() {

	}

	@PostConstruct
	public void init() {
		fileUploaderInit();
		userContactDetail.addOpenedChangeListener(e -> {
			if (userContactDetail.isOpened())
				userContactDetailsClick();
		});

		historyAndCommentsDetails.addOpenedChangeListener(e -> {
			if (historyAndCommentsDetails.isOpened())
				historyAndCommentsDetailsClick();
		});

		costAndQuantityDetails.addOpenedChangeListener(e -> {
			if (costAndQuantityDetails.isOpened())
				costAndQuantityDetailsClick();
		});
	}

	private void fileUploaderInit() {

		uploader = new Upload(buffer);
		uploader.setMaxFiles(1);
		uploader.setDropAllowed(false);
		uploader.setAcceptedFileTypes("image/*");
		uploader.getElement().removeProperty("capture");

		Icon uploadIc = VaadinIcon.CAMERA.create();
		uploadIc.getElement().setAttribute("style", "font-size: 28px !important; padding-left: 8px;top: -4px;");
		uploadButton.setIcon(uploadIc);
		uploadButton.addClassName("add-image");
		uploadButton.getElement().setAttribute("theme", "primary small icon");
		uploader.setUploadButton(uploadButton);

		uploader.addSucceededListener(event -> {
			succeedEvent = event;
			saveUploadedImage();
		});

		uploader.getElement().addEventListener("file-abort", remove -> {
			succeedEvent = null;
		});

		uploadLayout.add(uploader);
	}

	private void saveUploadedImage() {
		if (succeedEvent != null && selectedEntity != null) {
			addAttachmentData();
			setAttachment();
			succeedEvent = null;
			attachmentAdded();
		}
		uploader.getElement().setPropertyJson("files", Json.createArray());
	}

	private void attachmentAdded() {
		if(selectedEntity.getParentId()!=null) {
			List<ItemBean> items = historyModel.getEntitiesByKey(selectedEntity.getParentId());
			ItemBean itemBean = items.stream().filter(e -> e.getEntity().getId().equals(selectedEntity.getId())).findAny()
					.orElse(null);
			itemBean.setIsAttachment(true);
		}
		else {
			ItemBean itemBean=projectModel.getItemBean();
			itemBean.setIsAttachment(true);
		}
	}

	public void addAttachmentData() {
		JBAttachmentEntity attachment = new JBAttachmentEntity();
		attachment.setEntity(selectedEntity.getId());
		attachment.setName(succeedEvent.getFileName());
		attachment.setType(succeedEvent.getMIMEType());
		attachment.setSize((int) succeedEvent.getContentLength());
		attachment.setuploadDate(new Date());
		attachment.setDescription("");
		attachment.setUploaderUser(session.getUser().getId());
		attachment.setSortOrder(0);
		dbService.saveAttachment(attachment);

		userEvent.setEventType(SyncEventType.EVENT_ENTITY_ATTACHMENT_ADD);
		userEvent.setEntity(selectedEntity);
		userEvent.setAttachments(attachment);
		userEvent.setSender(session.getUser());
		userEvent.syncDataReceiver(selectedEntity);
		userEvent.addAttachment();
		userEvent.eventProcessor();

		fileMoved(attachment);
	}

	private void fileMoved(JBAttachmentEntity attachment) {
		try {
			File fileFolder = new File(Utils.imagePath);
			if (!fileFolder.exists())
				fileFolder.mkdirs();

			File file = createTempFile();
			String orientation = "";
			Metadata mata = ImageMetadataReader.readMetadata(file);
			for (Directory directory : mata.getDirectories()) {

				for (com.drew.metadata.Tag tag : directory.getTags()) {
					if (tag.getTagName().equalsIgnoreCase("Orientation")) {
						orientation = tag.getDescription();
						break;
					}
				}
			}

			String fileType = succeedEvent.getMIMEType();
			String fileName = String.valueOf(attachment.getId());

			BufferedImage img = ImageIO.read(file);
			fileType = fileType.split("\\/")[1];
			File targetFile = new File(Utils.imagePath + fileName);
			if (orientation.equalsIgnoreCase("Right side, top (Rotate 90 CW)"))
				ImageIO.write(Utils.rotateImage(img), fileType, targetFile);
			else
				ImageIO.write(img, fileType, targetFile);
			file.delete();
			Notification.show("Image saved successfully.", 2000, Position.MIDDLE);
			logger.info("Uploaded image save.");
		} catch (Exception e) {
			dbService.deleteAttachment(attachment);
			Notification.show("Attachment not added!", 2000, Position.MIDDLE);
			logger.error("Uploaded image not saved ", e);
		}
	}

	private File createTempFile() {
		try {

			File file = File.createTempFile(succeedEvent.getFileName(), "");

			byte[] buf = new byte[(int) succeedEvent.getContentLength()];
			InputStream is = buffer.getInputStream(succeedEvent.getFileName());
			is.read(buf);

			OutputStream outStream = new FileOutputStream(file);
			outStream.write(buf);

			outStream.flush();
			outStream.close();
			return file;
		} catch (IOException ex) {
			logger.error("An exception thrown in createTempFile itemdetails.", ex);
		}
		return null;
	}

	public static boolean isIPhone() {
		WebBrowser browser = UI.getCurrent().getSession().getBrowser();
		return browser.isIPhone() ? true : false;
	}

	public interface ItemDetailsModel extends TemplateModel {
		void setItemicon(String src);

		void setItemname(String name);
	}

	@Override
	public void setParameter(BeforeEvent event, Integer parameter) {
		if (session.getUser() != null) {
			dateformat = new SimpleDateFormat(session.getUser().getDateFormat());
			dateformat.setTimeZone(TimeZone.getTimeZone(session.getUser().getTimeZone()));

			historyModel.getParentArray().add(parameter); // add to parent array when forward
			SettingItemEvent myevent = new SettingItemEvent(false, true);
			settingEvent.fire(myevent);
			historyModel.setSelectedEntity(dbService.getEntity(parameter));
			setItemDetails(parameter);
			if (historyModel.getSelectedEntity().getStatus() == JibbyConfig.status[1])
				deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false, true));
		}
	}

	private void setItemDetails(Integer id) {

		selectedEntity = historyModel.getSelectedEntity();
		clearAll();

		TittleEvent te = new TittleEvent();
		te.setTittle("");
		tittleEvent.fire(te);
		setName();

		setStatus();
		setDescription();
		setChecklist();
		setAttachment();
		setAssignedHistory();
	}

	private void clearAll() {

		userRoleList.clear();
		ownerlist.clear();
		assignerlist.clear();
		followerlist.clear();
		connectionlist.clear();
		directOwnerlist.clear();
		directAssignerlist.clear();
		directFollowerlist.clear();
		directConnectionlist.clear();
		userContactPage.getUserContactModel().setHtml("");
		itemCost.getCostModel().setShowedit(false);
		historyAndCommentsPage.getHistoryAndCommentsModel().setChatItems(new ArrayList<ChatAndHistoryBean>());
		historyAndCommentsPage.getHistoryAndCommentsModel().setCreatorName("");
		historyAndCommentsPage.getHistoryAndCommentsModel().setEntityCreateDate("");
	}

	private void setAssignedHistory() {
		String assignTo = "";

		List<JBUserEntity> users = dbService.getAllAssigneeUserById(selectedEntity.getId(), 1);

		for (JBUserEntity user : users) {
			if (!assignTo.isEmpty())
				assignTo += ", ";
			assignTo += user.getFirstName() + " " + user.getLastName();
		}

		if (users.size() > 0) {
			collapseHistoryWrapper.setVisible(true);
			assignedHistoryPage.getAssignedHistoryModel().setAssignedHistory(assignTo);
		}
	}

	private void setAttachment() {

		collapseAttachmentWrapper.setVisible(false);
		attachmentPage.clear();
		List<JBAttachmentEntity> attachments = dbService.getAttachmentByEntity(selectedEntity.getId());
		if (attachments.size() > 0) {
			collapseAttachmentWrapper.setVisible(true);
			attachmentPage.setAttachmentList(attachments);
		}
	}

	private void setChecklist() {

		checklistPage.setup();
		List<JBChecklistItemEntity> checklists = dbService.getChecklistByEntity(selectedEntity.getId());

		if (isAssignee())
			checklistPage.getChecklistModel().setIsAssignee(true);
		else
			checklistPage.getChecklistModel().setIsAssignee(false);
		if (checklists.size() > 0) {
			checklistPage.getChecklistModel().setChecklists(checklists);
		}
	}

	private void setName() {
		getModel().setItemicon(selectedEntity.getEntityType() + "");
		getModel().setItemname(selectedEntity.getName());
	}

	private void setStatus() {
		itemStatus.getStatusModel()
				.setDone((selectedEntity.getStatus().equals(JibbyConfig.status[0]) ? "Done" : "Not Done"));
		itemStatus.getStatusModel().setPriority(JibbyConfig.priorities[selectedEntity.getPriority() - 1]);
		itemStatus.getStatusModel().setDuration(getDuration());
		if (isAssignee())
			itemStatus.getStatusModel().setShowedit(false);
		else
			itemStatus.getStatusModel().setShowedit(true);
	}

	private void setDescription() {
		itemDescription.getDescriptionModel()
				.setHtml(selectedEntity.getDescription() == null ? "" : selectedEntity.getDescription());
	}

	private void setCost(JBUserEntity user) {
		// worksheet type
		if (selectedEntity.getWorksheetType()
				.equals(Arrays.asList(JibbyConfig.worksheetTypes).indexOf("Worksheet Enabled") + 1)) {
			setCostQuantity(selectedEntity.getUnitQuantity(),
					selectedEntity.getCostPerUnit() * selectedEntity.getUnitQuantity(), selectedEntity.getId(), user);
		}
		// summery type
		else if (selectedEntity.getWorksheetType()
				.equals(Arrays.asList(JibbyConfig.worksheetTypes).indexOf("Summary") + 1)) {
			if (!historyModel.getSelectedItemHasChild()) {
				setCostQuantity(selectedEntity.getUnitQuantity(),
						selectedEntity.getCostPerUnit() * selectedEntity.getUnitQuantity(), selectedEntity.getId(),
						user);
			} else {
				setSummeryCostAndQuantities(selectedEntity, user);
			}
		} else {
			setCostQuantity(0.0, 0.0, selectedEntity.getId(), user);
		}
		logger.info("{} set cost successfully!.", user.getFirstName());
	}

	@SuppressWarnings("unchecked")
	public void setSummeryCostAndQuantities(JBEntity entity, JBUserEntity user) {

		List<Object[]> allEntities = (List<Object[]>) dbService.getAllItemByProjectId(
				entity.getParentProjectId() == null ? entity.getId() : entity.getParentProjectId(), user.getId());
		List<Double> costandquantity = new ArrayList<>();
		costandquantity.add(entity.getUnitQuantity());
		costandquantity.add(entity.getCostPerUnit() * entity.getUnitQuantity());
		setChildCostAndQuantitesList(allEntities, entity, costandquantity);

		setCostQuantity(costandquantity.get(0), costandquantity.get(1), entity.getId(), user);
		logger.info("{} set summary and quantities successfully!.", user.getFirstName());

	}

	public void setChildCostAndQuantitesList(List<Object[]> allEntities, JBEntity parententity,
			List<Double> costandquantity) {

		List<JBEntity> immediateItem = dbService.getAllEntitiesByParentOrderBy(parententity.getId(),
				projectModel.getSortOption().getCriteria(), projectModel.getSortOption().isAsc());
		for (JBEntity entity : immediateItem) {
			// return true For show items in tree OR not show;
			boolean hasChild = getItem(entity, entity.getId(), allEntities, null, true);
			if (hasChild) {
				setCostData(entity, costandquantity);
				setChildCostAndQuantitesList(allEntities, entity, costandquantity);
			} else if (!hasChild && isValidFilter(entity, null, true)) {
				setCostData(entity, costandquantity);
			}
		}
	}

	public void setCostData(JBEntity entity, List<Double> costandquantity) {
		Double costunit = entity.getCostPerUnit() * entity.getUnitQuantity();
		costandquantity.set(0, costandquantity.get(0) + entity.getUnitQuantity());
		costandquantity.set(1, costandquantity.get(1) + costunit);
	}

	private void setCostQuantity(Double unitQuantity, Double costintoUnit, Integer entityId, JBUserEntity user) {
		String decimalSeparator = user.getDecimalSeparator();
		String thousandSeparator = user.getThousandSeparator();
		String unit = unitQuantity > 0
				? Utils.dblToStrWithSeparator(unitQuantity, decimalSeparator, thousandSeparator, 2)
				: "  ";
		String cost = costintoUnit > 0
				? Utils.dblToStrWithSeparator(costintoUnit, decimalSeparator, thousandSeparator, 2)
				: "  ";

		String type = "";
		if (selectedEntity.getWorksheetType().equals(Arrays.asList(JibbyConfig.worksheetTypes).indexOf("Summary") + 1))
			type = "Summary: ";
		itemCost.getCostModel().setSummary(type);
		itemCost.getCostModel().setQuantity(unit);
		itemCost.getCostModel().setCost(cost);
		logger.info("{} set CostQuantity successfully!.", user.getFirstName());
	}

	public String getDuration() {
		String str = "";
		if (selectedEntity.getEstimatedStartDate() != null && selectedEntity.getEstimatedFinishDate() != null) {
			str = dateformat.format(selectedEntity.getEstimatedStartDate()) + " - "
					+ dateformat.format(selectedEntity.getEstimatedFinishDate());
		}
		return str;
	}

	private void userContactEventChange(@Observes UserAndContactChangeEvent event) {
		userContactPage.getUserContactModel().setHtml("");
		userRoleList.clear();
		ownerlist.clear();
		assignerlist.clear();
		followerlist.clear();
		connectionlist.clear();
		directOwnerlist.clear();
		directAssignerlist.clear();
		directFollowerlist.clear();
		directConnectionlist.clear();
		if (event.isChange()) {
			userContactDetailsClick();
		}
	}

	private void userContactDetailsClick() {
		if (userContactPage.getUserContactModel().getHtml().isEmpty()) {
			getUserRolesData();
			String html = getUserContactData(false);
			userContactPage.getUserContactModel().setHtml(html);
		}
	}

	private void getUserRolesData() {
		if (userRoleList.size() == 0) {
			userRoleList = dbService.getAllUserRolesByEntityId(historyModel.getParentArray());
			for (JBUserRoleEntity userroles : userRoleList) {
				if (userroles.getUserRole() - 1 == ownerId) {
					ownerlist.add(userroles.getUser());
				} else if (userroles.getUserRole() - 1 == assigneeId) {
					assignerlist.add(userroles.getUser());
				} else if (userroles.getUserRole() - 1 == followersId) {
					followerlist.add(userroles.getUser());
				} else if (userroles.getUserRole() - 1 == connectionId) {
					connectionlist.add(userroles.getUser());
				}
				if (userroles.getEntityId().equals(selectedEntity.getId())) { // for find direct user role from list
					if (userroles.getUserRole() - 1 == ownerId) {
						directOwnerlist.add(userroles.getUser());
					}
					if (userroles.getUserRole() - 1 == assigneeId) {
						directAssignerlist.add(userroles.getUser());
					}
					if (userroles.getUserRole() - 1 == followersId) {
						directFollowerlist.add(userroles.getUser());
					}
					if (userroles.getUserRole() - 1 == connectionId) {
						directConnectionlist.add(userroles.getUser());
					}
				}
			}
			selectedEntity.setOwnerlist(ownerlist);
			selectedEntity.setAssigneelist(assignerlist);
			selectedEntity.setFollowerlist(followerlist);
			selectedEntity.setConnectionlist(connectionlist);
			selectedEntity.setDirectOwnerlist(directOwnerlist);
			selectedEntity.setDirectAssigneelist(directAssignerlist);
			selectedEntity.setDirectFollowerlist(directFollowerlist);
			selectedEntity.setDirectConnectionlist(directConnectionlist);

		}
	}

	private String getUserContactData(boolean isCallFromSync) {

		if (isCallFromSync) {
			directOwnerlist = selectedEntity.getDirectOwnerlist();
			directAssignerlist = selectedEntity.getDirectAssigneelist();
			directFollowerlist = selectedEntity.getDirectFollowerlist();
			directConnectionlist = selectedEntity.getDirectConnectionlist();
			ownerlist = selectedEntity.getOwnerlist();
			assignerlist = selectedEntity.getAssigneelist();
			followerlist = selectedEntity.getFollowerlist();
			connectionlist = selectedEntity.getConnectionlist();

		}

		String owner = "", assigner = "", follower = "", connection = "";

		List<JBUserEntity> ownerList = new ArrayList<>();
		List<JBUserEntity> assignerList = new ArrayList<>();
		List<JBUserEntity> followerList = new ArrayList<>();
		List<JBUserEntity> connectionList = new ArrayList<>();

		List<JBUserEntity> directOwnerList = new ArrayList<>();
		List<JBUserEntity> directAssignerList = new ArrayList<>();
		List<JBUserEntity> directFollowerList = new ArrayList<>();
		List<JBUserEntity> directConnectionList = new ArrayList<>();

		Integer i = 0;
		for (JBUserEntity user : directOwnerlist) {
			if (!directOwnerList.contains(user)) {
				directOwnerList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (i != 0)
					owner += ", ";
				owner += "<span style = 'color:#000000;'>" + value + "</span>";
				i++;
			}
		}
		i = 0;
		for (JBUserEntity user : ownerlist) {
			if (!directOwnerList.contains(user) && !ownerList.contains(user)) {
				ownerList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (directOwnerList.size() > 0 || i != 0)
					owner += ", ";
				owner += "<span>" + value + "</span>";
				i++;

			}
		}
		i = 0;
		for (JBUserEntity user : directAssignerlist) {
			if (!directAssignerList.contains(user)) {
				directAssignerList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (i != 0)
					assigner += ", ";
				assigner += "<span style = 'color:#000000;'>" + value + "</span>";
				i++;
			}
		}
		i = 0;
		for (JBUserEntity user : assignerlist) {
			if (!directAssignerList.contains(user) && !assignerList.contains(user)) {
				assignerList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (directAssignerList.size() > 0 || i != 0)
					assigner += ", ";
				assigner += "<span>" + value + "</span>";
				i++;

			}
		}

		i = 0;
		for (JBUserEntity user : directFollowerlist) {
			if (!directFollowerList.contains(user)) {
				directFollowerList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (i != 0)
					follower += ", ";
				follower += "<span style = 'color:#000000;'>" + value + "</span>";
				i++;
			}
		}
		i = 0;
		for (JBUserEntity user : followerlist) {
			if (!directFollowerList.contains(user) && !followerList.contains(user)) {
				followerList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (directFollowerList.size() > 0 || i != 0)
					follower += ", ";
				follower += "<span>" + value + "</span>";
				i++;

			}
		}
		i = 0;
		for (JBUserEntity user : directConnectionlist) {
			if (!directConnectionList.contains(user)) {
				directConnectionList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (i != 0)
					connection += ", ";
				connection += "<span style = 'color:#000000;'>" + value + "</span>";
				i++;
			}
		}
		i = 0;
		for (JBUserEntity user : connectionlist) {
			if (!directConnectionList.contains(user) && !connectionList.contains(user)) {
				connectionList.add(user);
				String value = !user.getFirstName().isEmpty() || !user.getLastName().isEmpty()
						? user.getFirstName() + " " + user.getLastName()
						: user.getEmail();
				if (directConnectionList.size() > 0 || i != 0)
					connection += ", ";
				connection += "<span>" + value + "</span>";
				i++;

			}
		}

		String html = "<div class=marginTop><b style='color: #000;font-weight: 500;margin-right: 5px;'>Owners: </b><span style = 'color:#808080;'>"
				+ owner + "</span></div>"
				+ "<div><b style='color: #000;font-weight: 500;margin-right: 5px;'>Editors: </b><span style = 'color:#808080;'>"
				+ assigner + "</span></div>"
				+ "<div><b style='color: #000;font-weight: 500;margin-right: 5px;'>Readers: </b><span style = 'color:#808080;'>"
				+ follower + "</span></div>"
				+ "<div><b style='color: #000;font-weight: 500;margin-right: 5px;'>Connections: </b><span style = 'color:#808080;'>"
				+ connection + "</span></div>";

		return html;
	}

	private void historyAndCommentsDetailsClick() {
		historyAndCommentsPage.dataLoad();
	}

	private void costAndQuantityDetailsClick() {
		if (isAssignee())
			itemCost.getCostModel().setShowedit(false);
		else
			itemCost.getCostModel().setShowedit(true);
		setCost(session.getUser());
	}

	private boolean isAssignee() {
		getUserRolesData();
		if (ownerlist.contains(session.getUser()) || assignerlist.contains(session.getUser()))
			return true;
		return false;
	}

	public boolean getItem(JBEntity entity, int id, List<Object[]> entities2, List<Long> accessEntities,
			boolean isUserRole) {
		for (Object[] en : entities2) {
			long pid = (long) en[1];
			if (pid == id) {
				if (isValidFilter(en, accessEntities, entity, isUserRole)) {
					return true;
				} else {
					BigInteger itemid = (BigInteger) en[0];
					boolean value = getItem(entity, itemid.intValue(), entities2, accessEntities, isUserRole);
					if (value) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean isValidFilter(JBEntity entity, List<Long> acessEntities, boolean isUserRole) {
		return filter.isValidEntity(entity, acessEntities, isUserRole);
	}

	public boolean isValidFilter(Object[] entity, List<Long> acessEntities, JBEntity parentEntity, boolean isUserRole) {
		return filter.isValidEntity(entity, acessEntities, parentEntity, isUserRole);
	}

	private void setDescription(JBEntity entity) {
		itemDescription.getDescriptionModel().setHtml(entity.getDescription() == null ? "" : entity.getDescription());
	}

	public void changeCostAndQuantities(@Observes CostChangeEvent fevent) {
		if (fevent.isChange())
			setCost(session.getUser());
	}

	private void changeDescription(@Observes DescriptionEvent event) {
		setDescription(event.getEntity());
	}

	private void changeStatusForChecklist(@Observes ChangeCheckListEvent event) {
		setStatus();
	}

	private void changeStatus(@Observes StatusChangeEvent event) {
		if (event.isOthersChange()) {
			setStatus();
			if (!historyModel.getSelectedItemHasChild()
					&& historyModel.getSelectedEntity().getStatus().equals(JibbyConfig.status[0])) {
				List<ItemBean> items = historyModel.getSelectedEntity().getParentId() == null
						? historyModel.getEntitiesByKey(historyModel.getSelectedEntity().getId())
						: historyModel.getEntitiesByKey(historyModel.getSelectedEntity().getParentId());
				items.removeIf(e -> e.getEntity().getId().equals(historyModel.getSelectedEntity().getId()));

			}
		}
		if (event.isStatusChange())
			setChecklist();
	}

	private void resetAssignedHistory(@Observes MyActionlistAssignedEvent event) {
		if (event.isItem())
			setAssignedHistory();
	}

	private void changeAttachmentDescription(@Observes AttachmentDescriptionEvent event) {
		setAttachment();
	}

	private void firstTimeSyncGenerate(@Observes SyncModelGenerateEvent se) {
		if (se.isChange() && session.getUser() != null) {
			if (dataChangemodel == null)
				dataChangemodel = syncManager.getModel(session.getUser().getId());
		}
	}

	private void updateSyncData(ProcessData processData) {
		JBEntity entity = processData.getEntity();
		if (processData.getEventType() == SyncEventType.EVENT_ENTITY_ATTACHMENT_ADD && processData.getReceiver() != null
				&& historyModel.getSelectedEntity() != null) {
			if (processData.getItemModificationRemoveId() != null && processData.getSourceEntity() != null
					&& selectedEntity.getId().equals(processData.getSourceEntity().getId())) {
				Integer attachmentid = Integer.parseInt(processData.getItemModificationRemoveId().trim().split("-")[0]);
				List<JBAttachmentEntity> attachments = attachmentPage.getAttachmentModel().getAttachments();
				JBAttachmentEntity attachment = attachments.stream().filter(item -> item.getId().equals(attachmentid))
						.findAny().orElse(null);
				if (attachment != null) {
					attachments.remove(attachment);
				}
				if (attachments.size() == 0) {
					List<ItemBean> items = historyModel.getEntitiesByKey(processData.getSourceEntity().getParentId());
					ItemBean bean = items.stream().filter(x -> x.getId().equals(processData.getSourceEntity().getId()))
							.findAny().orElse(null);
					if (bean != null)
						bean.setIsAttachment(false);
					collapseAttachmentWrapper.setVisible(false);
				}
			}

			if (selectedEntity.getId().equals(processData.getEntity().getId())) {
				List<JBAttachmentEntity> attachments = attachmentPage.getAttachmentModel().getAttachments();
				JBAttachmentEntity attachment = attachments.stream()
						.filter(item -> item.getId().equals(processData.getAttachments().getId())).findAny()
						.orElse(null);
				if (attachment == null) {
					attachments.add(processData.getAttachments());
					attachmentPage.setAttachmentList(attachments);
					collapseAttachmentWrapper.setVisible(true);
				}
				if (attachments.size() > 0) {
					List<ItemBean> items = historyModel.getEntitiesByKey(processData.getEntity().getParentId());
					ItemBean bean = items.stream().filter(x -> x.getId().equals(processData.getEntity().getId()))
							.findAny().orElse(null);
					;
					if (bean != null)
						bean.setIsAttachment(true);
				}
			}

		} else if (processData.getEventType() == SyncEventType.EVENT_ENTITY_ATTACHMENT_REMOVE
				&& historyModel.getSelectedEntity() != null
				&& selectedEntity.getId().equals(processData.getEntity().getId())
				&& processData.getItemModificationRemoveId() != null) {

			Integer attachmentid = Integer.parseInt(processData.getItemModificationRemoveId().trim().split("-")[0]);
			List<JBAttachmentEntity> attachments = attachmentPage.getAttachmentModel().getAttachments();
			JBAttachmentEntity attachment = attachments.stream().filter(item -> item.getId().equals(attachmentid))
					.findAny().orElse(null);
			if (attachment != null) {
				attachments.remove(attachment);
			}

			if (attachments.size() == 0) {
				List<ItemBean> items = historyModel.getEntitiesByKey(processData.getEntity().getParentId());
				ItemBean bean = items.stream().filter(x -> x.getId().equals(processData.getEntity().getId())).findAny()
						.orElse(null);
				;
				if (bean != null)
					bean.setIsAttachment(false);
				collapseAttachmentWrapper.setVisible(false);
			}

		} else if (processData.getEventType() == SyncEventType.EVENT_ENTITY_CHANGED) {
			if (historyModel.getSelectedEntity() != null
					&& selectedEntity.getId().equals(processData.getEntity().getId())
					&& processData.getReceiver() != null) {
				if (processData.getOldEntity() != null
						&& (processData.getOldEntity().getStatus() != entity.getStatus()
								|| processData.getOldEntity().getPriority() != entity.getPriority()
								|| !processData.getOldEntity().getEstimatedStartDate()
										.equals(entity.getEstimatedStartDate())
								|| !processData.getOldEntity().getEstimatedFinishDate()
										.equals(entity.getEstimatedFinishDate()))
						|| !processData.getOldEntity().getName().equals(entity.getName())
						|| !processData.getOldEntity().getColor().equals(entity.getColor())
						|| processData.getOldEntity().getEntityType() != entity.getEntityType()) {

					historyModel.setSelectedEntity(entity);
					selectedEntity = historyModel.getSelectedEntity();
					setStatus();
					List<ItemBean> items = historyModel.getEntitiesByKey(entity.getParentId());
					ItemBean item = items.stream().filter(x -> x.getId().equals(entity.getId())).findAny().orElse(null);
					;
					if (item != null) {
						item.setEntity(selectedEntity);
					}
					if (!processData.getOldEntity().getName().equals(entity.getName())
							|| processData.getOldEntity().getEntityType() != entity.getEntityType()) {
						setName();
					}
					if (processData.getOldEntity().getStatus() != entity.getStatus()) {
						if (projectModel.getStatus().size() <= 1
								&& !projectModel.getStatus().contains(entity.getStatus()) && !item.getIsChild()) {
							if (item != null) {
								items.remove(item);
							}
							UI.getCurrent().getPage().getHistory().back();
						} else {
							if (entity.getStatus() == JibbyConfig.status[0])
								deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false, false));
							else
								deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false, true));
						}
					}
				}
				if (processData.getOldEntity() != null
						&& !processData.getOldEntity().getDescription().equals(entity.getDescription())) {
					historyModel.setSelectedEntity(entity);
					selectedEntity = historyModel.getSelectedEntity();
					setDescription();
					List<ItemBean> items = historyModel.getEntitiesByKey(entity.getParentId());
					ItemBean item = items.stream().filter(x -> x.getId().equals(entity.getId())).findAny().orElse(null);
					;
					if (item != null)
						item.setEntity(selectedEntity);

				}
				if (processData.getOldEntity() != null
						&& (!processData.getOldEntity().getUnitQuantity().equals(entity.getUnitQuantity())
								|| processData.getOldEntity().getCostPerUnit() != entity.getCostPerUnit()
								|| !processData.getOldEntity().getCostName().equals(entity.getCostName())
								|| !processData.getOldEntity().getUnitName().equals(entity.getUnitName())
								|| !processData.getOldEntity().getWorksheetType().equals(entity.getWorksheetType()))) {

					historyModel.setSelectedEntity(entity);
					selectedEntity = historyModel.getSelectedEntity();
					setCost(processData.getReceiver());
					List<ItemBean> items = historyModel.getEntitiesByKey(entity.getParentId());
					ItemBean item = items.stream().filter(x -> x.getId().equals(entity.getId())).findAny().orElse(null);
					;
					if (item != null)
						item.setEntity(selectedEntity);
				}
				if (processData.getUserRolelist() != null && processData.getUserRolelist().size() > 0) {
					historyModel.setSelectedEntity(entity);
					selectedEntity = historyModel.getSelectedEntity();

					String html = getUserContactData(true);
					userContactPage.getUserContactModel().setHtml(html);
				}
			}

		} else if (processData.getEventType() == SyncEventType.EVENT_TASK_ASSIGNED && processData.getEntity() != null
				&& selectedEntity.getId().equals(entity.getId()) && processData.getActionlists() != null
				&& processData.getActionlists().size() > 0) {

			String appendAssingedToUsers = assignedHistoryPage.getAssignedHistoryModel().getAssignedHistory() == null
					? ""
					: assignedHistoryPage.getAssignedHistoryModel().getAssignedHistory();
			for (JBUserEntity user : processData.getAssigneeList()) {
				appendAssingedToUsers += (appendAssingedToUsers == "" ? (user.getFirstName() + " " + user.getLastName())
						: ", " + (user.getFirstName() + " " + user.getLastName()));
			}
			if (processData.getAssigneeList().size() > 0) {
				collapseHistoryWrapper.setVisible(true);
				assignedHistoryPage.getAssignedHistoryModel().setAssignedHistory(appendAssingedToUsers);
			}

		}

		else if (processData.getEventType() == SyncEventType.EVENT_CHECKLIST_ADD && processData.getEntity() != null
				&& processData.getChecklists().size() > 0 && processData.getReceiver() != null
				&& historyModel.getSelectedEntity() != null) {
			List<JBChecklistItemEntity> list = checklistPage.getChecklistModel().getChecklists();
			list.addAll(processData.getChecklists());
			checklistPage.getChecklistModel().setChecklists(list);
		} else if (processData.getEventType() == SyncEventType.EVENT_CHECKLIST_EDIT && processData.getEntity() != null
				&& processData.getChecklists().size() > 0 && processData.getReceiver() != null
				&& historyModel.getSelectedEntity() != null) {
			List<JBChecklistItemEntity> list = checklistPage.getChecklistModel().getChecklists();
			JBChecklistItemEntity jbChecklistItemEntity = processData.getChecklists().get(0);
			JBChecklistItemEntity item = list.stream()
					.filter(filter -> filter.getId().equals(jbChecklistItemEntity.getId())).findAny().orElse(null);
			if (item != null)
				item.setName(jbChecklistItemEntity.getName());
		}

		else if (processData.getEventType() == SyncEventType.EVENT_CHECKLIST_REMOVE && processData.getEntity() != null
				&& processData.getReceiver() != null && historyModel.getSelectedEntity() != null) {
			checklistPage.getChecklistModel().getChecklists()
					.removeIf(filter -> filter.getId().equals(processData.getChecklists().get(0).getId()));
		}

		else if (processData.getEventType() == SyncEventType.EVENT_ENTITY_ATTACHMENT_DESCRIPTION
				&& processData.getReceiver() != null && historyModel.getSelectedEntity() != null
				&& processData.getEntity() != null) {
			Integer attachmentid = Integer.parseInt(processData.getItemModificationRemoveId().trim().split("-")[0]);
			List<JBAttachmentEntity> attachments = attachmentPage.getAttachmentModel().getAttachments();
			JBAttachmentEntity attachment = attachments.stream().filter(item -> item.getId().equals(attachmentid))
					.findAny().orElse(null);
			if (attachment != null) {
				attachment.setDescription(processData.getAttachments().getDescription());
			}
		}

		if (collapseChatWrapper.isVisible()) {
			historyAndCommentsPage.updateSyncData(processData);
		}
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		this.ui = attachEvent.getUI();
		if (this.ui != null)
			this.ui.getPushConfiguration().setPushMode(PushMode.MANUAL);
		if (dataChangemodel != null) {
			dataChangemodel.addDataChangeListener(this);
		}
		setUserAccessibility();
		MainLayout.get().getAppBar().getSettingLayout().setVisible(false);
		MainLayout.get().getAppBar().setNaviMode(NaviMode.CONTEXTUAL);
	}

	private void setUserAccessibility() {
		boolean isAssignee = isAssignee();
		MainLayout.get().getAppBar().getDetailsLayout().setVisible(isAssignee);
		uploadLayout.setVisible(isAssignee);
		userContactPage.getUserContactModel().setShowBtn(!isAssignee);
	}

	@Override
	protected void onDetach(DetachEvent detachEvent) {
		this.ui = null;
		if (dataChangemodel != null) {
			dataChangemodel.removeDataChangeListener(this);
		}
	}

	@Override
	public void onChanged(ModelUpdateEvent event) {
		updateUI(event.getProcessData());
	}

	private void updateUI(ProcessData data) {
		try {
			if (this.ui != null)
				this.ui.access(() -> {
					AsyncManager.register(this.ui, asyncTask -> {
						asyncTask.push(() -> {
							updateSyncData(data);
							ui.push();
						});
					});
				});
		} catch (Exception e) {
			logger.error("An exception thrown in itemdetails updateUI", e);
		}
	}
}
