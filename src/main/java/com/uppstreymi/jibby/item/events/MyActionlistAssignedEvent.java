package com.uppstreymi.jibby.item.events;

import com.uppstreymi.jibby.bean.JBChecklistItemEntity;

public class MyActionlistAssignedEvent {

	private boolean isItem,isCheckitem,isSetup;
	Integer itemtype,itemid;
	String itemName;
	JBChecklistItemEntity checkitem;
	public MyActionlistAssignedEvent(boolean isItem,boolean isCheckitem,
			boolean isSetup) {
		this.isItem=isItem;
		this.isCheckitem=isCheckitem;
		this.isSetup=isSetup;
	}
	
	public boolean isItem() {
		return isItem;
	}
	public boolean isCheckitem() {
		return isCheckitem;
	}
	
	public boolean isSetup() {
		return this.isSetup;
	}
	
	public void setItemType(Integer itemtype) {
		this.itemtype=itemtype;
	}
	
	public Integer getItemType() {
		return this.itemtype;
	}
	
	public void setItemid(Integer itemid) {
		this.itemid=itemid;
	}
	
	public Integer getItemid() {
		return this.itemid;
	}
	
	public void setItemName(String itemName) {
		this.itemName=itemName;
	}
	
	public String getItemName() {
		return this.itemName;
	}
	
	public void setCheckItem(JBChecklistItemEntity checkitem) {
		this.checkitem=checkitem;
	}
	
	public JBChecklistItemEntity getCheckItem() {
		return this.checkitem;
	}
}
