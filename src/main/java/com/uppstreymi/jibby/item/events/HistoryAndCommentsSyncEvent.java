package com.uppstreymi.jibby.item.events;

import com.uppstreymi.jibby.backend.sync.process.ProcessData;

public class HistoryAndCommentsSyncEvent {
	private ProcessData data;
	
	public HistoryAndCommentsSyncEvent() {
		
	}
	public HistoryAndCommentsSyncEvent(ProcessData data) {
		this.data=data;
	}
	
	public ProcessData getProcessData() {
		return this.data;
	}
	
}
