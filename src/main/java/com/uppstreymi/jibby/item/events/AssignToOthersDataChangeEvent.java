package com.uppstreymi.jibby.item.events;

public class AssignToOthersDataChangeEvent {
 
	private boolean isChange;
	
	public AssignToOthersDataChangeEvent(boolean isChange) {
		this.isChange=isChange;
	}
	
	public boolean isChange() {
		return isChange;
	}
}
