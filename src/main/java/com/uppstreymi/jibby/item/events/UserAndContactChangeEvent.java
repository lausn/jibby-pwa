package com.uppstreymi.jibby.item.events;

public class UserAndContactChangeEvent {

	private boolean isChange;
	public UserAndContactChangeEvent(boolean isChange) {
		this.isChange=isChange;
	}
	
	public boolean isChange() {
		return isChange;
	}
}
