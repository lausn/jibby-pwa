package com.uppstreymi.jibby.item.events;

import com.uppstreymi.jibby.bean.JBAttachmentEntity;

public class AttachmentDescriptionEvent {

	private JBAttachmentEntity attachmentEntity;
	
	public AttachmentDescriptionEvent(JBAttachmentEntity attachmentEntity) {
		this.attachmentEntity=attachmentEntity;
	}

	public JBAttachmentEntity getAttachmentEntity() {
		return attachmentEntity;
	}

	public void setAttachmentEntity(JBAttachmentEntity attachmentEntity) {
		this.attachmentEntity = attachmentEntity;
	}
}
