package com.uppstreymi.jibby.item.events;

public class ItemSortEvent {

	private boolean sorting;

	public ItemSortEvent(boolean sorting) {
		this.sorting = sorting;
	}

	public boolean isSorting() {
		return this.sorting;
	}
}
