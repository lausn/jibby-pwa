package com.uppstreymi.jibby.item.events;

public class SettingItemEvent {

	private boolean isActionView;
	private boolean isProjectView;
	public SettingItemEvent(boolean isActionView,boolean isProjectView) {
		this.isActionView=isActionView;
		this.isProjectView=isProjectView;
	}
	
	public boolean isActionView() {
		return isActionView;
	}
	public boolean isProjectView() {
		return isProjectView;
	}
}
