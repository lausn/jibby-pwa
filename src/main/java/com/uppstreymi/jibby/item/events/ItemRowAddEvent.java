package com.uppstreymi.jibby.item.events;

import java.io.Serializable;
import java.util.List;

import com.uppstreymi.jibby.bean.ItemBean;

public class ItemRowAddEvent implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private ItemBean item;
	private List<ItemBean> entitylist;

	public ItemRowAddEvent() {}
	public ItemRowAddEvent(ItemBean item) {
		this.item=item;
	}
	public ItemRowAddEvent(List<ItemBean> entitylist) {
		this.entitylist=entitylist;
	}
	
	public void setItemBean(ItemBean item) {
		this.item=item;
	}
	
	public ItemBean getItemBean() {
		return this.item;
	}
	
	public void setEntityList(List<ItemBean> entitylist) {
		this.entitylist=entitylist;
	}
	
	public List<ItemBean> getEntityList() {
		return this.entitylist;
	}
	
}
