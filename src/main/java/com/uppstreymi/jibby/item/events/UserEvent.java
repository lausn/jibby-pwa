package com.uppstreymi.jibby.item.events;

import com.uppstreymi.jibby.bean.JBUserEntity;

public class UserEvent {
	
	private JBUserEntity userEntity;
	private boolean isImageUploaded;

	public UserEvent(JBUserEntity userEntity,boolean isImageUploaded) {
		this.userEntity = userEntity;
		this.setImageUploaded(isImageUploaded);
	}

	public JBUserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(JBUserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public boolean isImageUploaded() {
		return isImageUploaded;
	}

	public void setImageUploaded(boolean isImageUploaded) {
		this.isImageUploaded = isImageUploaded;
	}
}
