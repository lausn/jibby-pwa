package com.uppstreymi.jibby.item.events;

public class ChatItemEvent {
	private boolean isDelete,isEdit,isLoad;
	private Integer chatSelectedId;
	
	public ChatItemEvent() {
		
	}
	public ChatItemEvent(boolean isDelete,boolean isEdit,boolean isLoad,Integer chatSelectedId) {
		this.isDelete=isDelete;
		this.chatSelectedId=chatSelectedId;
		this.isEdit=isEdit;
		this.isLoad=isLoad;
	}
	
	public boolean isDelete() {
		return this.isDelete;
	}
	public boolean isEdit() {
		return this.isEdit;
	}
	public boolean isLoad() {
		return this.isLoad;
	}
	public Integer getchatSelectedId() {
		return this.chatSelectedId;
	}
	
}
