package com.uppstreymi.jibby.item.events;

public class CostChangeEvent {

	private boolean isChange;
	public CostChangeEvent(boolean isChange) {
		this.isChange=isChange;
		
	}

	public boolean isChange() {
		return isChange;
	}
}
