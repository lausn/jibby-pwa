package com.uppstreymi.jibby.item.events;

import com.uppstreymi.jibby.bean.JBEntity;

public class TopLabelProjectEvent {

	private boolean isCreateTopLabelProject=false;
	private Integer isDeleteTopLabelProject=0;
	private JBEntity entity;
	
	public TopLabelProjectEvent(boolean isCreateTopLabelProject,JBEntity entity) {
		this.setCreateTopLabelProject(isCreateTopLabelProject);
		this.entity=entity;
	}
	
	public TopLabelProjectEvent(Integer isDeleteTopLabelProject,JBEntity entity) {
		this.setIsDeleteTopLabelProject(isDeleteTopLabelProject);
		this.entity=entity;
	}

	public boolean isCreateTopLabelProject() {
		return isCreateTopLabelProject;
	}

	public void setCreateTopLabelProject(boolean isCreateTopLabelProject) {
		this.isCreateTopLabelProject = isCreateTopLabelProject;
	}

	public JBEntity getEntity() {
		return entity;
	}

	public void setEntity(JBEntity entity) {
		this.entity = entity;
	}

	public Integer getIsDeleteTopLabelProject() {
		return isDeleteTopLabelProject;
	}

	public void setIsDeleteTopLabelProject(Integer isDeleteTopLabelProject) {
		this.isDeleteTopLabelProject = isDeleteTopLabelProject;
	}
}
