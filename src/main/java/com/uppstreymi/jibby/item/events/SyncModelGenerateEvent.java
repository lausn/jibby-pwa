package com.uppstreymi.jibby.item.events;

public class SyncModelGenerateEvent {
	private boolean isChange;
	public SyncModelGenerateEvent(boolean isChange) {
		this.isChange=isChange;
	}
	
	public boolean isChange() {
		return isChange;
	}
}
