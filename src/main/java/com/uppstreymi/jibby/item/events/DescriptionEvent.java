package com.uppstreymi.jibby.item.events;

import com.uppstreymi.jibby.bean.JBEntity;

public class DescriptionEvent {

	private JBEntity entity;
	
	public DescriptionEvent(JBEntity entity) {
		this.entity=entity;
	}

	public JBEntity getEntity() {
		return entity;
	}

	public void setEntity(JBEntity entity) {
		this.entity = entity;
	}
}
