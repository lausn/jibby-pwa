package com.uppstreymi.jibby.item.events;

public class FilterChangeEvent {
	private boolean optionchange,categorieschange,closedialog,filterdata;
	
	public FilterChangeEvent() {
	}
	public FilterChangeEvent(boolean closedialog) {
		this.closedialog=closedialog;
	}
	public FilterChangeEvent(boolean optionchange,boolean categorieschange,boolean closedialog) {
		this.optionchange = optionchange;
		this.categorieschange = categorieschange;
		this.closedialog = closedialog;
		
		
	}
	
	public void setFilter(boolean filterdata) {
		this.filterdata=filterdata;
	}
	
	public boolean isSetFilter() {
		return this.filterdata;
	}
	
	public void setOptionChange(boolean optionchange) {
		this.optionchange=optionchange;
	}
	
	public boolean isOptionChange() {
		return this.optionchange;
	}
	
	public void setCategorisChange(boolean categorieschange) {
		this.categorieschange=categorieschange;
	}
	
	public boolean isCategorisChange() {
		return this.categorieschange;
	}
	
	public void setdialogClose(boolean sortchange) {
		this.closedialog=closedialog;
	}
	
	public boolean isCloseDialog() {
		return this.closedialog;
	}
}
