package com.uppstreymi.jibby.item.events;

public class ChangeCheckListEvent {

	private boolean isChange;
	public ChangeCheckListEvent(boolean isChange) {
		this.isChange=isChange;
	}
	
	public boolean isChange() {
		return isChange;
	}
}
