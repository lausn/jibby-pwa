package com.uppstreymi.jibby.item.events;

public class StatusChangeEvent {

	private boolean isStatusChange,isOthersChange;
	public StatusChangeEvent(boolean isStatusChange,boolean isOthersChange) {
		this.isStatusChange=isStatusChange;
		this.isOthersChange=isOthersChange;
	}
	
	
	public void setStatusChange(boolean isStatusChange) {
		this.isStatusChange=isStatusChange;
	}
	
	public void setOthersChange(boolean isOthersChange) {
		this.isOthersChange=isOthersChange;
	}
	
	public boolean isStatusChange() {
		return isStatusChange;
	}
	public boolean isOthersChange() {
		return isOthersChange;
	}
	
}
