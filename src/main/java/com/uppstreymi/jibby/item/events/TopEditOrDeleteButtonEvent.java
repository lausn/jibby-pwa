package com.uppstreymi.jibby.item.events;

public class TopEditOrDeleteButtonEvent {
	private boolean isShowButton,isShowAssignBtn;
	
	public TopEditOrDeleteButtonEvent() {
		
	}
	public TopEditOrDeleteButtonEvent(boolean isShowButton,boolean isShowAssignBtn) {
		this.isShowButton=isShowButton;
		this.isShowAssignBtn =isShowAssignBtn;
	}
	
	public boolean isShowButton() {
		return this.isShowButton;
	}
	
	public boolean isShowAssignBtn() {
		return this.isShowAssignBtn;
	}
	
}
