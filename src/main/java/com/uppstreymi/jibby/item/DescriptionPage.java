package com.uppstreymi.jibby.item;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("description-page")
@JsModule("./src/views/item/description-page.js")
public class DescriptionPage extends PolymerTemplate<DescriptionPage.DescriptionPageModel> {

    public DescriptionPage() {
    	
    }
    
    public DescriptionPageModel getDescriptionModel() {
    	return getModel();
    }

    public interface DescriptionPageModel extends TemplateModel {
    	void setHtml(String str);
    }
}
