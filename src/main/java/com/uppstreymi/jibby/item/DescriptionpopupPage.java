package com.uppstreymi.jibby.item;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.item.events.DescriptionEvent;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.richtexteditor.RichTextEditor;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("descriptionpopup-page")
@JsModule("./src/views/item/descriptionpopup-page.js")
@UIScoped
public class DescriptionpopupPage extends PolymerTemplate<DescriptionpopupPage.DescriptionpopupPageModel> {

	@Id("rte")
	private RichTextEditor rte;
	@Id("btnSave")
	private Button btnSave;
	@Id("btnCancel")
	private Button btnCancel;
	
	@Inject SessionModel session;
	@Inject
	private DbService service;
	@Inject
	Event<DescriptionEvent> descriptionEvent;
	@Inject
	SyncUserEvent userEvent;
	private JBEntity entity;
	JBEntity oldentity;

	public DescriptionpopupPage() {

	}

	@PostConstruct
	private void init() {
		btnSave.addClickListener(e -> {
			entity.setDescription(rte.getHtmlValue());
			service.saveEntity(entity);
			
			if(!oldentity.getDescription().equals(entity.getDescription())){
				userEvent.setOldEntity(oldentity);
				insertItemModification(entity,Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("edit")+1);
				userEvent.addHistory();
				userEvent.eventProcessor();
			}

			DescriptionEvent event = new DescriptionEvent(entity);
			descriptionEvent.fire(event);
			closeWindow();
		});

		btnCancel.addClickListener(e -> {
			closeWindow();
		});
	}

	public void insertItemModification(JBEntity entity, Integer type) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entity.getId());
		itemModification.setUserId1(session.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);

		userEvent.setEventType(SyncEventType.EVENT_ENTITY_CHANGED);
		userEvent.addToHistoryList(itemModification);
		userEvent.setEntity(entity);
		userEvent.setSender(session.getUser());
		userEvent.syncDataReceiver(entity);
		
		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity
						: service.getEntity(entity.getParentProjectId()));
	}

	private void closeWindow() {
		Dialog dialog = (Dialog) getParent().get();
		dialog.close();
	}

	public interface DescriptionpopupPageModel extends TemplateModel {
		void setHtmlvalue(String str);

		void setRtevalue(String str);

		String getRtevalue();

		String getHtmlvalue();
	}

	public void setup(JBEntity selectedEntity) {
		if (selectedEntity != null) {
			rte.clear();
			this.entity=selectedEntity;
			oldentity= new JBEntity();
			oldentity.copyEntity(selectedEntity);
			getModel().setRtevalue(selectedEntity.getDescription().isEmpty() ? "" : selectedEntity.getDescription());
		}
	}
}
