package com.uppstreymi.jibby.item;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.backend.sync.process.ProcessData;
import com.uppstreymi.jibby.bean.ChatAndHistoryBean;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBChatMessageEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.item.events.ChatItemEvent;
import com.uppstreymi.jibby.item.events.TopEditOrDeleteButtonEvent;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("history-and-comments-page")
@JsModule("./src/views/item/history-and-comments-page.js")
@UIScoped
public class HistoryAndCommentsPage extends PolymerTemplate<HistoryAndCommentsPage.HistoryAndCommentsPageModel> {

	@Id("tfComments")
	private TextField tfComments;

	@Id("btnAddChat")
	private Button btnAddChat;

	@Inject
	private SessionModel session;

	@Inject
	private DbService dbService;

	@Inject
	private Event<TopEditOrDeleteButtonEvent> deleteOrEditButtonEvent;

	@Inject
	private HistorModel historyModel;

	@Inject
	private SyncUserEvent userEvent;

	List<Object[]> chatlists = new ArrayList<>();
	private boolean isEdit = false;
	private HashMap<Integer, Object[]> hmcommentsLabel = new HashMap<>();
	private HashMap<String, Object[]> hmHistoryLabel = new HashMap<>();
	private SimpleDateFormat dateformat;

	private UI ui;
	private Logger logger=LogManager.getLogger(HistoryAndCommentsPage.class);

	public HistoryAndCommentsPage() {
	}

	@PostConstruct
	private void init() {
		btnAddChat.addClickListener(e->{
			if(!tfComments.getValue().trim().isEmpty()) {
				if(!isEdit) { // new chat
					JBChatMessageEntity chat =insertChat();
					addChatToWrapper(chat);
				}else { // update chat
					if(hmcommentsLabel.get(historyModel.getSelectedChatId())!=null) {
						Object[] chat = getItem(chatlists,hmcommentsLabel.get(historyModel.getSelectedChatId()));
						if(chat!=null) {
							JBChatMessageEntity chats =new JBChatMessageEntity();
							chats.setId(historyModel.getSelectedChatId());
							chats.setText(Utils.transformURLIntoLinks(tfComments.getValue()));
							chats.setSender(session.getUser().getId());
							chats.setEntity(historyModel.getSelectedEntity().getId());
							chats.setDate((Date)chat[4]);
							dbService.saveChatAndComments(chats);
							chat[3]=Utils.transformURLIntoLinks(tfComments.getValue());
							getModel().setChatItems(buildChatAndHistoryBean(chatlists));

							userEvent.setEventType(SyncEventType.EVENT_ENTITY_COMMENT_EDIT);
							userEvent.setComments(chats);
							userEvent.setEntity(historyModel.getSelectedEntity());
							userEvent.setSender(session.getUser());
							userEvent.syncDataReceiver(historyModel.getSelectedEntity());

							userEvent.setTopLebelProjectEntity(
									historyModel.getSelectedEntity().getParentProjectId() == null ? historyModel.getSelectedEntity()
											: dbService.getEntity(historyModel.getSelectedEntity().getParentProjectId()));

							userEvent.addComments();
							userEvent.eventProcessor();
						}
					}
				}
				isEdit=false;
				historyModel.selectedChatId(null);
				tfComments.clear();
			}
		});
	}

	public JBChatMessageEntity insertChat(){
		JBChatMessageEntity chat =new JBChatMessageEntity();
		chat.setSender(session.getUser().getId());
		chat.setEntity(historyModel.getSelectedEntity().getId());
		chat.setText(Utils.transformURLIntoLinks(tfComments.getValue()));
		chat.setDate(new Date());
		chat=dbService.saveChatAndComments(chat);

		Set<JBUserEntity> senduserLists = new HashSet<>();
		senduserLists.addAll(historyModel.getSelectedEntity().getOwnerlist());
		senduserLists.addAll(historyModel.getSelectedEntity().getAssigneelist());
		senduserLists.addAll(historyModel.getSelectedEntity().getFollowerlist());
		senduserLists.remove(session.getUser());

		JBUserEntity userEntity=session.getUser();
		String username= session.getUser().getFirstName()+" "+session.getUser().getLastName();
		userEvent.setEventType(SyncEventType.EVENT_ENTITY_COMMENT_ADD);
		userEvent.setComments(chat);
		userEvent.setEntity(historyModel.getSelectedEntity());
		userEvent.setSender(userEntity);
		userEvent.syncDataReceiver(historyModel.getSelectedEntity());

		userEvent.setTopLebelProjectEntity(
				historyModel.getSelectedEntity().getParentProjectId() == null ? historyModel.getSelectedEntity()
						: dbService.getEntity(historyModel.getSelectedEntity().getParentProjectId()));

		for(JBUserEntity user : senduserLists){
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(historyModel.getSelectedEntity().getId());
			actionlistHistory.setItemtype(Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1);
			actionlistHistory.setUserid(user.getId());
			actionlistHistory.setMessage(username+" added a new comment in the task '"+historyModel.getSelectedEntity().getName()+"'");
			actionlistHistory.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("addCommentInHistory")+1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

			actionlistHistory=dbService.saveActionlistHistory(actionlistHistory);
			userEvent.addHistoryNotification(actionlistHistory);
		}

		if(senduserLists.size()==0){
			userEvent.addComments();
		}
		userEvent.eventProcessor();

		return chat;
	}

	private void addChatToWrapper(JBChatMessageEntity chat) {
		String username = session.getUser().getFirstName()+" "+session.getUser().getLastName();
		Object[] chats= {chat.getId(),chat.getSender(),chat.getEntity(),chat.getText(),chat.getDate(),null,null,null,username,null};

		chatlists.add(chats);
		logger.info("before = {}",chatlists.size());
		getModel().setChatItems(buildChatAndHistoryBean(chatlists));
	}

	public interface HistoryAndCommentsPageModel extends TemplateModel {

		void setChatItems(List<ChatAndHistoryBean> chatItems);
		void setCreatorName(String creatorName);
		void setEntityCreateDate(String entityCreateDate);
	}

	public void loadChatAndHistory(List<Object[]> chatlist) {
		hmHistoryLabel.clear();
		hmcommentsLabel.clear();
		chatlists.clear();
		chatlists.addAll(chatlist);
		getModel().setChatItems(buildChatAndHistoryBean(this.chatlists));
		clear();
	}

	public HistoryAndCommentsPageModel getHistoryAndCommentsModel() {
		return getModel();
	}

	private List<ChatAndHistoryBean> buildChatAndHistoryBean(List<Object[]> items){
		List<ChatAndHistoryBean> chatCommentsBean = new ArrayList<>();
		for (Object[] item: items) {
			ChatAndHistoryBean bean= new ChatAndHistoryBean();
			bean.setId(Integer.parseInt(item[0]+""));
			bean.setSender(Integer.parseInt(item[1]+""));
			bean.setEntity(Integer.parseInt(item[2]+""));
			bean.setText(item[3].toString().replaceAll("\n", "<br>"));
			bean.setDate(dateformat.format(item[4]));
			bean.setMimetype(item[5]+"");
			bean.setTypes(item[6]==null?null:Integer.parseInt(item[6]+""));
			bean.setAssignee(item[7]==null?null:Integer.parseInt(item[7]+""));
			bean.setUsername(item[8]+"");
			bean.setAssigneeName(item[9]+"");
			bean.setIsChat(item[6]==null?true:false);
			bean.setIsHistory(item[6]!=null?true:false);
			if(item[6]!=null && item[6].toString().equals("1"))
				bean.setIsAttachment(true);
			else 
				bean.setIsAttachment(false);

			chatCommentsBean.add(bean);
			if(item[6]!=null) {
				if(Integer.parseInt(item[6].toString())==(Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("attachment")+1))
					hmHistoryLabel.put(item[0]+"-"+item[2], item);
				else
					hmHistoryLabel.put(item[0]+"", item);
			}else
				hmcommentsLabel.put(Integer.valueOf(item[0]+""), item);
		}
		return chatCommentsBean;
	}

	@ClientCallable
	private void _selectedChat(Integer chatId,Integer senderId,String text){
		if(session.getUser().getId().equals(senderId)){
			historyModel.selectedChatId(chatId);
			historyModel.setSelectedChatText(text);
			tfComments.clear();
			deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(true,true));

		}else {
			clear();
		}
	}

	@EventHandler
	private void _deselectChat(){
		deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false,true));
	}

	public void chatEvent(@Observes ChatItemEvent event){
		if(event.isLoad())
			dataLoad();
		else if(event.getchatSelectedId() != null) {
			Integer chatId = event.getchatSelectedId();
			if(event.isDelete()) {
				dbService.deleteChatData(chatId);
				if(hmcommentsLabel.get(chatId)!=null) {
					Object[] chat = getItem(chatlists,hmcommentsLabel.get(chatId));
					if(chat!=null)
						chatlists.remove(chat);
					hmcommentsLabel.remove(chatId);
				}
				userEvent.setEventType(SyncEventType.EVENT_ENTITY_COMMENT_REMOVE);
				userEvent.setEntity(historyModel.getSelectedEntity());
				userEvent.setSender(session.getUser());
				userEvent.syncDataReceiver(historyModel.getSelectedEntity());
				userEvent.removeComments(chatId);
				userEvent.eventProcessor();

				getModel().setChatItems(buildChatAndHistoryBean(chatlists));
				historyModel.selectedChatId(null);
				isEdit = false;
			}else if(event.isEdit()) {
				isEdit = true;
				tfComments.setValue(historyModel.getSelectedChatText());
				tfComments.focus();
			}
			deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false,true));
		}
	}

	public void dataLoad() {
		JBUserEntity creator = dbService.getUserById(historyModel.getSelectedEntity().getCreator());
		String name = creator.getFirstName()+" "+creator.getLastName();
		dateformat=new SimpleDateFormat(session.getUser().getDateFormat());
		dateformat.setTimeZone(TimeZone.getTimeZone(session.getUser().getTimeZone()));
		getModel().setCreatorName(name);
		getModel().setEntityCreateDate(dateformat.format(historyModel.getSelectedEntity().getCreationDate()));

		List<Object[]> chatlist = dbService.getChatAndHistorylistByEntity(String.valueOf(historyModel.getSelectedEntity().getId()));

		loadChatAndHistory(chatlist);
	}

	public void clear() {
		historyModel.selectedChatId(null);
		historyModel.setSelectedChatText("");
		tfComments.clear();
		deleteOrEditButtonEvent.fire(new TopEditOrDeleteButtonEvent(false,true));
	}

	public void updateSyncData(ProcessData data) {
		if(data.getEventType()==SyncEventType.EVENT_ENTITY_COMMENT_ADD && 
				data.getComments() != null && data.getComments().getEntity().equals(historyModel.getSelectedEntity().getId()) &&
				hmcommentsLabel.get(data.getComments().getId())==null){
			String username= data.getSender().getFirstName()+" "+data.getSender().getLastName();
			Object[] chat= {data.getComments().getId(),data.getComments().getSender(),data.getComments().getEntity(),data.getComments().getText(),data.getComments().getDate(),null,null,null,username,null};
			chatlists.add(chat);
			getModel().setChatItems(buildChatAndHistoryBean(chatlists));

		}
		else if(data.getEventType()==SyncEventType.EVENT_ENTITY_COMMENT_EDIT && 
				data.getComments() != null && data.getComments().getEntity().equals(historyModel.getSelectedEntity().getId()) &&
				hmcommentsLabel.get(data.getComments().getId())!=null){
			Object[] chat = getItem(chatlists,hmcommentsLabel.get(data.getComments().getId()));
			if(chat!=null) {
				chat[3]=data.getComments().getText();
				getModel().setChatItems(buildChatAndHistoryBean(chatlists));
			}
		}
		else if(data.getEventType()==SyncEventType.EVENT_ENTITY_COMMENT_REMOVE && 
				data.getEntity().getId().equals(historyModel.getSelectedEntity().getId()) && hmcommentsLabel.get(data.getComments().getId())!=null){
			if(data.getComments() != null) {
				Object[] chat = getItem(chatlists,hmcommentsLabel.get(data.getComments().getId()));
				if(chat!=null) {
					chatlists.remove(chat);
					hmcommentsLabel.remove(data.getComments().getId());
					getModel().setChatItems(buildChatAndHistoryBean(chatlists));
				}
			}

		}
		else if(data.getEventType()==SyncEventType.EVENT_ENTITY_ATTACHMENT_ADD){
			if(data.getSourceEntity()!=null && data.getItemModificationRemoveId()!=null && data.getSourceEntity().getId().equals(historyModel.getSelectedEntity().getId())) {
				if(hmHistoryLabel.get(data.getItemModificationRemoveId()) !=null){
					Object[] chat = getItem(chatlists,hmHistoryLabel.get(data.getItemModificationRemoveId()));
					if(chat!=null) {
						hmHistoryLabel.remove(data.getItemModificationRemoveId());
						chatlists.remove(chat);
						getModel().setChatItems(buildChatAndHistoryBean(chatlists));
					}
				}
			}

			if(data.getAttachments().getEntity().equals(historyModel.getSelectedEntity().getId())) {
				if(hmHistoryLabel.get(data.getAttachments().getId()+"-"+data.getEntity().getId()) ==null) {
					Object[] chat= {data.getAttachments().getId(),data.getAttachments().getUploaderUser(),data.getAttachments().getEntity(),data.getAttachments().getName(),
							data.getAttachments().getUploadDate(),data.getAttachments().getType(),1,null,data.getSender().getFirstName()+" "+data.getSender().getLastName(),null};
					chatlists.add(chat);
					getModel().setChatItems(buildChatAndHistoryBean(chatlists));
				}
			}
		}else if(data.getEventType()==SyncEventType.EVENT_ENTITY_UNSUBSCRIBE && 
				data.getHistorylist() != null && data.getHistorylist().size()>0){

			Integer i = 0;
			for(Integer id : data.getUnsubscribeEntitylist()){
				if(id.equals(historyModel.getSelectedEntity().getId())){
					JBItemModification historyData = data.getHistorylist().get(i);
					if(hmHistoryLabel.get(historyData.getId()+"") ==null){	
						Object[] chat= {historyData.getId(),data.getSender().getId(),historyData.getEntityId(),"",historyData.getModificationDate(),null,historyData.getModificationType(),historyData.getUserId2(),data.getSender().getFirstName()+" "+data.getSender().getLastName(),null};
						chatlists.add(chat);
					}
				}
				i++;
			}
			getModel().setChatItems(buildChatAndHistoryBean(chatlists));
		}else{
			if(data.getEntity()!=null && data.getEntity().getId().equals(historyModel.getSelectedEntity().getId())){

				if(data.getHistorylist() != null && data.getHistorylist().size()>0){
					Integer i = 0;
					for(JBItemModification historyData: data.getHistorylist()){
						if(hmHistoryLabel.get(historyData.getId()+"") ==null){	
							JBUserEntity assignee;
							String assigneeName = null;
							if(data.getAssigneeList() != null && data.getAssigneeList().size()>0){
								assignee = data.getAssigneeList().get(i);
								assigneeName = assignee.getFirstName()+" "+assignee.getLastName();
							}

							Object[] chat= {historyData.getId(),data.getSender().getId(),historyData.getEntityId(),"",historyData.getModificationDate(),null,historyData.getModificationType(),historyData.getUserId2(),data.getSender().getFirstName()+" "+data.getSender().getLastName(),assigneeName};
							chatlists.add(chat);

							i++;
						}
					}
					getModel().setChatItems(buildChatAndHistoryBean(chatlists));
				}if(data.getItemModificationRemoveId() !=null){ //for remove both attachment and modification label from history
					if (hmHistoryLabel.get(data.getItemModificationRemoveId()) != null) {
						Object[] chat = getItem(chatlists,hmHistoryLabel.get(data.getItemModificationRemoveId()));
						if(chat!=null) {
							hmHistoryLabel.remove(data.getItemModificationRemoveId());
							chatlists.remove(chat);
							getModel().setChatItems(buildChatAndHistoryBean(chatlists));
						}
					}
				}
			}

		}
	}


	private Object[] getItem(List<Object[]> list, Object[] element) {
		for (Object[] object : list) {
			if(object.equals(element))
				return object;
		}
		return null;
	}
}
