package com.uppstreymi.jibby.utils;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.vaadin.cdi.annotation.UIScoped;

@UIScoped
public class JibbyColBFilter {

	@Inject ProjectViewModel model;

	// valid check for every 1st Top lebel item for Project
	public boolean isValidEntity(JBEntity entity,List<Long> acessEntities,boolean isUserRoleEntity){
		boolean isValid=false;
		boolean isUserRole=acessEntities==null?(isUserRoleEntity?true:false):isAccess(acessEntities,entity.getId());
		if(model.getTypes().contains(entity.getEntityType())
				&& model.getStatus().contains(entity.getStatus())
				&& getValidDate(entity.getEstimatedStartDate(),model.getStartDate(),model.getStartType())
				&& getValidDate(entity.getEstimatedFinishDate(),model.getFinishDate(),model.getFinsihType())
				&& isUserRole){
			isValid=true;
		}
		return isValid;
	}

	// valid check for every child item
	public boolean isValidEntity(Object[] entity,List<Long> acessEntities,JBEntity parentEntity,boolean isUserRoleEntity){
		boolean isValid=false;
		Integer entityId=((BigInteger) entity[0]).intValue();
		boolean isUserRole=acessEntities==null?(isUserRoleEntity?true:false):isAccess(acessEntities,entityId,parentEntity.getId());
		if(model.getTypes().contains((int)entity[2])
				&& model.getStatus().contains((int)entity[3])
				&& getValidDate((Date)entity[4],model.getStartDate(),model.getStartType())
				&& getValidDate((Date)entity[5],model.getFinishDate(),model.getFinsihType())
				&& isUserRole){
			isValid=true;
		}
		return isValid;
	}

	// valid estimateStartDate and estimateFinishDate
	public boolean getValidDate(Date estimatedate,LocalDate optionsDate,Integer type){
		if(type==0){ // All
			return true;
		}else{
			if(estimatedate==null){
				return false;
			}
			Date optiondate =Utils.localDateToDate(optionsDate);
			if(type==1){
				if(estimatedate.compareTo(optiondate)==1)
					return true;
			}else{
				if(estimatedate.compareTo(optiondate)==-1)
					return true;
			}
		}
		return false;
	}

	// valid UserRoles Access
	public boolean isAccess(List<Long> acessEntities,Integer entityId,Integer parentId){
		
		if(acessEntities.contains((long)entityId) || acessEntities.contains((long)parentId)){
			return true;
		}
		
		return false;
	}
	
	public boolean isAccess(List<Long> acessEntities,Integer entityId){

		if(acessEntities.contains((long)entityId)){
			return true;
		}
		
		return false;
	}
}
