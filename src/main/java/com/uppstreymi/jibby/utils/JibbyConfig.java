package com.uppstreymi.jibby.utils;

import java.time.LocalDate;
import java.util.Date;

public class JibbyConfig {
	
	public static volatile String serviceDomain;
	
	public static final String REST_URL = "vitvistcore/api/";
	
	public static void setServiceDomain(String serviceDomain) {
		JibbyConfig.serviceDomain = serviceDomain+REST_URL;
	}
	
	public static String jibbyGA="UA-126621251-2";
	
	public static Object sortCaption[][] = {
			{"name (A-Z)","a.name",true},
			{"name (Z-A)","a.name",false},
			{"creation (old-new)","a.creationDate",true},
			{"creation (new-old)","a.creationDate",false},
			{"start (old-new)","a.estimatedStartDate",true},
			{"start (new-old)","a.estimatedStartDate",false},
			{"end (old-new)","a.estimatedFinishDate",true},
			{"end (new-old)","a.estimatedFinishDate",false},
			{"tags (A-Z)","a.keywords",true},
			{"tags (Z-A)","a.keywords",false}};
	
	public static String merchantId = "130409";
	public static String pageId = "01";//"02";
	public static String silenPostPassword = "f0aEt4v9";//"MyPassw0rd1";
	public static String notificationPassword = "f0aEt4v9";//"MyPassw0rd1";
	public static String checkoutLocation = "https://secure.dalpay.is/cgi-bin/order2/processorder1.pl";
	public static String rebillingLocation = "https://secure.dalpay.is/cgi-bin/auto/chreb.cgi";
	public static String rebillingPassword = "a4h3Fg6j";
	
	
	public static String payPalCheckoutLocation = "https://www.paypal.com/cgi-bin/webscr"; // for live paypal Checkout
	public static String payPalnotify_url = "https://app.jibby.com/paypal-silentpost/"; // for live IPN(Instant Payment Notification) for instant back-end-back notification web servlet url.
	public static String ipnValidUrl="https://ipnpb.paypal.com/cgi-bin/webscr";  // for live IPN(Instant Payment Notification) for instant back-end-back notification VERFICATION check url.
	public static String payPalMarchantId = "UTEK7H76LPEK6"; // for live paypal MarchentId
	public static String payPalMarchantEmail = "admin@jibby.com"; // for live paypal Marchent business  
	public static String periodType="M"; // for live recurring time period (D=days, W=weeks, M=months, Y=years)
	 
	
	/*public static String payPalCheckoutLocation = "https://www.sandbox.paypal.com/cgi-bin/webscr"; // for test paypal Checkout
	public static String payPalnotify_url = "https://jibbydev.lausn.is/paypal-silentpost/"; // for test IPN(Instant Payment Notification) for instant back-end-back notification web servlet url.
	public static String ipnValidUrl="https://ipnpb.sandbox.paypal.com/cgi-bin/webscr";  // for test IPN(Instant Payment Notification) for instant back-end-back notification VERFICATION check url.
	public static String payPalMarchantId = "U86AUG5RTZD4E"; // for test paypal MarchentId
	public static String payPalMarchantEmail = "testjibby@gmail.com"; // for test paypal Marchent business  
	public static String periodType="D"; // for test recurring time period (D=days, W=weeks, M=months, Y=years)
	*/
	 
	public static double basePrice = 10.00;
	public static double discountRatio = 0.89966;
	public static double maxDiscountRate = 0.5;
	public static String[] paymentProviders = {"dalpay","paypal","valitor"};
	public static String[] subscriptionTypes = {"single","licence"};
	public static String subscriptionItemName = "Jibby Monthly Subscription";
	
	public static int showchildcountorder = 25;

	public static String userTypes[] = {"notActivated","user","admin","disabled","vip","invited"} ;
	
	public static String userStatus[] = {"Trial","Expired","Subscribed","Presubscribed","Unsubscribed","Licensed",
			"Legacy","Admin","Disabled","VIP","Invited"};
	
	public static Integer trialLength=30;
	public static Integer activationExpiredTimeMinute=15;
	public static Date legacyDate = Utils.localDateToDate(Utils.strTolocalDate("2014-09-30"));
	
	public static String userRoles[] = {"owner","assignee","followers","connections"} ;
	
	public static String defaultUnitNames[] ={"hour","kg","pound","km","mile"};
	
	public static String defaultCostNames[] = {"USD","EUR","GBP"};

	public static String entityTypes[] ={"Project","Information","Deliverable","Task","Timesheet","Event",
								        "Meeting & notes","Memo","Document","Financial","JibbyMail","Blog","Contact"};
	
	public static String duration[] = {"30D","1M","1M","1D","8H","4H","1H","1M","1M","1M","1M","1M","1M"};
	
	public static String emailAboutEntityChangesTypes[] = {"Off","Hourly","Daily","Weekly"};
	
	public static String reminderEntityUnfinished[] = {"Off","Daily","Weekly"};
	
	public static String emailProviders[] = {"none","","gmail"};
	
	public static String actionlistType[] = {"item","checkitem","todo","myTodayItem","myTodayCheckItem"};
	
	public static String actionlistMessageType[] = {"assingedToUser","markedByAssignee","addCommentInHistory"};

	public static String myTodayType[] = {"myToday","myTomorrow","myPast"}; 
	
	public static String myTodayTypeCaption[] = {"My Today","My Tomorrow"};
	
	public static LocalDate estimateDate = Utils.strTolocalDate("2013-06-15");
	
	public static String myActionlistTabCaption[] = {"My Items","Assigned to me","Assigned to others"};
	
	//public static String myActionlistSelectOption[] = {"Dashboard","Chat & Comments"};

	public static String topTabOption[] = {"Dashboard","Kanban Board","My action list","Notification"};
	
	public static String topRightButton[] = {"Share Button"};
	
	public static Integer status[] = {4,2}; // done =4 , not done=2
		
	public static Integer checkItemStatus[] = {1,0}; //done =1 , not done = 0;
			
	public static String priorities[] = {"Low","Medium","High"};
	
	public static String worksheetTypes[] = {"Worksheet Disabled","Worksheet Enabled","Summary"};
	
	public static String recurrences[] = {"Not recurring","Recurring daily","Recurring weekly","Recurring monthly","Recurring annually"};
	
	public static String cmbUnitNames[] = {"hours","days","pcs","lot","units","m","m2","km","dekk","ársleyfi"};
	
	public static String cmbCostNames[] = {"ISK","EUR","USD"};
	
	public static Integer maxSizeOfEmailAttachmentInMb = 20; 
	
	public static String chkGroupCriteriaLabel[]= {"Title","Description","Checklists","Comments","Attachments"};
	
	public static String chkGroupCriteriaValue[]= {"1","2","3","4","5"};
	
	public static Integer maxSizeOfAvatarInMb = 5; 
	
	public static Integer maxEntityMessageLength = 8000; 
	
	public static String allowedExtensionOfImage[] = {"jpg", "jpeg", "png", "gif", "bmp"};
	
	public static String itemModificationStatus[] = {"attachment","edit", "done", "undone","move",
			"assigntoother","myitem","mytoday","mytommorrow","unsubscribe",
			"addowner","removeowner","addassigner","removeassigner","addfollower","removefollower","addconnection","removeconnection",
			"addchecklist","removechecklist","editchecklist","donechecklist","undonechecklist","movechecklist"}; // null for comments index
		
	public static String replyType[] = {"Forward","Reply to sender","Reply All"};
	
	public static Integer defaultWidthOfUserImage = 128; //in pixel
	
	public static Integer defaultHeightOfUserImage = 128; //in pixel
	
	public static Integer subscriptionActive[] = {0,1}; // 1 for active
	
	public static Integer licenseActive[] = {0,1}; // 1 for active
	
	public static Integer messageRead[] = {0,1}; // 1 for read, 0 for unread
	
	public static String defaultKanbanColor = "#9dc4d4"; //blue
	
	public static String[] allKanbanColor = {"#9dc4d4","#98eb9b","#d3889d","#dfe9ff","#f7d159","#fff1a8","#ic_yellow"}; 
	
	public static String messageTypes[] = {"itemChange","chatInvite"};
	
	public static String itemChangeTypes[] = {"created","changed","has uploaded an attachment to","added a new checklist item to","checked","unchecked"};
	
	public static String dashboardLayout[] = {"status","assignedHistory","timeline","description","kanbanboard","checklist","chat","userContact","attachment"};
	
	public static String[] en_Month = { "Jan", "Feb", "Mar", "Apr", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Des" };
	
	public static String[] editTabNames= {"basic","timestatus","checklist","attachment"};
	
	public static String[] emailActiviesStartSelectLabel= {"Off","1 hour prior to the event","2 hour prior to the event",
			"4 hour prior to the event","6 hour prior to the event","12 hour prior to the event","1 day prior to the event",
			"2 days prior to the event","3 days prior to the event","5 days prior to the event","7 days prior to the event"};
	
	public static String[] emailActiviesStartSelectValue= {"-1","1","2","4","6","12","24","48","72","120","168"};
	
	public static String[] dateFormatLabel= {"2018.12.01.","2018 Dec 1","'18 Dec 1","01.12.2018.","1 Dec 2018","1 Dec '18"
			,"01.31.2018.","Dec 1 2018","Dec 1 '18"};
	
	public static String[] dateFormatValue= {"yyyy.MM.dd. HH:mm","yyyy MMM d HH:mm","''yy MMM d HH:mm","dd.MM.yyyy. HH:mm",
			"d MMM yyyy HH:mm","d MMM ''yy HH:mm","MM.dd.yyyy. HH:mm","MMM d yyyy HH:mm","MMM d ''yy HH:mm"}; 
	
	public static String[] timeZoneLable= {"Abidjan","Accra","Addis_Ababa","Algiers","Asmara",
			"Asmera","Bamako","Bangui","Banjul","Bissau","Blantyre","Brazzaville",
			"Bujumbura","Cairo","Casablanca","Ceuta","Conakry","Dakar","Dar_es_Salaam",
			"Djibouti","Douala","El_Aaiun","Freetown","Gaborone","Harare","Johannesburg",
			"Juba","Kampala","Khartoum","Kigali","Kinshasa","Lagos","Libreville","Lome","Luanda",
			"Lubumbashi","Lusaka","Malabo","Maputo","Maseru","Mbabane","Mogadishu","Monrovia","Nairobi",
			"Ndjamena","Niamey","Nouakchott","Ouagadougou","Porto-Novo","Sao_Tome","Timbuktu","Tripoli",
			"Tunis","Windhoek","Adak","Anchorage","Anguilla","Antigua","Araguaina","Argentina/Buenos_Aires",
			"Argentina/Catamarca","Argentina/ComodRivadavia","Argentina/Cordoba","Argentina/Jujuy","Argentina/La_Rioja","Argentina/Mendoza",
			"Argentina/Rio_Gallegos","Argentina/Salta","Argentina/San_Juan","Argentina/San_Luis","Argentina/Tucuman",
			"Argentina/Ushuaia","Aruba","Asuncion","Atikokan","Atka","Bahia","Bahia_Banderas","Barbados",
			"Belem","Belize","Blanc-Sablon","Boa_Vista","Bogota","Boise","Buenos_Aires","Cambridge_Bay",
			"Campo_Grande","Cancun","Caracas","Catamarca","Cayenne","Cayman","Chicago","Chihuahua",
			"Coral_Harbour","Cordoba","Costa_Rica","Creston","Cuiaba","Curacao","Danmarkshavn","Dawson",
			"Dawson_Creek","Denver","Detroit","Dominica","Edmonton","Eirunepe","El_Salvador","Ensenada","Fort_Wayne",
			"Fortaleza","Glace_Bay","Godthab","Goose_Bay","Grand_Turk","Grenada","Guadeloupe","Guatemala","Guayaquil",
			"Guyana","Halifax","Havana","Hermosillo","Indiana/Indianapolis","Indiana/Knox","Indiana/Marengo","IndianaPetersburg",
			"Indiana/Tell_City","Indiana/Vevay","Indiana/Vincennes","Indiana/Winamac","Indianapolis","Inuvik","Iqaluit","Jamaica","Jujuy","Juneau",
			"Kentucky/Louisville","Kentucky/Monticello","Knox_IN","Kralendijk","La_Paz","Lima","Los_Angeles",
			"Louisville","Lower_Princes","Maceio","Managua","Manaus","Marigot","Martinique","Matamoros",
			"Mazatlan","Mendoza","Menominee","Merida","Metlakatla","Mexico_City","Miquelon","Moncton","Monterrey","Montevideo",
			"Montreal","Montserrat","Nassau","New_York","Nipigon","Nome","Noronha","North_Dakota/Beulah","North_Dakota/Center","North_Dakota/New_Salem",
			"Ojinaga","Panama","Pangnirtung","Paramaribo","Phoenix","Port-au-Prince","Port_of_Spain","Porto_Acre","Porto_Velho","Puerto_Rico",
			"Rainy_River","Rankin_Inlet","Recife","Regina","Resolute","Rio_Branco","Rosario","Santa_Isabel","Santarem","Santiago",
			"Santo_Domingo","Sao_Paulo","Scoresbysund","Shiprock","Sitka","St_Barthelemy","St_Johns","St_Kitts","St_Lucia","St_Thomas",
			"St_Vincent","Swift_Current","Tegucigalpa","Thule","Thunder_Bay","Tijuana","Toronto","Tortola","Vancouver","Virgin",
			"Whitehorse","Winnipeg","Yakutat","Yellowknife","Casey","Davis","DumontDUrville","Macquarie","Mawson","McMurdo",
			"Palmer","Rothera","South_Pole","Syowa","Vostok","Longyearbyen","Aden","Almaty","Amman","Anadyr","Aqtau","Aqtobe","Ashgabat (Ashkhabad)",
			"Ashkhabad","Baghdad","Bahrain","Baku","Bangkok","Beirut","Bishkek","Brunei","Calcutta","Choibalsan","Chongqing","Chungking","Colombo","Dacca","Damascus",
			"Dhaka","Dili","Dubai","Dushanbe","Gaza","Harbin","Hebron","Ho_Chi_Minh","Hong_Kong","Hovd","Irkutsk","Istanbul","Jakarta","Jayapura","Jerusalem","Kabul",
			"Kamchatka","Karachi","Kashgar","Kathmandu","Katmandu","Kolkata","Krasnoyarsk","Kuala_Lumpur","Kuching","Kuwait","Macao","Macau","Magadan","Makassar","Manila",
			"Muscat","Nicosia","Novokuznetsk","Novosibirsk","Omsk","Oral","Phnom_Penh","Pontianak","Pyongyang","Qatar","Qyzylorda","Rangoon","Riyadh","Saigon","Sakhalin","Samarkand",
			"Seoul","Shanghai","Singapore","Taipei","Tashkent","Tbilisi","Tehran","/Tel_Aviv","Thimbu","Thimphu","Tokyo","Ujung_Pandang","Ulaanbaatar","Ulan_Bator",
			"Urumqi","Vientiane","Vladivostok","Yakutsk","Yekaterinburg","Yerevan","Azores","Bermuda","Canary","Cape_Verde","Faeroe","","Jan_Mayen","Madeira","Reykjavik",
			"South_Georgia","St_Helena","Stanley","ACT","Adelaide","Brisbane","Broken_Hill","Canberra","Currie","Darwin","Eucla","Hobart","LHI","Lindeman",
			"Lord_Howe","Melbourne","North","NSW","Perth","Queensland","South","Sydney","Tasmania","Victoria","West","Yancowinna","Amsterdam","Andorra","Athens",
			"Belfast","Belgrade","Berlin","Bratislava","Brussels","Bucharest","Budapest","Chisinau","Copenhagen","Dublin","Gibraltar","Guernsey","Helsinki","Isle_of_Man","Istanbul","Jersey",
			"Kaliningrad","Kiev","Lisbon","Ljubljana","London","Luxembourg","Madrid","Malta","Mariehamn","Minsk","Monaco","Moscow","Nicosia","Oslo","Paris","Podgorica","Prague","Riga",
			"Rome","Samara","San_Marino","Sarajevo","Simferopol","Skopje","Sofia","Stockholm","Tallinn","Tirane","Tiraspol","Uzhgorod","Vaduz","Vatican","Vienna","Vilnius","Volgograd",
			"Warsaw","Zagreb","Zaporozhye","Zurich","Antananarivo","Chagos","Christmas","Cocos","Comoro","Kerguelen","Mahe","Maldives","Mauritius","Mayotte","Reunion","Apia","Auckland","Chatham",
			"Chuuk","Easter","Efate","Enderbury","Fakaofo","Fiji","Funafuti","Galapagos","Gambier","Guadalcanal","Guam","Honolulu","Johnston","Kiritimati","Kosrae","Kwajalein","Majuro",
			"Marquesas","Midway","Nauru","Niue","Norfolk","Noumea","Pago_Pago","Palau","Pitcairn","Pohnpei","Ponape","Port_Moresby","Rarotonga","Saipan","Samoa","Tahiti","Tarawa",
			"Tongatapu","Truk","Wake","Wallis","Yap","UTC"};
	public static String[] timeZoneValue= {"Africa/Abidjan","Africa/Accra","Africa/Addis_Ababa","Africa/Algiers","Africa/Asmara",
			"Africa/Asmera","Africa/Bamako","Africa/Bangui","Africa/Banjul","Africa/Bissau","Africa/Blantyre","Africa/Brazzaville",
			"Africa/Bujumbura","Africa/Cairo","Africa/Casablanca","Africa/Ceuta","Africa/Conakry","Africa/Dakar","Africa/Dar_es_Salaam",
			"Africa/Djibouti","Africa/Douala","Africa/El_Aaiun","Africa/Freetown","Africa/Gaborone","Africa/Harare","Africa/Johannesburg",
			"Africa/Juba","Africa/Kampala","Africa/Khartoum","Africa/Kigali","Africa/Kinshasa","Africa/Lagos","Africa/Libreville","Africa/Lome","Africa/Luanda",
			"Africa/Lubumbashi","Africa/Lusaka","Africa/Malabo","Africa/Maputo","Africa/Maseru","Africa/Mbabane","Africa/Mogadishu","Africa/Monrovia","Africa/Nairobi",
			"Africa/Ndjamena","Africa/Niamey","Africa/Nouakchott","Africa/Ouagadougou","Africa/Porto-Novo","Africa/Sao_Tome","Africa/Timbuktu","Africa/Tripoli",
			"Africa/Tunis","Africa/Windhoek","America/Adak","America/Anchorage","America/Anguilla","America/Antigua","America/Araguaina","America/Argentina/Buenos_Aires",
			"America/Argentina/Catamarca","America/Argentina/ComodRivadavia","America/Argentina/Cordoba","America/Argentina/Jujuy","America/Argentina/La_Rioja","America/Argentina/Mendoza",
			"America/Argentina/Rio_Gallegos","America/Argentina/Salta","America/Argentina/San_Juan","America/Argentina/San_Luis","America/Argentina/Tucuman",
			"America/Argentina/Ushuaia","America/Aruba","America/Asuncion","America/Atikokan","America/Atka","America/Bahia","America/Bahia_Banderas","America/Barbados",
			"America/Belem","America/Belize","America/Blanc-Sablon","America/Boa_Vista","America/Bogota","America/Boise","America/Buenos_Aires","America/Cambridge_Bay",
			"America/Campo_Grande","America/Cancun","America/Caracas","America/Catamarca","America/Cayenne","America/Cayman","America/Chicago","America/Chihuahua",
			"America/Coral_Harbour","America/Cordoba","America/Costa_Rica","America/Creston","America/Cuiaba","America/Curacao","America/Danmarkshavn","America/Dawson",
			"America/Dawson_Creek","America/Denver","America/Detroit","America/Dominica","America/Edmonton","America/Eirunepe","America/El_Salvador","America/Ensenada","America/Fort_Wayne",
			"America/Fortaleza","America/Glace_Bay","America/Godthab","America/Goose_Bay","America/Grand_Turk","America/Grenada","America/Guadeloupe","America/Guatemala","America/Guayaquil",
			"America/Guyana","America/Halifax","America/Havana","America/Hermosillo","America/Indiana/Indianapolis","America/Indiana/Knox","America/Indiana/Marengo","America/Indiana/Petersburg",
			"America/Indiana/Tell_City","America/Indiana/Vevay","America/Indiana/Vincennes","America/Indiana/Winamac","America/Indianapolis","America/Inuvik","America/Iqaluit","America/Jamaica","America/Jujuy","America/Juneau",
			"America/Kentucky/Louisville","America/Kentucky/Monticello","America/Knox_IN","America/Kralendijk","America/La_Paz","America/Lima","America/Los_Angeles",
			"America/Louisville","America/Lower_Princes","America/Maceio","America/Managua","America/Manaus","America/Marigot","America/Martinique","America/Matamoros",
			"America/Mazatlan","America/Mendoza","America/Menominee","America/Merida","America/Metlakatla","America/Mexico_City","America/Miquelon","America/Moncton","America/Monterrey","America/Montevideo",
			"America/Montreal","America/Montserrat","America/Nassau","America/New_York","America/Nipigon","America/Nome","America/Noronha","America/North_Dakota/Beulah","America/North_Dakota/Center","America/North_Dakota/New_Salem",
			"America/Ojinaga","America/Panama","America/Pangnirtung","America/Paramaribo","America/Phoenix","America/Port-au-Prince","America/Port_of_Spain","America/Porto_Acre","America/Porto_Velho","America/Puerto_Rico",
			"America/Rainy_River","America/Rankin_Inlet","America/Recife","America/Regina","America/Resolute","America/Rio_Branco","America/Rosario","America/Santa_Isabel","America/Santarem","America/Santiago",
			"America/Santo_Domingo","America/Sao_Paulo","America/Scoresbysund","America/Shiprock","America/Sitka","America/St_Barthelemy","America/St_Johns","America/St_Kitts","America/St_Lucia","America/St_Thomas",
			"America/St_Vincent","America/Swift_Current","America/Tegucigalpa","America/Thule","America/Thunder_Bay","America/Tijuana","America/Toronto","America/Tortola","America/Vancouver","America/Virgin",
			"America/Whitehorse","America/Winnipeg","America/Yakutat","America/Yellowknife","Antarctica/Casey","Antarctica/Davis","Antarctica/DumontDUrville","Antarctica/Macquarie","Antarctica/Mawson","Antarctica/McMurdo",
			"Antarctica/Palmer","Antarctica/Rothera","Antarctica/South_Pole","Antarctica/Syowa","Antarctica/Vostok","Arctic/Longyearbyen","Asia/Aden","Asia/Almaty","Asia/Amman","Asia/Anadyr","Asia/Aqtau","Asia/Aqtobe","Asia/Ashgabat",
			"Asia/Ashkhabad","Asia/Baghdad","Asia/Bahrain","Asia/Baku","Asia/Bangkok","Asia/Beirut","Asia/Bishkek","Asia/Brunei","Asia/Calcutta","Asia/Choibalsan","Asia/Chongqing","Asia/Chungking","Asia/Colombo","Asia/Dacca","Asia/Damascus",
			"Asia/Dhaka","Asia/Dili","Asia/Dubai","Asia/Dushanbe","Asia/Gaza","Asia/Harbin","Asia/Hebron","Asia/Ho_Chi_Minh","Asia/Hong_Kong","Asia/Hovd","Asia/Irkutsk","Asia/Istanbul","Asia/Jakarta","Asia/Jayapura","Asia/Jerusalem","Asia/Kabul",
			"Asia/Kamchatka","Asia/Karachi","Asia/Kashgar","Asia/Kathmandu","Asia/Katmandu","Asia/Kolkata","Asia/Krasnoyarsk","Asia/Kuala_Lumpur","Asia/Kuching","Asia/Kuwait","Asia/Macao","Asia/Macau","Asia/Magadan","Asia/Makassar","Asia/Manila",
			"Asia/Muscat","Asia/Nicosia","Asia/Novokuznetsk","Asia/Novosibirsk","Asia/Omsk","Asia/Oral","Asia/Phnom_Penh","Asia/Pontianak","Asia/Pyongyang","Asia/Qatar","Asia/Qyzylorda","Asia/Rangoon","Asia/Riyadh","Asia/Saigon","Asia/Sakhalin","Asia/Samarkand",
			"Asia/Seoul","Asia/Shanghai","Asia/Singapore","Asia/Taipei","Asia/Tashkent","Asia/Tbilisi","Asia/Tehran","Asia/Tel_Aviv","Asia/Thimbu","Asia/Thimphu","Asia/Tokyo","Asia/Ujung_Pandang","Asia/Ulaanbaatar","Asia/Ulan_Bator",
			"Asia/Urumqi","Asia/Vientiane","Asia/Vladivostok","Asia/Yakutsk","Asia/Yekaterinburg","Asia/Yerevan","Atlantic/Azores","Atlantic/Bermuda","Atlantic/Canary","Atlantic/Cape_Verde","Atlantic/Faeroe","Atlantic/Faroe","Atlantic/Jan_Mayen","Atlantic/Madeira","Atlantic/Reykjavik",
			"Atlantic/South_Georgia","Atlantic/St_Helena","Atlantic/Stanley","Australia/ACT","Australia/Adelaide","Australia/Brisbane","Australia/Broken_Hill","Australia/Canberra","Australia/Currie","Australia/Darwin","Australia/Eucla","Australia/Hobart","Australia/LHI","Australia/Lindeman",
			"Australia/Lord_Howe","Australia/Melbourne","Australia/North","Australia/NSW","Australia/Perth","Australia/Queensland","Australia/South","Australia/Sydney","Australia/Tasmania","Australia/Victoria","Australia/West","Australia/Yancowinna","Europe/Amsterdam","Europe/Andorra","Europe/Athens",
			"Europe/Belfast","Europe/Belgrade","Europe/Berlin","Europe/Bratislava","Europe/Brussels","Europe/Bucharest","Europe/Budapest","Europe/Chisinau","Europe/Copenhagen","Europe/Dublin","Europe/Gibraltar","Europe/Guernsey","Europe/Helsinki","Europe/Isle_of_Man","Europe/Istanbul","Europe/Jersey",
			"Europe/Kaliningrad","Europe/Kiev","Europe/Lisbon","Europe/Ljubljana","Europe/London","Europe/Luxembourg","Europe/Madrid","Europe/Malta","Europe/Mariehamn","Europe/Minsk","Europe/Monaco","Europe/Moscow","Europe/Nicosia","Europe/Oslo","Europe/Paris","Europe/Podgorica","Europe/Prague","Europe/Riga",
			"Europe/Rome","Europe/Samara","Europe/San_Marino","Europe/Sarajevo","Europe/Simferopol","Europe/Skopje","Europe/Sofia","Europe/Stockholm","Europe/Tallinn","Europe/Tirane","Europe/Tiraspol","Europe/Uzhgorod","Europe/Vaduz","Europe/Vatican","Europe/Vienna","Europe/Vilnius","Europe/Volgograd",
			"Europe/Warsaw","Europe/Zagreb","Europe/Zaporozhye","Europe/Zurich","Indian/Antananarivo","Indian/Chagos","Indian/Christmas","Indian/Cocos","Indian/Comoro","Indian/Kerguelen","Indian/Mahe","Indian/Maldives","Indian/Mauritius","Indian/Mayotte","Indian/Reunion","Pacific/Apia","Pacific/Auckland","Pacific/Chatham",
			"Pacific/Chuuk","Pacific/Easter","Pacific/Efate","Pacific/Enderbury","Pacific/Fakaofo","Pacific/Fiji","Pacific/Funafuti","Pacific/Galapagos","Pacific/Gambier","Pacific/Guadalcanal","Pacific/Guam","Pacific/Honolulu","Pacific/Johnston","Pacific/Kiritimati","Pacific/Kosrae","Pacific/Kwajalein","Pacific/Majuro",
			"Pacific/Marquesas","Pacific/Midway","Pacific/Nauru","Pacific/Niue","Pacific/Norfolk","Pacific/Noumea","Pacific/Pago_Pago","Pacific/Palau","Pacific/Pitcairn","Pacific/Pohnpei","Pacific/Ponape","Pacific/Port_Moresby","Pacific/Rarotonga","Pacific/Saipan","Pacific/Samoa","Pacific/Tahiti","Pacific/Tarawa",
			"Pacific/Tongatapu","Pacific/Truk","Pacific/Wake","Pacific/Wallis","Pacific/Yap","UTC"};
}


