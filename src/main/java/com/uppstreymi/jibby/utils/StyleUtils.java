package com.uppstreymi.jibby.utils;

public enum StyleUtils {

	PIN_DOWNY("pin-downy"), PIN_LASER_EMON("pin-laser-emon"), PIN_ROSE("pin-rose");

	private String name = null;

	StyleUtils(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name();
	}

	public String getName() {
		return name;
	}

}
