package com.uppstreymi.jibby.utils;

import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.button.Button;

/**
 * A Designer generated component for the dialog-html.html template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("dialog-html")
@JsModule("./src/views/dialog-html.js")
public class DialogHtml extends PolymerTemplate<DialogHtml.DialogHtmlModel> {

    @Id("dialogtext")
	private Div dialogtext;
	@Id(" btncancel")
	private Button btncancel;
	@Id(" btnsave")
	private Button btnsave;

	
    public DialogHtml() {
    	
    }
    
    public Div getDialog() {
    	return this.dialogtext;
    }
    
    public void setDialog(String text) {
    	this.dialogtext.setText(text);
    }
    
    public Button getDialogCancel() {
    	return this.btncancel;
    }
    
    public Button getDialogOk() {
    	return this.btnsave;
    }

    public interface DialogHtmlModel extends TemplateModel {
    }
}
