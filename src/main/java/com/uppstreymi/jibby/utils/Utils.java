package com.uppstreymi.jibby.utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.InputStream;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinSession;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.xml.bind.DatatypeConverter;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.login.LoginPage;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Utils {

	public static final String JSON_UTF8 = "application/json; charset=UTF-8";
	private static Logger logger=LogManager.getLogger(Utils.class);

	private static final String ALGO = "AES";
	private static final byte[] keyValue = new byte[] { 'T', 'h', 'e', 'B', 'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't',
			'K', 'e', 'y' };
	static final String PASSWORDPATTERN = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom RND = new SecureRandom();
	public static String ProtocolName = "http://"; // app work with HTTP or
	// HTTPS (follow UI class)
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	static final String LICENCEPATTERN = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789";
	static final String ORDERPATTERN = "0123456789";

	public static String colASelectedProject = "colAActiveProject";
	public static String colASort = "colASort";
	public static String colAType = "colATypes";
	public static String colAOptionStatus = "itemStatus";
	public static String colAOptionUserRoles = "rules";
	public static String colAOptionStart = "start";
	public static String colAOptionFinish = "end";
	public static String colAOptionStartDate = "startDate";
	public static String colAOptionFinishDate = "finishDate";
	public static String colAOptionIsPublic = "projectType";

	public static String colBFoldKey = "colBfolditem";
	public static String colBSelectedItem = "colBActiveItem";

	public static String colCdeshboardMenuSelected = "dashboardActiveViews";
	public static String colCActiveTab = "colCActiveTab";

	public static String colCMyactionHideItem = "myactionlistHideDoneItem";
	public static String colCMyItemsSelectOption = "myItemSelectOption";
	public static String colCAssigntoMeSelectOption = "assignToMeSelectOption";
	public static String colCAssigntoOthersSelectOption = "assignToOthersSelectOption";
	public static String colCMyActionlistTab = "myactionlistActiveTab";

	public static String INSTANCE_ROOT = System.getProperty("com.sun.aas.instanceRoot");
	public static String imagePath = INSTANCE_ROOT + File.separator + "docroot" + File.separator + "jibby"
			+ File.separator + "uploads" + File.separator;
	public static String userImagePath = INSTANCE_ROOT + File.separator + "docroot" + File.separator + "jibby"
			+ File.separator + "usercontent" + File.separator + "avatars" + File.separator;
	public static String googleDriveFileUserPath = INSTANCE_ROOT + File.separator + "docroot" + File.separator + "jibby"
			+ File.separator;

	public static String zamzarApiKey = "bd6e6779befab6cc71581ddb33357b13c8823457";
	public static String zamzarApi = "https://api.zamzar.com/v1";

	public static String jibbyTitle = "Jibby - Get Organized";

	public static boolean isValidateMail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	public static Date firstDayCurrentMonth() {
		Calendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.set(Calendar.DAY_OF_MONTH, 1);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		Date monthStart = gc.getTime();
		return monthStart;
	}

	public static Date firstDayCurrentWeek() {
		Calendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.set(Calendar.DAY_OF_WEEK, 1);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		Date monthStart = gc.getTime();
		return monthStart;
	}

	public static Date addDaywithDate(Date from, Integer days) {
		Calendar gc = new GregorianCalendar();
		gc.setTime(from);
		gc.add(Calendar.DAY_OF_MONTH, days);
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	public static Date subDaywithDate(Date form, Integer days) {
		Calendar gc = new GregorianCalendar();
		gc.setTime(form);
		gc.add(Calendar.DAY_OF_MONTH, -days);
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	public static Date getBeginningDate(Date date) {
		Calendar myCal = new GregorianCalendar();
		myCal.setTime(date);
		myCal.set(Calendar.HOUR_OF_DAY, 0);
		myCal.set(Calendar.MINUTE, 0);
		myCal.set(Calendar.SECOND, 0);
		myCal.set(Calendar.MILLISECOND, 0);
		return myCal.getTime();
	}

	public static Date getEndDate(Date date) {
		Calendar myCal = new GregorianCalendar();
		myCal.setTime(date);
		myCal.set(Calendar.HOUR_OF_DAY, 23);
		myCal.set(Calendar.MINUTE, 59);
		myCal.set(Calendar.SECOND, 59);
		myCal.set(Calendar.MILLISECOND, 0);
		return myCal.getTime();
	}

	public static Date previousDay(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		// subtracting a day
		cal.add(Calendar.DATE, -days);
		Date enddate = cal.getTime();
		try {
			enddate = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
					.parse(new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime()) + " 23:59:59");
		} catch (ParseException e) {
			logger.error("An exception thrown in previousDay",e);
		}
		// return cal.getTime();
		return enddate;
	}

	public static Date getFirstDayOfWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_WEEK, 2);
		Date firstdate = null;
		try {
			firstdate = new SimpleDateFormat("yyyy-MM-dd")
					.parse(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));
		} catch (Exception e) {
			firstdate = calendar.getTime();
			logger.error("An exception thrown in Get FirstDayOfWeek",e);
		}
		return firstdate;
	}

	public static Date getAddDaysStartTime(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getAddMinutesStartTime(Date date, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// cal.set(Calendar.DATE,0);
		// cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.add(Calendar.MINUTE, minutes);
		// cal.set(Calendar.SECOND, 0);
		// cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date addDaysStartTime(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		// cal.set(Calendar.HOUR_OF_DAY, 0);
		// cal.add(Calendar.MINUTE,minutes);
		// cal.set(Calendar.SECOND, 0);
		// cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getAddHoursStartTime(Date date, int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// cal.set(Calendar.DATE, 0);
		cal.add(Calendar.HOUR_OF_DAY, hours);
		// cal.set(Calendar.MINUTE, 0);
		// cal.set(Calendar.SECOND, 0);
		// cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getAddDaysEndTime(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getFirstDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getLastDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);
		Date lastDayOfMonth = calendar.getTime();

		try {
			lastDayOfMonth = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
					.parse(new SimpleDateFormat("dd-MM-yyyy").format(lastDayOfMonth) + " 23:59:59");
		} catch (ParseException e) {
			logger.error("An exception thrown in Get LastDayOfMonth",e);
		}
		return lastDayOfMonth;
	}

	public static Date getFirstDayOfYear() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getLastDayOfYear() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getFirstDayOfPreviousYear() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, -1);// previous year
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getLastDayOfPreviousYear() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, -1);// previous year
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getFirstDayOfYear(Integer year) {
		Calendar calendarStart = new GregorianCalendar();
		calendarStart.set(Calendar.YEAR, year);
		calendarStart.set(Calendar.MONTH, 0);
		calendarStart.set(Calendar.DAY_OF_MONTH, 1);
		calendarStart.set(Calendar.HOUR, 0);
		calendarStart.set(Calendar.MINUTE, 0);
		calendarStart.set(Calendar.SECOND, 0);
		return calendarStart.getTime();
	}

	public static Date getLastDayOfYear(Integer year) {
		Calendar calendarStart = new GregorianCalendar();
		calendarStart.set(Calendar.YEAR, year);
		calendarStart.set(Calendar.MONTH, 11);
		calendarStart.set(Calendar.DAY_OF_MONTH, 31);
		calendarStart.set(Calendar.HOUR, 23);
		calendarStart.set(Calendar.MINUTE, 59);
		calendarStart.set(Calendar.SECOND, 59);
		return calendarStart.getTime();
	}

	public static Date getFirstDayOfMonthYear(Integer month, Integer year) {
		Calendar calendarStart = new GregorianCalendar();
		calendarStart.set(Calendar.YEAR, year);
		calendarStart.set(Calendar.MONTH, month);
		calendarStart.set(Calendar.DAY_OF_MONTH, 1);
		calendarStart.set(Calendar.HOUR, 0);
		calendarStart.set(Calendar.MINUTE, 0);
		calendarStart.set(Calendar.SECOND, 0);
		return calendarStart.getTime();
	}

	public static Date getLastDayOfMonthYear(Integer month, Integer year) {
		Calendar calendarStart = new GregorianCalendar();
		calendarStart.set(Calendar.YEAR, year);
		calendarStart.set(Calendar.MONTH, month);
		calendarStart.set(Calendar.DATE, calendarStart.getActualMaximum(Calendar.DATE));
		calendarStart.set(Calendar.HOUR, 23);
		calendarStart.set(Calendar.MINUTE, 59);
		calendarStart.set(Calendar.SECOND, 59);
		calendarStart.add(Calendar.DATE, -1);
		return calendarStart.getTime();
	}

	public static String date2Str(Object date) {
		return new SimpleDateFormat("dd.MM.yyy").format(date);
	}

	public static String datetime2Str(Object date) {
		return new SimpleDateFormat("dd.MM.yyy hh-mm-ss").format(date);
	}

	public static String datetimes2Str(Object date) {
		return new SimpleDateFormat("yyyy.MM.dd hh:mm").format(date);
	}

	public static String dbl2Str(double value, int decimals) {
		Locale locale = new Locale("is");
		String formatStr = "%,." + decimals + "f";
		return String.format(locale, formatStr, value);
	}

	public static double decimalFormat(double value, int decimals) {
		String formatStr = "%." + decimals + "f";
		return Double.valueOf(String.format(Locale.US, formatStr, value));
	}

	public static String date2HourMins(Date dateValue) {
		return new SimpleDateFormat("HH:mm").format(dateValue);
	}

	public static String getLiveUrl() {
		return String.valueOf(VaadinSession.getCurrent().getAttribute("url"));
	}

	public static String getLiveDocumentUrl() {
		String url = Utils.getLiveUrl() + "/docpreview";
		return url.replaceAll("(?<!http:|https:)//", "/");
	}

	public static UUID getUUID() {
		return UUID.randomUUID();
	}

	public static String getSessionLanguage() {
		String lang = "is";
		Object sessionLang = VaadinSession.getCurrent().getAttribute("language");
		if (sessionLang != null && sessionLang instanceof String && !((String) sessionLang).isEmpty())
			lang = (String) sessionLang;
		return lang;
	}

	// store cookies
	public static Cookie getCookieByName(String name) {
		// Fetch all cookies from the request
		Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();

		// Iterate to find cookie by its name
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (name.equals(cookie.getName())) {
					return cookie;
				}
			}
		}

		return null;
	}

	static public void createCookie(String name, String value) {
		Cookie cookie = new Cookie(name, value);
		cookie.setMaxAge(15552000);// Cookie is stored for 6 month
		cookie.setPath("/"); // So it does not depends on the request path (like
		// /JavaBlackBelt/ui/UIDL) --
		VaadinService.getCurrentResponse().addCookie(cookie);
	}

	static public void destroyCookieByName(String name) {
		Cookie cookie = getCookieByName(name);

		if (cookie != null) {
			cookie.setValue(null);
			// By setting the cookie maxAge to 0 it will deleted immediately
			cookie.setMaxAge(0);
			cookie.setPath("/");
			VaadinService.getCurrentResponse().addCookie(cookie);
		}
	}

	public static void showSucessMessage(String message) {
		Notification.show(message, 2000, Position.BOTTOM_CENTER);
	}

	public static void showWarning(String message) {
		Notification.show(message, 2000, Position.BOTTOM_CENTER);
	}

	public static String encrypt(String Data) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}

	public static String decrypt(String encryptedData) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	public static String sha1(String input) {
		String sha1 = null;
		try {
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
			msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
			sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("An exception thrown in sha1",e);
		}
		return sha1.toLowerCase();
	}

	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}

	public static Date dateForDateAndTime(Date theDate, Date theTime) {
		GregorianCalendar cal1 = new GregorianCalendar();
		cal1.setTime(theDate);
		GregorianCalendar cal2 = new GregorianCalendar();
		cal2.setTime(theTime);
		cal1.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
		cal1.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
		cal1.set(Calendar.SECOND, cal2.get(Calendar.SECOND));
		cal1.set(Calendar.MILLISECOND, 0);
		return cal1.getTime();
	}

	public static boolean fileMoved(String filePath, String filename, File file) {

		File fileFolder = new File(filePath);
		if (!fileFolder.exists()) {
			try {
				if (fileFolder.mkdirs()) {
					if (file.renameTo(new File(filePath + "/" + filename))) {
						return true;

					} else {
						return false;
					}

				} else {
					return false;
				}
			} catch (Exception e) {
				logger.error("An exception thrown in fileMoved",e);
				return false;
			}
		} else {

			if (file.renameTo(new File(filePath + "/" + filename))) {
				return true;

			} else {
				return false;
			}
		}
	}

	public static boolean deleteFile(String filepath) {

		try {
			File file = new File(filepath);
			if (file.delete()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error("An exception thrown in deleteFile",e);
			return true;
		}
	}

	public static StreamResource getStream(String imagePath, String filename) {

		StreamResource resource = null;
		if (new File(imagePath + filename).exists()) {
			resource = new StreamResource(filename, () -> {
				try {
					InputStream a = Files.newInputStream(Paths.get(imagePath + filename), StandardOpenOption.READ);
					return a;
				} catch (Exception e) {
					logger.error("An exception thrown in getStream",e);
					return null;
				}
			});
		}

		return resource;
	}

	public static Date addDateWithFrequencyType(Date dateValue, Integer frequencyType, Integer frequency) {
		Calendar cal = new GregorianCalendar();
		if (dateValue != null)
			cal.setTime(dateValue);
		else
			cal.setTime(new Date());

		// 0-> Days, 1-> Weeks, 2-> Months, 3 -> Years
		if (frequencyType == 0)
			cal.add(Calendar.DAY_OF_MONTH, frequency);
		else if (frequencyType == 1)
			cal.add(Calendar.WEEK_OF_MONTH, frequency);
		else if (frequencyType == 2)
			cal.add(Calendar.MONTH, frequency);
		else if (frequencyType == 3)
			cal.add(Calendar.YEAR, frequency);

		return cal.getTime();
	}

	public static Date subDateWithFrequencyType(Date dateValue, Integer frequencyType, Integer frequency) {
		Calendar cal = new GregorianCalendar();
		if (dateValue != null)
			cal.setTime(dateValue);
		else
			cal.setTime(new Date());

		// 0-> Days, 1-> Weeks, 2-> Months, 3 -> Years
		if (frequencyType == 0)
			cal.add(Calendar.DAY_OF_MONTH, -frequency);
		else if (frequencyType == 1)
			cal.add(Calendar.WEEK_OF_MONTH, -frequency);
		else if (frequencyType == 2)
			cal.add(Calendar.MONTH, -frequency);
		else if (frequencyType == 3)
			cal.add(Calendar.YEAR, -frequency);

		return cal.getTime();
	}

	public static long daysDifference(Date startDate, Date endDate) {
		Calendar startCal = new GregorianCalendar();
		startCal.setTime(startDate);
		startCal.set(Calendar.HOUR_OF_DAY, 0);
		startCal.set(Calendar.MINUTE, 0);
		startCal.set(Calendar.SECOND, 0);
		startCal.set(Calendar.MILLISECOND, 0);

		Calendar endCal = new GregorianCalendar();
		endCal.setTime(endDate);
		endCal.set(Calendar.HOUR_OF_DAY, 23);
		endCal.set(Calendar.MINUTE, 59);
		endCal.set(Calendar.SECOND, 59);
		endCal.set(Calendar.MILLISECOND, 0);

		return ChronoUnit.DAYS.between(startCal.toInstant(), endCal.toInstant());
	}

	public static Date addMonthWithCurrentDate(Integer month) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(new Date());

		cal.add(Calendar.MONTH, month);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date addMonthWithGivenDate(Date date, Integer month) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		cal.add(Calendar.MONTH, month);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date firstDayOfNextMonth() {
		Calendar gc = new GregorianCalendar();
		gc.setTime(new Date());

		gc.add(Calendar.MONTH, 1);

		gc.set(Calendar.DAY_OF_MONTH, 1);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	public static Date lastDayOfNextMonth() {
		Calendar gc = new GregorianCalendar();
		gc.setTime(new Date());

		gc.add(Calendar.MONTH, 1);

		gc.set(Calendar.DAY_OF_MONTH, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	public static Date lastDayOfCurrentMonth() {
		Calendar gc = new GregorianCalendar();
		gc.setTime(new Date());

		// gc.add(Calendar.MONTH, 1);

		gc.set(Calendar.DAY_OF_MONTH, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	public static boolean isWithinRange(Date startDT, Date endDT, Date checkDT) {
		checkDT = getEndDate(checkDT);
		return checkDT.getTime() >= startDT.getTime() && checkDT.getTime() <= endDT.getTime();
	}

	public static List<Date> getAllDatesGivenDateRange(Date startDT, Date endDT) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startDT);

		while (calendar.getTime().before(endDT)) {
			Date result = calendar.getTime();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	public static List<Date> getAllScheduledDatesGivenDateRange(Date startDT, Date endDT, Date trgtDT, int frequency,
			int frequencyType) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(trgtDT);

		// 0-> Days, 1-> Weeks, 2-> Months, 3 -> Years
		if (frequencyType == 0)
			calendar.add(Calendar.DAY_OF_MONTH, frequency);
		else if (frequencyType == 1)
			calendar.add(Calendar.WEEK_OF_MONTH, frequency);
		else if (frequencyType == 2)
			calendar.add(Calendar.MONTH, frequency);
		else if (frequencyType == 3)
			calendar.add(Calendar.YEAR, frequency);

		while (calendar.getTime().before(endDT) && calendar.getTime().after(startDT)) {
			Date result = calendar.getTime();
			dates.add(result);

			// 0-> Days, 1-> Weeks, 2-> Months, 3 -> Years
			if (frequencyType == 0)
				calendar.add(Calendar.DAY_OF_MONTH, frequency);
			else if (frequencyType == 1)
				calendar.add(Calendar.WEEK_OF_MONTH, frequency);
			else if (frequencyType == 2)
				calendar.add(Calendar.MONTH, frequency);
			else if (frequencyType == 3)
				calendar.add(Calendar.YEAR, frequency);
		}
		return dates;
	}

	// Convert String to Local Date
	public static LocalDate strTolocalDate(String st) {
		LocalDate localDate = LocalDate.parse(st);
		return localDate;
	}

	// Convert java.util.Date to java.time.LocalDateTime
	public static LocalDateTime dateToLocalDateTime(Date ts) {
		Instant instant = Instant.ofEpochMilli(ts.getTime());
		LocalDateTime res = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		return res;
	}

	// Convert java.util.Date to java.time.LocalDate
	public static LocalDate dateToLocalDate(Date date) {
		Instant instant = Instant.ofEpochMilli(date.getTime());
		LocalDate res = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
		return res;
	}

	// Convert java.time.LocalDateTime to java.util.Date
	public static Date localDateTimeToDate(LocalDateTime ldt) {
		Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		return res;
	}

	// Convert java.time.LocalDate to java.util.Date
	public static Date localDateToDate(LocalDate ld) {
		Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		return res;
	}

	public static String randomStringGenerator(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(PASSWORDPATTERN.charAt(RND.nextInt(PASSWORDPATTERN.length())));
		return sb.toString();
	}

	public static String getExtensionOfFile(String fileName) {

		String fileExtension = "";
		// Get file Name first

		// If fileName do not contain "." or starts with "." then it is not a
		// valid file
		if (fileName.contains(".") && fileName.lastIndexOf(".") != 0) {
			fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
		}
		return fileExtension;
	}

	public static String trimTextInTheMiddle(String text, int length) {
		if (text.length() <= length) {
			return text;
		} else {
			String prefix = text.substring(0, (int) Math.floor(length / 2));
			String suffix = text.substring(text.length() - (int) Math.ceil(length / 2), text.length());
			return new StringBuilder(prefix).append("..").append(suffix).toString();
		}
	}

	public static String trimTextInTheFirst(String text, int length) {
		if (text.length() <= length) {
			return text;
		} else {
			String prefix = text.substring(0, length);
			return new StringBuilder(prefix).append("...").toString();
		}
	}

	public static String getItemIcon(Integer itemType) {
		String str = "";

		switch (itemType) {
		case 1:
			str = "project.png";
			break;
		case 2:
			str = "information.png";
			break;
		case 3:
			str = "deliverable.png";
			break;
		case 4:
			str = "task_old.png";
			break;
		case 5:
			str = "timesheet.png";
			break;
		case 6:
			str = "event.png";
			break;
		case 7:
			str = "meeting.png";
			break;
		case 8:
			str = "memo.png";
			break;
		case 9:
			str = "document.png";
			break;
		case 10:
			str = "financial.png";
			break;
		case 11:
			str = "mail.png";
			break;
		case 12:
			str = "blog.png";
			break;
		case 13:
			str = "contact.png";
			break;

		default:
			break;
		}

		return str;
	}

	private static String txtToHtml(String s) {
		StringBuilder builder = new StringBuilder();
		boolean previousWasASpace = false;
		for (char c : s.toCharArray()) {
			if (c == ' ') {
				if (previousWasASpace) {
					builder.append("&nbsp;");
					previousWasASpace = false;
					continue;
				}
				previousWasASpace = true;
			} else {
				previousWasASpace = false;
			}
			switch (c) {
			case '<':
				builder.append("&lt;");
				break;
			case '>':
				builder.append("&gt;");
				break;
			case '&':
				builder.append("&amp;");
				break;
			case '"':
				builder.append("&quot;");
				break;
			case '\n':
				builder.append("<br>");
				break;
			// We need Tab support here, because we print StackTraces as HTML
			case '\t':
				builder.append("&nbsp; &nbsp; &nbsp;");
				break;
			default:
				builder.append(c);

			}
		}
		String converted = builder.toString();
		String str = "(?i)\\b((?:https?://|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:\'\".,<>?«»“”‘’]))";
		Pattern patt = Pattern.compile(str);
		Matcher matcher = patt.matcher(converted);
		converted = matcher.replaceAll("<a href=\"$1\">$1</a>");
		return converted;
	}

	/**
	 * This function resize the image file and returns the BufferedImage object that
	 * can be saved to file system.
	 */
	public static BufferedImage resizeImage(final Image image, int width, int height, String fileType) {
		Integer bufferimageType;
		if (fileType.equals("png")) {
			bufferimageType = BufferedImage.TYPE_INT_ARGB;
		} else
			bufferimageType = BufferedImage.TYPE_INT_RGB;

		final BufferedImage bufferedImage = new BufferedImage(width, height, bufferimageType);
		final Graphics2D graphics2D = bufferedImage.createGraphics();
		graphics2D.setComposite(AlphaComposite.Src);
		// below three lines are for RenderingHints for better image quality at cost of
		// higher processing time
		graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2D.drawImage(image, 0, 0, width, height, null);
		graphics2D.dispose();
		return bufferedImage;
	}

	/**
	 * This function rotate the image file and returns the BufferedImage object that
	 * can be saved to file system.
	 */
	public static BufferedImage rotateImage(BufferedImage image) {
		final double rads = Math.toRadians(90);
		final double sin = Math.abs(Math.sin(rads));
		final double cos = Math.abs(Math.cos(rads));
		final int w = (int) Math.floor(image.getWidth() * cos + image.getHeight() * sin);
		final int h = (int) Math.floor(image.getHeight() * cos + image.getWidth() * sin);
		final BufferedImage rotatedImage = new BufferedImage(w, h, image.getType());
		final AffineTransform at = new AffineTransform();
		at.translate(w / 2, h / 2);
		at.rotate(rads, 0, 0);
		at.translate(-image.getWidth() / 2, -image.getHeight() / 2);
		final AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		return rotateOp.filter(image, rotatedImage);
	}

	/* date to string with user time format and zone */
	public static String datetimes2StrWithFormatAndZone(Object date, String userDateformat, String userTimezone) {
		SimpleDateFormat dateformat;
		dateformat = new SimpleDateFormat(userDateformat);
		dateformat.setTimeZone(TimeZone.getTimeZone(userTimezone));
		return dateformat.format(date);
	}

	/* String to local date time */

	public static LocalDateTime strToLocalDateTime(String str, String userdateFormat) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(userdateFormat);
		LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

		return dateTime;

	}

	// Convert java.time.LocalDateTime to java.util.Date
	public static Date localDateTimeToDate(LocalDateTime ldt, String userTimeZone) {
		Instant instant = ldt.atZone(ZoneId.of(userTimeZone)).toInstant();
		Date res = Date.from(instant);
		return res;
	}

	// Conver java.millisecont to java.util.Date
	public static Date millisToDate(long millis) {
		Date date = new Date();
		date.setTime(millis);
		return date;
	}

	public static String getImageByType(String contentType) {
		String img = "";

		switch (contentType) {
		case "image/jpeg":
			img = "images/ic_jpg.png";
			break;
		case "image/png":
			img = "images/ic_png.png";
			break;
		case "application/pdf":
			img = "images/pdf_50x50.png";
			break;
		case "image/vnd.dwg":
			img = "images/ic_dwg50x50.png";
			break;
		case "text/plain":
			img = "images/ic_txt50x50.png";
			break;
		case "application/x-httpd-php":
			img = "images/ic_txt50x50.png";
			break;
		case "application/msword":
		case "application/zip":
		case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
			img = "images/ic_word50x50.png";
			break;
		case "application/msexcel":
		case "application/vnd.ms-excel":
		case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
			img = "images/ic_xlsx50x50.png";
			break;
		default:
			img = "images/ic_txt50x50.png";
		}

		return img;
	}

	public static String jsonBuilder(ActiveViewDTO activeViewDTO) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();

			String json = objectMapper.writeValueAsString(activeViewDTO);

			return json;
		} catch (Exception e) {
			logger.error("An exception thrown in utils jsonBuilder",e);
		}

		return null;
	}

	public static Date addYearWithDate(Date date, Integer year) {
		Calendar gc = new GregorianCalendar();
		gc.setTime(date);
		gc.add(Calendar.YEAR, year);
		/*
		 * gc.set(Calendar.HOUR_OF_DAY, 0); gc.set(Calendar.MINUTE, 0);
		 * gc.set(Calendar.SECOND, 0); gc.set(Calendar.MILLISECOND, 0);
		 */
		return gc.getTime();
	}

	public static String getTimeDifference(Date d2, Date d1, String userFormat, String timeZone) {
		/*
		 * Date diff = new Date(d2.getTime() - d1.getTime());
		 * 
		 * Calendar calendar = GregorianCalendar.getInstance(); calendar.setTime(diff);
		 * int days = calendar.get(Calendar.DAY_OF_YEAR); int hours =
		 * calendar.get(Calendar.HOUR); int minutes = calendar.get(Calendar.MINUTE);
		 */

		long diff = d2.getTime() - d1.getTime();
		long minutes = diff / (60 * 1000) % 60;
		long hours = diff / (60 * 60 * 1000) % 24;
		long days = diff / (24 * 60 * 60 * 1000);

		DateFormat format = new SimpleDateFormat("MMM dd");
		format.setTimeZone(TimeZone.getTimeZone(timeZone));
		String recivedate = format.format(d1);
		// System.out.println(days+"\n"+hours+"\n"+minutes);

		if (hours < 1 && days == 0)
			return recivedate + " (" + minutes + " minutes ago)";
		if (hours >= 1 && days == 0)
			return recivedate + " (" + hours + " hours ago)";
		if (days > 0 && days < 15)
			return recivedate + " (" + (days + 1) + " days ago)";

		DateFormat formats = new SimpleDateFormat(userFormat);
		formats.setTimeZone(TimeZone.getTimeZone(timeZone));
		String reciveOlddate = formats.format(d1);
		return reciveOlddate;
	}

	public static String dblToStrWithSeparator(double value, String decimalSeparator, String groupingSeparator,
			int decimals) {
		String hash = "####################";
		String formatedValue = "";
		String formatString = decimals == 0 ? "####" : "####." + hash.substring(0, decimals);

		Locale currentLocale = Locale.getDefault();
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);

		if (decimalSeparator != null && !decimalSeparator.isEmpty())
			otherSymbols.setDecimalSeparator(decimalSeparator.charAt(0));

		if (groupingSeparator != null && !groupingSeparator.isEmpty())
			otherSymbols.setGroupingSeparator(groupingSeparator.charAt(0));

		DecimalFormat df = new DecimalFormat(formatString, otherSymbols);
		df.setGroupingSize(3);
		df.setGroupingUsed(true);
		if (groupingSeparator == null || groupingSeparator.isEmpty())
			df.setGroupingUsed(false);

		formatedValue = df.format(value);
		return formatedValue;
	}

	public static Date[] getMinMaxTimeScaleDate(Date startDate, Date endDate) {

		long days = daysDifference(startDate, endDate);
		if (days < 8) {
			startDate = getAddDaysStartTime(startDate, 0);
			endDate = getAddDaysEndTime(startDate, 6);
		} else if (days > 7 && days < 31) {

			startDate = getAddDaysStartTime(startDate, 0);
			endDate = getAddDaysEndTime(startDate, 30);

		} else if (days > 30 && days < 365) {
			startDate = getFirstDayOfMonth(startDate);
			endDate = getAddDaysEndTime(startDate, 365);
		} else {
			startDate = getFirstDayOfYear(startDate);
			endDate = getLastDayOfYear(endDate);
		}

		return new Date[] { startDate, endDate };

	}

	public static Date getFirstDayOfYear(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getLastDayOfYear(Date date) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTime(date);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DATE, 31);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	public static String getKanbanItemColor(String color) {
		String str = "";

		if (color == null) {
			str = "kancolor2";
			return str;
		} else {
			switch (color) {
			case "#98eb9b":
				str = "kancolor1";
				break;
			case "#9dc4d4":
				str = "kancolor2";
				break;
			case "#d3889d":
				str = "kancolor3";
				break;
			case "#dfe9ff":
				str = "kancolor4";
				break;
			case "#f7d159":
				str = "kancolor5";
				break;
			case "#fff1a8":
				str = "kancolor6";
				break;

			default:
				str = "kancolor2";
			}

			return str;
		}
	}

	public static String randomLicenceCodeGenerator(int len) {
		StringBuilder sb = new StringBuilder(len);
		String st = "";
		for (int i = 0; i < len; i++)
			sb.append(LICENCEPATTERN.charAt(RND.nextInt(LICENCEPATTERN.length())));

		for (int i = 0; i < sb.length(); i++) {
			if (i % 5 == 0 && i > 0) {
				st += '-';
			}
			st += sb.charAt(i);
		}

		return st;
	}

	public static String randomPurchaseOrderGenerator() {
		Random rnd1 = new Random();
		int number1 = rnd1.nextInt(999999);
		Random rnd2 = new Random();
		int number2 = rnd2.nextInt(9999999);

		return number1 + "." + number2;
	}

	public static boolean isOnTrial(Date expDate) {
		if (expDate.compareTo(new Date()) == -1) {
			return false;
		}
		return true;
	}

	public static Date getTrialEndDate(Date regDate) {
		return Utils.addDaysStartTime(regDate, JibbyConfig.trialLength);
	}

	public static String getFilterUrl(String url) {
		String lastChar = String.valueOf(url.charAt(url.length() - 1));
		if (!lastChar.equals("/"))
			url = url + "/";
		return url;
	}

	public static String maskNumber(String cardNumber, String mask) {

		// format the number
		int index = 0;
		StringBuilder maskedNumber = new StringBuilder();
		for (int i = 0; i < mask.length(); i++) {
			char c = mask.charAt(i);
			if (c == '#') {
				maskedNumber.append(cardNumber.charAt(index));
				index++;
			} else if (c == 'x') {
				maskedNumber.append(c);
				index++;
			} else {
				maskedNumber.append(c);
			}
		}

		// return the masked number
		return maskedNumber.toString();
	}

	public static String transformURLIntoLinks(String text) {
		String urlValidationRegex = "\\b((https?|ftp):\\/\\/)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[A-Za-z]{2,6}\\b(\\/[-a-zA-Z0-9@:%_\\+.~#?&//=]*)*(?:\\/|\\b)";

		Pattern p = Pattern.compile(urlValidationRegex);
		Matcher m = p.matcher(text);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String found = m.group(0);
			m.appendReplacement(sb, "<a href='" + found + "'>" + found + "</a>");
		}
		m.appendTail(sb);
		return sb.toString();
	}

	public static String getUrl() {
		HttpServletRequest httpServletRequest = ((VaadinServletRequest) VaadinService.getCurrentRequest())
				.getHttpServletRequest();
		String requestUrl = httpServletRequest.getRequestURL().toString();
		return requestUrl;
	}

	private static Client createSSLClient() {
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			@Override
			public boolean isTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				return true;
			}
		};

		SSLContext sslContext = null;
		try {
			sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return ClientBuilder.newBuilder().sslContext(sslContext)
				.hostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();
	}
}
