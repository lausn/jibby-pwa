package com.uppstreymi.jibby.utils;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;

public class JibbyConfirmDialog extends Dialog {

	public DialogHtml dialoghtm= new DialogHtml();
	Div main=new Div();
	
	public JibbyConfirmDialog(String dialog) {
		main.add(dialoghtm);
		dialoghtm.setDialog(dialog);
		this.add(main);
		this.setHeight("150px");
		this.getElement().getStyle().set("padding", "20px");
		
		this.dialoghtm.getDialogCancel().addClickListener(e->{
			this.close();
		});
		
	}
}
