package com.uppstreymi.jibby.customcomponent;

import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

/**
 * A Designer generated component for the title-page.html template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("title-page")
@JsModule("./src/views/customcomponents/title-page.js")
public class TitlePage extends PolymerTemplate<TitlePage.TitlePageModel> {

    /**
     * Creates a new TitlePage.
     */
    public TitlePage() {
        // You can initialise any data required for the connected UI components here.
    }
    
    public TitlePageModel getTitleModel() {
    	return getModel();
    }

    /**
     * This model binds properties between TitlePage and title-page.html
     */
    public interface TitlePageModel extends TemplateModel {
        // Add setters and getters for template properties here.
    	void setTitlename(String str);
    }
}
