package com.uppstreymi.jibby.customcomponent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.ProjectLoadEvent;
import com.uppstreymi.jibby.item.events.FilterChangeEvent;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.componentfactory.EnhancedDatePicker;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("option-html")
@JsModule("./src/views/customcomponents/option-html.js")
@UIScoped
public class OptionHtml extends PolymerTemplate<OptionHtml.OptionHtmlModel> {

	@Inject private ProjectViewModel model;
	@Inject private SessionModel session;
	@Inject private Event<ProjectLoadEvent> projectLoadEvent;
	@Inject private Event<FilterChangeEvent> filterChangeEvent;
	@Inject private DbService dbService;

	RadioButtonGroup<String> statusrdo = new RadioButtonGroup<>();
	RadioButtonGroup<String> startrdo = new RadioButtonGroup<>();
	RadioButtonGroup<String> finishrdo = new RadioButtonGroup<>();
	RadioButtonGroup<String> publicrdo = new RadioButtonGroup<>();
	@Id("dtStart")
	private EnhancedDatePicker dtStart;

	@Id("dtFinish")
	private EnhancedDatePicker dtFinish;

	@Id("cbReader")
	private Checkbox cbReader;

	@Id("cbOwner")
	private Checkbox cbOwner;

	@Id("cbEditor")
	private Checkbox cbEditor;

	@Id("cbAllRoles")
	private Checkbox cbAllRoles;
	@Id("statusWrapper")
	private Div statusWrapper;
	@Id("startWrapper")
	private Div startWrapper;
	@Id("finishWrapper")
	private Div finishWrapper;
	@Id("publicWrapper")
	private Div publicWrapper;
	@Id("btnCancel")
	private Button btnCancel;
	@Id("btnSave")
	private Button btnSave;
	
	private Checkbox onHold = new Checkbox("On Hold");

	
	public OptionHtml() {
		
		onHold.getElement().getStyle().set("display", "block");
		onHold.getElement().getStyle().set("font-size", "16px");
		onHold.getElement().getStyle().set("margin-bottom", "5px");
				
		List<String> statsitems=new ArrayList<>();
		statsitems.add("0");
		statsitems.add("2");
		statsitems.add("4");
		List<String> startitems=new ArrayList<>();
		startitems.add("0");
		startitems.add("-1");
		startitems.add("1");
		List<String> publicitems=new ArrayList<>();
		publicitems.add("0");
		publicitems.add("1");
		statusrdo.setItems(statsitems);;
		startrdo.setItems(startitems);
		finishrdo.setItems(startitems);
		publicrdo.setItems(publicitems);

		List<String> statusCaption=new ArrayList<>();
		statusCaption.add("All");
		statusCaption.add("Not Done");
		statusCaption.add("Done");
		List<String> startCaption=new ArrayList<>();
		startCaption.add("All");
		startCaption.add("Before");
		startCaption.add("After");

		List<String> publicCaption=new ArrayList<>();
		publicCaption.add("Private");
		publicCaption.add("Public");

		statusrdo.setRenderer(new TextRenderer<String>(s->{return statusCaption.get(statsitems.indexOf(s));}));
		startrdo.setRenderer(new TextRenderer<String>(ss->{return startCaption.get(startitems.indexOf(ss));}));
		finishrdo.setRenderer(new TextRenderer<String>(fs->{return startCaption.get(startitems.indexOf(fs));}));
		publicrdo.setRenderer(new TextRenderer<String>(p->{return publicCaption.get(publicitems.indexOf(p));}));

		statusWrapper.add(statusrdo, onHold);
		startWrapper.add(startrdo);
		finishWrapper.add(finishrdo);
		publicWrapper.add(publicrdo);

	}

	@PostConstruct
	public void init() {
				
		cbAllRoles.addValueChangeListener(e->{
			if(cbAllRoles.getValue()) {
				cbEditor.setValue(true);
				cbOwner.setValue(true);
				cbReader.setValue(true);
			}
		});
		
		cbEditor.addClickListener(e -> {
			cbAllRoles.setValue((cbEditor.getValue() && cbOwner.getValue() && cbReader.getValue()));
		});
		
		cbOwner.addClickListener(e -> {
			cbAllRoles.setValue((cbEditor.getValue() && cbOwner.getValue() && cbReader.getValue()));
		});
		
		cbReader.addClickListener(e -> {
			cbAllRoles.setValue((cbEditor.getValue() && cbOwner.getValue() && cbReader.getValue()));
		});

		btnCancel.addClickListener(e->{
			filterChangeEvent.fire(new FilterChangeEvent(true));
		});

		btnSave.addClickListener(e->{
			filterChangeEvent.fire(new FilterChangeEvent(true,false,true));
		});
	}

	
	public void filterItemList(@Observes FilterChangeEvent filter) {
		
		if(filter.isOptionChange())
			filterOption();
		
		if(filter.isSetFilter())
			setOption();
	}

	public void loadProjectFirstTime(@Observes ProjectLoadEvent projectevent){
		if(projectevent.isLoadSettings()) {
			setOption();
		}
	}

	private void setOption() {

		ActiveViewDTO activeView=session.getActiveViewDTO();
		List<Integer> status=new ArrayList<Integer>();

		// status set
		if(activeView.getColAOptions().getItemStatus()!=null){
			if(activeView.getColAOptions().getItemStatus().equals(0)){ // For All status
				status.add(2);
				status.add(4);
			}else{
				status.add(activeView.getColAOptions().getItemStatus());
			}
		}else {
			status.add(2);
		}
		
		List<Integer> userViewType=new ArrayList<Integer>();
		if(activeView.getColAOptions().getRules()!=null) {
			userViewType=activeView.getColAOptions().getRules();
		}else {
			userViewType.add(1);
			userViewType.add(2);
			userViewType.add(3);
		}

		cbOwner.setValue(userViewType.contains(1));
		cbEditor.setValue(userViewType.contains(2));
		cbReader.setValue(userViewType.contains(3));
		cbAllRoles.setValue((cbEditor.getValue() && cbOwner.getValue() && cbReader.getValue()));
		
		statusrdo.setValue(status.size()>1?"0":status.get(0)+"");
		publicrdo.setValue(activeView.getColAOptions().getProjectType()==null?"0":activeView.getColAOptions().getProjectType()+"");
		startrdo.setValue(activeView.getColAOptions().getStartType()==null?"0":activeView.getColAOptions().getStartType()+"");
		finishrdo.setValue(activeView.getColAOptions().getFinishType()==null?"0":activeView.getColAOptions().getFinishType()+"");
		dtStart.setValue(activeView.getColAOptions().getStartDate()==null?JibbyConfig.estimateDate:Utils.dateToLocalDate((Date)activeView.getColAOptions().getStartDate()));
		dtFinish.setValue(activeView.getColAOptions().getFinishDate()==null?JibbyConfig.estimateDate:Utils.dateToLocalDate((Date)activeView.getColAOptions().getFinishDate()));
		
		if(activeView.getColAOptions().getOnHold() !=null){
			onHold.setValue(activeView.getColAOptions().getOnHold()==1?true:false);
			model.setOnHold(activeView.getColAOptions().getOnHold());
		}else{
			onHold.setValue(false);
			session.getActiveViewDTO().getColAOptions().setOnHold(0);
			model.setOnHold(0);
			saveActiveUI();
		}
		
		if(userViewType.size()==0)
			userViewType.add(-1);// not any selected userViewTypes 
		
		model.setStatus(status);
		model.setUserRole(userViewType);
		model.setStartType(Integer.parseInt(startrdo.getValue()));
		model.setFinishType(Integer.parseInt(finishrdo.getValue()));
		model.setStartDate(dtStart.getValue());
		model.setFinishDate(dtFinish.getValue());
		model.setIsPublic(Integer.parseInt(publicrdo.getValue()));
	}

	private void filterOption() {
				
		List<Integer> userrole=new ArrayList<Integer>();
		if(cbOwner.getValue())
			userrole.add(1);
		if(cbEditor.getValue())
			userrole.add(2);
		if(cbReader.getValue())
			userrole.add(3);

		if(userrole.size()==0)
			userrole.add(-1);// not any selected userViewTypes 

		// status
		List<Integer> status=new ArrayList<Integer>();
		if(statusrdo.getValue()==null)
			status.add(2);
		else if(statusrdo.getValue().equals("0")){ // For All status
			status.add(2);
			status.add(4);
		}else{
			status.add(Integer.parseInt(statusrdo.getValue()));
		}

		model.setUserRole(userrole);
		model.setStatus(status);
		model.setStartType(Integer.parseInt(startrdo.getValue()==null?"0":startrdo.getValue()));
		model.setFinishType(Integer.parseInt(finishrdo.getValue()==null?"0":finishrdo.getValue()));
		model.setStartDate(dtStart.getValue()==null?JibbyConfig.estimateDate:dtStart.getValue());
		model.setFinishDate(dtFinish.getValue()==null?JibbyConfig.estimateDate:dtFinish.getValue());
		model.setIsPublic(Integer.parseInt(publicrdo.getValue()==null?"0":publicrdo.getValue()));
		model.setOnHold(onHold.getValue() ? 1 : 0);

		List<Integer> userrolestype=new ArrayList<Integer>();
		userrolestype.addAll(userrole);
		userrolestype.removeIf(a->a.equals(-1));
		
		session.getActiveViewDTO().getColAOptions().setFinishDate(Utils.localDateToDate(model.getFinishDate()));
		session.getActiveViewDTO().getColAOptions().setStartDate(Utils.localDateToDate(model.getStartDate()));
		session.getActiveViewDTO().getColAOptions().setStartType(model.getStartType());
		session.getActiveViewDTO().getColAOptions().setFinishType(model.getFinsihType());
		session.getActiveViewDTO().getColAOptions().setRules(userrolestype);
		session.getActiveViewDTO().getColAOptions().setItemStatus(Integer.parseInt(statusrdo.getValue()));
		session.getActiveViewDTO().getColAOptions().setProjectType(model.isPublic());
		session.getActiveViewDTO().getColAOptions().setOnHold(model.getOnHold());

		ProjectLoadEvent projectEvent = new ProjectLoadEvent(false);
		projectEvent.setFiltering(true);
		projectLoadEvent.fire(projectEvent);

	}
	
	private void saveActiveUI(){
		String json = Utils.jsonBuilder(session.getActiveViewDTO());
		if(json !=null){
			session.getUser().setActiveUI(json);
			dbService.saveUser(session.getUser());
		}
	}

	public interface OptionHtmlModel extends TemplateModel {

	}

	public void setup() {
		dtStart.setPattern("yyyy-MM-dd");
		dtFinish.setPattern("yyyy-MM-dd");
	}
}
