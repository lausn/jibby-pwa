package com.uppstreymi.jibby.customcomponent;

import java.util.Random;

import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.item.ItemDetails;
import com.uppstreymi.jibby.item.ItemPage;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * A Designer generated component for the jibby-item.html template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("jibby-item")
@JsModule("./src/views/customcomponents/jibby-item.js")
public class JibbyItem extends PolymerTemplate<JibbyItem.JibbyItemModel>  {
//
//	@Id("nextbtn")
//	private PaperIconButton nextbtn;

	@Id("itemname")
	private Button itemName;


	/**
	 * Creates a new JibbyItem.
	 */
	public JibbyItem(ItemBean itemBean) {
		// You can initialise any data required for the connected UI components here.
		/*JBEntity entity=itemBean.getEntity();
		itemName.setText(entity.getName());
		Image icon=new Image("frontend/images/itemtype/"+entity.getEntityType()+".png","");
		
		itemName.setIcon(icon);
		if(!itemBean.isChild())
			nextbtn.setVisible(false);

		nextbtn.getElement().addEventListener("click", e->{
			System.out.println("click");
			nextbtn.getUI().ifPresent(ee->ee.navigate(ItemPage.class,entity.getId()));
		});
		itemName.getElement().addEventListener("click", ne->{
			itemName.getUI().ifPresent(nameevent->nameevent.navigate(ItemDetails.class,entity.getId()));
		});
		
		});*/

	}


	/**
	 * This model binds properties between JibbyItem and jibby-item.html
	 */
	public interface JibbyItemModel extends TemplateModel {
		// Add setters and getters for template properties here.
	}
}
