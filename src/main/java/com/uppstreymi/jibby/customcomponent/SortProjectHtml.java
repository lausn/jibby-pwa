package com.uppstreymi.jibby.customcomponent;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.uppstreymi.jibby.ProjectLoadEvent;
import com.uppstreymi.jibby.bean.SortCriteria;
import com.uppstreymi.jibby.item.events.FilterChangeEvent;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("sort-project-html")
@JsModule("./src/views/customcomponents/sort-project-html.js")
@UIScoped
public class SortProjectHtml extends PolymerTemplate<SortProjectHtml.SortProjectHtmlModel> {

	@Inject private ProjectViewModel model;
	@Inject private SessionModel session;
	@Inject private Event<ProjectLoadEvent> projectLoadEvent;
	@Inject private Event<FilterChangeEvent> filterChangeEvent;
	List<SortCriteria> list = new ArrayList<>();


	RadioButtonGroup<SortCriteria> rdSort = new RadioButtonGroup<>();
	@Id("sortwrapper")
	private Div sortwrapper;
	@Id("btnCancel")
	private Button btnCancel;

	boolean isload=true;
	
    public SortProjectHtml() {
    	rdSort.addClassName("sortRadio");
		int i=1;
		for(Object[] sort:JibbyConfig.sortCaption){
			SortCriteria sc= new SortCriteria();
			sc.setId(i);
			sc.setCaption(String.valueOf(sort[0]));
			sc.setCriteria(String.valueOf(sort[1]));
			sc.setAsc((Boolean)sort[2]);

			i++;
			list.add(sc);
		}

		rdSort.setItems(list);
		rdSort.setRenderer(new TextRenderer<>(SortCriteria::getCaption));
		sortwrapper.add(rdSort);
    }
    
    @PostConstruct
	public void init() {

    	btnCancel.addClickListener(e->{
			filterChangeEvent.fire(new FilterChangeEvent(true));
		});

		rdSort.addValueChangeListener(e->{
			if(!isload) {
				model.setProjectSortOption(e.getValue());
				session.getActiveViewDTO().setProjectSortOption(list.indexOf(e.getValue())+1);
				ProjectLoadEvent pe=new ProjectLoadEvent(false);
				pe.setSorting(true);
				projectLoadEvent.fire(pe);
			}
		});
	}
    
 // Reload Call
 	public void filterItemList(@Observes FilterChangeEvent filter) {
 		// TODO Auto-generated method stub
 		if(filter.isSetFilter())
 			setOption();
 	}
 	
 	// First time call
 	public void loadProjectFirstTime(@Observes ProjectLoadEvent projectevent){
 		if(projectevent.isLoadSettings()) {
 			setOption();
 		}
 	}

 	private void setOption() {
 		isload=true;
 		rdSort.setValue(list.get((session.getActiveViewDTO().getProjectSortOption()-1)));
 		model.setProjectSortOption(rdSort.getValue());
 		isload=false;
 	}

    public interface SortProjectHtmlModel extends TemplateModel {
    }
}
