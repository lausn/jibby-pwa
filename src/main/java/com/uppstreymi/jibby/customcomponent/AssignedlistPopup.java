package com.uppstreymi.jibby.customcomponent;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("assignedlist-popup")
@JsModule("./src/views/customcomponents/assignedlist-popup.js")
@UIScoped
public class AssignedlistPopup extends PolymerTemplate<AssignedlistPopup.AssignedlistPopupModel> {


	@Inject private SessionModel session;
	@Inject private DbService service;
	@Inject private HistorModel historyModel;
	@Inject private AppModel appModel;

	@Id("assignedUserWrapper")
	private Div assignedUserWrapper;

	@Id("btnOk")
	private Button btnOk;

	private boolean isFromNewItem = false;


	private CheckboxGroup<JBUserEntity> checkgroup = new CheckboxGroup<JBUserEntity>();
	@Id("btnClose")
	private Button btnClose;
	
	@Inject
	SyncUserEvent userEvent;
	
	Logger logger=LogManager.getLogger(AssignedlistPopup.class);

	public AssignedlistPopup() {
		checkgroup.addClassName("userCheck");
		assignedUserWrapper.add(checkgroup);
	}

	@PostConstruct
	private void init() {
		/*checkgroup.setRenderer(new ComponentRenderer<>(e->{
			String name = "";
			if(e.getFirstName().isEmpty() && e.getLastName().isEmpty())
				name =  e.getEmail();
			else
				name = e.getFirstName()+" "+e.getLastName();

			Div lbl = new Div();
			lbl.getElement().setAttribute("style","width: 100%;display: inline-flex;");

			String html = "<div style='width: calc(100% - 30px);'>"+name+"</div><div><img class='' style='width:30px;height:30px;border-radius:100%;float:left;' alt='' src='"+ userPic(e.getId())+"'></div>";
			lbl.getElement().setProperty("innerHTML",html);

			return lbl;


		}));*/
		
		checkgroup.setItemLabelGenerator(e -> {
			return getUsernameAndPic(e);
		});
				
		btnOk.addClickListener(e->{
			closeWindow();
			if(!isFromNewItem) {
				if(getSelectedUsers().size()>0) {
					assignedToSelectedUser();
					Notification.show("Successfully assigned");
				}else {
					Notification.show("Select a user first");
				}
			}
		});
		
		btnClose.addClickListener(e->{
			closeWindow();
		});
	}
	
	private String getUsernameAndPic(JBUserEntity e) {
		String name = "";
		if(e.getFirstName().isEmpty() && e.getLastName().isEmpty()) 
			name =e.getEmail(); else name = e.getFirstName()+" "+e.getLastName();
		String html = "<div style='width: calc(100% - 30px);display: inline-block;font-size: 13px;'>"
				 +name+"</div><div style='display: inline-block;vertical-align: middle;'><img class='' style='width:30px;height:30px;border-radius:100%;float:left;' alt='' src='"
				 + userPic(e.getId())+"'></div>";
		return html;
	}

	private void closeWindow() {
		Dialog dialog = (Dialog)getParent().get();
    	dialog.close();
	}

	private void assignedToSelectedUser() {
		for(JBUserEntity user: getSelectedUsers()){
			Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1;
			JBActionlist actionlist = new JBActionlist();
			actionlist.setAddedby(session.getUser().getId());
			actionlist.setAssignedto(user.getId());
			actionlist.setItemid(historyModel.getSelectedEntity().getId());
			actionlist.setItemname("");
			actionlist.setDateadded(new Date());
			actionlist.setItemtype(itemtype);
			actionlist.setItemdatetime(null);

			String userName = session.getUser().getFirstName()+" "+session.getUser().getLastName();
			JBActionlistHistory actionlistHistory = new JBActionlistHistory();
			actionlistHistory.setCreated(new Date());
			actionlistHistory.setItemid(historyModel.getSelectedEntity().getId());
			actionlistHistory.setItemtype(itemtype);
			actionlistHistory.setUserid(user.getId());
			actionlistHistory.setMessage(userName+" assigned the task \""+historyModel.getSelectedEntity().getName()+"\" to you.");
			actionlistHistory.setMessagetype(Arrays.asList(JibbyConfig.actionlistMessageType).indexOf("assingedToUser")+1);
			actionlistHistory.setIsRead(JibbyConfig.messageRead[0]);

			service.saveActionlist(actionlist);
			actionlistHistory=service.saveActionlistHistory(actionlistHistory);

//			userEvent.addHistoryNotification(actionlistHistory);
//			userEvent.addToNotifyUserList(user.getId());
//			userEvent.setNotification(actionlistHistory);
//			userEvent.addToActionList(actionlist);
//			userEvent.setTopLebelProjectEntity(service.getEntity(newEntity.getParentProjectId()));

			insertItemModification(historyModel.getSelectedEntity().getId(),Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("assigntoother")+1,user.getId());
		}

	}

	public void loadAssigneduserListByEntity(Set<JBUserEntity> selectedUsers, Integer parentId) {
		List<JBUserEntity> assignedList = new ArrayList<JBUserEntity>();
		List<Integer> parentArray =  historyModel.getParentArray();

		if(!parentArray.contains(parentId))
			parentArray.add(parentId); // add the current parent

		List<JBUserRoleEntity> assignedUserList= service.getAssignedUserListByEntity(parentArray);
		for(JBUserRoleEntity userRole: assignedUserList){
			if(!assignedList.contains(userRole.getUser()) && !userRole.getUser().getId().equals(session.getUser().getId())){
				assignedList.add(userRole.getUser());
			}		
		}
		checkgroup.setItems(assignedList);
		checkgroup.getChildren().forEach(e -> {
			Checkbox checkBox = (Checkbox) e;
			checkBox.getElement().setAttribute("style", "padding-left: 10px;width:95% !important;");
			checkBox.setLabelAsHtml(checkBox.getLabel());
		});
		checkgroup.setValue(selectedUsers); // select the previous selected checkbox
	}

	public void loadAssigneduserListByParentArray() {
		List<JBUserEntity> assignedList = new ArrayList<JBUserEntity>();
		List<Integer> parentArray =  historyModel.getParentArray();

		List<JBUserRoleEntity> assignedUserList= service.getAssignedUserListByEntity(parentArray);
		for(JBUserRoleEntity userRole: assignedUserList){
			if(!assignedList.contains(userRole.getUser()) && !userRole.getUser().getId().equals(session.getUser().getId())){
				assignedList.add(userRole.getUser());
			}		
		}
		checkgroup.setItems(assignedList);
	}

	public interface AssignedlistPopupModel extends TemplateModel {

	}

	public Set<JBUserEntity> getSelectedUsers() {
		return checkgroup.getSelectedItems();
	}

	public void clear() {
		checkgroup.clear();
	}

	private String userPic(Integer userId){
		
		String url=Utils.getFilterUrl(appModel.getPropertiesModel().getJibbyAppUrl());
		
		try{
			File preFile = new File(Utils.userImagePath + userId);

			if(!preFile.exists())
				return "./images/avatar.png";
			else{
				return url+"jibby/usercontent/avatars/"+userId;
			}
		}catch (Exception e) {
			logger.error("Userimagepath exception thrown in assignedlist popup",e);
			return "./images/avatar.png";
		}
	}

	public void isFromNewItemCreate(boolean isFromNew) {
		isFromNewItem = isFromNew;
	}

	public void insertItemModification(Integer entityId,Integer type,Integer user2){
		JBItemModification itemModification=new JBItemModification();
		itemModification.setEntityId(entityId);
		itemModification.setUserId1(session.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setUserId2(user2);
		itemModification.setModificationDate(new Date());
		service.saveItemModification(itemModification);
	}
}
