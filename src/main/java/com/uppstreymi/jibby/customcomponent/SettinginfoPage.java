package com.uppstreymi.jibby.customcomponent;

import javax.annotation.PostConstruct;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("settinginfo-page")
@JsModule("./src/views/customcomponents/settinginfo-page.js")
@UIScoped
public class SettinginfoPage extends PolymerTemplate<SettinginfoPage.SettinginfoPageModel> {


	//@Inject SessionModel sessionModel;
	//@Inject private FiltersPage filterPage;

	Dialog window  = new Dialog();
	
	@Id("paperFilterWrapper")
	private Element paperFilterWrapper;

	public SettinginfoPage() {

	}

	@PostConstruct
	public void init() {
		//window.add(filterPage);
		//getModel().setSessionUserId(sessionModel.getUser().getId());
	}

	/*@ClientCallable
	private void menuChangedProcess(String id) {
		if(id.equals("btnProjectview")) {
			if(getModel().getProjectview())
				UI.getCurrent().getPage().getHistory().back();
		}else if(id.equals("btnMyAction")) {
			if(getModel().getMyitemview())
				getUI().ifPresent(e->{e.navigate(MyactionPage.class);});
		}else if(id.equals("btnFilter")) {
			window.open();
		}else if(id.equals("btnLogout")) {
			logout();
		}
	}

	public void closewindow(@Observes FilterChangeEvent fevent) {
		if(fevent.isCloseDialog())
			window.close();
	}

	public void setActiveView(@Observes SettingItemEvent myactionevent) {
		paperFilterWrapper.setVisible(true);
		paperFilterWrapper.setVisible(!myactionevent.isActionView()); //filter menu is not shown in my action page
			
		getModel().setProjectview(!myactionevent.isProjectView());
		getModel().setMyitemview(!myactionevent.isActionView());
	}

	public SettinginfoPageModel getSettingModel() {
		return getModel();
	}

	private void logout(){
		sessionModel.setUser(null);
		sessionModel.setActiveViewDTO(null);
		getUI().get().getSession().close();
		UI.getCurrent().getPage().executeJavaScript("location.assign('')");
	}*/

	/**
	 * This model binds properties between SettinginfoPage and settinginfo-page.html
	 */
	public interface SettinginfoPageModel extends TemplateModel {
		// Add setters and getters for template properties here.
		/*void setUsername(String str);
		void setProjectview(boolean view);
		boolean getProjectview();
		void setMyitemview(boolean view);
		boolean getMyitemview();
		
		void setUserId(String userid);*/
	}
}
