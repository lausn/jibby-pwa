package com.uppstreymi.jibby.customcomponent;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.uppstreymi.jibby.item.events.FilterChangeEvent;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("filters-page")
@JsModule("./src/views/customcomponents/filters-page.js")
@UIScoped
public class FiltersPage extends PolymerTemplate<FiltersPage.FiltersPageModel> {

	
	@Inject private OptionHtml optionHTML;
	@Inject private CategoriesHtml categoriesHTML;
	@Inject private SortHtml sortHTML;
	@Inject private SortProjectHtml sortProjectHTML;
	
	@Id("optionPage")
	private Div optionPage;

	@Id("categoriesPage")
	private Div categoriesPage;

	@Id("sortPage")
	private Div sortPage;
	
	@Id("sortProjectPage")
	private Div sortProjectPage;


	public FiltersPage() {
	}

	@PostConstruct
	private void init() {
		optionPage.add(optionHTML);
		categoriesPage.add(categoriesHTML);
		sortPage.add(sortHTML);
		sortProjectPage.add(sortProjectHTML);
	}
	
	
	
	public void filterItemList(@Observes FilterChangeEvent filter) {
		if(filter.isCloseDialog()) {
			closeWindow();
		}
	}
	
	private void closeWindow() {
		Dialog dialog = (Dialog)getParent().get();
    	dialog.close();
	}

	public interface FiltersPageModel extends TemplateModel {
		
	}

	public void setup() {
		optionHTML.setup();
	}
}
