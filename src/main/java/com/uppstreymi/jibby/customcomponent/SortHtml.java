package com.uppstreymi.jibby.customcomponent;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.uppstreymi.jibby.ProjectLoadEvent;
import com.uppstreymi.jibby.bean.SortCriteria;
import com.uppstreymi.jibby.item.events.FilterChangeEvent;
import com.uppstreymi.jibby.item.events.ItemSortEvent;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("sort-html")
@JsModule("./src/views/customcomponents/sort-html.js")
@UIScoped
public class SortHtml extends PolymerTemplate<SortHtml.SortHtmlModel> {

	@Inject private ProjectViewModel model;
	@Inject private SessionModel session;
	@Inject private Event<ItemSortEvent> itemSortEvent;
	@Inject private Event<FilterChangeEvent> filterChangeEvent;
	List<SortCriteria> list = new ArrayList<>();


	RadioButtonGroup<SortCriteria> rdSort = new RadioButtonGroup<>();
	@Id("sortwrapper")
	private Div sortwrapper;
	@Id("btnCancel")
	private Button btnCancel;

	boolean isload=true;
	/**
	 * Creates a new SortHtml.
	 */
	public SortHtml() {
		// You can initialise any data required for the connected UI components here.

		rdSort.addClassName("sortRadio");
		int i=1;
		for(Object[] sort:JibbyConfig.sortCaption){
			SortCriteria sc= new SortCriteria();
			sc.setId(i);
			sc.setCaption(String.valueOf(sort[0]));
			sc.setCriteria(String.valueOf(sort[1]));
			sc.setAsc((Boolean)sort[2]);

			i++;
			list.add(sc);
		}

		rdSort.setItems(list);
		rdSort.setRenderer(new TextRenderer<>(SortCriteria::getCaption));
		sortwrapper.add(rdSort);

	}

	@PostConstruct
	public void init() {

		btnCancel.addClickListener(e->{
			filterChangeEvent.fire(new FilterChangeEvent(true));
		});

		rdSort.addValueChangeListener(e->{
			if(!isload) {
				model.setSortOption(e.getValue());
				session.getActiveViewDTO().setSortOption(list.indexOf(e.getValue())+1);
				ItemSortEvent eventSortEvent=new ItemSortEvent(true);
				itemSortEvent.fire(eventSortEvent);
			}
		});


	}
	
	// Reload Call
	public void filterItemList(@Observes FilterChangeEvent filter) {
		if(filter.isSetFilter())
			setOption();
	}
	
	// First time call
	public void loadProjectFirstTime(@Observes ProjectLoadEvent projectevent){
		if(projectevent.isLoadSettings()) {
			setOption();
		}
	}

	private void setOption() {
		isload=true;
		rdSort.setValue(list.get((session.getActiveViewDTO().getSortOption()-1)));
		model.setSortOption(rdSort.getValue());
		isload=false;
	}

	public interface SortHtmlModel extends TemplateModel {

	}
}
