package com.uppstreymi.jibby.customcomponent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.NotificationBean;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.myaction.MyItemBean;
import com.uppstreymi.jibby.myaction.SelectedItemView;
import com.uppstreymi.jibby.service.DbService;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("notification-view-popup")
@JsModule("./src/views/customcomponents/notification-view-popup.js")
@UIScoped
public class NotificationViewPopup extends PolymerTemplate<NotificationViewPopup.NotificationViewPopupModel> {

	@Id("btnBackNotiDialog")
	private Button btnBackNotiDialog;

	@Inject
	DbService dbservice;
	@Inject
	private SessionModel session;
	@Inject SelectedItemView selectedItemView;

	private SimpleDateFormat dateformat;
	Logger logger=LogManager.getLogger(NotificationViewPopup.class);

	public NotificationViewPopup() {

	}

	@PostConstruct
	private void init() {
		btnBackNotiDialog.addClickListener(e -> {
			Dialog parent = (Dialog) this.getParent().get();
			parent.close();
		});
	}

	public interface NotificationViewPopupModel extends TemplateModel {
		void setNotificationItems(String items);

		void setNotificationChecklists(String checklists);
	}

	public void loadData(JBUserEntity user) {
		dateformat = new SimpleDateFormat(user.getDateFormat());
		dateformat.setTimeZone(TimeZone.getTimeZone(user.getTimeZone()));
		List<Object[]> notificationsItems = dbservice.getNotificationOfItem(user.getId(), 1,
				session.getCustomer().getId());
		List<Object[]> notificationsChecklist = dbservice.getNotificationOfChecklist(user.getId(), 2,
				session.getCustomer().getId());
		getModel().setNotificationItems(buildJsonForItem(notificationsItems));

		getModel().setNotificationChecklists(buildJsonForCheck(notificationsChecklist));
	}

	private String buildJsonForItem(List<Object[]> items) {

		List<NotificationBean> notificationBeans = new ArrayList<>();

		for (Object[] item : items) {
			NotificationBean bean = new NotificationBean();
			bean.setId(Integer.parseInt(item[0] + ""));
			bean.setCreated(dateformat.format(item[1]));
			bean.setItemid(Integer.parseInt(item[2] + ""));
			bean.setItemtype(Integer.parseInt(item[3] + ""));
			bean.setUserid(Integer.parseInt(item[4] + ""));
			bean.setMessagetype(Integer.parseInt(item[5] + ""));
			bean.setMessage(item[6] + "");
			bean.setToplabelid(Integer.parseInt(item[7] + ""));
			bean.setToplabelname(item[8] + "");
			bean.setToplabeltype(Integer.parseInt(item[9] + ""));

			notificationBeans.add(bean);
		}

		ObjectMapper map = new ObjectMapper();
		try {
			String json = map.writeValueAsString(notificationBeans);
			return json;
		} catch (JsonProcessingException e) {
			logger.error("buildJsonForItem exception in notificationviewpopup",e);
		}
		return "{}";
	}

	private String buildJsonForCheck(List<Object[]> items) {

		List<NotificationBean> notificationBeans = new ArrayList<>();

		for (Object[] item : items) {
			NotificationBean bean = new NotificationBean();
			bean.setId(Integer.parseInt(item[0] + ""));
			bean.setCreated(dateformat.format(item[1]));
			bean.setItemid(Integer.parseInt(item[2] + ""));
			bean.setItemtype(Integer.parseInt(item[3] + ""));
			bean.setUserid(Integer.parseInt(item[4] + ""));
			bean.setMessagetype(Integer.parseInt(item[5] + ""));
			bean.setMessage(item[6] + "");
			bean.setToplabelid(Integer.parseInt(item[7] + ""));
			bean.setEntity(Integer.parseInt(item[8] + ""));
			bean.setToplabelname(item[9] + "");
			bean.setToplabeltype(Integer.parseInt(item[10] + ""));

			notificationBeans.add(bean);
		}

		ObjectMapper map = new ObjectMapper();
		try {
			String json = map.writeValueAsString(notificationBeans);
			return json;
		} catch (JsonProcessingException e) {
			logger.error("buildJsonForCheck exception in notificationviewpopup",e);
		}
		return "{}";
	}
	
	@ClientCallable
	private void _detailsItemClick(String items) {
		try{
			ObjectMapper objectMapper=new ObjectMapper();
			NotificationBean notificatiobBean=objectMapper.readValue(items,NotificationBean.class);
			Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(selectedItemView);
			selectedItemView.setup(notificatiobBean.getItemid(),"Notification Item");
			dialog.open();
			
		}catch(Exception e){
			logger.error("Objectmapping exception on detailsitemclick in notificationviewpopup",e);

		}
	}
}
