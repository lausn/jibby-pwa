package com.uppstreymi.jibby.customcomponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;


import com.uppstreymi.jibby.ProjectLoadEvent;
import com.uppstreymi.jibby.item.events.FilterChangeEvent;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * A Designer generated component for the categories-html.html template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("categories-html")
@JsModule("./src/views/customcomponents/categories-html.js")
@UIScoped
public class CategoriesHtml extends PolymerTemplate<CategoriesHtml.CategoriesHtmlModel> {

	@Inject private ProjectViewModel model;
	@Inject private SessionModel session;
	@Inject private Event<ProjectLoadEvent> projectLoadEvent;
	@Inject private Event<FilterChangeEvent> filterChangeEvent;

	CheckboxGroup<String> ckGroup=new CheckboxGroup<>();


	@Id("btnCancel")
	private Button btnCancel;
	@Id("btnSave")
	private Button btnSave;
	@Id("typeWrapper")
	private Div typeWrapper;


	/**
	 * Creates a new CategoriesHtml.
	 */
	public CategoriesHtml() {
		// You can initialise any data required for the connected UI components here.
		Set<String> selectitem = new HashSet();
		for(String caption:JibbyConfig.entityTypes) {
			selectitem.add(caption);
		}
		typeWrapper.add(ckGroup);
		ckGroup.addClassName("typesCheckbox");
		ckGroup.setItems(JibbyConfig.entityTypes);
		ckGroup.setValue(selectitem);
	}

	@PostConstruct
	public void init() {
		btnCancel.addClickListener(e->{
			filterChangeEvent.fire(new FilterChangeEvent(true));
		});

		btnSave.addClickListener(e->{
			filterChangeEvent.fire(new FilterChangeEvent(false,true,true));
		});

	}

	public void filterItemList(@Observes FilterChangeEvent filter) {
		// TODO Auto-generated method stub
		if(filter.isCategorisChange())
			filterCategories();

		if(filter.isSetFilter())
			setOption();
	}

	private void setOption() {

		ckGroup.deselectAll();
		Set<String> selectitem = new HashSet();
		if(session.getActiveViewDTO().getItemTypes()!=null){
			List<Integer> types=session.getActiveViewDTO().getItemTypes();;
			for(String caption:JibbyConfig.entityTypes){
				if(types.contains(Arrays.asList(JibbyConfig.entityTypes).indexOf(caption)+1))
					selectitem.add(caption);
			}
			model.setTypes(types);
		}else{
			List<Integer> types=new ArrayList<Integer>();
			for(String caption:ckGroup.getSelectedItems()){
				types.add(Arrays.asList(JibbyConfig.entityTypes).indexOf(caption)+1);
			}
			session.getActiveViewDTO().setItemTypes(types);
			model.setTypes(types);
		}
		ckGroup.setValue(selectitem);

	}

	// First time call
	public void loadProjectFirstTime(@Observes ProjectLoadEvent projectevent){
		if(projectevent.isLoadSettings()) {
			setOption();
		}
	}

	public void filterCategories() {
		//Set here model data
		List<Integer> types=new ArrayList<Integer>();
		for(String caption:ckGroup.getSelectedItems()){
			types.add(Arrays.asList(JibbyConfig.entityTypes).indexOf(caption)+1);
		}
		session.getActiveViewDTO().setItemTypes(types);
		model.setTypes(types);
		
		ProjectLoadEvent projectEvent = new ProjectLoadEvent(false);
		projectEvent.setFiltering(true);
		projectLoadEvent.fire(projectEvent);
	}

	/**
	 * This model binds properties between CategoriesHtml and categories-html.html
	 */
	public interface CategoriesHtmlModel extends TemplateModel {
		// Add setters and getters for template properties here.
	}
}
