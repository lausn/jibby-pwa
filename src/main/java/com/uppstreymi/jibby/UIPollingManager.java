package com.uppstreymi.jibby;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.UI;


@UIScoped
public class UIPollingManager
{

	private Map<UI, Map<Object, Integer>> pollRequests;

	public UIPollingManager()
	{
		pollRequests = new WeakHashMap<>(); // Let's use weak references in case someone forgets to unregister properly
	}

	/**
	 * Registers a poll request for the given UI. Sets the pollInterval of this UI to the lowest registered interval.
	 * @param ui
	 * @param requestor
	 * @param pollIntervalInMillis poll interval in milliseconds
	 */
	public void registerPollRequest(UI ui, Object requestor, int pollIntervalInMillis)
	{
		Map<Object, Integer> uiRequests = pollRequests.get(ui);
		if (uiRequests == null)
		{
			uiRequests = new HashMap<>();
			pollRequests.put(ui, uiRequests);
		}

		uiRequests.put(requestor, pollIntervalInMillis);

		setPollInterval(ui);
	}

	/**
	 * Removes a poll request for the given UI (if existent). Sets the pollInterval of this UI to the lowest registered interval
	 * remaining or -1 if no more requests exist for the UI
	 * @param ui
	 * @param requestor
	 */
	public void unregisterPollRequest(UI ui, Object requestor)
	{
		Map<Object, Integer> uiRequests = pollRequests.get(ui);
		if (uiRequests != null)
		{
			uiRequests.remove(requestor);

			// Remove the UI from our map if no requests exist anymore
			if (uiRequests.size() <= 0) pollRequests.remove(ui);
		}

		setPollInterval(ui);
	}

	/**
	 * Removes all poll requests of the given UI and sets the pollInterval to -1
	 * @param ui
	 */
	public void unregisterAllPollRequests(UI ui)
	{
		pollRequests.remove(ui);

		ui.setPollInterval(-1);
	}

	/**
	 * Sets the pollInterval of the given UI to the lowest registered interval time of this UI
	 * @param ui
	 */

	private void setPollInterval(UI ui)
    {
        Map<Object, Integer> uiRequests = pollRequests.get(ui);
        if (uiRequests != null)
        {
            ui.setPollInterval(getLowestNumber(uiRequests.values()));
        }else{
            ui.setPollInterval(-1);
        }
    }

	/**
	 * Returns the lowest number of a given Integer-Collection. Returns -1 if no valid Integer is included in the collection.
	 * @param intervalArray
	 * @return
	 */
	private Integer getLowestNumber(Collection<Integer> intervalArray)
	{
		Integer lowestNum = null;

		for (Integer i : intervalArray)
		{
			if (i != null && ( lowestNum == null || i < lowestNum )) lowestNum = i;
		}

		if (lowestNum == null) return -1;
		else
			return lowestNum;
	}
}