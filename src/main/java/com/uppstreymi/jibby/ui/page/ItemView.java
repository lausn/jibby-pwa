package com.uppstreymi.jibby.ui.page;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.item.ItemPage;
import com.uppstreymi.jibby.ui.MainLayout;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;

@Route(value ="item", layout = MainLayout.class)
@UIScoped
public class ItemView extends ViewFrame implements HasUrlParameter<Integer>{

	@Inject
	ItemPage itemView;
	
	Logger logger=LogManager.getLogger(ItemView.class);
	
	public ItemView(){
		
	}
	
	@PostConstruct
	public void init(){
		logger.info("item page init!");
		setViewContent(itemView);
	}

	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter Integer parameter) {
		itemView.setParameter(event, parameter) ;
	}
}
