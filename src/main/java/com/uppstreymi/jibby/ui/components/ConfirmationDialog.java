/*
 * Copyright 2000-2017 Vaadin Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uppstreymi.jibby.ui.components;

import java.io.Serializable;
import java.util.function.Consumer;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;


/**
 * A generic dialog for confirming or cancelling an action.
 *
 * @param <T>
 *            The type of the action's subject
 */
public class ConfirmationDialog<T extends Serializable>
        extends Dialog {

    private final H3 titleField = new H3();
    private final Div messageLabel = new Div();
    private final Div extraMessageLabel = new Div();
 
    private final Button confirmButton = new Button();
    private final Button cancelButton = new Button("Cancel");
    private Registration registrationForConfirm;
    private Registration registrationForCancel;
	Icon close = VaadinIcon.CLOSE.create();
	public void setCloseVisible(boolean isvisible) {
		this.close.setVisible(isvisible);
	}
    /**
     * Constructor.
     */
    public ConfirmationDialog() {
        setCloseOnEsc(false);
        setCloseOnOutsideClick(false);
        this.setId("my-dialog-styles");
        this.getElement().setAttribute("theme", "vaadin-dialog-overlay");

        confirmButton.addClickListener(e -> close());
        confirmButton.getElement().setAttribute("theme", "tertiary");
        confirmButton.getElement().setAttribute("style","float:right;");
        confirmButton.setAutofocus(true);
        cancelButton.addClickListener(e -> close());
        cancelButton.getElement().setAttribute("theme", "tertiary");
        cancelButton.getElement().setAttribute("style","float:right;");

        HorizontalLayout buttonBar = new HorizontalLayout(confirmButton,
                cancelButton);
        buttonBar.setClassName("buttons confirm-buttons");
        buttonBar.getElement().setAttribute("style", "width:100%; padding:0px !important; margin:0px !important;display:inline-block;");
        
        messageLabel.getElement().getStyle().set("font-size", "14px");
        
        Div labels = new Div(messageLabel, extraMessageLabel);
        labels.setClassName("confirm-text");
        labels.getElement().setAttribute("style","margin-bottom: 10px;max-width: 430px;margin-top: 8px;");
        
        titleField.setClassName("confirm-title");
        titleField.getElement().setAttribute("style","margin-top: 5px;max-width: 510px;min-width: 250px;display: inline-block;");
        
		close.setSize("15px");
		close.getStyle().set("float", "right");
		close.getStyle().set("margin-right", "3px");
		close.getStyle().set("margin-top", "8px");
		close.getStyle().set("fill", "black");
		
		close.addClickListener(e -> {
			close();
		});
        
        
        
        
        VerticalLayout main = new VerticalLayout();
        main.setSpacing(false);
        main.getElement().setAttribute("style", "padding:10px;");
        Div header = new Div();
        header.getElement().setAttribute("style", "width: 100%;border-bottom: 1px solid rgb(221, 221, 221);");
        header.add(titleField,close);
        
        main.add(header, labels, buttonBar);
        
        add(main);
    }

    /**
     * Opens the confirmation dialog.
     *
     * The dialog will display the given title and message(s), then call
     * <code>confirmHandler</code> if the Confirm button is clicked, or
     * <code>cancelHandler</code> if the Cancel button is clicked.
     *
     * @param title
     *            The title text
     * @param message
     *            Detail message (optional, may be empty)
     * @param additionalMessage
     *            Additional message (optional, may be empty)
     * @param actionName
     *            The action name to be shown on the Confirm button
     * @param isDisruptive
     *            True if the action is disruptive, such as deleting an item
     * @param item
     *            The subject of the action
     * @param confirmHandler
     *            The confirmation handler function
     * @param cancelHandler
     *            The cancellation handler function
     */
    public void open(String title, String message, String additionalMessage,
            String actionName,String cancelName, boolean isDisruptive, T item, Consumer<T> confirmHandler,
            Runnable cancelHandler) {
        titleField.setText(title);
        messageLabel.setText(message);
        extraMessageLabel.setText(additionalMessage);
        confirmButton.setText(actionName);
        cancelButton.setText(cancelName);
        
        confirmButton.setVisible(!actionName.isEmpty());

        if (registrationForConfirm != null) {
            registrationForConfirm.remove();
        }
        registrationForConfirm = confirmButton
                .addClickListener(e -> confirmHandler.accept(item));
        if (registrationForCancel != null) {
            registrationForCancel.remove();
        }
        registrationForCancel = cancelButton
                .addClickListener(e -> cancelHandler.run());

        if (isDisruptive) {
            confirmButton.getElement().setAttribute("theme", "tertiary error");
        }
        open();
    }
    
    public void setCancelButtonStyle(String style) {
    	cancelButton.getElement().setAttribute("style",style);
    }
    
    public void setConfirmButtonStyle(String style) {
    	confirmButton.getElement().setAttribute("style",style);
    }
    
    public void setTitleHtml(String html) {
    	titleField.getElement().setProperty("innerHTML",html);
    	titleField.getElement().setAttribute("style","max-width: 420px;margin-top: 5px;font-weight: 500;line-height: 30px;");
    }
}