package com.uppstreymi.jibby.ui.components.navigation.bar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.flow.helper.AsyncManager;

import com.uppstreymi.jibby.backend.sync.event.SyncEventType;
import com.uppstreymi.jibby.backend.sync.event.SyncUserEvent;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.DataChangeModelListener;
import com.uppstreymi.jibby.backend.sync.manager.SyncDataChangeModel.ModelUpdateEvent;
import com.uppstreymi.jibby.backend.sync.manager.SynchronizeManager;
import com.uppstreymi.jibby.backend.sync.process.ProcessData;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBChatMessageEntity;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.customcomponent.FiltersPage;
import com.uppstreymi.jibby.customcomponent.NotificationViewPopup;
import com.uppstreymi.jibby.item.ChatInvitationUserPopupPage;
import com.uppstreymi.jibby.item.DescriptionpopupPage;
import com.uppstreymi.jibby.item.MyactionlistpopupPage;
import com.uppstreymi.jibby.item.events.MyActionlistAssignedEvent;
import com.uppstreymi.jibby.item.events.SyncModelGenerateEvent;
import com.uppstreymi.jibby.item.events.TopLabelProjectEvent;
import com.uppstreymi.jibby.item.events.UserEvent;
import com.uppstreymi.jibby.model.EntityUserAccess;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.myaction.MyactionPage;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.support.JibbySupport;
import com.uppstreymi.jibby.ui.MainLayout;
import com.uppstreymi.jibby.ui.components.ConfirmationDialog;
import com.uppstreymi.jibby.ui.components.FlexBoxLayout;
import com.uppstreymi.jibby.ui.util.UIUtils;
import com.uppstreymi.jibby.user.UserSettingsPopupPage;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.shared.communication.PushMode;

@CssImport("./styles/components/app-bar.css")
@UIScoped
public class AppBar extends FlexBoxLayout implements DataChangeModelListener {

	private String CLASS_NAME = "app-bar";

	private FlexBoxLayout container;

	private Button menuIcon;
	private Button contextIcon;

	private Div title;
	private FlexBoxLayout actionItems;
	private Button settings;
	private Image profile = new Image();
	private Button btnEdit;
	private Button btnAssignItem;
	private Button btnDelete;
	private Button btnNotification;
	private Button btnInvite;
	private Div detailsLayout = new Div();
	private Div settingLayout = new Div();

	Dialog filterwindow = new Dialog();
	Dialog assigneToDialog = new Dialog();

	@Inject
	SessionModel sessionModel;
	@Inject
	SynchronizeManager syncManager;
	@Inject
	private FiltersPage filterPage;
	@Inject
	private HistorModel historyModel;
	@Inject
	private NotificationViewPopup notificationView;
	@Inject
	private JibbySupport supportView;
	@Inject
	private MyactionlistpopupPage myactionlistpopup;
	@Inject
	private DescriptionpopupPage descriptionpopupPage;
	@Inject
	private UserSettingsPopupPage userSettingsPopupPage;
	@Inject
	private ChatInvitationUserPopupPage chatInvitationUserPopupPage;
	@Inject
	private Event<MyActionlistAssignedEvent> myActionEvent;
	@Inject
	private ProjectViewModel projectModel;
	@Inject
	private EntityUserAccess entityUserAccess;
	@Inject
	private DbService dbService;
	@Inject
	SyncUserEvent userEvent;
	@Inject
	private Event<MyactionDataLoadEvent> myactionDataLoadEvent;
	@Inject
	private Event<TopLabelProjectEvent> topLabelProjectEvent;

	private Div notificationNumber;
	private Div notificationBar = new Div();
	private List<JBActionlistHistory> unreadNotification = new ArrayList<>();
	private List<Integer> children = new ArrayList<>();
	private SyncDataChangeModel dataChangemodel;
	MenuItem itemProjectView, itemMyAction;
	private UI ui;

	ResourceBundle rs = ResourceBundle.getBundle("config", Locale.ENGLISH);
	Logger logger = LogManager.getLogger(AppBar.class);

	public enum NaviMode {
		MENU, CONTEXTUAL
	}

	public AppBar() {
		setClassName(CLASS_NAME);

		initMenuIcon();
		initContextIcon();
		initTitle("");

		initNotificiation();
		initEdit();
		initProfile();
		initSettings();

		initActionItems();
		initNotificationNumber();
		notificationBar.add(notificationNumber, btnNotification);
		initContainer();
	}

	private void initNotificationNumber() {
		notificationNumber = new Div();
		notificationNumber.setVisible(false);
		notificationNumber.getElement().setAttribute("style",
				"position: absolute;\n" + "    height: 17px;\n" + "    width: 17px;\n" + "    text-align: center;\n"
						+ "    border-radius: 18px;\n" + "    margin-bottom: -20px;\n" + "    margin-left: 23px;\n"
						+ "    background: red;\n" + "    font-weight: bold;\n" + "    color: #fff;\n"
						+ "    font-size: 11px;\n" + "    padding: 2px;");
	}

	@PostConstruct
	public void init() {
		filterwindow.add(filterPage);
	}

	private void setAllUnreadToRead(List<JBActionlistHistory> unreadNotification) {
		if (unreadNotification.size() > 0) {
			for (JBActionlistHistory actionlistHistory : unreadNotification) {
				actionlistHistory.setIsRead(JibbyConfig.messageRead[1]);
				dbService.saveActionlistHistory(actionlistHistory);
			}
			unreadNotification.clear();
		}
	}

	private void firstTimeSyncGenerate(@Observes SyncModelGenerateEvent se) {
		if (se.isChange() && sessionModel.getUser() != null) {
			if (dataChangemodel == null)
				dataChangemodel = syncManager.getModel(sessionModel.getUser().getId());
		}
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		this.ui = attachEvent.getUI();
		if (this.ui != null)
			this.ui.getPushConfiguration().setPushMode(PushMode.MANUAL);
		if (dataChangemodel != null) {
			dataChangemodel.addDataChangeListener(this);
		}
	}

	@Override
	protected void onDetach(DetachEvent detachEvent) {
		this.ui = null;
		if (dataChangemodel != null) {
			dataChangemodel.removeDataChangeListener(this);
		}
	}

	@Override
	public void onChanged(ModelUpdateEvent event) {
		updateUI(event.getProcessData());
	}

	private void updateUI(ProcessData data) {

		try {
			if (this.ui != null)
				this.ui.access(() -> {
					AsyncManager.register(this.ui, asyncTask -> {
						asyncTask.push(() -> {
							updateNotification(data);
							ui.push();

						});
					});
				});
		} catch (Exception e) {
			logger.error("ui is not refreshed in AppBar!", e);
		}
	}

	private void updateNotification(ProcessData data) {
		if (data.getNotification() != null) {
			notificationNumber.setVisible(true);
			notificationNumber.setText((unreadNotification.size() + 1) + "");
			unreadNotification.add(data.getNotification());
		}
	}

	public void setNaviMode(NaviMode mode) {
		if (mode.equals(NaviMode.MENU)) {
			menuIcon.setVisible(true);
			contextIcon.setVisible(false);
		} else {
			menuIcon.setVisible(false);
			contextIcon.setVisible(true);
		}
	}

	private void initMenuIcon() {
		menuIcon = UIUtils.createTertiaryInlineButton(VaadinIcon.MENU);
		menuIcon.addClassName(CLASS_NAME + "__navi-icon");
		menuIcon.getElement().setAttribute("style", "color:#fff;");
		menuIcon.addClickListener(e -> MainLayout.get().getNaviDrawer().toggle());
		UIUtils.setAriaLabel("Menu", menuIcon);
		UIUtils.setLineHeight("1", menuIcon);
	}

	private void initContextIcon() {
		contextIcon = UIUtils.createTertiaryInlineButton(VaadinIcon.CHEVRON_LEFT);
		contextIcon.addClassNames(CLASS_NAME + "__context-icon");
		contextIcon.setVisible(false);
		UIUtils.setAriaLabel("Back", contextIcon);
		UIUtils.setLineHeight("1", contextIcon);
		contextIcon.getElement().setAttribute("style", "color:#fff;");
		contextIcon.addClickListener(e -> {
			UI.getCurrent().getPage().getHistory().back();
		});
	}

	private void initTitle(String title) {
		this.title = new Div();
		this.title.setClassName(CLASS_NAME + "__title");
	}

	private void initNotificiation() {

		btnNotification = UIUtils.createButton(VaadinIcon.BELL, ButtonVariant.LUMO_TERTIARY);
		btnNotification.getElement().setAttribute("style", "color:#fff;");
		btnNotification.addClickListener(e -> {
			setAllUnreadToRead(unreadNotification);
			notificationNumber.setVisible(false);
			Dialog dialog = new Dialog();
			dialog.getElement().setAttribute("theme", "my-custom-dialog");
			dialog.setHeight("100vh");
			dialog.setWidth("100vw");
			dialog.add(notificationView);
			dialog.open();
			notificationView.loadData(sessionModel.getUser());
		});
	}

	
	
	private void initSupport() {

		Dialog dialog = new Dialog();
		dialog.getElement().setAttribute("theme", "my-custom-dialog");
		dialog.setHeight("100vh");
		dialog.setWidth("100vw");
		supportView.setUp();
		dialog.add(supportView);
		dialog.open();
	}
	
	private void initEdit() {

		btnAssignItem = UIUtils.createButton(VaadinIcon.PLUS_CIRCLE, ButtonVariant.LUMO_TERTIARY);
		btnAssignItem.getElement().setAttribute("style", "color:#fff;");
		btnAssignItem.addClickListener(e -> {
			Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("item") + 1;
			MyActionlistAssignedEvent openmyactionevent = new MyActionlistAssignedEvent(false, false, true);
			openmyactionevent.setItemid(historyModel.getSelectedEntity().getId());
			openmyactionevent.setItemType(itemType);
			openmyactionevent.setItemName(historyModel.getSelectedEntity().getName());
			myActionEvent.fire(openmyactionevent);
			assigneToDialog.add(myactionlistpopup);
			assigneToDialog.open();
			assigneToDialog.setCloseOnOutsideClick(true);
		});

		btnEdit = UIUtils.createButton(VaadinIcon.EDIT, ButtonVariant.LUMO_TERTIARY);
		btnEdit.getElement().setAttribute("style", "color:#fff;");
		btnEdit.addClickListener(e -> {
			Dialog dialog = new Dialog();
			dialog.add(descriptionpopupPage);
			descriptionpopupPage.setup(historyModel.getSelectedEntity());
			dialog.open();
			dialog.setCloseOnOutsideClick(true);
		});

		btnDelete = UIUtils.createButton(VaadinIcon.TRASH, ButtonVariant.LUMO_TERTIARY);
		btnDelete.getElement().setAttribute("style", "color: #ed462f;");
		btnDelete.addClickListener(e -> {
			deleteItem();
		});

		btnInvite = UIUtils.createButton(VaadinIcon.COMMENT_ELLIPSIS_O, ButtonVariant.LUMO_TERTIARY);
		btnInvite.getElement().setAttribute("style", "color:#fff;");
		btnInvite.addClickListener(e -> {
			Dialog dialog = new Dialog();
			dialog.add(chatInvitationUserPopupPage);
			chatInvitationUserPopupPage.setup();
			dialog.setWidth("90vw");
			dialog.setHeight("75vh");
			dialog.open();
			dialog.setCloseOnOutsideClick(true);
		});

		detailsLayout.setVisible(false);

		detailsLayout.add(btnInvite, btnAssignItem, btnEdit, btnDelete);

	}

	private void deleteItem() {
		children.clear();
		JBEntity entity = entityUserAccess.getAllUsersAccessUser(historyModel.getSelectedEntity());

		boolean isDirectROleUser = false;
		boolean isMultipleDirectOwner = false;
		boolean isDirectOwner = false;
		boolean isPublic = false;

		JBUserEntity user = sessionModel.getUser();
		Integer ownerId = Arrays.asList(JibbyConfig.userRoles).indexOf("owner");
		Integer assigneeId = Arrays.asList(JibbyConfig.userRoles).indexOf("assignee");
		Integer followersId = Arrays.asList(JibbyConfig.userRoles).indexOf("followers");
		if (entity.getDirectOwnerlist().contains(user) || entity.getDirectAssigneelist().contains(user)
				|| entity.getDirectFollowerlist().contains(user))
			isDirectROleUser = true;
		if (!isDirectROleUser) { // check the direct role any of it's descendants
			getChildrenRecursively(entity);
			if (!children.isEmpty()) {
				List<JBUserRoleEntity> userRoleList = dbService.getAllUserRolesByEntityAndUser(children, user);
				for (JBUserRoleEntity role : userRoleList) {
					if (role.getUserRole() - 1 == ownerId || role.getUserRole() - 1 == assigneeId
							|| role.getUserRole() - 1 == followersId) {
						isDirectROleUser = true;
						break;
					}
				}
			}
		}

		if (entity.getDirectOwnerlist().contains(user))
			isDirectOwner = true;

		if (entity.getDirectOwnerlist().size() > 1) {
			isMultipleDirectOwner = true;
		}
		if (entity.getIsPublic() == JibbyConfig.checkItemStatus[0])
			isPublic = true;

		List<ItemBean> items = historyModel
				.getEntitiesByKey(entity.getParentId() == null ? entity.getId() : entity.getParentId());

		if (isDirectROleUser) { // User has direct role on entity, or any of it's descendants
			if (isDirectOwner) {
				if (isMultipleDirectOwner) {// There is another direct owner on entity

					ConfirmationDialog<Integer> confirmationDialog = new ConfirmationDialog<Integer>();
					String title = "Please Confirm:";
					String message = rs.getString("WARNING_UNSUBSCRIBE_CONFIRM");
					String okText = "Yes,I am";
					String cancelText = "Cancel";
					confirmationDialog.open(title, message, "", okText, cancelText, true, 0, (es) -> {
						unsubscribeEntityAndAllOfItsChildren(entity, user);
						removeItemFromItemPage(entity, items);
					}, () -> {
					});

				} else {// if single owner

					boolean isTopLevelProject = entity.getParentProjectId() == null;
					ConfirmationDialog<Integer> confirmationDialog = new ConfirmationDialog<Integer>();
					String title = "Please Confirm:";
					String message = isTopLevelProject ? rs.getString("CONFIRM_MOVE_TO_TRASH_PROJECT")
							: "Do you really want to permanently delete this Item?";
					String okText = isTopLevelProject ? "Move to trash" : "Yes,I am";
					String cancelText = "Cancel";
					confirmationDialog.open(title, message, "", okText, cancelText, true, 0, (es) -> {
						permanentlyDeleteItem(entity, items);
					}, () -> {
					});
				}
			} else { // User doesn't have owner role on entity
				if (isPublic) { // if item is public

					ConfirmationDialog<Integer> confirmationDialog = new ConfirmationDialog<Integer>();
					String title = "Please Confirm:";
					String message = rs.getString("publicItemUnsubscribeConfirmMessage");
					String okText = "Yes,I am";
					String cancelText = "Cancel";
					// unsubscribe
					confirmationDialog.open(title, message, "", okText, cancelText, true, 0, (es) -> {
						unsubscribeEntityAndAllOfItsChildren(entity, user);
						removeItemFromItemPage(entity, items);
					}, () -> {
					});

				} else {
					ConfirmationDialog<Integer> confirmationDialog = new ConfirmationDialog<Integer>();
					String title = "Please Confirm:";
					String message = rs.getString("privateItemUnsubscribeConfirmMessage");
					String okText = "Yes,I am";
					String cancelText = "Cancel";
					confirmationDialog.open(title, message, "", okText, cancelText, true, 0, (es) -> {
						unsubscribeEntityAndAllOfItsChildren(entity, user);
						removeItemFromItemPage(entity, items);
					}, () -> {
					});
				}
			}
		} else {// User has only inherited roles on entity
			if (isPublic)
				Notification.show("You can't unsubscribe from public items.");
			else
				Notification.show(
						"You only have inherited user rights to the selected item to unsubscribe,please find the parent item where your rights have been set.");
		}
	}

	private void removeItemFromItemPage(JBEntity entity, List<ItemBean> items) {
		items.removeIf(e -> e.getEntity().getId().equals(entity.getId()));
		UI.getCurrent().getPage().getHistory().back();
	}

	private void permanentlyDeleteItem(JBEntity selectedItem, List<ItemBean> items) {
		try {
			boolean isDeleteSuccss = false;
			if (selectedItem.getParentProjectId() != null) { // For child item no need to move item to trash.
				boolean hasChild = dbService.hasChild(selectedItem.getId());
				if (!hasChild) {

					dbService.deleteEntity(selectedItem);

					deleteAllChecklistByEntity(selectedItem);
					deleteAllAttachmentByEntity(selectedItem);
					deleteAllUserRolesByEntity(selectedItem);
					deleteAllChatMessageByEntity(selectedItem);
					isDeleteSuccss = true;

				} else
					Notification.show(rs.getString("WARNING_CANNOT_BEDELETE"));
			} else {
				dbService.moveToTrash(selectedItem);
				dbService.updateCustomerForUserRoles(sessionModel.getCustomer().getId(), false, selectedItem.getId());
				isDeleteSuccss = true;
			}

			if (isDeleteSuccss) {
				if (selectedItem.getParentId() == null) { // top lebel item
					userEvent.setIsTopLabelProjectRemove(true);
					sessionModel.getActiveViewDTO().setColAActiveProjectId(null);
					saveActiveUI();
					getoPage(selectedItem);
				}
				userEvent.setEventType(SyncEventType.EVENT_ENTITY_REMOVE);
				userEvent.setEntity(selectedItem);
				userEvent.setSender(sessionModel.getUser());
				userEvent.syncDataReceiver(selectedItem);
				userEvent.setTopLebelProjectEntity(selectedItem.getParentProjectId() == null ? selectedItem
						: dbService.getEntity(selectedItem.getParentProjectId()));
				userEvent.deleteEntityItem();
				userEvent.eventProcessor();
				removeItemFromItemPage(selectedItem, items);
			}

		} catch (Exception e) {
			logger.info(rs.getString("WARNING_CANNOT_BEDELETE"));
		}
	}

	private void getoPage(JBEntity selectedItem) {
		topLabelProjectEvent.fire(new TopLabelProjectEvent(1, selectedItem));
	}

	private void deleteAllChatMessageByEntity(JBEntity selectedItem) {

		List<JBChatMessageEntity> chatlistData = dbService.getChatlistByEntity(selectedItem.getId());
		for (JBChatMessageEntity chatlist : chatlistData) {
			dbService.deleteChatData(chatlist.getId());
		}
	}

	private void deleteAllUserRolesByEntity(JBEntity selectedItem) {

		List<JBUserRoleEntity> roles = dbService.getUserRolesByEntity(selectedItem.getId());
		for (JBUserRoleEntity userRole : roles) {
			dbService.removerUserRoleEntity(userRole);
		}
	}

	private void deleteAllAttachmentByEntity(JBEntity selectedItem) {
		if (projectModel.getItemBean().getIsAttachment()) {
			List<JBAttachmentEntity> attachmentData = dbService.getAttachmentByEntity(selectedItem.getId());
			for (JBAttachmentEntity attachment : attachmentData) {
				if (dbService.deleteAttachment(attachment)) {
				}
			}
		}
	}

	private void deleteAllChecklistByEntity(JBEntity selectedItem) {
		if (projectModel.getItemBean().getIsChecklist()) {
			List<JBChecklistItemEntity> checkListData = dbService.getChecklistByEntity(selectedItem.getId());
			for (JBChecklistItemEntity checklist : checkListData) {
				dbService.deleteCheckItem(checklist);
			}
		}
	}

	private void saveActiveUI() {
		String json = Utils.jsonBuilder(sessionModel.getActiveViewDTO());
		if (json != null) {
			sessionModel.getUser().setActiveUI(json);
			dbService.saveUser(sessionModel.getUser());
		}
	}

	private void unsubscribeEntityAndAllOfItsChildren(JBEntity entity, JBUserEntity user) {
		getChildrenRecursively(entity); // recursive get all the children
		children.add(entity.getId());
		List<JBUserRoleEntity> roleList = new ArrayList<>();
		roleList.addAll(dbService.getAllUserRolesByEntityAndUser(children, user));

		for (JBUserRoleEntity userroles : roleList) {
			Integer entityId = userroles.getEntityId();
			userEvent.addToUnsubscribeEntityList(entityId);
			dbService.removerUserRoleEntity(userroles);
			insertItemModification(entityId,
					Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("unsubscribe") + 1);
		}
		userEvent.setEventType(SyncEventType.EVENT_ENTITY_UNSUBSCRIBE);
		userEvent.setEntity(entity);
		userEvent.setSender(sessionModel.getUser());
		userEvent.syncDataReceiver(entity);

		userEvent.setTopLebelProjectEntity(
				entity.getParentProjectId() == null ? entity : dbService.getEntity(entity.getParentProjectId()));

		userEvent.unsubscribeFromEntity();
		userEvent.eventProcessor();
	}

	public void insertItemModification(Integer entityId, Integer type) {
		JBItemModification itemModification = new JBItemModification();
		itemModification.setEntityId(entityId);
		itemModification.setUserId1(sessionModel.getUser().getId());
		itemModification.setModificationType(type);
		itemModification.setModificationDate(new Date());
		dbService.saveItemModification(itemModification);

		userEvent.addToHistoryList(itemModification);
	}

	private void getChildrenRecursively(JBEntity entity) { // recursively get all the child by entity
		for (JBEntity child : dbService.getAllChildByEntityId(entity.getId())) {
			children.add(child.getId());
			getChildrenRecursively(child);
		}
	}

	private void initSettings() {
		settings = UIUtils.createButton(VaadinIcon.COG, ButtonVariant.LUMO_TERTIARY);

		settings.getElement().setAttribute("style", "color:#fff;");

		ContextMenu contextMenu = new ContextMenu(settings);
		contextMenu.setOpenOnClick(true);

		itemProjectView = contextMenu
				.addItem(createContextMenuComponent(VaadinIcon.ALIGN_JUSTIFY, "#3c3c3c", "Project View"), e -> {
					UI.getCurrent().getPage().getHistory().back();
					setContextMenuVisibility(false, true);
				});
		itemMyAction = contextMenu.addItem(createContextMenuComponent(VaadinIcon.BULLETS, "#3c3c3c", "My Action List"),
				e -> {
					getUI().ifPresent(es -> {

						myactionDataLoadEvent.fire(new MyactionDataLoadEvent());
						es.navigate(MyactionPage.class);
						setContextMenuVisibility(true, false);
					});
				});
		contextMenu.addItem(createContextMenuComponent(VaadinIcon.FILTER, "#3c3c3c", "Filter"), e -> openFilterPage());
		
		contextMenu.addItem(createContextMenuComponent(VaadinIcon.PAPERPLANE, "#3c3c3c", "Support and feedback"), e -> initSupport());
		contextMenu.addItem(createLogoutContextMenu(VaadinIcon.SIGN_OUT, "#d50000", "Logout"), e -> logout());
		settingLayout.add(settings, profile);
		setContextMenuVisibility(false, true);
	}

	private void openFilterPage() {
		filterPage.setup();
		filterwindow.open();
	}

	public void setContextMenuVisibility(boolean project, boolean myAction) {
		itemProjectView.setVisible(project);
		itemMyAction.setVisible(myAction);
	}

	private Component createContextMenuComponent(VaadinIcon vaadinIcon, String color, String name) {
		Div div = new Div();
		div.getElement().setAttribute("style",
				"border-bottom: 1px solid #e0e0e0;padding-bottom: 13px;margin-left: -13px;margin-right: -13px;");
		Icon icon = new Icon(vaadinIcon);
		icon.getElement().setAttribute("style", "width: 25px;height: 25px;");
		Button button = new Button(icon);
		button.setEnabled(false);
		button.getElement().setAttribute("style", "height: 15px;color: " + color + ";background-color: #fff;");
		button.setText(name);
		div.add(button);
		return div;
	}

	private Component createLogoutContextMenu(VaadinIcon vaadinIcon, String color, String name) {
		Div div = new Div();
		div.getElement().setAttribute("style", "margin-left: -13px;margin-right: -13px;");
		Icon icon = new Icon(vaadinIcon);
		icon.getElement().setAttribute("style", "width: 25px;height: 25px;");
		Button button = new Button(icon);
		button.setEnabled(false);
		button.getElement().setAttribute("style", "height: 15px;color: " + color + ";background-color: #fff;");
		button.setText(name);
		div.add(button);
		return div;
	}

	private void initProfile() {
		profile.getElement().setAttribute("style",
				"width:25px;height:25px;border-radius: 100%;" + "margin-left: 5px;position: relative; top: 0.4rem;");
		profile.addClickListener(e -> {
			Dialog dialog = new Dialog();
			dialog.add(userSettingsPopupPage);
			userSettingsPopupPage.setup(sessionModel.getUser());
			dialog.open();
			dialog.setCloseOnOutsideClick(true);
		});
	}

	private void userData(@Observes UserEvent event) {
		StreamResource resource = Utils.getStream(Utils.userImagePath, event.getUserEntity().getId() + "");
		if (resource != null)
			profile.setSrc(resource);
		else
			profile.setSrc("./images/avatar.png");

		if (!event.isImageUploaded()) {
			unreadNotification = dbService.getAllUnreadNotification(sessionModel.getUser().getId());
			notificationNumber.setText(String.valueOf(unreadNotification.size()));
			if (unreadNotification.size() == 0)
				notificationNumber.setVisible(false);
			else
				notificationNumber.setVisible(true);
		}
	}

	private void logout() {
		sessionModel.setUser(null);
		sessionModel.setActiveViewDTO(null);
		getUI().get().getSession().close();
		UI.getCurrent().getPage().executeJs("location.assign('')");
	}

	private void initActionItems() {
		actionItems = new FlexBoxLayout();
		actionItems.addClassName(CLASS_NAME + "__action-items");
		actionItems.setVisible(false);
	}

	private void initContainer() {
		container = new FlexBoxLayout(menuIcon, contextIcon, this.title, actionItems, notificationBar, detailsLayout,
				settingLayout);
		container.addClassName(CLASS_NAME + "__container");
		container.setAlignItems(FlexComponent.Alignment.CENTER);
		add(container);
	}

	public Div getDetailsLayout() {
		return detailsLayout;
	}

	public Button getMenuIcon() {
		return menuIcon;
	}

	public Button getContextIcon() {
		return contextIcon;
	}

	public void setContextIcon(Icon icon) {
		contextIcon.setIcon(icon);
	}

	public String getTitle() {
		return this.title.getText();
	}

	public Div getTitleDiv() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title.setText(title);
		this.title.getElement().getStyle().set("width", !title.isEmpty() ? "calc(100% - 170px)" : "auto");
	}

	public Component addActionItem(Component component) {
		actionItems.add(component);
		updateActionItemsVisibility();
		return component;
	}

	public Button addActionItem(VaadinIcon icon) {
		Button button = UIUtils.createButton(icon, ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY);
		addActionItem(button);
		return button;
	}

	public void removeAllActionItems() {
		actionItems.removeAll();
		updateActionItemsVisibility();
	}

	public Div getSettingLayout() {
		return settingLayout;
	}

	public void reset() {
		title.setText(
				projectModel.getDrawerActiveProject() != null ? projectModel.getDrawerActiveProject().getName() : "");
		setNaviMode(AppBar.NaviMode.MENU);
		removeAllActionItems();
		this.title.getElement().getStyle().set("width", !title.getText().isEmpty() ? "calc(100% - 170px)" : "auto");

	}

	private void updateActionItemsVisibility() {
		actionItems.setVisible(actionItems.getComponentCount() > 0);
	}

}
