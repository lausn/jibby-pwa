package com.uppstreymi.jibby.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.CEvent;
import com.uppstreymi.jibby.ProjectLoadEvent;
import com.uppstreymi.jibby.UIPollingManager;
import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBFavouritesEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.bean.SortCriteria;
import com.uppstreymi.jibby.item.events.FilterChangeEvent;
import com.uppstreymi.jibby.item.events.TittleEvent;
import com.uppstreymi.jibby.item.events.TopLabelProjectEvent;
import com.uppstreymi.jibby.item.events.UserEvent;
import com.uppstreymi.jibby.login.LoginPage;
import com.uppstreymi.jibby.model.HistorModel;
import com.uppstreymi.jibby.model.ProjectViewModel;
import com.uppstreymi.jibby.model.SessionModel;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.ui.components.ConfirmationDialog;
import com.uppstreymi.jibby.ui.components.FlexBoxLayout;
import com.uppstreymi.jibby.ui.components.navigation.bar.AppBar;
import com.uppstreymi.jibby.ui.components.navigation.bar.AppBar.NaviMode;
import com.uppstreymi.jibby.ui.components.navigation.drawer.NaviDrawer;
import com.uppstreymi.jibby.ui.components.navigation.drawer.NaviItem;
import com.uppstreymi.jibby.ui.components.navigation.drawer.NaviMenu;
import com.uppstreymi.jibby.ui.page.ItemView;
import com.uppstreymi.jibby.ui.util.UIUtils;
import com.uppstreymi.jibby.ui.util.css.FlexDirection;
import com.uppstreymi.jibby.ui.util.css.Overflow;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.ErrorHandler;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.PageConfigurator;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.lumo.Lumo;

@CssImport(value = "./styles/components/charts.css", themeFor = "vaadin-chart", include = "vaadin-chart-default-theme")
@CssImport(value = "./styles/components/floating-action-button.css", themeFor = "vaadin-button")
@CssImport(value = "./styles/components/grid.css", themeFor = "vaadin-grid")
@CssImport("./styles/lumo/border-radius.css")
@CssImport("./styles/lumo/icon-size.css")
@CssImport("./styles/lumo/margin.css")
@CssImport("./styles/lumo/padding.css")
@CssImport("./styles/lumo/shadow.css")
@CssImport("./styles/lumo/spacing.css")
@CssImport("./styles/lumo/typography.css")
@CssImport("./styles/misc/box-shadow-borders.css")
@CssImport(value = "./styles/styles.css", include = "lumo-badge")
@JsModule("@vaadin/vaadin-lumo-styles/badge")
@JsModule("./styles/shared-styles.js")
@Viewport("width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover")
@PWA(name = "Jibby", shortName = "Jibby", themeColor = "black-translucent", backgroundColor = "#10708e")
@UIScoped
public class MainLayout extends FlexBoxLayout implements RouterLayout, PageConfigurator, AfterNavigationObserver {

	private static final String CLASS_NAME = "root";

	private FlexBoxLayout row;
	private NaviDrawer naviDrawer;
	private FlexBoxLayout column;

	private Div appHeaderInner;
	private FlexBoxLayout viewContainer;

	@Inject
	AppBar appBar;
	NaviMenu menu;
	private NaviItem activeButton;
	private Integer selectedEntity;
	@Inject
	SessionModel sessionModel;
	@Inject
	DbService dbService;
	@Inject
	private ProjectViewModel projectModel;
	@Inject
	private HistorModel historyModel;
	@Inject
	private Event<FilterChangeEvent> filterChangeEvent;
	@Inject
	private Event<UserEvent> userEvent;
	private List<Integer> projectListEntityIds = new ArrayList<>();
	private HashMap<Integer, NaviItem> hmprojects = new HashMap<>();
	private HashMap<Integer, NaviItem> hmFavouriteProject = new HashMap<>();
	List<JBEntity> projectList;
	List<JBFavouritesEntity> favouriteProjectList = new ArrayList<>();
	List<JBEntity> trashProjectList = new ArrayList<>();
	private boolean isProjectDetailsShow = false;
	private ResourceBundle rs = ResourceBundle.getBundle("config", Locale.ENGLISH);
	Logger logger = LogManager.getLogger(MainLayout.class);
	@Inject
	UIPollingManager pollManager;
	private Image trashIcon=new Image();
	public MainLayout() {
		VaadinSession.getCurrent().setErrorHandler((ErrorHandler) errorEvent -> {
			logger.info("ErrorEventThrowable In MainLayout. {}", errorEvent.getThrowable());
			Notification.show("We are sorry, but an internal error occurred");
		});

		addClassName(CLASS_NAME);
		setFlexDirection(FlexDirection.COLUMN);
		getElement().getStyle().set("width", "100vw");
		getElement().getStyle().set("height", "100vh");
	}

	@PostConstruct
	private void init() {
		// Initialise the UI building blocks
		initStructure();

		// Populate the navigation drawer
		initNaviItems();
		// Configure the headers and footers (optional)
		initHeadersAndFooters();
		if (sessionModel.getUser() != null) {
			setSettingInfo();
			loadProjectList(false);
			FilterChangeEvent fe = new FilterChangeEvent();
			fe.setFilter(true);
			filterChangeEvent.fire(fe);
			userEvent.fire(new UserEvent(sessionModel.getUser(), false));
		}
	}

	private void initStructure() {
		naviDrawer = new NaviDrawer();

		viewContainer = new FlexBoxLayout();
		viewContainer.addClassName(CLASS_NAME + "__view-container");
		viewContainer.setOverflow(Overflow.HIDDEN);

		column = new FlexBoxLayout(viewContainer);
		column.addClassName(CLASS_NAME + "__column");
		column.setFlexDirection(FlexDirection.COLUMN);
		column.setFlexGrow(1, viewContainer);
		column.setOverflow(Overflow.HIDDEN);

		row = new FlexBoxLayout(naviDrawer, column);
		row.addClassName(CLASS_NAME + "__row");
		row.setFlexGrow(1, column);
		row.setOverflow(Overflow.HIDDEN);
		add(row);
		setFlexGrow(1, row);
	}

	private void initNaviItems() {
		menu = naviDrawer.getMenu();
	}

	void setTittle(@Observes TittleEvent t) {
		appBar.setTitle(t.getTittle());
		isProjectDetailsShow = t.isProjectItem();
	}

	void topLabelProject(@Observes TopLabelProjectEvent event) {
		if (event.isCreateTopLabelProject()) {
			createProjectMenuItem(event.getEntity());
			activeButton.removeClassName("active");
			getoPage(event.getEntity());
		}
		if (event.getIsDeleteTopLabelProject().equals(1)) {
			projectListEntityIds.removeIf(e -> e.equals(event.getEntity().getId()));
			projectList.removeIf(e -> e.getId().equals(event.getEntity().getId()));
			favouriteProjectList.removeIf(e -> e.getId().equals(event.getEntity().getId()));
			trashProjectList.add(event.getEntity());
			createTrashProjectMenuItem(event.getEntity());
			activeButton.removeClassName("active");
			NaviItem item = hmprojects.get(event.getEntity().getId());
			if (item != null) {
				item.getParent().get().getElement().removeChild(item.getElement());
				hmprojects.remove(event.getEntity().getId());
			}
			item = hmFavouriteProject.get(event.getEntity().getId());
			if (item != null) {
				item.getParent().get().getElement().removeChild(item.getElement());
				hmFavouriteProject.remove(event.getEntity().getId());
			}
			trashIcon.setSrc("./images/ic_colb_top_trash.png");
			clear();
			if (projectList.size() > 0)
				getoPage(projectList.get(0));
		}
	}

	private void clear() {
		projectModel.setItemBean(null);
		projectModel.setDrawerActiveProject(null);
		historyModel.getEntities().clear();
		historyModel.getItemBean().clear();
		historyModel.getParentArray().clear();
		selectedEntity = null;
		sessionModel.getActiveViewDTO().setColAActiveProjectId(null);
	}

	public void loadProjectFirstTime(@Observes ProjectLoadEvent projectevent) {
		logger.info("Should be call after login!");
		if (projectevent.isLoadSettings()) {
			setSettingInfo();
		}

		if (projectevent.isSorting()) {
			saveActiveUI();
			projectSort();
		} else
			loadProjectList(projectevent.isFiltering());
	}

	public void setSettingInfo() {
		setOptionModelData();
	}

	private void setOptionModelData() {
		saveActiveUI();
		setActiveOption();
		projectModel.setTypes(sessionModel.getActiveViewDTO().getItemTypes());
		setSortList();
		selectedEntity = sessionModel.getActiveViewDTO().getColAActiveProjectId();
	}

	private void setActiveOption() {
		List<Integer> status = new ArrayList<Integer>();
		ActiveViewDTO activeView = sessionModel.getActiveViewDTO();

		if (activeView.getColAOptions().getItemStatus() != null) {
			if (activeView.getColAOptions().getItemStatus().equals(0)) { // For All status
				status.add(2);
				status.add(4);
			} else {
				status.add(activeView.getColAOptions().getItemStatus());
			}
		} else {
			status.add(2);
		}

		List<Integer> userrole = new ArrayList<Integer>();
		if (activeView.getColAOptions().getRules() != null) {
			userrole = activeView.getColAOptions().getRules();
		} else {
			userrole.add(1);
			userrole.add(2);
			userrole.add(3);
		}

		if (userrole.size() == 0)
			userrole.add(-1);// not any selected userViewTypes

		projectModel.setUserRole(userrole);
		projectModel.setStatus(status);
		projectModel.setStartType(
				activeView.getColAOptions().getStartType() == null ? 0 : activeView.getColAOptions().getStartType());
		projectModel.setFinishType(
				activeView.getColAOptions().getFinishType() == null ? 0 : activeView.getColAOptions().getFinishType());
		projectModel.setStartDate(activeView.getColAOptions().getStartDate() == null ? JibbyConfig.estimateDate
				: Utils.dateToLocalDate((Date) activeView.getColAOptions().getStartDate()));
		projectModel.setFinishDate(activeView.getColAOptions().getFinishDate() == null ? JibbyConfig.estimateDate
				: Utils.dateToLocalDate((Date) activeView.getColAOptions().getFinishDate()));
		projectModel.setIsPublic(activeView.getColAOptions().getProjectType() == null ? 0
				: activeView.getColAOptions().getProjectType());
		projectModel.setOnHold(
				activeView.getColAOptions().getOnHold() == null ? 0 : activeView.getColAOptions().getOnHold());

	}

	private void setSortList() {
		List<SortCriteria> list = new ArrayList<>();

		int i = 1;
		for (Object[] sort : JibbyConfig.sortCaption) {
			SortCriteria sc = new SortCriteria();
			sc.setId(i);
			sc.setCaption(String.valueOf(sort[0]));
			sc.setCriteria(String.valueOf(sort[1]));
			sc.setAsc((Boolean) sort[2]);

			i++;
			list.add(sc);
		}
		projectModel.setSortOption(list.get((sessionModel.getActiveViewDTO().getSortOption() - 1)));
		projectModel.setProjectSortOption(list.get(sessionModel.getActiveViewDTO().getProjectSortOption() - 1));
	}

	private void saveActiveUI() {
		String json = Utils.jsonBuilder(sessionModel.getActiveViewDTO());
		if (sessionModel.getActiveViewDTO() != null && json != null) {
			sessionModel.getUser().setActiveUI(json);
			dbService.saveUser(sessionModel.getUser());
		}
	}

	private void loadProjectList(boolean isFiltering) {

		try {
			logger.info("Mobile menu project loading");

			List<Integer> publicroles = new ArrayList<>();
			if (projectModel.isPublic() == 0) {
				publicroles.add(0);
				publicroles.add(1);
			} else {
				publicroles.add(1);
			}

			List<JBCustomerEntity> customers = dbService.getCustomersByUser(sessionModel.getUser().getId());
			List<Integer> customerIds = new ArrayList<>();
			if (customers.size() == 1)
				customerIds.add(0);
			else {
				for (JBCustomerEntity customer : customers) {
					if (!sessionModel.getCustomer().getId().equals(customer.getId()))
						customerIds.add(customer.getId());
				}
			}
			Integer ownerRole = Arrays.asList(JibbyConfig.userRoles).indexOf("owner") + 1;

			projectList = dbService
					.getAllProjectForColA(sessionModel.getUser(), publicroles,
							projectModel.getProjectSortOption().getCriteria(),
							projectModel.getProjectSortOption().isAsc(), projectModel.getUserRole(), customerIds)
					.stream().filter(e -> e.getIsDelete() == null || e.getIsDelete() == 0).collect(Collectors.toList());
			favouriteProjectList = dbService.getAllFavouriteProjectsByUser(sessionModel.getUser().getId());
			trashProjectList = dbService.getAllTrashProjectForColA(sessionModel.getUser(), ownerRole,
					sessionModel.getCustomer().getId(), projectModel.getProjectSortOption().isAsc(),
					projectModel.getProjectSortOption().getCriteria());

			projectList = getOnHoldProjectList(projectList);

			setProject();

			if (projectList.size() > 0) {
				if (selectedEntity == null) {
					getoPage(projectList.get(0));
				} else {
					if (projectListEntityIds.contains(selectedEntity)) {
						getoPage(projectList.get(projectListEntityIds.indexOf(selectedEntity)));
					} else {
						getoPage(projectList.get(0));
					}
				}
			} else
				clear();

			if (isFiltering)
				UI.getCurrent().navigate(ItemView.class);

			saveActiveUI();
		} catch (Exception e) {
			logger.error("An exception thrown in mobile menu project load.", e);
		}
	}

	private List<JBEntity> getOnHoldProjectList(List<JBEntity> projectList) {
		List<JBUserRoleEntity> userRolesList = new ArrayList<>();
		if (projectList.size() > 0) {
			List<Integer> projectIds = projectList.stream().map(JBEntity::getId).collect(Collectors.toList());
			userRolesList = dbService.getAllUserRolesByEntityAndUser(projectIds, sessionModel.getUser());
			List<JBEntity> removalList = new ArrayList<>();
			for (JBEntity entity : projectList) {
				if (entity.getOnHold() != null && entity.getOnHold() == 1) {
					JBUserRoleEntity roles = userRolesList.stream()
							.filter(e -> e.getEntityId().equals(entity.getId()) && e.getUserRole()
									.equals(Arrays.asList(JibbyConfig.userRoles).indexOf("owner") + 1))
							.findAny().orElse(null);
					if (roles != null && projectModel.getOnHold() == 0) {
						removalList.add(entity);
					} else if (roles == null) {
						removalList.add(entity);
					}
				}
			}
			projectList.removeAll(removalList);
		}
		return projectList;
	}

	public void setProject() {

		menu.removeAllSubItem();
		
		Div trashProjectDiv = new Div(); 
		Div trashProjectLabel=new Div();
		Label label=new Label("Trash");
		trashIcon.setSrc("./images/ic_trash_open.png");
		trashIcon.getElement().setAttribute("style", "padding-right:10px;width:16px;");
		trashProjectLabel.addClassName("projectspace-label");
		trashProjectLabel.add(trashIcon,label);
		trashProjectDiv.add(trashProjectLabel);

		Div favouriteProjectDiv = new Div();
		Div favouriteDivLabel = new Div();

		favouriteDivLabel.setText("Favorites");
		favouriteDivLabel.addClassName("projectspace-label");
		favouriteProjectDiv.add(favouriteDivLabel);

		Div allProjectDiv = new Div();
		Div allDivLabel = new Div();
		allDivLabel.setText("All");
		allDivLabel.addClassName("projectspace-label");
		allProjectDiv.add(allDivLabel);

		menu.getSubItemDiv().add(trashProjectDiv, favouriteProjectDiv, allProjectDiv);

		hmprojects.clear();
		hmFavouriteProject.clear();
		projectListEntityIds.clear();

		for (JBEntity entity : projectList) {
			JBFavouritesEntity favourite = favouriteProjectList.stream()
					.filter(item -> item.getEntityId().equals(entity.getId())).findAny().orElse(null);
			createProjectMenuItem(entity);
			if (favourite != null) {
				createFavouriteProjectMenuItem(entity);
			}
		}
		for (JBEntity trashEntity : trashProjectList) {
			createTrashProjectMenuItem(trashEntity);
		}
		if (trashProjectList.size() > 0) {
			trashIcon.setSrc("./images/ic_colb_top_trash.png");
		}
		menu.buildSubItem();
	}

	private void createTrashProjectMenuItem(JBEntity trashEntity) {
		NaviItem menuItem = menu.addNaviItem(trashEntity.getName(), null, false, true);
		menuItem.addClickListener(e -> {
			openRestoreProjectConfirmDialog(menuItem, trashEntity);
		});
	}

	private void openRestoreProjectConfirmDialog(NaviItem trashItem, JBEntity trashEntity) {
		ConfirmationDialog<Integer> confirmationDialog = new ConfirmationDialog<Integer>();
		String title = "Please Confirm:";
		String message = rs.getString("CONFIRM_RESTORE_PROJECT_FROM_TRASH");
		String okText = "Yes, restore project";
		String cancelText = "Cancel";
		confirmationDialog.open(title, message, "", okText, cancelText, true, 0, (es) -> {
			restoreProject(trashItem, trashEntity);
		}, () -> {
		});
	}

	private void restoreProject(NaviItem trashItem, JBEntity trashEntity) {

		dbService.restoreFromTrash(trashEntity);
		
		/*Update all entities customer in user roles*/
		updateAllEntitiesCustomerInUserRoles(trashEntity);
		
		JBUserEntity user = sessionModel.getUser();
		JBFavouritesEntity favourite = dbService.getFavouriteByUseridAndEntityId(user.getId(), trashEntity.getId());
		projectList.add(trashEntity);
		if (favourite != null){
			favouriteProjectList.add(favourite);
			createFavouriteProjectMenuItem(trashEntity);
		}
		trashProjectList.removeIf(e -> e.getId().equals(trashEntity.getId()));
		trashItem.getParent().get().getElement().removeChild(trashItem.getElement());
		if (trashProjectList.size() == 0) {
			trashIcon.setSrc("./images/ic_trash_open.png");
		}
		createProjectMenuItem(trashEntity);
		activeButton.removeClassName("active");
		getoPage(trashEntity);
		UI.getCurrent().navigate(ItemView.class);
	}
	
	private void updateAllEntitiesCustomerInUserRoles(JBEntity trashEntity) {
		boolean isOwnCustomerForProject = dbService.isOwnCustomerForProject(trashEntity.getId(), sessionModel.getCustomer().getId());	
		if(!isOwnCustomerForProject)
			dbService.updateCustomerForUserRoles(sessionModel.getCustomer().getId(), true, trashEntity.getId());
	}

	private void createFavouriteProjectMenuItem(JBEntity entity) {

		NaviItem menuItem = menu.addNaviItem(entity.getName(), null, true, false);
		addFavIcon(menuItem);
		menuItem.addClickListener(e -> {

			activeButton.removeClassName("active");

			activeButton = menuItem;
			activeButton.addClassName("active");

			ItemBean itemBean = new ItemBean(entity);
			itemBean.setIsRoles(false);
			projectModel.setItemBean(itemBean);
			projectModel.setDrawerActiveProject(entity);
			historyModel.removeEntitiesKey(entity.getId());
			historyModel.getParentArray().clear();
			historyModel.getItemBean().remove(entity.getId());

			activeButton = menuItem;
			selectedEntity = entity.getId();
			sessionModel.getActiveViewDTO().setColAActiveProjectId(entity.getId());
			saveActiveUI();
			UI.getCurrent().navigate(ItemView.class);

		});
		hmFavouriteProject.put(entity.getId(), menuItem);
	}

	private void addFavIcon(NaviItem item) {

		Div favIconDiv = new Div();

		favIconDiv.getElement().setAttribute("style", "width: 15px;margin-right: 6px;");
		Icon favIcon = VaadinIcon.STAR.create();

		favIcon.getElement().setAttribute("style", "color: #f9e218; margin-left: 10px; width:20px; height: 20px;");
		favIconDiv.add(favIcon);
		item.addComponentAsFirst(favIconDiv);
	}

	private void createProjectMenuItem(JBEntity entity) {
		projectListEntityIds.add(entity.getId());
		NaviItem menuItem = menu.addNaviItem(entity.getName(), null, false, false);
		menuItem.addClickListener(e -> {

			activeButton.removeClassName("active");

			activeButton = menuItem;
			activeButton.addClassName("active");

			ItemBean itemBean = new ItemBean(entity);
			itemBean.setIsRoles(false);
			projectModel.setItemBean(itemBean);
			projectModel.setDrawerActiveProject(entity);
			historyModel.removeEntitiesKey(entity.getId());
			historyModel.getParentArray().clear();
			historyModel.getItemBean().remove(entity.getId());

			activeButton = menuItem;
			selectedEntity = entity.getId();
			sessionModel.getActiveViewDTO().setColAActiveProjectId(entity.getId());
			saveActiveUI();
			UI.getCurrent().navigate(ItemView.class);

		});
		hmprojects.put(entity.getId(), menuItem);
	}

	public void getoPage(JBEntity entity) {
		ItemBean itemBean = new ItemBean(entity);
		itemBean.setIsRoles(false);
		projectModel.setItemBean(itemBean);
		projectModel.setDrawerActiveProject(entity);
		historyModel.removeEntitiesKey(entity.getId());
		historyModel.getItemBean().remove(entity.getId());
		historyModel.getParentArray().clear();
		selectedEntity = entity.getId();
		sessionModel.getActiveViewDTO().setColAActiveProjectId(entity.getId());

		activeButton = hmprojects.get(entity.getId());
		activeButton.addClassName("active");
	}

	private void projectSort() {

		SortCriteria sc = projectModel.getProjectSortOption();
		try {
			Collections.sort(projectList, new Comparator<JBEntity>() {

				@Override
				public int compare(JBEntity o1, JBEntity o2) {

					if (sc.getCriteria().equalsIgnoreCase("a.name"))
						return sc.isAsc() ? o1.getName().compareToIgnoreCase(o2.getName())
								: o2.getName().compareToIgnoreCase(o1.getName());
					else if (sc.getCriteria().equalsIgnoreCase("a.creationDate") && o1.getCreationDate() != null
							&& o2.getCreationDate() != null)
						return sc.isAsc() ? o1.getCreationDate().compareTo(o2.getCreationDate())
								: o2.getCreationDate().compareTo(o1.getCreationDate());
					else if (sc.getCriteria().equalsIgnoreCase("a.estimatedStartDate")
							&& o1.getEstimatedStartDate() != null && o2.getEstimatedStartDate() != null)
						return sc.isAsc() ? o1.getEstimatedStartDate().compareTo(o2.getEstimatedStartDate())
								: o2.getEstimatedStartDate().compareTo(o1.getEstimatedStartDate());
					else if (sc.getCriteria().equalsIgnoreCase("a.estimatedFinishDate")
							&& o1.getEstimatedFinishDate() != null && o2.getEstimatedFinishDate() != null)
						return sc.isAsc() ? o1.getEstimatedFinishDate().compareTo(o2.getEstimatedFinishDate())
								: o2.getEstimatedFinishDate().compareTo(o1.getEstimatedFinishDate());
					else if (sc.getCriteria().equalsIgnoreCase("a.keywords"))
						return sc.isAsc() ? o1.getKeywords().compareToIgnoreCase(o2.getKeywords())
								: o2.getKeywords().compareToIgnoreCase(o1.getKeywords());
					else
						return 0;
				}
			});

			setProject();

			if (selectedEntity != null && hmprojects.get(selectedEntity) != null) {
				activeButton = hmprojects.get(selectedEntity);
				activeButton.addClassName("active");
			}
		} catch (Exception e) {
			logger.error("An exception thrown in mobile projectList sort.", e);
		}
	}

	private void initHeadersAndFooters() {

		UIUtils.setTheme(Lumo.DARK, appBar);
		setAppHeaderInner(appBar);

	}

	void setBackNavigate(@Observes CEvent c) {
		logger.info(c.isBack() + ":back");
		if (c.isBack())
			appBar.setNaviMode(NaviMode.CONTEXTUAL);
		else {
			appBar.setContextMenuVisibility(false, true);
			appBar.setNaviMode(NaviMode.MENU);
		}
	}

	private void setAppHeaderInner(Component... components) {
		if (appHeaderInner == null) {
			appHeaderInner = new Div();
			appHeaderInner.addClassName("app-header-inner");
			column.getElement().insertChild(0, appHeaderInner.getElement());
		}
		appHeaderInner.removeAll();
		appHeaderInner.add(components);
	}

	@Override
	public void configurePage(InitialPageSettings settings) {
		settings.addMetaTag("theme-color", "#10708e");
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		this.viewContainer.getElement().appendChild(content.getElement());
	}

	public NaviDrawer getNaviDrawer() {
		return naviDrawer;
	}

	public static MainLayout get() {
		return (MainLayout) UI.getCurrent().getChildren().filter(component -> component.getClass() == MainLayout.class)
				.findFirst().get();
	}

	public AppBar getAppBar() {
		return appBar;
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {

		if (sessionModel.getUser() != null)
			afterNavigationWithoutTabs(event);
		else {
			getUI().ifPresent(nameevent -> nameevent.navigate(LoginPage.class));
		}

	}

	private NaviItem getActiveItem(AfterNavigationEvent e) {
		for (NaviItem item : naviDrawer.getMenu().getNaviItems()) {
			if (item.isHighlighted(e)) {
				return item;
			}
		}
		return null;
	}

	private void afterNavigationWithoutTabs(AfterNavigationEvent e) {
		NaviItem active = getActiveItem(e);
		if (active != null) {
			getAppBar().setTitle(active.getText());
		}
	}
}
