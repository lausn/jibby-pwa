package com.uppstreymi.jibby;

import javax.servlet.annotation.WebServlet;

import com.vaadin.cdi.CdiVaadinServlet;

@WebServlet(urlPatterns = "/*", name = "UIServlet", asyncSupported = true)
public class Servlet extends CdiVaadinServlet {
	private static final long serialVersionUID = -2009645086669155271L;
	
}