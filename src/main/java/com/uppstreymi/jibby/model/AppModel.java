package com.uppstreymi.jibby.model;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;


@SuppressWarnings("serial")
@ApplicationScoped
public class AppModel implements Serializable {


	private PropertiesModel propsModel;
	
	
	public void setPropertiesModel(PropertiesModel propsModel){
		this.propsModel=propsModel;
	}
	
	public PropertiesModel getPropertiesModel() {
		return propsModel;
	}
}
