package com.uppstreymi.jibby.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.service.DbService;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.vaadin.cdi.annotation.UIScoped;

@UIScoped
public class EntityUserAccess {

	@Inject DbService service;

	public JBEntity getAllUsersAccessUser(JBEntity entity){

		Integer ownerId=Arrays.asList(JibbyConfig.userRoles).indexOf("owner");
		Integer assigneeId=Arrays.asList(JibbyConfig.userRoles).indexOf("assignee");
		Integer followersId=Arrays.asList(JibbyConfig.userRoles).indexOf("followers");
		Integer connectionId=Arrays.asList(JibbyConfig.userRoles).indexOf("connections");

		List<JBUserEntity> ownerlist = new ArrayList<JBUserEntity>();
		List<JBUserEntity> assignerlist = new ArrayList<JBUserEntity>();
		List<JBUserEntity> followerlist = new ArrayList<JBUserEntity>();
		List<JBUserEntity> connectionlist = new ArrayList<JBUserEntity>();

		List<JBUserEntity> directOwnerlist = new ArrayList<JBUserEntity>(); 
		List<JBUserEntity> directAssignerlist = new ArrayList<JBUserEntity>(); 
		List<JBUserEntity> directFollowerlist = new ArrayList<JBUserEntity>(); 
		List<JBUserEntity> directConnectionlist = new ArrayList<JBUserEntity>(); 

		List<Integer> parentlist= service.getParentsListByEntityId(entity.getId());
		List<JBUserRoleEntity> userRoleList = service.getAllUserRolesByEntityId(parentlist);
		for(JBUserRoleEntity userroles:userRoleList){
			if(userroles.getUserRole()-1 == ownerId){
				ownerlist.add(userroles.getUser());
			}
			else if(userroles.getUserRole()-1 == assigneeId){
				assignerlist.add(userroles.getUser());
			}
			else if(userroles.getUserRole()-1 == followersId){
				followerlist.add(userroles.getUser());
			}
			else if(userroles.getUserRole()-1 == connectionId){
				connectionlist.add(userroles.getUser());
			}
			if(userroles.getEntityId().equals(entity.getId())){ // for find direct user role from list 
				if(userroles.getUserRole()-1 == ownerId){ 
					directOwnerlist.add(userroles.getUser()); 
				} 
				if(userroles.getUserRole()-1 == assigneeId){ 
					directAssignerlist.add(userroles.getUser()); 
				} 
				if(userroles.getUserRole()-1 == followersId){ 
					directFollowerlist.add(userroles.getUser()); 
				} 
				if(userroles.getUserRole()-1 == connectionId){ 
					directConnectionlist.add(userroles.getUser()); 
				} 
			}   

		}

		entity.setOwnerlist(ownerlist);
		entity.setAssigneelist(assignerlist);
		entity.setFollowerlist(followerlist);
		entity.setConnectionlist(connectionlist);

		entity.setDirectOwnerlist(directOwnerlist);
		entity.setDirectAssigneelist(directAssignerlist);
		entity.setDirectFollowerlist(directFollowerlist);
		entity.setDirectConnectionlist(directConnectionlist);

		return entity;
	}

}
