package com.uppstreymi.jibby.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.ActiveViewDTO;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBEmailSetting;
import com.uppstreymi.jibby.bean.JBUserEntity;

@SessionScoped
public class SessionModel implements Serializable {

	public static AtomicLong INSTANCE_COUNT = new AtomicLong(0);
    private boolean loggedIn,isExpandColC;
	private JBUserEntity user;
	private String userStatus;
	
	private JBEmailSetting emailSetting;
	private JBAttachmentEntity attachment;
	JBCustomerEntity customer;
	
	private ActiveViewDTO activeViewDTO;
	private Logger logger=LogManager.getLogger(SessionModel.class);
	
	@PostConstruct
	public void onNewSession(){
		
		INSTANCE_COUNT.incrementAndGet();
		logger.info("session created {}.",INSTANCE_COUNT.get());
	}

    @PreDestroy
    public void onSessionDestruction(){
        INSTANCE_COUNT.decrementAndGet();
        logger.info("session detstroy{}.",INSTANCE_COUNT.get());
    }
    
    public void setCustomer(JBCustomerEntity customer) {
    	this.customer=customer;
    }
    
    public JBCustomerEntity getCustomer() {
    	return this.customer;
    }

	public void setUser(JBUserEntity user){
		this.user=user;
	}
	
	public JBUserEntity getUser(){
		return user;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	public void setEmailSetting(JBEmailSetting emailSetting){
		this.emailSetting=emailSetting;
	}
	public JBEmailSetting getEmailSetting(){
		return emailSetting;
	}
	
	public void setAttachment(JBAttachmentEntity attachment){
		this.attachment=attachment;
	}
	public JBAttachmentEntity getAttachment(){
		return attachment;
	}
	
	public void setActiveViewDTO(ActiveViewDTO activeViewDTO){
		this.activeViewDTO=activeViewDTO;
	}
	public ActiveViewDTO getActiveViewDTO(){
		return this.activeViewDTO;
	}
	
	public void setIsExpandColC(boolean isExpandColC) {
		this.isExpandColC = isExpandColC;
	}
	
	public boolean isExpandColC() {
		return this.isExpandColC;
	}
	
	public void setUserStatus(String userStatus) {
		this.userStatus=userStatus;
	}
	
	public String getUserStatus() {
		return this.userStatus;
	}

	private List<AppViewModelChangeListener> listeners = null;

	public interface AppViewModelChangeListener {
		public void modelChanged(AppViewModelChangeEvent event);
	}

	public class AppViewModelChangeEvent {
		public AppViewModelChangeEvent() {
		}
	}

	public void fireModelChangedEvent() {
		if (listeners != null) {
			AppViewModelChangeEvent event = new AppViewModelChangeEvent();
			for (AppViewModelChangeListener listener : listeners) {
				listener.modelChanged(event);
			}
		}
	}

	public void addAppViewModelChangeListener(AppViewModelChangeListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<AppViewModelChangeListener>();
		}
		listeners.add(listener);
	}

	public void removeAppViewModelModelChangeListener(AppViewModelChangeListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<AppViewModelChangeListener>();
		}
		listeners.remove(listener);
	}

}
