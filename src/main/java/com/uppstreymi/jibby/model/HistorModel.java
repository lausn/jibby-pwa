package com.uppstreymi.jibby.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.vaadin.cdi.annotation.UIScoped;

@UIScoped
public class HistorModel {

	private Stack<Integer>stages =new Stack<>();
	private HashMap<Integer,ItemBean> hmEntity=new HashMap<>();
	private HashMap<Integer,List<ItemBean>> hmChildEntities = new HashMap<>();
	private List<Integer> parentArray = new ArrayList<>();
	private Integer addItemParent;
	private Integer chatId;
	private String chatText;
	private boolean selectedItemHasChild;
	private JBEntity entity;
	
	public Stack<Integer> getStages(){
		return this.stages;
	}
	
	public HashMap<Integer,ItemBean> getItemBean(){
		return this.hmEntity;
	}
	
	public HashMap<Integer,List<ItemBean>> getEntities(){
		return this.hmChildEntities;
	}
	
	public void setEntitiesByKey(Integer key,List<ItemBean> entities){
		hmChildEntities.put(key, entities);
	}
	
	public List<ItemBean> getEntitiesByKey(Integer key){
		return hmChildEntities.get(key);
	}
	
	public void removeEntitiesKey(Integer key){
		hmChildEntities.remove(key);
	}

	public List<Integer> getParentArray(){
		return this.parentArray;
	}

	public void setAddItemParentId(Integer addItemParent) {
		this.addItemParent =addItemParent;
	}
	
	public Integer getAddItemParentId() {
		return this.addItemParent;
	}

	public void selectedChatId(Integer chatId) {
		this.chatId = chatId;
	}
	
	public Integer getSelectedChatId() {
		return this.chatId;
	}

	public void setSelectedChatText(String chatText) {
		this.chatText = chatText;
	}
	
	public String getSelectedChatText(){
		return this.chatText;
	}
	
	public void setSelectedItemHasChild(boolean selectedItemHasChild) {
		this.selectedItemHasChild=selectedItemHasChild;
	}
	
	public boolean getSelectedItemHasChild() {
		return this.selectedItemHasChild;
	}
	
	public void setSelectedEntity(JBEntity entity) {
		this.entity=entity;
	}
	
	public JBEntity getSelectedEntity() {
		return this.entity;
	}
}
