package com.uppstreymi.jibby.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.uppstreymi.jibby.bean.ItemBean;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.SortCriteria;
import com.vaadin.cdi.annotation.UIScoped;

@UIScoped
public class ProjectViewModel {

	private JBEntity drawerActiveProject;
	private List<Integer> userRole;
	private List<Integer> status; // not done
	private List<Integer> types;
	private Integer startType;
	private LocalDate startDate;
	private Integer finishType;
	private LocalDate finishDate;
	private ItemBean itemBean;
	private SortCriteria sort;
	private Integer isPublicItem=0; // public=1; private=0;
	private Integer navegateParentId;
	private SortCriteria projectsort;
	private Integer onHold = 0;

	public void setTypes(List<Integer> types){
		this.types=types;
	}
	public List<Integer> getTypes(){
		return types;
	}

	public void setStatus(List<Integer> status){
		this.status=status;
	}
	public List<Integer> getStatus(){
		return status;
	}
	
	public void setStartType(Integer startType){
		this.startType=startType;
	}
	public Integer getStartType(){
		return this.startType;
	}

	public void setStartDate(LocalDate startDate){
		this.startDate=startDate;
	}
	public LocalDate getStartDate(){
		return this.startDate;
	}

	public void setFinishType(Integer finishType){
		this.finishType=finishType;
	}
	public Integer getFinsihType(){
		return this.finishType;
	}

	public void setFinishDate(LocalDate finishDate){
		this.finishDate=finishDate;
	}
	public LocalDate getFinishDate(){
		return this.finishDate;
	}
	
	public void setUserRole(List<Integer> userRole){
		this.userRole=userRole;
	}
	public List<Integer> getUserRole(){
		return this.userRole;
	}
	
	public void setSortOption(SortCriteria sc){
		this.sort=sc;
	}
	public SortCriteria getSortOption(){
		return sort;
	}
	
	public void setProjectSortOption(SortCriteria sc){
		this.projectsort=sc;
	}
	public SortCriteria getProjectSortOption(){
		return projectsort;
	}

	public void setIsPublic(Integer isPublic){
		this.isPublicItem=isPublic;
	}
	public Integer isPublic(){
		return this.isPublicItem;
	}
	
	public void setItemBean(ItemBean itemBean) {
		this.itemBean=itemBean;
	}
	
	public ItemBean getItemBean() {
		return this.itemBean;
	}
	public void setDrawerActiveProject(JBEntity drawerActiveProject){
		this.drawerActiveProject=drawerActiveProject;
	}
	public JBEntity getDrawerActiveProject(){
		return this.drawerActiveProject;
	}
	
	public void setNavegateParentId(Integer navegateParentId) {
		this.navegateParentId=navegateParentId;
	}
	
	public Integer getNavegateParentId() {
		return this.navegateParentId;
	}
	
	public Integer getOnHold() {
		return this.onHold;
	}

	public void setOnHold(Integer onHold) {
		this.onHold = onHold;
	}
	/**end colA filter option **/


	public void changeModelEvent(){
		fireModelChangedEvent();
	}

	private List<ProjectViewModelChangeListener> listeners = null;

	public interface ProjectViewModelChangeListener {
		public void modelChanged(ProjectViewModelChangeEvent event);
	}

	public class ProjectViewModelChangeEvent {
		public ProjectViewModelChangeEvent() {
		}
	}

	protected void fireModelChangedEvent() {
		if (listeners != null) {
			ProjectViewModelChangeEvent event = new ProjectViewModelChangeEvent();
			for (ProjectViewModelChangeListener listener : listeners) {
				listener.modelChanged(event);
			}
		}
	}

	public void addProjectViewModelChangeListener(ProjectViewModelChangeListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<ProjectViewModelChangeListener>();
		}
		listeners.add(listener);
	}

	public void removeProjectViewModelModelChangeListener(ProjectViewModelChangeListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<ProjectViewModelChangeListener>();
		}
		listeners.remove(listener);
	}

}