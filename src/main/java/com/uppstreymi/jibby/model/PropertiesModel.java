package com.uppstreymi.jibby.model;

import java.util.List;

public class PropertiesModel {

	private boolean publicEmailSending;
	private boolean emailSendingDisabled;
	private Integer emailSendingTimeHour = 9;
	private Integer emailSendingTimeMinute = 30;
	private List<String> defultMail;
	private Integer myfirstjibby_projectid=0;
	private String jibbyappurl="";
	private String mobileappurl="";
	private String syncappurl="";

	
	public Integer getEmailSendingTimeHour() {
		return this.emailSendingTimeHour;
	}

	public void setEmailSendingTimeHour(Integer emailSendingTimeHour) {
		this.emailSendingTimeHour = emailSendingTimeHour;
	}

	public Integer getEmailSendingTimeMinute() {
		return this.emailSendingTimeMinute;
	}

	public void setEmailSendingTimeMinute(Integer emailSendingTimeMinute) {
		this.emailSendingTimeMinute = emailSendingTimeMinute;
	}

	public boolean getemailSendingDisabled() {
		return this.emailSendingDisabled;
	}

	public void setemailSendingDisabled(boolean emailSendingDisabled) {
		this.emailSendingDisabled = emailSendingDisabled;
	}
	
	public boolean getPublicEmailSending(){
		return this.publicEmailSending;
	}
	
	public void setPublicEmailSending(boolean publicEmailSending) {
		this.publicEmailSending = publicEmailSending;
	}
	
	public List<String> getDefultMail(){
		return this.defultMail;
	}
	
	public void setDefultMail(List<String> defultMail) {
		this.defultMail = defultMail;
	}
	
	public void setMyfirstjibbyProjectId(Integer str) {
		this.myfirstjibby_projectid=str;
	}
	
	public Integer getMyfirstjibbyProjectId() {
		return this.myfirstjibby_projectid;
	}
	
	public void setJibbyAppUrl(String url) {
		this.jibbyappurl=url;
	}
	
	public String getJibbyAppUrl() {
		return this.jibbyappurl;
	}
	
	public void setMobileAppUrl(String url) {
		this.mobileappurl=url;
	}
	
	public String getMobileAppUrl() {
		return this.mobileappurl;
	}
	
	public void setSyncAppUrl(String url) {
		this.syncappurl=url;
	}
	
	public String getSyncAppUrl() {
		return this.syncappurl;
	}

}
