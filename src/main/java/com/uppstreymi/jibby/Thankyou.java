package com.uppstreymi.jibby;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@WebServlet(urlPatterns = {"/thanks","/thanks/*"})

public class Thankyou extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Logger logger=LogManager.getLogger(Thankyou.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		try {
			String html= IOUtils.toString((InputStream)getClass().getClassLoader().getResourceAsStream("htmlpage/thankyou.html"), "UTF-8");
			  //  enter code here
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy");
			String year = format.format(date);
			html=html.replace("####", year);
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.println(html);
			writer.flush();
			
		} catch (Exception e) {
			logger.error("File reading error in thankyou page",e);
		}

	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	@Override
	public void init(ServletConfig config) throws ServletException {

	}

	@Override
	public void destroy() {

	}

}