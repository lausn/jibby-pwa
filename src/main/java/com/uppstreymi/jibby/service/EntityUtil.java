package com.uppstreymi.jibby.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EntityUtil {

	private static Logger logger=LogManager.getLogger(EntityUtil.class);
	public static Object[] getNativeRec(EntityManager em,String sql){
		Object[] result=(Object[])em.createNativeQuery(sql).getSingleResult();
		return result;
	}
	public static List<?> getMoreNativeRec(EntityManager em,String sql){
		List<?> result=em.createNativeQuery(sql).getResultList();
		return result;
	}
	public static List<Object[]> getMoreNativeObjectRec(EntityManager em,String sql){
		List<Object[]> result=em.createNativeQuery(sql).getResultList();
		return result;
	}
	public static List<Integer> getMoreNativeIntegerRec(EntityManager em,String sql){
		List<Integer> result=em.createNativeQuery(sql).getResultList();
		return result;
	}
	public static List<String> getMoreNativeStringRec(EntityManager em,String sql){
		List<String> result=em.createNativeQuery(sql).getResultList();
		return result;
	}
	public static void insertRec(EntityManager em, Object entity) throws Exception {
		em.persist(entity);
		em.flush();
	}
	
	public static Object createAndInsertRec(EntityManager em, Class entityClass) throws Exception {
		Object o = null;
		o = entityClass.newInstance();
		em.persist(o);
		return o;
	}
	
	public static void updateRec(EntityManager em, String sql) throws Exception {
		/*entity = em.merge(entity);
		em.flush();*/
		em.createNativeQuery(sql).executeUpdate();
	}
	
	public static void deleteRec(EntityManager em, Object entity) throws Exception {
		// Merge to make entity managed by EntityManager, then we can remove (delete) it
		entity = em.merge(entity);
		em.remove(entity);
	}

	public static List getRows(EntityManager em, String entity, String query, List params, String orderBy) {
		String sql = "SELECT o FROM " + entity + " o";
		if (query != null && query.length() > 0)
			sql += " WHERE " + query;
		if (orderBy != null && orderBy.length() > 0)
			sql += " ORDER BY " + orderBy;
		Query qq = EntityUtil.buildQuery(em, sql, params.toArray());
		return qq.getResultList();
	}
	
	public static Object findOne(EntityManager em, String sql) {
		return findOne(em, sql, null);
	}
	
	public static Object findOne(EntityManager em, String sql, Object p1) {
		Object[] params = new Object[] { p1 };
		return findOne(em, sql, params);
	}
	
	public static Object findOne(EntityManager em, String sql, Object p1, Object p2) {
		Object[] params = new Object[] { p1, p2 };
		return findOne(em, sql, params);
	}
	
	public static Object findOne(EntityManager em, String sql, Object p1, Object p2, Object p3) {
		Object[] params = new Object[] { p1, p2, p3 };
		return findOne(em, sql, params);
	}
	
	public static Object findOne(EntityManager em, String sql, Object[] params) {
		Object result = null;
		try {
			Query q = buildQuery(em, sql, params);
			result = q.getSingleResult();
		}
		catch (NoResultException nrex) {
			logger.error("An NoResultException thrown in findOne in EntityUtil",nrex);
			result = null;
		}
		return result;
	}
	
	public static Object findTopOne(EntityManager em, String sql, Object[] params) {
		Object result = null;
		try {
			Query q = buildQuery(em, sql, params);
			q.setMaxResults(1);
			q.getFirstResult();
			result = q.getSingleResult();
		}
		catch (NoResultException nrex) {
			logger.error("An NoResultException thrown in findTopOne in EntityUtil",nrex);
			result = null;
		}
		return result;
	}
	
	public static List findMany(EntityManager em, String sql, Object[] params) {
		List result = null;
		try {
			Query q = buildQuery(em, sql, params);
			result = q.getResultList();
		}
		catch (NoResultException nrex) {
			logger.error("An NoResultException thrown in findMany in EntityUtil",nrex);
			result = null;
		}
		return result;
	}
	public static List findMany(EntityManager em, String sql, Object[] params,int topRow) {
		List result = null;
		try {
			Query q = buildQuery(em, sql, params);
			q.setMaxResults(topRow);
			result = q.getResultList();
		}
		catch (NoResultException nrex) {
			logger.error("An NoResultException thrown in findMany in EntityUtil",nrex);
			result = null;
		}
		return result;
	}
	
	public static List findMany(EntityManager em, String sql, Object p1) {
		Object[] params = new Object[] { p1 };
		return findMany(em, sql, params);
	}
	
	public static List getAll(EntityManager em, String entityClass, String orderBy) {
		if (orderBy == null)
			return findMany(em, "SELECT o FROM " + entityClass + " o", null);
		else
			return findMany(em, "SELECT o FROM " + entityClass + " o ORDER BY o." + orderBy, null);
	}

	public static Query buildQuery(EntityManager em, String sql, Object[] params) {
		Query q = em.createQuery(sql);
		if (params != null) {
			for (int pno = 0; pno < params.length; pno++)
				q.setParameter(pno + 1, params[pno]);
		}
		return q;
	}

}
