package com.uppstreymi.jibby.service;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.model.AppModel;
import com.uppstreymi.jibby.model.PropertiesModel;
import com.uppstreymi.jibby.utils.Utils;


@Singleton
@LocalBean
@Startup
public class JibbyProperties {

	@Inject private AppModel appModel;
	Logger logger=LogManager.getLogger(JibbyProperties.class);
	private PropertiesModel propsModel = new PropertiesModel();

	Properties props = new Properties();
	private long timeStamp;
	private boolean isUpdate;

	File file;

	@PostConstruct
	public void JibbyProperties() {
		createFile();
	}
	
	public void createFile() {

		props = new java.util.Properties();

		String fileName = "jibby.properties";
		String path = Utils.INSTANCE_ROOT;

		path = path + File.separator + "config" + File.separator;

		file = new File(path + fileName);
		
		if (!file.exists()) {
			writeFile(file);
		}
		if(file.exists())
			readFile();
	}

	public void writeFile(File file){
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			props.setProperty("jibby.email.sending.disabled", "false");		//default property set
			props.setProperty("jibby.email.sending.public", "false");		//default property set
			props.setProperty("jibby.email.sending.defaultmail", "hordur@lausn.is,helgi@market.is,bjaddt@gmail.com,raasaddev@gmail.com");		//default property set
			props.setProperty("myfirstjibby_projectid", "748646");
			props.setProperty("jibby.app.url", "https://app.jibby.com");
			props.setProperty("jibby.mobile.app.url", "https://app.jibby.com/jibby-pwa");
			props.setProperty("jibby.sync.app.url", "https://app.jibby.com/jibby-sync");
			props.store(fos, null);

		} catch (Exception e) {
			logger.error("An exception thrown in writeFile in jibbyproperties",e);
		}
	}
	
	
	@Schedule(minute = "*/01", hour = "*", persistent = false)
	public void chckIsUpdate() {
		
		if (file.exists()) {
			isUpdate = isFileUpdated(file);
			logger.info("jibby.properties file found and need to read? {}",isUpdate);
			if (isUpdate)
				readFile();
		} else{
			logger.info("not file found");
			createFile();
		}
	}

	private boolean isFileUpdated(File file) {

		long timeStamp = file.lastModified();

		if (this.timeStamp != timeStamp) {

			this.timeStamp = timeStamp;
			// Yes, file is updated
			return true;
		}
		// No, file is not updated
		return false;
	}
	
	public void readFile() {

		FileInputStream fis = null;
		try {

			fis = new FileInputStream(file);
			props = new java.util.Properties();
			props.load(fis);
			Enumeration<?> e = props.propertyNames();
			while (e.hasMoreElements()) {

				String key = (String) e.nextElement();
				String value = props.getProperty(key);

				if (key.equals("jibby.email.sending.disabled"))
					propsModel.setemailSendingDisabled(Boolean.valueOf(value));
				if (key.equals("jibby.email.sending.public"))
					propsModel.setPublicEmailSending(Boolean.valueOf(value));
				if (key.equals("jibby.email.sending.defaultmail"))
					propsModel.setDefultMail(new ArrayList<String>(Arrays.asList(String.valueOf(value).split(","))));
				if (key.equals("myfirstjibby_projectid"))
					propsModel.setMyfirstjibbyProjectId(Integer.valueOf(value));
				if (key.equals("jibby.app.url"))
					propsModel.setJibbyAppUrl(String.valueOf(value));
				if (key.equals("jibby.mobile.app.url"))
					propsModel.setMobileAppUrl(String.valueOf(value));
				if (key.equals("jibby.sync.app.url"))
					propsModel.setSyncAppUrl(String.valueOf(value));

			}
			appModel.setPropertiesModel(propsModel);

		} catch (Exception e) {
			logger.error("An exception thrown in readFile in jibbyproperties",e);
		}
	}
}
