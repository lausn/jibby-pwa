package com.uppstreymi.jibby.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.mail.smtp.SMTPTransport;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.utils.Utils;
import com.vaadin.cdi.annotation.UIScoped;

@UIScoped
public class MailgunService {

	/*public static String HOST = "smtp.mailgun.org";
	public static String USER = "postmaster@sandboxab2ce6928971437587b87e7a7f348dab.mailgun.org";
	public static String PASSWORD = "a7793efe24a3fecf6659f29cdd08cdf3";*/

	public static String HOST = "smtp.mailgun.org";
	public  String USER = "postmaster@mg.jibby.com";
	public  String PASSWORD = "4e13d79a3f37eb91a058c25165eac301";

	//public static String USER = "postmaster@mg.lausn.is";
	//public static String PASSWORD = "1b4f62e4f5bdd5944f67be1dcbd43527";

	Logger logger=LogManager.getLogger(MailgunService.class);

	public SMTPTransport sendSMTPMail(String fromAddress, String fromName, List<String> emailList, String toName, String subject, String cc, String bcc, String text, String html, String calendar,List<JBAttachmentEntity>attachments,String replyTo,JBEntity entity) throws UnsupportedEncodingException, MalformedURLException{
		return send(fromAddress, fromName,  emailList,  toName,  subject,  cc,  bcc,  text,  html,  calendar,attachments, replyTo, entity);
	}
	public SMTPTransport sendSMTPMail(String fromAddress, String fromName, List<String> emailList, String toName, String subject, String cc, String bcc, String text, String html, String calendar,List<JBAttachmentEntity>attachments) throws UnsupportedEncodingException, MalformedURLException{
		return send(fromAddress, fromName,  emailList,  toName,  subject,  cc,  bcc,  text,  html,  calendar,attachments, null, null);
	}

	public SMTPTransport send(String fromAddress, String fromName, List<String> emailList, String toName, String subject, String cc, String bcc, String text, String html, String calendar,List<JBAttachmentEntity>attachments,String replyTo,JBEntity entity) throws UnsupportedEncodingException, MalformedURLException{
		Properties props = System.getProperties();
		props.put("mail.smtps.host", HOST);
		props.put("mail.smtps.auth", "true");
		String attendee="";
		try {
			Session session = Session.getInstance(props, null);
			MimeMessage msg = new MimeMessage(session);

			msg.setSentDate(new Date());

			if (fromAddress != null && fromName != null)
				msg.setFrom(new InternetAddress(fromAddress, fromName, "utf-8"));

			if (emailList != null && toName != null){
				List<InternetAddress> addresses=new ArrayList<>();
				for(String email:emailList){
					if(calendar!=null)
						attendee+="ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:"+email+"\n" ;

					addresses.add(new InternetAddress(email));

				}

				msg.setRecipients(Message.RecipientType.TO, addresses.toArray(new InternetAddress[addresses.size()]));
			}

			if(replyTo !=null){
				msg.setReplyTo(new InternetAddress[]{new InternetAddress(replyTo)});
			}


			if (cc != null)
				msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));

			if (bcc != null)
				msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));

			if (subject != null)
				msg.setSubject(subject, "utf-8");

			// Create the message
			Multipart mixedMultiPart = new MimeMultipart("mixed");
			Multipart alternativeMultiPart = new MimeMultipart("alternative");

			MimeBodyPart alternativeBodyPart = new MimeBodyPart();
			alternativeBodyPart.setContent(alternativeMultiPart);

			// Insert text content
			if (text != null && text != "") {
				BodyPart textBodyPart = new MimeBodyPart();
				textBodyPart.setContent(text, "text/plain; charset=\"utf-8\"");
				alternativeMultiPart.addBodyPart(textBodyPart);
			}

			// Insert HTML content
			if (html != null && html != "") {
				BodyPart htmlBodyPart = new MimeBodyPart();
				htmlBodyPart.setContent(html, "text/html; charset=\"utf-8\"");
				alternativeMultiPart.addBodyPart(htmlBodyPart);
			}


			if(calendar !=null){
				BodyPart messageBodyPart = new MimeBodyPart();
				calendar=calendar.replace("#content#", attendee);
				// Fill the message
				messageBodyPart.addHeader("Content-Class", "urn:content-classes:calendarmessage");
				messageBodyPart.setContent(calendar, "text/calendar;method=REQUEST");
				alternativeMultiPart.addBodyPart(messageBodyPart);
			}

			mixedMultiPart.addBodyPart(alternativeBodyPart);
			// Append attachments
			if (attachments !=null && attachments.size()>0) {

				for(JBAttachmentEntity attachment: attachments){

					String path = Utils.imagePath+attachment.getId();
					MimeBodyPart mbp3 = new MimeBodyPart();
					mbp3.setDisposition(javax.mail.Part.ATTACHMENT);
					File f = new File(path);
					if(f.exists()){
						DataSource source = new FileDataSource(path);
						mbp3.setDataHandler(new DataHandler(source));
						mbp3.setFileName(attachment.getName());
						mixedMultiPart.addBodyPart(mbp3);
					}	
				}
			}

			msg.setContent(mixedMultiPart);

			SMTPTransport t = (SMTPTransport) session.getTransport("smtps");
			t.connect(HOST, USER, PASSWORD);
			if(entity !=null){
				msg.saveChanges();
				String mesgId ="<"+entity.getParentProjectId()+"."+entity.getId()+"."+System.currentTimeMillis()+"@mg.jibby.com>";
				msg.setHeader("Message-Id",mesgId);
			}
			t.sendMessage(msg, msg.getAllRecipients());
			t.close();

			return t;
		} catch (AddressException e) {
			logger.error("An AddressException thrown in MailgunService",e);
			//JibbyNotification.showWarningMessage("mail not send successfully! "+ e.getMessage());
		} catch (MessagingException e) {
			//e.printStackTrace();
			logger.error("An MessagingException thrown in MailgunService",e);
			//JibbyNotification.showWarningMessage("mail not send successfully! "+ e.getMessage());
		}
		return null;
	}
}
