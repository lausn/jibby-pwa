package com.uppstreymi.jibby.service;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.uppstreymi.jibby.bean.JBEntity;


public class HttpMailgunService {

	//public static String API_BASE_URL = "https://api.mailgun.net/v3/sandboxab2ce6928971437587b87e7a7f348dab.mailgun.org/messages";
	//public static String API_KEY = "key-d6876610b5ebc5c54623784e0d088b5b";

	public static String API_BASE_URL = "https://api.mailgun.net/v3/mg.jibby.com/messages";
	public static String API_KEY = "key-4e72e66b391b240ba565be60da9a39fe";
	Logger logger=LogManager.getLogger(HttpMailgunService.class);

	public Integer sendHTTPMail(String fromAddress, String fromName, String toAddress, 
			String toName, String subject, String cc, String bcc, 
			String text, String html, String attachmentURLs,List<String> toAddressList,String replyTo,JBEntity entity){
		File file = new File(attachmentURLs);
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("from", fromName+" "+fromAddress);


		if(toAddressList==null)
			parameters.put("to", toName+" "+toAddress);
		else {
			String allAddress = "";
			for(String address: toAddressList) {
				if(!allAddress.isEmpty())
					allAddress+=","+address;
				else
					allAddress+=address;
			}
			parameters.put("to", allAddress);
		}

		if(!cc.isEmpty())
			parameters.put("cc", cc);

		if(!bcc.isEmpty())
			parameters.put("bcc", bcc);

		parameters.put("subject", subject);

		if(!text.isEmpty())
			parameters.put("text", text);
		if(!replyTo.isEmpty())
			parameters.put("h:Reply-To", replyTo);
		
		if(entity !=null){
			String mesgId = entity.getParentProjectId()+"."+entity.getId()+"."+System.currentTimeMillis()+"@sandboxab2ce6928971437587b87e7a7f348dab.mailgun.org";
			parameters.put("h:message-id",mesgId);
		}
		
		try {
			if(!html.isEmpty())
				parameters.put("html",html.trim());
			if(file.exists())
				parameters.put("attachment", file);
			
			Unirest.setHttpClient(createSSLClient());
			HttpResponse<JsonNode> request = null;

			request=Unirest.post(API_BASE_URL)
					.basicAuth("api", API_KEY)
					.fields(parameters)
					.asJson();
			
			return request.getStatus();
		}catch(Exception e) {
			logger.error("An exception thrown in sendHTTPMail in HttpMailgunService",e);
			return null;
		}
	}

	private CloseableHttpClient createSSLClient() {
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			@Override
			public boolean isTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub
				return true;
			}
		};

		SSLContext sslContext = null;
		try {
			sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		} catch (Exception e) {
			logger.error("An exception thrown in CloseableHttpClient in HttpMailgunService",e);
		}
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
				SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		return HttpClients.custom().setSSLSocketFactory(sslsf).build();
	}
}
