package com.uppstreymi.jibby.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uppstreymi.jibby.bean.JBActionlist;
import com.uppstreymi.jibby.bean.JBActionlistHistory;
import com.uppstreymi.jibby.bean.JBAttachmentEntity;
import com.uppstreymi.jibby.bean.JBChatMessageEntity;
import com.uppstreymi.jibby.bean.JBChecklistItemEntity;
import com.uppstreymi.jibby.bean.JBClientAppsInfoEntity;
import com.uppstreymi.jibby.bean.JBCreditcardsEntity;
import com.uppstreymi.jibby.bean.JBCustomerEntity;
import com.uppstreymi.jibby.bean.JBCustomerUsersEntity;
import com.uppstreymi.jibby.bean.JBEmailSetting;
import com.uppstreymi.jibby.bean.JBEntity;
import com.uppstreymi.jibby.bean.JBFavouritesEntity;
import com.uppstreymi.jibby.bean.JBGoogleDriveEntity;
import com.uppstreymi.jibby.bean.JBInvitation;
import com.uppstreymi.jibby.bean.JBInvoicesEntity;
import com.uppstreymi.jibby.bean.JBItemModification;
import com.uppstreymi.jibby.bean.JBKanbanBoardColumns;
import com.uppstreymi.jibby.bean.JBLicencesEntity;
import com.uppstreymi.jibby.bean.JBLicencesHistory;
import com.uppstreymi.jibby.bean.JBLicenseInvitation;
import com.uppstreymi.jibby.bean.JBMessages;
import com.uppstreymi.jibby.bean.JBPaidUserEntity;
import com.uppstreymi.jibby.bean.JBPayment;
import com.uppstreymi.jibby.bean.JBSubscriptionsEntity;
import com.uppstreymi.jibby.bean.JBUserEntity;
import com.uppstreymi.jibby.bean.JBUserRoleEntity;
import com.uppstreymi.jibby.bean.OrderNumber;
import com.uppstreymi.jibby.login.UserType;
import com.uppstreymi.jibby.repository.JBActionlistHistoryRepository;
import com.uppstreymi.jibby.repository.JBActionlistRepository;
import com.uppstreymi.jibby.repository.JBAttachmentEntityRepository;
import com.uppstreymi.jibby.repository.JBChatMessageRepository;
import com.uppstreymi.jibby.repository.JBChecklistItemRepository;
import com.uppstreymi.jibby.repository.JBClientAppsInfoRepository;
import com.uppstreymi.jibby.repository.JBCreditcardsRepository;
import com.uppstreymi.jibby.repository.JBCustomerRepository;
import com.uppstreymi.jibby.repository.JBCustomerUsersRepository;
import com.uppstreymi.jibby.repository.JBEmailSettingRepository;
import com.uppstreymi.jibby.repository.JBEntityRepository;
import com.uppstreymi.jibby.repository.JBFavouritesRepository;
import com.uppstreymi.jibby.repository.JBGoogleDriveRepository;
import com.uppstreymi.jibby.repository.JBInvitationRepository;
import com.uppstreymi.jibby.repository.JBInvoicesRepository;
import com.uppstreymi.jibby.repository.JBItemModificationRepository;
import com.uppstreymi.jibby.repository.JBKanbanBoardColumnsRepository;
import com.uppstreymi.jibby.repository.JBLicenceInvitationRepository;
import com.uppstreymi.jibby.repository.JBLicencesHistoryRepository;
import com.uppstreymi.jibby.repository.JBLicencesRepository;
import com.uppstreymi.jibby.repository.JBMessagesRepository;
import com.uppstreymi.jibby.repository.JBPaidUserEntityRepository;
import com.uppstreymi.jibby.repository.JBPaymentRepository;
import com.uppstreymi.jibby.repository.JBSubscriptionsRepository;
import com.uppstreymi.jibby.repository.JBUserEntityRepository;
import com.uppstreymi.jibby.repository.JBUserRoleRepository;
import com.uppstreymi.jibby.utils.JibbyConfig;
import com.uppstreymi.jibby.utils.Utils;


@Stateless
@LocalBean
public class DbService {

	@PersistenceContext
	private EntityManager em;

	@Inject private JBEntityRepository jbEntityRepository;
	@Inject private JBChecklistItemRepository jbChecklistItemsRepository;
	@Inject private JBChatMessageRepository jbChatlistRepository;
	@Inject private JBUserEntityRepository jbUserEntityRepository;
	@Inject private JBAttachmentEntityRepository jbAttachmentEntityRepository;
	@Inject private JBEmailSettingRepository jbEmailSettingRepository ;
	@Inject private JBActionlistRepository jbActionlistRepository ;
	@Inject private JBActionlistHistoryRepository jbActionlistHistoryRepository ;
	@Inject private JBUserRoleRepository jbUserRoleRepository;
	@Inject private JBMessagesRepository jbMessagesRepository;
	@Inject private JBSubscriptionsRepository jbSubscriptionRepository;
	@Inject private JBLicencesRepository jbLicencesRepository;
	@Inject private JBLicencesHistoryRepository jbLicencesHistoryRepository;
	@Inject private JBItemModificationRepository jbItemModificationRepository;
	@Inject private JBKanbanBoardColumnsRepository jbKanbanRepository;
	@Inject private JBInvitationRepository jbInvitationRepository;
	@Inject private JBPaymentRepository jbPaymentRepository;
	@Inject private JBClientAppsInfoRepository jbClientAppsInfoRepository;
	@Inject private JBPaidUserEntityRepository jbPaidUserRepository;
	@Inject private JBCreditcardsRepository jbCreditcardRepository;
	@Inject private JBInvoicesRepository jbInvoicesRepository;
	@Inject private JBGoogleDriveRepository jbGoogleDriveRepository;
	@Inject private JBCustomerUsersRepository jbCustomerUsersRepository;
	@Inject private JBCustomerRepository jbCustomerRepository;
	@Inject private JBLicenceInvitationRepository jbLicenceInvitationRepository;
	@Inject private JBFavouritesRepository jbFavouriteRepository;

	private Logger logger=LogManager.getLogger(DbService.class);

	//Customers
	public List<JBCustomerUsersEntity> getCustomerAccess(Integer userId){
		return jbCustomerUsersRepository.findByUserid(userId).getResultList();
	}

	public List<JBCustomerEntity> getCustomersByUser(Integer userId) {
		return jbCustomerRepository.findByCustomerByUser(userId).getResultList();
	}

	public List<JBCustomerEntity> getCustomersByCustomerUser(List<Integer> customerIds) {
		return jbCustomerRepository.findByCustomerAccess(customerIds).getResultList();
	}

	public JBCustomerEntity getCustomerByGUID(String guid) throws Exception {
		try{
			JBCustomerEntity customer = jbCustomerRepository.findBySubscriptionguid(guid);
			return customer;
		}catch(Exception e){
			return null;
		}
	}

	public JBCustomerEntity saveCustomer(JBCustomerEntity customer){
		return jbCustomerRepository.save(customer);
	}

	public JBCustomerUsersEntity saveCustomerUser(JBCustomerUsersEntity customerUser){
		return jbCustomerUsersRepository.save(customerUser);
	}

	//GoogleDrive
	public JBGoogleDriveEntity saveGoogleDrive(JBGoogleDriveEntity googleDrive){
		googleDrive = jbGoogleDriveRepository.save(googleDrive);
		return googleDrive;
	}

	public List<JBGoogleDriveEntity> getGoogleDrives(){
		return jbGoogleDriveRepository.findAllOrderByCreatedateAsc().getResultList();
	}

	public List<JBGoogleDriveEntity> getGoogleDrivesByIsUpload(Integer isUploaded){
		return jbGoogleDriveRepository.findByIsUploadedOrderByUserId(isUploaded).getResultList();
	}

	public List<Object> getGoogleDrivesByUser(Integer isUploaded,Integer userId){
		List<Object> projectNames= jbEntityRepository.googleDriveProjectNames(userId,isUploaded).getResultList();
		return projectNames;
	}

	public JBGoogleDriveEntity getGoogleDriveByUserIdAndEntityId(Integer userId,Integer entityId){
		try {
			return jbGoogleDriveRepository.findByUserIdAndEntityId(userId,entityId);
		}catch(Exception e){
			return null;
		}
	}

	public List<Object> getGoogleDriveProjectName(Integer entityId) {
		List<Object> result= jbEntityRepository.projectNames(entityId).getResultList();
		return result;
	}

	public void deleteGoogleDrive(JBGoogleDriveEntity googleDrive) {
		jbGoogleDriveRepository.remove(jbGoogleDriveRepository.findBy(googleDrive.getId()));
	}

	// Login Check
	public JBUserEntity logIn(String userName, String password) {
		try{
			JBUserEntity user = jbUserEntityRepository.findByEmailAndPassword(userName,Utils.sha1(password));
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public JBUserEntity getUserByEmail(String email) throws Exception {
		try{
			JBUserEntity user = jbUserEntityRepository.findByEmail(email);
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public JBUserEntity getUserByEmailAndTempguid(String email,String guid) throws Exception {
		try{
			JBUserEntity user = jbUserEntityRepository.findByEmailAndTempguid(email,guid);
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public JBUserEntity getUserByGUID(String guid) throws Exception {
		try{
			JBUserEntity user = jbUserEntityRepository.findByGuid(guid);
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public JBUserEntity saveUser(JBUserEntity user){
		return jbUserEntityRepository.save(user);
	}

	public JBUserEntity getUserById(Integer id){
		JBUserEntity user = jbUserEntityRepository.findBy(id);
		return user;
	}

	public JBPaidUserEntity getPaidUserById(Integer id){
		JBPaidUserEntity user = jbPaidUserRepository.findBy(id);
		return user;
	}

	public JBPaidUserEntity getPaidUserByEmail(String email){
		try{
			JBPaidUserEntity user = jbPaidUserRepository.findByEmail(email);
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public JBPaidUserEntity savePaidUser(JBPaidUserEntity user){
		return jbPaidUserRepository.save(user);
	}

	public void deletePaidUser(JBPaidUserEntity user) {
		jbPaidUserRepository.remove(jbPaidUserRepository.findBy(user.getId()));
	}

	public JBUserEntity getUserByIdAndUnfinishedActivities(Integer id,List<Integer> unfinishedActivities){
		try {
			JBUserEntity user = jbUserEntityRepository.findByUserIdAndUnfinishedActivities(id,unfinishedActivities);
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public List<JBUserEntity> getAllUser(){
		return jbUserEntityRepository.findAll();
	}

	public List<JBUserRoleEntity> getAssignedUserListByEntity(List<Integer>parentArray) {
		List<JBUserRoleEntity> userRole=jbUserRoleRepository.findAllUserbyEntity(parentArray).getResultList();
		return userRole;
	}

	public List<JBUserRoleEntity> getAllUserRolesByEntityId(List<Integer> parentArray) {
		List<JBUserRoleEntity> userRoles=jbUserRoleRepository.findByEntityIdOrderByUserRole(parentArray).getResultList();
		return userRoles;
	}
	public List<JBUserRoleEntity> getAllUserRolesByEntityAndUser(List<Integer> array,JBUserEntity user) {
		List<JBUserRoleEntity> userRoles=jbUserRoleRepository.findByEntityIdAndUserId(user,array).getResultList();
		return userRoles;
	}


	public List<JBUserRoleEntity> getAllUserRolesByEntityAndUserWithRoles(Integer entityId,JBUserEntity user,List<Integer> roles){
		List<JBUserRoleEntity> userRoleEntities=jbUserRoleRepository.findByEntityIdAndUserWithRolesOrderByUserRole(entityId, user, roles).getResultList();
		return userRoleEntities;
	}

	public List<JBUserRoleEntity> getAllUserRolesByUserWithRoles(JBUserEntity user,List<Integer> roles){
		List<JBUserRoleEntity> userRoleEntities=jbUserRoleRepository.findByUserWithRolesOrderByUserRole( user, roles).getResultList();
		return userRoleEntities;
	}


	public List<JBUserRoleEntity> getAllUserRolesByEntityAndUsersAndRoles(Integer entityId,Integer roles,Set<JBUserEntity> users){
		List<JBUserRoleEntity> userRoleEntities=jbUserRoleRepository.findByEntityIdAndUsersAndRoles(entityId,roles,users).getResultList();
		return userRoleEntities;
	}

	public List<JBUserRoleEntity> getUserRolesByEntity(Integer entityId){
		List<JBUserRoleEntity> userRoleEntities=jbUserRoleRepository.findByEntityId(entityId).getResultList();
		return userRoleEntities;
	}

	public JBUserRoleEntity getTop1ByUserRolesAndEntity(Integer entityId){
		JBUserRoleEntity userRoleEntities=jbUserRoleRepository.findTop1ByEntityIdAndUserRole(entityId,1);
		return userRoleEntities;
	}

	public JBUserRoleEntity saveUserRoleEntity(JBUserRoleEntity userRole) {
		jbUserRoleRepository.save(userRole);
		return userRole;
	}
	public void removerUserRoleEntity(JBUserRoleEntity userroles) {
		jbUserRoleRepository.remove(jbUserRoleRepository.findBy(userroles.getId()));
	}

	public List<JBEntity> getAllItemByParentId(Integer parentId) throws Exception {
		List<JBEntity> entities=jbEntityRepository.findByparentProjectId(parentId).getResultList();
		return entities;
	}

	public List<JBEntity> getAllEntities() throws Exception {
		List<JBEntity> entities=jbEntityRepository.findAll();
		return entities;
	}
	public List<JBEntity> getAllEntitiesByIds(List<Integer> id) {
		List<JBEntity> entities=jbEntityRepository.findByEntityId(id).getResultList();
		return entities;
	}

	public List<JBEntity> getAllEntitiesByIdsAndStatus(List<Integer> id,List<Integer> status) {
		List<JBEntity> entities=jbEntityRepository.findByEntityIdAndStatus(id,status).getResultList();
		return entities;
	}

	public List<JBEntity> getAllEntitiesByIdsAndProjectId(List<Integer> id,Integer parentProjectId) {
		List<JBEntity> entities=jbEntityRepository.findByEntityIdAndParentProjectId(id,parentProjectId).getResultList();
		return entities;
	}
	public List<JBEntity> getAllChildByEntityId(Integer entityId) {
		List<JBEntity> entities=jbEntityRepository.findByParentId(entityId).getResultList();
		return entities;
	}
	public List<Object[]> getAllEntitiesByParent(Integer parentId) {
		List<Object[]> entities=jbEntityRepository.findByAllParentId(parentId).getResultList();
		return entities;
	}
	public List<JBEntity> getAllEntitiesByParentId(Integer parentId) {
		List<JBEntity> entities=jbEntityRepository.findByParentId(parentId).getResultList();
		return entities;
	}

	public List<Object[]> getAllEntitiesByParents(List<Integer> parentId) {
		List<Object[]> entities=jbEntityRepository.findByParentIds(parentId).getResultList();
		return entities;
	}

	public List<Object[]> getAllEntitiesByParentAndStatusOrderBy(Integer parentId,JBUserEntity user,String order,Boolean sort) {
		if(sort){
			List<Object[]> entities=jbEntityRepository.findByParentIdAndStatusOrderBy(parentId).orderAsc(order, false).getResultList();
			return entities;
		}
		else{
			List<Object[]> entities=jbEntityRepository.findByParentIdAndStatusOrderBy(parentId).orderDesc(order, false).getResultList();
			return entities;
		}

	}

	public List<JBEntity> getAllEntitiesByParentOrderBy(Integer parentId,String order,Boolean sort) {
		if(sort){
			List<JBEntity> entities=jbEntityRepository.findByParentIdOrderBy(parentId).orderAsc(order, false).getResultList();
			return entities;
		}
		else{
			List<JBEntity> entities=jbEntityRepository.findByParentIdOrderBy(parentId).orderDesc(order, false).getResultList();
			return entities;
		}

	}

	public Object[] getIsChild(Integer parentId) {
		Object[]  entities=jbEntityRepository.getIsChild(parentId);
		return entities;
	}

	public List<JBEntity> getAllEntitiesByUser(Integer id){
		List<JBEntity> entities=jbEntityRepository.findByCreatorAndParentIdIsNullOrderByNameAsc(id).getResultList();
		return entities;
	}

	public List<JBEntity> getAllProjectForColA(JBUserEntity user,List<Integer> publicroles,String order,Boolean sort,List<Integer> userRoles,List<Integer> customerIds){
		if(sort){
			List<JBEntity> entities=jbEntityRepository.findColAProject(user,publicroles,userRoles,customerIds).orderAsc(order, false).getResultList();
			return entities;
		}else{
			List<JBEntity> entities=jbEntityRepository.findColAProject(user,publicroles,userRoles,customerIds).orderDesc(order, false).getResultList();
			return entities;
		}
	}

	public JBEntity saveEntity(JBEntity entity) {
		return jbEntityRepository.save(entity);
	}

	public void deleteEntity(JBEntity entity) {
		jbEntityRepository.remove(jbEntityRepository.findBy(entity.getId()));
	}

	public JBEntity getEntity(Integer id){
		return jbEntityRepository.findBy(id);
	}

	// checklist items
	public List<JBChecklistItemEntity> getChecklistByEntity(Integer id){
		List<JBChecklistItemEntity> checklist=jbChecklistItemsRepository.findByEntity(id).getResultList();
		return checklist;
	}

	public List<JBChecklistItemEntity> getAllChecklistByEntitys(List<Integer> itemids){
		List<JBChecklistItemEntity> checklist=jbChecklistItemsRepository.findAllChecklistByEntitys(itemids).getResultList();
		return checklist;
	}

	public List<JBChecklistItemEntity> getAllDoneOrUndoneChecklistByEntity(Integer id,Integer isChecked){
		List<JBChecklistItemEntity> checklist=jbChecklistItemsRepository.findByEntityAndIsChecked(id,isChecked).getResultList();
		return checklist;
	}
	public JBChecklistItemEntity saveChecklistItems(JBChecklistItemEntity checklist) {
		jbChecklistItemsRepository.save(checklist);
		return checklist;
	}
	public void deleteCheckItem(JBChecklistItemEntity checklistItemEntity) {
		jbChecklistItemsRepository.remove(jbChecklistItemsRepository.findBy(checklistItemEntity.getId()));
	}

	//Email Settings Data
	public JBEmailSetting saveEmailSettings(JBEmailSetting emailSetting) {
		jbEmailSettingRepository.save(emailSetting);
		return emailSetting;
	}

	public JBEmailSetting getEmailSettingByUser(JBUserEntity user){
		try{
			JBEmailSetting emailSetting = jbEmailSettingRepository.findByUser(user.getId());
			return emailSetting;
		}
		catch(Exception e){
			return null;
		}
	}

	public JBEmailSetting getEmailSettingByUserId(Integer userId){
		try{
			JBEmailSetting emailSetting = jbEmailSettingRepository.findByUser(userId);
			return emailSetting;
		}
		catch(Exception e){
			return null;
		}

	}

	//Attachment Data
	public List<JBAttachmentEntity> getAttachmentByEntity(Integer id){
		List<JBAttachmentEntity> attachment=jbAttachmentEntityRepository.findByEntity(id).getResultList();
		return attachment;
	}

	public JBAttachmentEntity getAttachmentById(Integer id){
		JBAttachmentEntity attachment=jbAttachmentEntityRepository.findBy(id);
		return attachment;
	}

	public List<JBAttachmentEntity> getAllAttachmentsByIds(List<Integer> ids){
		List<JBAttachmentEntity> attachments=jbAttachmentEntityRepository.findByEntityId(ids).getResultList();
		return attachments;
	}

	public boolean deleteAttachment(JBAttachmentEntity attachment) {
		try {
			jbAttachmentEntityRepository.remove(jbAttachmentEntityRepository.findBy(attachment.getId()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public JBAttachmentEntity saveAttachment(JBAttachmentEntity attachment) {
		jbAttachmentEntityRepository.save(attachment);
		return attachment;
	}

	// chat list
	public List<JBChatMessageEntity> getChatlistByEntity(Integer id){
		List<JBChatMessageEntity> chatlist=jbChatlistRepository.findByEntity(id).getResultList();
		return chatlist;
	}
	public List<Object[]> getChatAndHistorylistByEntity(String id){
		String sql="select f.*,concat(u2.firstName,' ',u2.lastName) as assigneeName from "
				+ "(select c.*,concat(u1.firstName,' ',u1.lastName) as username from "
				+ "(select id,userId1 as sender,entityId as entity,'' as text,modificationDate as date, "
				+ "null as mimetype,modificationtype as types,userId2 as assignee from jb_itemmodification as m where entityId in ("+id+") "
				+ "union "
				+ "select id,sender,entity,text,date,null as mimetype,null as types,null as assignee from jb_chatmessages as a where entity in ("+id+") "
				+ "union "
				+ "select id,uploaderUser as sender,entity,name as text,uploadDate as date,type as mimetype,1 as types,null as assignee from jb_attachments as b where entity in ("+id+")) as c "
				+ "inner join "
				+ "jb_users as u1 "
				+ "on c.sender=u1.id) as f "
				+ "left join "
				+ "jb_users as u2 "
				+ "on f.assignee=u2.id "
				+ "order by f.date  asc";

		List<Object[]> chatlist=EntityUtil.getMoreNativeObjectRec(em, sql);
		return chatlist;
	}

	public List<Object[]> getAllChatsByEntity(String id){
		String sql="select c.*,concat(u1.firstName,' ',u1.lastName) as username, null as assigneeName "
				+ "from (select id,sender,entity,"
				+ "text,date,null as mimetype,null as types,null as assignee from jb_chatmessages as a "
				+ "where entity in ("+id+")) as c inner join jb_users as u1 on c.sender=u1.id order by date  asc";

		List<Object[]> chatlist=EntityUtil.getMoreNativeObjectRec(em, sql);
		return chatlist;
	}

	public List<Object[]> getAllHistorysByEntity(String id){
		String sql="select f.*,concat(u2.firstName,' ',u2.lastName) as assigneeName "
				+ "from (select c.*,concat(u1.firstName,' ',u1.lastName) as username "
				+ "from (select id,userId1 as sender,entityId as entity,'' as text,modificationDate as date,"
				+ "null as mimetype,modificationtype as types,userId2 as assignee "
				+ "from jb_itemmodification as m where entityId in ("+id+") "
				+ "union select id,uploaderUser as sender,entity,name as text,"
				+ "uploadDate as date,type as mimetype,1 as types,null as assignee from jb_attachments as b "
				+ "where entity in ("+id+")) as c inner join jb_users as u1 on c.sender=u1.id) as f "
				+ "left join jb_users as u2 on f.assignee=u2.id order by f.date  asc";

		List<Object[]> chatlist=EntityUtil.getMoreNativeObjectRec(em, sql);
		return chatlist;
	}

	public JBChatMessageEntity saveChatAndComments(JBChatMessageEntity chat){
		jbChatlistRepository.save(chat);
		return chat;
	} 
	public void deleteChatData(Integer chatlist) {
		jbChatlistRepository.remove(jbChatlistRepository.findBy(chatlist));
	}

	// My action list
	public void deleteMyActionListRow(Integer id) {// for assigned to me item and checklist
		jbActionlistRepository.remove(jbActionlistRepository.findBy(id));
	}
	public void deleteNotification(Integer id) {
		// TODO Auto-generated method stub
		jbActionlistHistoryRepository.remove(jbActionlistHistoryRepository.findBy(id));
	}

	//actionlist
	public JBActionlist saveActionlist(JBActionlist actionlist) {
		jbActionlistRepository.save(actionlist);
		return actionlist;
	}
	public JBActionlist getActionlist(Integer id){
		return jbActionlistRepository.findBy(id);
	}

	public List<JBActionlist> getActionlistByEntityIdAndTypeAndAddedBy(Integer itemid,Integer itemtype,Integer addedby){
		return jbActionlistRepository.findByItemidAndItemtypeAndAddedby(itemid,itemtype,addedby).getResultList();
	}


	//actionlist history
	public JBActionlistHistory saveActionlistHistory(JBActionlistHistory actionlistHistory) {
		return jbActionlistHistoryRepository.save(actionlistHistory);
	}

	public List<?> getAllAssigneeUserByCheckItem(Integer itemId){
		String sql = "select a.itemid,GROUP_CONCAT((select concat(' ',firstName,' ',lastName) name from jb_users u where u.id=a.assignedto)) assignedto "
				+ "from jb_actionlist a where a.itemid in (select c.id from jb_checklistitems c where c.entity="+itemId+") and  a.assignedto > 0 group by a.itemid";
		List<?> result=EntityUtil.getMoreNativeRec(em, sql);
		return result;
	}

	public List<?> getAllItemByProjectId(Integer parentId,Integer userId) {
		String sql = "Select id,parentid,entityType,status,estimatedStartDate,estimatedFinishDate"				
				+ " from jb_entities e where parentProjectId="+parentId+"";
		List<?> results=EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<Long> getAllAcessEntity(Integer userId,String userRoles,Integer projectid) {
		String sql = "SELECT u.entityid FROM jb_userroletoentity u WHERE u.userRole in ("+userRoles+") and u.userId="+userId+" "
				+ "and entityid in (select id from jb_entities where id ="+projectid+" or parentProjectId="+projectid+")";
		List<Long> results=(List<Long>)EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<?> getAllDoneChildEntitiesByParent(Integer parentId,Integer projectId,String type) {
		String sql = "SELECT a.id FROM "
				+ "(SELECT  p.id,p.parentid,p.progress,p.entityType FROM  "
				+ "(SELECT c.id,c.parentid,c.progress,c.entityType FROM jb_entities c WHERE c.parentProjectId="+ projectId +" ORDER BY c.parentid,c.id) p,"
				+ "(SELECT @pv := "+ parentId +") initialisation WHERE  FIND_IN_SET(p.parentid, @pv) > 0 "
				+ "AND @pv := CONCAT(@pv, ',', p.id)) a "
				+ "WHERE a.progress=100 AND a.entityType in "+type+"";
		List<?> results=EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}
	public List<?> getAllNotDoneChildEntitiesByParentAndFilterByTypes(Integer parentId,Integer projectId,String type) {
		String sql = "SELECT a.id FROM "
				+ "(SELECT  p.id,p.parentid,p.progress,p.entityType FROM  "
				+ "(SELECT c.id,c.parentid,c.progress,c.entityType FROM jb_entities c WHERE c.parentProjectId="+ projectId +" ORDER BY c.parentid,c.id) p,"
				+ "(SELECT @pv := "+ parentId +") initialisation WHERE  FIND_IN_SET(p.parentid, @pv) > 0 "
				+ "AND @pv := CONCAT(@pv, ',', p.id)) a "
				+ "WHERE a.progress!=100 AND a.entityType in "+type+"";
		List<?> results=EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<?> getAllChildEntitiesByParentAndFilterByTypes(Integer parentId,Integer projectId,String type) {
		String sql = "SELECT a.id FROM "
				+ "(SELECT  p.id,p.parentid,p.entityType FROM  "
				+ "(SELECT c.id,c.parentid,c.entityType FROM jb_entities c WHERE c.parentProjectId="+ projectId +" ORDER BY c.parentid,c.id) p,"
				+ "(SELECT @pv := "+ parentId +") initialisation WHERE  FIND_IN_SET(p.parentid, @pv) > 0 "
				+ "AND @pv := CONCAT(@pv, ',', p.id)) a "
				+ "WHERE  a.entityType in "+type+"";
		List<?> results=EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<Object[]> getMyItem(Integer userId,Integer status) {
		Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1; //for items
		String str="";
		if(status!=null)
			str=" and e.status="+status+" ";
		String sql = " select m.*,n.name as toplabelname,n.entityType as toplabeltype from" 
				+" (select a.id,a.itemtype,e.entityType,a.itemid,a.dateadded,a.addedby,e.name,e.status,"
				+" ifnull(e.parentProjectId,e.id) as toplabelid"
				+" from jb_actionlist as a inner join jb_entities as e on a.itemid=e.id"
				+" where a.itemtype="+itemType+" "+str+" and addedby="+userId+" and (a.assignedto is NULL or a.assignedto=0)) as m"
				+" inner join jb_entities as n on n.id=m.toplabelid"
				+" order by toplabelname,m.status,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}
	public List<Object[]> getAssignedToMeItem(Integer userId,Integer status) {
		Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1; //for items
		String str="";
		if(status!=null)
			str=" and e.status="+status+" ";
		String sql = "SELECT m.*,n.name as toplabelname,n.entityType as toplabeltype FROM " 
				+ " (SELECT a.id,a.itemtype,e.entityType,a.itemid,a.dateadded,a.addedby,e.name,e.status," 
				+ " ifnull(e.parentProjectId,e.id) as toplabelid"
				+ " FROM jb_actionlist as a INNER JOIN jb_entities as e on a.itemid=e.id" 
				+ " WHERE a.itemtype="+itemType+" "+str+" and a.assignedto="+userId+") as m"
				+ " INNER JOIN jb_entities as n on n.id=m.toplabelid" 
				+ " ORDER BY toplabelname,m.status,name";

		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}
	public List<Object[]> getMyChecklist(Integer userId,Integer status) {
		Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1; //for checklist
		String str="";
		if(status!=null)
			str=" and c.isChecked="+status+" ";
		String sql = "	select m.id,m.itemtype,0 as entityType,m.itemid,m.dateadded,m.addedby,m.name,m.isChecked as status,"
				+ "m.toplabelid,n.name as toplabelname,n.entityType as toplabeltype,m.entity,m.sortOrder from"
				+" (select a.id,a.itemtype,a.itemid,a.dateadded,a.addedby,c.name,c.isChecked,c.entity,ifnull(e.parentProjectId,e.id)"
				+" as toplabelid,c.sortOrder"
				+" from jb_actionlist as a inner join jb_checklistitems as c on a.itemid=c.id" 
				+" inner join jb_entities as e on c.entity=e.id"
				+" where a.itemtype="+itemType+" "+str+" and addedby="+userId+" and (a.assignedto is NULL or a.assignedto=0)) as m"
				+" inner join jb_entities as n on n.id=m.toplabelid"
				+" order by toplabelname,m.isChecked,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}
	public List<Object[]> getAssignedToMeChecklist(Integer userId,Integer status) {
		Integer itemType = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1; //for checklist
		String str="";
		if(status!=null)
			str=" and c.isChecked="+status+" ";
		String sql = "select m.id,m.itemtype,0 as entityType,m.itemid,m.dateadded,m.addedby,m.name,m.isChecked as status,"
				+ " m.toplabelid,n.name as toplabelname,n.entityType as toplabeltype,m.entity,m.sortOrder from"
				+" (select a.id,a.itemtype,a.itemid,a.dateadded,a.addedby,c.name,c.isChecked,c.entity,"
				+" ifnull(e.parentProjectId,e.id) as toplabelid,c.sortOrder"
				+" from jb_actionlist as a inner join jb_checklistitems as c on a.itemid=c.id" 
				+" inner join jb_entities as e on c.entity=e.id"
				+" where a.itemtype="+itemType+" "+str+" and a.assignedto="+userId+") as m"
				+" inner join jb_entities as n on n.id=m.toplabelid"
				+" order by toplabelname,m.isChecked,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getNotificationOfItem(Integer userId, int itemType,Integer customerId) {
		String sql = "SELECT m.*,n.name as toplabelname,n.entityType as toplabeltype FROM"
				+" (select a.id,a.created,a.itemid,a.itemtype,a.userid,a.messagetype,a.message,ifnull"
				+" (e.parentProjectId,e.id) as toplabelid FROM jb_actionlisthistory as a"
				+" INNER JOIN jb_entities as e on a.itemid=e.id where itemtype = "+itemType+" and userid= "+userId
				+" and DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= created) as m INNER JOIN jb_entities as n on n.id=m.toplabelid ORDER BY id desc";
		List<Object[]> results = EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getNotificationOfChecklist(Integer userId, int itemType,Integer customerId) {
		String sql = " Select m.*,n.name as toplabelname,n.entityType as toplabeltype from"
				+"	(select a.id,a.created,a.itemid,a.itemtype,a.userid,a.messagetype,a.message,ifnull(e.parentProjectId,e.id) as toplabelid,c.entity"
				+"	from jb_actionlisthistory as a inner join jb_checklistitems as c on a.itemid=c.id"
				+"	 inner join jb_entities as e on c.entity=e.id"
				+"	where a.itemtype= "+itemType+" and userid= "+userId+" and DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= created) as m"
				+"	inner join jb_entities as n on n.id=m.toplabelid"
				+"	order by id desc";
		List<Object[]> results = EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAssignedToOtherItemAssignedName(Integer userId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1; //for items
		String str="";
		if(status!=null)
			str=" and e.status="+status+"";
		String sql = " select distinct  CONCAT(u.firstName,' ', u.lastName) as assignedname, a.assignedto"
				+" from jb_actionlist as a "
				+" inner join jb_users as u "
				+" on a.assignedto=u.id inner join jb_entities as e"
				+" on e.id=a.itemid"
				+" where a.addedby="+userId+" and a.itemtype="+itemtype+" and a.assignedto>0 "+str+""
				+" order by assignedname";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAssignedToOtherItemByAsssignedId(Integer userId,Integer assignedId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1; //for items
		String str="";
		if(status!=null)
			str=" and e.status="+status+"";
		String sql="SELECT m.*,n.name as toplabelname,n.entityType as toplabeltype FROM "
				+ "(select a.id,a.itemtype,e.entityType,a.itemid,a.dateadded,a.addedby,e.name,e.status,"
				+ "ifnull(e.parentProjectId,e.id) as toplabelid "
				+ "from jb_actionlist as a inner join jb_entities as e on a.itemid=e.id "
				+ "inner join jb_users as u on a.assignedto=u.id "
				+" where a.itemtype="+itemtype+" and addedby="+userId+" and a.assignedto="+assignedId+" "+str+") as m "
				+ "INNER JOIN jb_entities as n on n.id=m.toplabelid "
				+ "order by toplabelname,m.status,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAssignedToOtherChecklistAssignedName(Integer userId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1; //for checklist
		String str="";
		if(status!=null)
			str=" and c.isChecked="+status+" ";
		String sql = " select distinct  CONCAT(u.firstName,' ', u.lastName) as assignedname, a.assignedto"
				+" from jb_actionlist as a "
				+" inner join jb_users as u"
				+" on a.assignedto=u.id inner join jb_checklistitems as c"
				+" on c.id=a.itemid"
				+" where a.addedby="+userId+" and a.itemtype="+itemtype+" and a.assignedto>0 "+str+""
				+" order by assignedname";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}
	public List<Object[]> getAssignedToOtherChecklistByAsssignedId(Integer userId,Integer assignedId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1; //for checklist
		String str="";
		if(status!=null)
			str=" and c.isChecked="+status+" ";
		String sql="select m.*,n.name as toplabelname,n.entityType as toplabeltype from "
				+ "(select a.id,a.itemtype,a.itemid,a.dateadded,a.addedby,"
				+ "c.name,c.isChecked,c.entity,ifnull(e.parentProjectId,e.id) as toplabelid,c.sortOrder "
				+ "from jb_actionlist as a inner join jb_checklistitems as c on a.itemid=c.id "
				+ "inner join jb_entities as e on c.entity=e.id "
				+ "inner join jb_users as u on a.assignedto=u.id "
				+ "where a.itemtype="+itemtype+" and addedby="+userId+" and a.assignedto="+assignedId+" "+str+")as m "
				+ "inner join jb_entities as n on n.id=m.toplabelid "
				+ "order by dateadded,toplabelname,m.isChecked,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}
	public List<Object[]> getMyTodoItem(Integer userId) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("todo")+1; //for todo item
		String sql = " select id,itemname from jb_actionlist where"
				+" itemtype="+itemtype+" and addedby="+userId+" order by id";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public boolean itemOnActionList(Integer entityId,Integer itemType,Integer assignedUser,Integer userId){
		if(entityId == 0) // for todo item all time return false
			return false;
		else{
			String sql;
			if(assignedUser>0)
				sql = " SELECT * FROM jb_actionlist  WHERE itemid = "+entityId+" and itemtype="+itemType+" and  assignedto="+assignedUser+"";
			else
				sql =  "SELECT * FROM jb_actionlist  WHERE itemid = "+entityId+" and itemtype="+itemType+" and ((addedby="+userId+" and  assignedto=0) or assignedto="+userId+")";

			List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
			if(results.size()>0){
				return true;
			}
			else{
				return false;
			}
		}	
	}
	
	public boolean isUserAssigner(Integer entityId,Integer itemType,Integer assigner){
		String sql = " SELECT * FROM jb_actionlist  WHERE itemid = "+entityId+" and itemtype="+itemType+" and  addedby="+assigner+"";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		if(results.size()>0){
			return true;
		}
		else{
			return false;
		}	
	}
	public List<?> getAllAssignerByEntity(Integer itemId, Integer itemType, Integer userId) {
		String sql = " select distinct addedby from jb_actionlist WHERE itemid ="+itemId+""
				+" and itemtype="+itemType+" and addedby <> "+userId+"";
		List<?> results=EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<JBUserEntity> getAllAssigneeUserById(Integer itemid,Integer itemtype){
		List<JBUserEntity> users=jbActionlistRepository.findAllAssigneeById(itemid, itemtype).getResultList();
		return users;
	}

	// Today tools
	public List<Object[]> getAllTodayItemByType(Integer userId,Integer myTodayTypes,Integer status) {
		String myTodayDate = "";
		String checkstatus="";
		String itemstatus="";
		if(status!=null){
			itemstatus=" and e.status="+status+"";
			checkstatus=" and c.isChecked=0";
		}
		if(myTodayTypes == Arrays.asList(JibbyConfig.myTodayType).indexOf("myToday")){
			myTodayDate = "DATE_FORMAT(a.itemdatetime, '%Y-%m-%d') = CURDATE()";
		}
		else if(myTodayTypes == Arrays.asList(JibbyConfig.myTodayType).indexOf("myTomorrow")){
			myTodayDate = "DATE_FORMAT(a.itemdatetime, '%Y-%m-%d') = DATE_ADD(CURDATE(), INTERVAL 1 DAY)";
		}
		else if(myTodayTypes == Arrays.asList(JibbyConfig.myTodayType).indexOf("myPast")){
			myTodayDate = "DATE_FORMAT(a.itemdatetime, '%Y-%m-%d') < CURDATE()";
		}
		Integer todayItemtype =  Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem")+1;
		Integer todayChecktype =  Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem")+1;
		String sql = "	select z.id,z.itemtype,z.entityType,z.itemid,z.dateadded,z.addedby,z.name,z.status,"
				+ "z.parentProjectId,z.toplabelname,z.toplabeltype,z.entity,z.sortOrder,z.itemdatetime,z.hours from"
				+" (select m.*,n.name as toplabelname,n.entityType as toplabeltype from"
				+" (select a.id,a.itemtype,e.entityType,a.itemid,a.dateadded,a.addedby,e.name,e.status,'' as entity,'' as sortOrder,"
				+" parentProjectId,a.itemdatetime,"
				+" hour(a.itemdatetime) as hours"
				+" from jb_actionlist as a inner join jb_entities as e on a.itemid=e.id"
				+" where (a.itemtype="+todayItemtype+" and addedby="+userId+" and"
				+" "+myTodayDate+" "+itemstatus+"))as m"
				+" inner join jb_entities as n on n.id=m.parentProjectId"
				+" union all"
				+" select m.*,n.name as toplabelname,n.entityType as toplabeltype from"
				+" (select a.id,a.itemtype,0,a.itemid,a.dateadded,a.addedby,c.name,c.isChecked,c.entity,c.sortOrder,"
				+" e.parentProjectId,a.itemdatetime,"
				+" hour(a.itemdatetime) as hours" 
				+" from jb_actionlist as a inner join jb_checklistitems as c on a.itemid=c.id" 
				+" inner join jb_entities as e on c.entity=e.id"
				+" where (a.itemtype="+todayChecktype+" and addedby="+userId+" and"
				+" "+myTodayDate+" "+checkstatus+")) as m"
				+" inner join jb_entities as n on n.id=m.parentProjectId"
				+" union all"
				+" select a.id,0 as itemtype,8 as entityType,a.itemid,a.dateadded,a.addedby,a.itemname as name,"
				+" 2 as status,'' as entity,'' as sortOrder,0 as parentProjectId,a.itemdatetime,"
				+" hour(a.itemdatetime) as hours,'Item from todo list' as toplabelname,8 as toplabeltype" 
				+" from jb_actionlist as a where a.itemid=0 and a.itemtype="+todayItemtype+" and a.addedby="+userId+" and" 
				+" "+myTodayDate+")as z"
				+" order by hours,itemdatetime";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getParentsByEntityId(Integer entityId) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("todo")+1; //for todo item
		String sql = " SELECT T2.id, T2.name,T2.entityType"
				+" FROM ("
				+"	SELECT"
				+"  @r AS _id,"
				+"  (SELECT @r := parentid FROM jb_entities WHERE id = _id) AS parent_id,"
				+" 	@l := @l + 1 AS lvl"
				+"	FROM"
				+"	(SELECT @r := "+entityId+", @l := 0) vars,"
				+" 	jb_entities m"
				+"	WHERE @r <> 0) T1"
				+"	JOIN jb_entities T2"
				+"	ON T1._id = T2.id"
				+"	ORDER BY T1.lvl DESC;";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Integer> getParentsListByEntityId(Integer entityId) {
		String sql = " SELECT T2.id"
				+" FROM ("
				+"	SELECT"
				+"  @r AS _id,"
				+"  (SELECT @r := parentid FROM jb_entities WHERE id = _id) AS parent_id,"
				+" 	@l := @l + 1 AS lvl"
				+"	FROM"
				+"	(SELECT @r := "+entityId+", @l := 0) vars,"
				+" 	jb_entities m"
				+"	WHERE @r <> 0) T1"
				+"	JOIN jb_entities T2"
				+"	ON T1._id = T2.id"
				+"	ORDER BY T1.lvl DESC;";
		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}


	public boolean hasChild(Integer parentId){
		String sql = "Select id from jb_entities where parentid = "+parentId+"";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		if(results.size()>0){
			return true;
		}
		else{
			return false;
		}

	}

	public List<?> getAllAssignerMyActionlist() {
		String sql = " select distinct addedby from jb_actionlist where addedby>0 and itemtype in ("+(Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1)+","+(Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1)+","+(Arrays.asList(JibbyConfig.actionlistType).indexOf("todo")+1)+") "
				+ "union "
				+ "select distinct assignedto from jb_actionlist where assignedto>0 and itemtype in ("+(Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1)+","+(Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1)+") "
				+ "union "
				+ "select distinct addedby from jb_actionlist where addedby>0 and DATE_FORMAT(itemdatetime, '%Y-%m-%d') = CURDATE() and  itemtype in("+(Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem")+1)+","+(Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem")+1)+")" ;
		List<?> results=EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<JBUserEntity> getUserListByMessageRecipent(List<Integer> emailAbout){
		List<JBUserEntity> userlist= jbUserEntityRepository.findByMessageRecipent(emailAbout).getResultList();
		return userlist;
	}

	public List<JBMessages> getMessagesByRecipent(Integer recipents){
		List<JBMessages> messages= jbMessagesRepository.findByRecipent(recipents).orderAsc("m.date", false).getResultList();
		return messages;
	}

	public JBMessages saveMessages(JBMessages messages){
		return jbMessagesRepository.save(messages);
	}

	//kanban board

	public JBKanbanBoardColumns saveKanbanData(JBKanbanBoardColumns kanbanColumn) {
		return jbKanbanRepository.save(kanbanColumn);
	}

	public void removeKanbanColumn(JBKanbanBoardColumns column) {
		jbKanbanRepository.remove(jbKanbanRepository.findBy(column.getId()));
	}

	//Subscription
	public List<JBSubscriptionsEntity> getAllSubscription(){
		return jbSubscriptionRepository.findAll();
	}
	//Licences
	public List<JBLicencesEntity> getAllLicences(){
		return jbLicencesRepository.findAll();
	}

	//ItemModification
	public JBItemModification saveItemModification(JBItemModification itemModification){
		return jbItemModificationRepository.save(itemModification);
	}
	public Integer deleteItemModificationByEntityAndUserAndCompleted(Integer entity,Integer userid){
		try{
			String sql = "select userid1,id from jb_itemmodification where entityId="+entity+" and modificationtype in ("+(Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("done")+1)+","+(Arrays.asList(JibbyConfig.itemModificationStatus).indexOf("undone")+1)+") order by modificationDate desc limit 1";
			Object[] results=EntityUtil.getNativeRec(em, sql);
			if(results[0].equals(userid)){
				jbItemModificationRepository.remove(jbItemModificationRepository.findBy(Integer.parseInt(String.valueOf(results[1]))));
				return Integer.parseInt(String.valueOf(results[1]));
			}
		}catch(Exception e){
			return null;
		}
		return null;
	}

	//kanban board
	public List<JBKanbanBoardColumns> getAllKanbanDataByParentProjectId(int parentProjectId) {
		List<JBKanbanBoardColumns> kanbanData=jbKanbanRepository.findByEntityId(parentProjectId).orderAsc("sortOrder").getResultList();
		return kanbanData;

	}

	public List<?> getMyActionlistDoneItem(){ 
		String sql="select a.id from jb_actionlist as a where a.itemtype in("+(Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayItem")+1)+","+(Arrays.asList(JibbyConfig.actionlistType).indexOf("myTodayCheckItem")+1)+") and a.id <> 0 and (a.itemid in " 
				+ "(SELECT e.id FROM jb_entities as e WHERE e.id=a.itemid " 
				+ "and e.status="+JibbyConfig.status[0]+") or a.itemid in " 
				+ "(SELECT c.id FROM jb_checklistitems as c WHERE c.id=a.itemid " 
				+ "and c.isChecked="+JibbyConfig.checkItemStatus[0]+"))"; 
		List<?> results=EntityUtil.getMoreNativeRec(em, sql); 
		return results; 
	} 

	public JBChatMessageEntity getChatById(Integer id){
		JBChatMessageEntity chat = jbChatlistRepository.findBy(id);
		return chat;
	}

	public List<Object[]> getAssignedToOtherItem(Integer userId,Integer status, Integer filterType, Integer colASelectedProjectId) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1; //for items
		String str="";
		if(status!=null)
			str=" and e.status="+status+"";
		
		String addedBy = "";
		if (filterType != 3)
			addedBy = " addedby= " + userId + " and ";
		
		String selectedProject = "";
		if (filterType != 2 && colASelectedProjectId != null)
			selectedProject = " where m.toplabelid=" + colASelectedProjectId + " ";
		
		String sql="SELECT m.*,n.name as toplabelname,n.entityType as toplabeltype FROM "
				+ "(select a.id,a.itemtype,e.entityType,a.itemid,a.dateadded,a.addedby,e.name,e.status,"
				+ "ifnull(e.parentProjectId,e.id) as toplabelid,a.assignedto,CONCAT(u.firstname,' ',u.lastname) as username "
				+ "from jb_actionlist as a inner join jb_entities as e on a.itemid=e.id "
				+ "inner join jb_users as u on a.assignedto=u.id "
				+" where a.itemtype="+itemtype+" and "+addedBy+" a.assignedto>0 "+str+") as m "
				+ "INNER JOIN jb_entities as n on n.id=m.toplabelid "+selectedProject
				+ "order by username,toplabelname,m.status,name";
		
		//logger.info("sql--> "+sql);
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAssignedToOtherCheckItem(Integer userId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1; //for checklist
		String str="";
		if(status!=null)
			str=" and c.isChecked="+status+" ";
		String sql="select m.id,m.itemtype,0 as entityType,m.itemid,m.dateadded,m.addedby,m.name,m.isChecked as status," + 
				"m.toplabelid,m.assignedto,m.username,n.name as toplabelname,n.entityType as toplabeltype,m.entity,m.sortOrder from " + 
				"(select a.id,a.itemtype,a.itemid,a.dateadded,a.addedby," + 
				"c.name,c.isChecked,c.entity,ifnull(e.parentProjectId,e.id) as toplabelid,c.sortOrder,a.assignedto,CONCAT(u.firstname,' ',u.lastname) as username " + 
				"from jb_actionlist as a inner join jb_checklistitems as c on a.itemid=c.id " + 
				"inner join jb_entities as e on c.entity=e.id " + 
				"inner join jb_users as u on a.assignedto=u.id " + 
				"where a.itemtype="+itemtype+" and addedby="+userId+" and a.assignedto>0 "+str+")as m " + 
				"inner join jb_entities as n on n.id=m.toplabelid " + 
				"order by username,toplabelname,status,name;";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAssignedToOtherItemsByAdded(Integer userId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("item")+1; //for items
		String str="";
		if(status!=null)
			str=" and e.status="+status+"";
		String sql="SELECT m.*,n.name as toplabelname,n.entityType as toplabeltype FROM "
				+ "(select a.id,e.entityType,e.name,e.status,"
				+ "ifnull(e.parentProjectId,e.id) as toplabelid,CONCAT(u.firstName,' ', u.lastName) as assignedname, a.assignedto "
				+ "from jb_actionlist as a inner join jb_entities as e on a.itemid=e.id "
				+ "inner join jb_users as u on a.assignedto=u.id "
				+" where a.itemtype="+itemtype+" and addedby="+userId+" and a.assignedto>0 "+str+") as m "
				+ "INNER JOIN jb_entities as n on n.id=m.toplabelid "
				+ "order by assignedname,toplabelname,m.status,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAssignedToOtherChecklistByAdded(Integer userId,Integer status) {
		Integer itemtype = Arrays.asList(JibbyConfig.actionlistType).indexOf("checkitem")+1; //for checklist
		String str="";
		if(status!=null)
			str=" and c.isChecked="+status+" ";
		String sql="select m.*,n.name as toplabelname,n.entityType as toplabeltype from "
				+ "(select a.id,c.name,c.isChecked,ifnull(e.parentProjectId,e.id) as toplabelid,"
				+ "CONCAT(u.firstName,' ', u.lastName) as assignedname, a.assignedto "
				+ "from jb_actionlist as a inner join jb_checklistitems as c on a.itemid=c.id "
				+ "inner join jb_entities as e on c.entity=e.id "
				+ "inner join jb_users as u on a.assignedto=u.id "
				+ "where a.itemtype="+itemtype+" and addedby="+userId+" and a.assignedto>0 "+str+")as m "
				+ "inner join jb_entities as n on n.id=m.toplabelid "
				+ "order by assignedname,toplabelname,m.isChecked,name";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	//// --------subscription work-----

	public boolean isUserSubscribe(Integer userId){
		String sql = " SELECT * FROM jb_subscriptions where userId ="+userId;
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		if(results.size()>0){
			return true;
		}
		else{
			return false;
		}	
	}

	public JBSubscriptionsEntity getSubscriptionByUser(JBUserEntity user) {
		try{
			JBSubscriptionsEntity subscriptionData=jbSubscriptionRepository.findTop1ByuserIdOrderByIdDesc(user);
			return subscriptionData;
		}catch(Exception e){
			return null;
		}
	}

	public List<JBSubscriptionsEntity> getSubscriptionByUserAndCustomer(JBUserEntity user,Integer customerId) {

		List<String> orderNumbers= getAllSubscriptionPaymentOrderNumberByUserIdCustomerId(user.getId(),customerId, 1);
		if(orderNumbers.size()>0) {
			List<JBSubscriptionsEntity> subscriptionData=jbSubscriptionRepository.findByuserIdAndCustomerOrderByIdDesc(user,orderNumbers).getResultList();
			return subscriptionData;
		}
		List<JBSubscriptionsEntity> subscriptionData=new ArrayList<>();
		return subscriptionData;

	}

	public JBSubscriptionsEntity getSubscriptionByOrderNumber(String orderNumber) {
		try{
			JBSubscriptionsEntity subscriptionData=jbSubscriptionRepository.findByOrderNumber(orderNumber);
			return subscriptionData;
		}catch(Exception e){
			return null;
		}
	}

	public JBSubscriptionsEntity saveSubscription(JBSubscriptionsEntity subscription) {
		jbSubscriptionRepository.save(subscription);
		return subscription;
	}

	public void deleteSubscription(JBSubscriptionsEntity subscription) {
		jbSubscriptionRepository.remove(jbSubscriptionRepository.findBy(subscription.getId()));
	}

	///-------- licenses work-------
	public boolean isUserHasLicense(Integer userId) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM jb_licences where buyerUserId = "+userId;
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		if(results.size()>0){
			return true;
		}
		else{
			return false;
		}	
	}

	public JBLicencesEntity getLicenseByUserAndCustomer(JBUserEntity user,Integer customerId) {
		try{
			JBLicencesEntity license=jbLicencesRepository.findTop1ByUserAndCustomerIdOrderByValidTillDateDesc(user,customerId);
			return license;
		}catch(Exception e){
			return null;
		}
	}

	public JBLicencesEntity getLicenseById(Integer id) {
		try{
			JBLicencesEntity license=jbLicencesRepository.findBy(id);
			return license;
		}catch(Exception e){
			return null;
		}
	}

	public JBLicenseInvitation getLicenseInvitationByGuid(String guid) {
		try{
			JBLicenseInvitation licenseInvitation = jbLicenceInvitationRepository.findByGuid(guid);
			return licenseInvitation;
		}catch(Exception e){
			return null;
		}
	}

	public JBLicenseInvitation getLicenseInvitationByLicenseIdAndReceiverId(Integer licenseId,Integer receiverId) {
		try{
			JBLicenseInvitation licenseInvitation = jbLicenceInvitationRepository.findByLicenseIdAndReceiverId(licenseId,receiverId);
			return licenseInvitation;
		}catch(Exception e){
			return null;
		}
	}

	public void deleteLicenseInvitation(JBLicenseInvitation licenseInvite) {
		jbLicenceInvitationRepository.remove(jbLicenceInvitationRepository.findBy(licenseInvite.getId()));
	}

	public JBLicencesEntity getLicensesByCodeNumber(String licenseCode) {
		try{
			JBLicencesEntity licence= jbLicencesRepository.findByLicenceCode(licenseCode);
			return licence;
		}catch(Exception e){
			return null;
		}
	}

	public List<JBLicencesEntity> getAllLicensesByBuyer(JBUserEntity user) {
		List<JBLicencesEntity> licenseData=jbLicencesRepository.findByBuyer(user).getResultList();
		return licenseData;
	}

	public List<JBLicencesEntity> getMultiActiveLicensesByUserAndOrderNumber(JBUserEntity user,String orderNumber,Integer custId) {
		List<JBLicencesEntity> licenseData=jbLicencesRepository.getMultiActiveLicensesByUserAndCustomerAndOrderNumber(user,orderNumber,custId).getResultList();
		return licenseData;
	}

	public List<JBLicencesEntity> getAllActiveLicensesByOrderNumber(String orderNumber) {
		List<JBLicencesEntity> licenseData=jbLicencesRepository.getAllActiveLicensesByOrderNumber(orderNumber).getResultList();
		return licenseData;
	}

	public List<JBLicencesEntity> getAllLicensesByOrderNumber(String orderNumber) {
		List<JBLicencesEntity> licenseData=jbLicencesRepository.getAllLicensesByOrderNumber(orderNumber).getResultList();
		return licenseData;
	}

	public JBLicencesEntity getValidLicensesByUserAndCustomer(JBUserEntity user,Integer custId) {
		JBLicencesEntity license=null;
		List<JBLicencesEntity> licenseData=jbLicencesRepository.getValidLicensesByUserAndCustomerId(user,new Date(),custId).getResultList();
		if(licenseData!=null && licenseData.size()>0){
			return license=licenseData.get(0);
		}
		return license;
	}

	public boolean isVaildLicense(JBUserEntity user,Integer customerId) {
		List<JBLicencesEntity> licenseData=jbLicencesRepository.getValidLicensesByUserAndCustomer(user,new Date(),customerId).getResultList();
		if(licenseData!=null && licenseData.size()>0){
			return true;
		}
		return false;
	}

	public List<OrderNumber> getAllPurchaseOrderByUser(JBUserEntity user) {
		List<String> orderNumbers=jbLicencesRepository.getAllBuyerOrderNumber(user).getResultList();
		List<OrderNumber> orders= new ArrayList<>();
		for(String order:orderNumbers){
			OrderNumber orderEntity=new OrderNumber();
			orderEntity.setOrderNumber(order);
			orders.add(orderEntity);
		}
		return orders;
	}

	public JBLicencesEntity saveLicense(JBLicencesEntity license) {
		jbLicencesRepository.save(license);
		return license;
	}

	public JBLicenseInvitation saveLicenseInvitation(JBLicenseInvitation licenseInvitation) {
		jbLicenceInvitationRepository.save(licenseInvitation);
		return licenseInvitation;
	}

	// Licence History
	public HashMap<String,List<JBLicencesHistory>> getLicenceHistoryByLicenceCodes(List<String> licencecodelist){
		List<JBLicencesHistory> licenceHostorys= jbLicencesHistoryRepository.findHistoryOrderByDateDesc(licencecodelist).getResultList();
		HashMap<String,List<JBLicencesHistory>> historylist=new HashMap<String,List<JBLicencesHistory>>();
		for(JBLicencesHistory licenceHostory:licenceHostorys){
			if(historylist.get(licenceHostory.getLicencesCode())==null)
				historylist.put(licenceHostory.getLicencesCode(), new ArrayList<JBLicencesHistory>());
			historylist.get(licenceHostory.getLicencesCode()).add(licenceHostory);
		}
		return historylist;
	}

	public JBLicencesHistory getLicenceHistoryDisconnectByUserAndCustomer(JBUserEntity user,Integer customerId){
		try{
			JBLicencesHistory licenceHostory= jbLicencesHistoryRepository.findTop1ByUserIdAndCustomerIdAndTextOrderByDateDesc(user,customerId,"Disconnected");
			return licenceHostory;
		}catch (Exception e) {
			return null;
		}
	}

	public JBLicencesHistory saveLicenseHistory(JBLicencesHistory licenseHistory) {
		jbLicencesHistoryRepository.save(licenseHistory);
		return licenseHistory;
	}
	
	public List<JBActionlistHistory> getAllUnreadNotification(Integer userId){
		List<JBActionlistHistory> unreadNotification = jbActionlistHistoryRepository.findByUseridAndIsRead(userId,0).getResultList();
		return unreadNotification;
	}

	// invitation work

	public JBInvitation saveInvitation(JBInvitation invitation) {
		jbInvitationRepository.save(invitation);
		return invitation;
	}

	public JBUserEntity getUserByActiveCodeAndType(String acid,Integer type) throws Exception {

		try{
			JBUserEntity user = jbUserEntityRepository.findByActivationCodeAndType(acid,type);
			return user;
		}catch(Exception e){
			return null;
		}
	}

	public Object[] entityAttachmentOrChecklistCount(Integer entityId) {
		Object[]  entities=jbEntityRepository.entityAttachmentOrChecklistCount(entityId);
		return entities;
	}

	// Payment service
	public JBPayment savePayment(JBPayment payment){
		jbPaymentRepository.save(payment);
		return payment;
	}

	public JBPayment getPaymentByOrder(String orderNumber){
		try{
			JBPayment payment= jbPaymentRepository.findTop1ByOrderNumberOrderByIdDesc(orderNumber);
			return payment;
		}catch(Exception e){
			return null;
		}
	}

	public List<JBPayment> getAllPaymentByProviderAndOrderNumber(Integer provider,List<String> orderNumbers){
		List<JBPayment> payments=jbPaymentRepository.getAllPaymentByProviderAndOrderNumber(provider, orderNumbers).getResultList();
		return payments;
	}

	public List<String> getAllActiveValidSubscriptionOrderNumberByTodate(Date start,Date end){
		List<String> subscriptions=jbSubscriptionRepository.getAllActiveValidSubscriptionOrderNumberByTodate(start, end).getResultList();
		return subscriptions;
	}

	public List<String> getAllActiveValidLicenseOrderNumberByTodate(Date start,Date end){
		List<String> licences=jbLicencesRepository.getAllActiveValidLicenseOrderNumberByTodate(start, end).getResultList();
		return licences;
	}

	// creditcards

	public JBCreditcardsEntity getCreditCardByUserId(Integer userId){
		try {
			JBCreditcardsEntity creditcard=jbCreditcardRepository.findByUserId(userId);
			return creditcard;
		}catch(Exception e) {
			return null;
		}
	}

	public List<JBCreditcardsEntity> getAllExpiredCreditCards(String expiryDate){
		List<JBCreditcardsEntity> creditCards=jbCreditcardRepository.getAllExpiredCreditCards(expiryDate).getResultList();
		return creditCards;
	}

	public JBCreditcardsEntity saveCreditcards(JBCreditcardsEntity creditcards) {
		jbCreditcardRepository.save(creditcards);
		return creditcards;
	}

	public JBInvoicesEntity saveInvoices(JBInvoicesEntity invoice) {
		jbInvoicesRepository.save(invoice);
		return invoice;
	}

	/**
	 * Returns the status of the user, payment-wise.
	 * 
	 * Algorithm:
	 * Is user admin?
	 * -> Yes: user has admin status
	 * Is user disabled?
	 * -> Yes: user has disabled status
	 * Is user regular user?
	 * -> Yes: has user registered before a certain date (legacy)?
	 *		-> Yes: user has legacy status
	 *		-> No: does user have an active licence linked to the account?
	 *			-> Yes: user has licensed status
	 *			-> No: does user have a subscription associated with the account?
	 *				-> Yes: is the user on trial period?
	 *					-> Yes: user has presubscribed status
	 *					-> No: user has subscribed status
	 *				-> No: is user on trial period?
	 *					-> Yes: user has trial status
	 *					-> No: user has expired status
	 * -> No: error
	 * 
	 * @return string
	 */
	public String getUserStatus(JBUserEntity user,Integer customerId)
	{
		String status = "";
		if (Arrays.asList(JibbyConfig.userTypes).indexOf("admin")==user.getType())
		{
			status = UserType.Admin.name();
		}
		else if (Arrays.asList(JibbyConfig.userTypes).indexOf("vip")==user.getType())
		{
			status = UserType.VIP.name();
		}
		else if (Arrays.asList(JibbyConfig.userTypes).indexOf("disabled")==user.getType())
		{
			status = UserType.Disabled.name();
		}
		else if (Arrays.asList(JibbyConfig.userTypes).indexOf("invited")==user.getType())
		{
			status = UserType.Invited.name();
		}
		else if (Arrays.asList(JibbyConfig.userTypes).indexOf("user")==user.getType())
		{
			status=getLicenceOrSubscriptionStatus(user,customerId);
			if(!status.isEmpty()){
				return status;
			}
			else if (user.getRegistrationDate().compareTo(JibbyConfig.legacyDate) ==-1)
			{
				status = UserType.Legacy.name();
			}
			// NOTE: code block for no trial limit
			/*else if (Utils.isOnTrial(user.getExpiredDate()))
			{
				status = UserType.Trial.name();
			}*/
			else
			{
				// NOTE: code block for no trial limit
				//status =UserType.Expired.name();
				status = UserType.Trial.name();
			}
		}

		return status;
	}

	public String getLicenceOrSubscriptionStatus(JBUserEntity user,Integer customerId){
		String status="";
		// Has any valid License
		if (isVaildLicense(user,customerId))
		{	
			status = UserType.Licensed.name();
		}
		// Has any Subscription
		else if (getSubscriptionByUserAndCustomer(user, customerId).size()>0)
		{
			List<JBSubscriptionsEntity> subscriptions=getSubscriptionByUserAndCustomer(user,customerId);
			JBSubscriptionsEntity subscription=subscriptions.get(0);
			if (subscription.getValidTillDate()!=null && subscription.getValidTillDate().compareTo(new Date()) !=-1 && subscription.getIsActive()==1)
			{
				status =UserType.Subscribed.name();
			}
			else if (subscription.getValidTillDate()!=null && subscription.getValidTillDate().compareTo(new Date()) !=-1)
			{
				status =UserType.Unsubscribed.name();
			}
			// NOTE: code block for no trial limit
			/*else if (subscription.getIsActive()==1 && subscription.getValidTillDate()==null && Utils.isOnTrial(user.getExpiredDate()))
			{
				status =UserType.Presubscribed.name();
			}*/
			else
			{
				// NOTE: code block for no trial limit
				//status =UserType.Expired.name();
				status =UserType.Trial.name();
			}
		}
		return status;
	}

	public boolean isUserExpiredOver30Days(JBUserEntity user,Integer customerId)
	{
		JBSubscriptionsEntity subscription=null;
		JBLicencesEntity userlicence= getLicenseByUserAndCustomer(user,customerId);
		//JBSubscriptionsEntity subscription=getSubscriptionByUser(user);
		List<JBSubscriptionsEntity> subscriptions=getSubscriptionByUserAndCustomer(user,customerId);
		if(subscriptions.size()>0)
			subscription=subscriptions.get(0);

		// Has any License
		if(userlicence!=null){ 
			if((Utils.addDaysStartTime(userlicence.getValidTillDate(), 30)).compareTo(new Date()) ==-1)
				return true;
			else
				return false;
		}
		// Has any Subscription
		else if (subscription!=null)
		{
			if (subscription.getValidTillDate()!=null && (Utils.addDaysStartTime(subscription.getValidTillDate(), 30)).compareTo(new Date()) ==-1)
			{
				return true;
			}else{
				return false;
			}
		}

		// if licence discounnect
		else if(userlicence==null && subscription==null){
			JBLicencesHistory licenceHistory= getLicenceHistoryDisconnectByUserAndCustomer(user,customerId);
			if(licenceHistory!=null){ 
				if((Utils.addDaysStartTime(licenceHistory.getDate(), 30)).compareTo(new Date()) ==-1)
					return true;
				else
					return false;
			}
		}
		return true;
	}

	public String getUserMemberType(JBUserEntity user,Integer customerId) {
		String status = getUserStatus(user,customerId);
		return getMemberType(user,status,customerId);
	}
	
	public String getMemberType(JBUserEntity user,String status,Integer customerId) {
		if(status.equals(UserType.Admin.name()) ||
				status.equals(UserType.VIP.name())) {
			return UserType.LifeMember.name();
		}
		else if(status.equals(UserType.Legacy.name()) || 
				status.equals(UserType.Subscribed.name()) ||
				status.equals(UserType.Unsubscribed.name()) ||
				!isUserExpiredOver30Days(user,customerId)) {
			return UserType.PaidMember.name();
		}
		else {
			return UserType.FreeMember.name();
		}

	}

	// Rest api
	public JBClientAppsInfoEntity getClientAppInfoByGuid(String guid, Integer enable) {
		try {
			return jbClientAppsInfoRepository.findByGuidAndEnable(guid, enable);
		} catch (Exception e) {
			return null;
		}
	}

	//search feature
	public List<Integer> getIdsByEntityNameAndKeyword(String keyword,String ids,String status) {
		String sql = "SELECT id FROM jb_entities where name like '%"+keyword+"%' and parentProjectId in ("+ids+") and status in ("+status+")" ;
		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<Integer> getIdsByEntityDescription(String keyword,String ids) {
		String sql = "SELECT id FROM jb_entities where description like '%"+keyword+"%'";	
		if(!ids.isEmpty())
			sql += " and id not in ("+ids+")";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<Integer> getIdsByChecklistName(String keyword,String ids) {
		String sql = "SELECT entity FROM jb_checklistitems where name like '%"+keyword+"%'";
		if(!ids.isEmpty())
			sql += " and entity not in ("+ids+")";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<Integer> getIdsByAttachmentName(String keyword,String ids) {
		String sql = "SELECT entity FROM jb_attachments where name like '%"+keyword+"%'";
		if(!ids.isEmpty())
			sql += " and entity not in ("+ids+")";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<Integer> getIdsByChatText(String keyword,String ids) {
		String sql = "SELECT entity FROM jb_chatmessages where text like '%"+keyword+"%'";
		if(!ids.isEmpty())
			sql += " and entity not in ("+ids+")";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<Long> getDirectRoleEntitysId(JBUserEntity user) {
		String sql = "SELECT b.entityId FROM jb_userroletoentity b WHERE b.userId="+user.getId();

		List<Long> results= (List<Long>) EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}

	public List<Integer> getDirectRoleEntitysIds(JBUserEntity user) {
		String sql = "SELECT b.entityId FROM jb_userroletoentity b WHERE b.userId="+user.getId();

		List<Integer> results= (List<Integer>) EntityUtil.getMoreNativeRec(em, sql);
		return results;
	}


	public List<Object[]> getAllEntitiesByUser(JBUserEntity user,String projectIds) {
		String sql = "select id,parentid,parentProjectid from jb_entities where parentProjectId in( " + 
				"SELECT id FROM jb_entities a WHERE  a.id in " + 
				"("+projectIds+"))";

		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAllEntitiesByUserAndKeyWord(JBUserEntity user,String projectIds,String keyword) {
		String sql = "Select id,name,parentid,parentProjectId from jb_entities " + 
				" where name like '%"+keyword+"%' and parentProjectId in " + 
				"("+projectIds+"))";

		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Integer> getAllEntityIdsByUser(JBUserEntity user) {
		String sql = "select id,parentid,parentProjectid from jb_entities where parentProjectId in( " + 
				"SELECT id FROM jb_entities a WHERE  a.id in " + 
				"(SELECT DISTINCT COALESCE(c.parentProjectId,c.id) FROM jb_entities c WHERE " + 
				" c.id in " + 
				"(SELECT b.entityId FROM jb_userroletoentity b WHERE b.userId="+user.getId()+")))";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<Integer> getAllProjectByUser(JBUserEntity user) {
		String sql = "SELECT id FROM jb_entities a WHERE  a.id in " + 
				"(SELECT DISTINCT COALESCE(c.parentProjectId,c.id) FROM jb_entities c WHERE " + 
				" c.id in " + 
				"(SELECT b.entityId FROM jb_userroletoentity b WHERE b.userId="+user.getId()+"))";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public boolean hasEntityAcess(JBUserEntity user,JBEntity entity) {
		String sql = "SELECT entityId FROM jibby.jb_userroletoentity where userId ="+user.getId()+" and \n" + 
				"			entityId in (SELECT T2.id FROM (SELECT @r AS _id,\n" + 
				"				(SELECT @r := parentid FROM jb_entities WHERE id = _id) AS parent_id,\n" + 
				"				@l := @l + 1 AS lvl\n" + 
				"				FROM\n" + 
				"				(SELECT @r := "+entity.getId()+", @l := 0) vars,\n" + 
				"				jb_entities m\n" + 
				"				WHERE @r <> 0) T1\n" + 
				"				JOIN jb_entities T2\n" + 
				"				ON T1._id = T2.id\n" + 
				"				ORDER BY T1.lvl DESC)";

		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		if(results.size()>0)
			return true;

		return false;
	}

	/**search stuff***/
	public List<Integer> getAllIdsByParentProjectId(String ids,String status) {
		String sql = "SELECT id FROM jb_entities where  parentProjectId in ("+ids+") and status in ("+status+")" ;
		List<Integer> results=EntityUtil.getMoreNativeIntegerRec(em, sql);
		return results;
	}

	public List<JBEntity> getAllEntitiesByParentProjectIds(List<Integer> entityIds){
		return jbEntityRepository.findByParentProjectId(entityIds).getResultList();
	}



	public List<Object[]> getAllSearchData(){
		String sql = "SELECT id,parentid,parentProjectId,name,description FROM jb_entities" ;
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}


	//search 
	public List<Object[]> getSearchAllEntitiesByUserFilter(JBUserEntity user,String projectIds) {
		String sql = "select id,parentid,parentProjectid from jb_entities where parentProjectId in( " + 
				"SELECT id FROM jb_entities a WHERE  a.id in " + 
				"("+projectIds+")) or id in ("+projectIds+") order by parentid";

		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAllRowDataByEntitiesId(String ids){
		String sql = "select entity,id,'attachment' as itemType,name,type,null as isChecked,uploadDate as date,null as sender from jb_attachments where entity in ("+ids+") " + 
				"union all " + 
				"select entity,id,'checklist',name,'',isChecked,null,null from jb_checklistitems where entity in ("+ids+") " + 
				"union all " + 
				"SELECT entity,id,'chats',text,'',null,date,sender FROM jb_chatmessages where entity in ("+ids+")";

		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAllUserDataByUserIds(String ids){
		String sql = "SELECT distinct id,email,concat(firstName,' ',lastName) as name,dateFormat,timeZone FROM jb_users where id in ("+ids+")";
		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<Object[]> getAllItemByParentProjectId(Integer projectId){
		String sql = "SELECT a.id,a.parentid,a.parentProjectId,a.name,a.description FROM jb_entities a WHERE a.parentProjectId = "+projectId+" or a.id="+projectId+" order by a.parentId,a.id";

		List<Object[]> results=EntityUtil.getMoreNativeObjectRec(em, sql);
		return results;
	}

	public List<String> getAllSubscriptionPaymentOrderNumberByUserIdCustomerId(Integer userId,Integer customerId,Integer itemType){
		String sql = "select b.orderNumber from jb_payments b where b.userId="+userId+" and b.customer_id="+customerId+" and b.itemType="+itemType+" ";
		List<String> results=EntityUtil.getMoreNativeStringRec(em, sql);
		return results;
	}

	public List<JBFavouritesEntity> getAllFavouriteProjectsByUser(Integer id) {
		return  jbFavouriteRepository.findByUserId(id).getResultList();
	}
	
	public JBEntity moveToTrash(JBEntity jbEntity) {
		jbEntity.setDeleteDate(new Date());
		jbEntity.setIsDelete(1);
		return jbEntityRepository.save(jbEntity);
	}

	public List<JBEntity> getAllTrashProjectForColA(JBUserEntity user, Integer roles, Integer customerId, boolean isAsc, String criteria) {
		if(isAsc)
			return jbEntityRepository.findColATrashProject(user, roles, customerId).orderAsc(criteria, false).getResultList();
		else
			return jbEntityRepository.findColATrashProject(user, roles, customerId).orderDesc(criteria, false).getResultList();
	}

	public JBEntity restoreFromTrash(JBEntity jbEntity) {
		jbEntity.setDeleteDate(null);
		jbEntity.setIsDelete(0);
		jbEntity.setOnHold(0);
		return jbEntityRepository.save(jbEntity);
	}

	public JBFavouritesEntity getFavouriteByUseridAndEntityId(Integer userId, Integer entityId) {
		try{
			return jbFavouriteRepository.findByUserIdAndEntityId(userId,entityId);
		}catch(Exception e){
			return null;
		}
	}

	public boolean updateCustomerForUserRoles(Integer customerId, boolean isAllChildUpdate,Integer entityId) {
		try {
			String sub = isAllChildUpdate?"parentProjectId = "+entityId+" or ":" ";
			
			String sql = "update jb_userroletoentity set customer_id = "+customerId+" where entityId in (select id from jb_entities where "+sub+" id = "+entityId+") and id<>0";
			
			EntityUtil.updateRec(em, sql);
			return true;
			
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isOwnCustomerForProject(Integer parentProjectId,Integer customerId) {

		String sql = "SELECT customer_id FROM jb_userroletoentity where  entityId = (select id from jb_entities where parentProjectId = "+parentProjectId+" limit 1);";

		List<Integer> customers=EntityUtil.getMoreNativeIntegerRec(em, sql);

		if(customers.contains(customerId))
			return true;

		return false;
	}

}