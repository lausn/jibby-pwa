import '@polymer/polymer/lib/elements/custom-style.js';
import '@vaadin/vaadin-lumo-styles/badge.js';

const $_documentContainer = document.createElement('template');

$_documentContainer.innerHTML = `
<style>
/*@import url('https://fonts.googleapis.com/css?family=Open+Sans');*/
@font-face {font-family: "Open Sans";
   src: url("./fonts/OpenSans-Regular-webfont.woff") format("woff"); /* chrome firefox */
}

@font-face {font-family: "Open Sans semibold";
   src: url("./fonts/OpenSans-Semibold-webfont.woff") format("woff"); /* chrome firefox */ 
}
@font-face {font-family: "Montserrat";
   src: url("./fonts/Montserrat-ExtraBold.woff") format("woff"); /* chrome firefox */
   
    
}

@font-face {font-family: "Roboto";
     src: url("./fonts/roboto-condensed-v18-latin-regular.woff") format("woff"); /* chrome firefox */
  
}



</style>
<!-- shared styles for all views -->

<dom-module id="textfield-style" theme-for="vaadin-text-field">
  <template>
    <style>
    :host(#email) [part="label"],:host(#password) [part="label"] {
		color:#fff !important;
	}
	:host(#fullname) [part="label"],:host(#re-password) [part="label"] {
		color:#fff !important;
	}
	:host(#email) [part="input-field"],:host(#password) [part="input-field"] {
    	background-color: #fafafa24;
	}
	:host(#fullname) [part="input-field"],:host(#re-password) [part="input-field"] {
    	background-color: #fafafa24;
	}
    </style>
  </template>
</dom-module>

<dom-module id="tabs-style" theme-for="vaadin-tabs">
  <template>
    <style>
	   :host([orientation="horizontal"]) [part="tabs"]{
	   		margin: 0 !important;
	   }
    </style>
  </template>
</dom-module>

<dom-module id="button-styles" theme-for="vaadin-button">
  <template>
    <style>

      :host(vaadin-button),
      [part~=themed-button] {
        /* Styles for a button */
        
      }
      :host(#itemname) .vaadin-button-container,:host(#btnLogout) .vaadin-button-container,:host(#btnFilter) .vaadin-button-container{
    		justify-content: left;
    		text-align: left;
    		color: #506f7d;
      }
      :host(#btnLogout) .vaadin-button-container,
       :host(#btnMyAction) .vaadin-button-container,
       :host(#btnProjectview) .vaadin-button-container,
       :host(#btnUsername) .vaadin-button-container{
       		justify-content: left;
    		text-align: left;
    		color: #000;
       }
      :host(#itemname) [part] ::slotted(iron-icon) {
			    height:17px;
			    width:17px;
		}
      :host(#parentItem) .vaadin-button-container{
    		color: #fff;
      }
      :host(.btn-tab){
     	 --lumo-primary-color:hsl(198, 56%, 36%);
      }
      
      :host(.btnRowDelete){
      	    background-color: transparent !important;
      }
      
      :host(.jibbySubmit){
      	    background-color: #10708e !important;
      	    color : #fff !important;
      	    border-radius: 0px !important;
      	    font-weight: 600;
      }
      :host(.jibbyCancel){
      	    background-color: #f44336 !important;
      	    color : #fff !important;
      	    border-radius: 0px !important;
      	    font-weight: 600;
      }
      
    </style>
  </template>
</dom-module>

<dom-module id="tab-styles" theme-for="vaadin-tab">
  <template>
    <style>
		:host{
			--lumo-font-size-s: 0.8rem;
			--lumo-primary-color: #FFC107;
			font-size: var(--lumo-font-size-s);
		}
      :host([selected])::before, :host([selected])::after {
    		width:100%;
		}
    </style>
  </template>
</dom-module>

<dom-module id="jibby-grid" theme-for="vaadin-grid">
  <template>
    <style>
   
	  :host(#gridUser) [part~="cell"] ::slotted(vaadin-grid-cell-content){
	  		font-size:15px;
	  }
	  
	 :host(#gridUser) [part="row"]:first-child [part~="header-cell"]:first-child{
            display:none;
   }
	
	  
    </style>
  </template>
</dom-module>
<!-- shared styles for all views -->
<dom-module id="shared-styles">
  <template>
    <style>
      *,
      *::before,
      *::after,
      ::slotted(*) {
        box-sizing: border-box;
      }
      :root{
      	--lumo-font-size-s:0.7rem;
      	font-size: var(--lumo-font-size-s);
      }
      :host([hidden]),
      [hidden] {
        display: none !important;
      }
      h2,
      h3 {
        margin-top: var(--lumo-space-m);
        margin-bottom: var(--lumo-space-s);
      }

      h2 {
        font-size: var(--lumo-font-size-xxl);
      }

      h3 {
        font-size: var(--lumo-font-size-xl);
      }

      .scrollable {
        padding: var(--lumo-space-m);
        overflow: auto;
        -webkit-overflow-scrolling: touch;
      }

      .count {
        display: inline-block;
        background: var(--lumo-shade-10pct);
        border-radius: var(--lumo-border-radius);
        font-size: var(--lumo-font-size-s);
        padding: 0 var(--lumo-space-s);
        text-align: center;
      }

      .total {
        padding: 0 var(--lumo-space-s);
        font-size: var(--lumo-font-size-l);
        font-weight: bold;
        white-space: nowrap;
      }

      @media (min-width: 600px) {
        .total {
          min-width: 0;
          order: 0;
          padding: 0 var(--lumo-space-l);
        }
      }

      @media (max-width: 600px) {
        search-bar {
          order: 1;
        }
      }

      .flex {
        display: flex;
      }

      .flex1 {
        flex: 1 1 auto;
      }

      .bold {
        font-weight: 600;
      }
      
      .margintop10px{
      	margin-top: 10px;
      }
    
    </style>
  </template>
</dom-module>

 <dom-module id="my-button" theme-for="vaadin-button">  
  <template>  
    <style>  
    :host(.leftMenu){
    background: transparent;
    margin: 0;
    }
    :host(.active){
		background:#0a5a73;
	}
:host(.leftMenu) .vaadin-button-container div[part="label"] {  
    text-align: left;  
    width: 100%;  
    color:#fff;
}  
	
    </style>  
    
  </template>  
</dom-module> 

<dom-module id="my-dialog-customizations" theme-for="vaadin-dialog-overlay">
  <template>
    <style>
	  :host([theme~="my-custom-dialog"]){
		   top: 0;
	    left: 0;
	    bottom: 0;
	    width: 100%;
	  }
	</style>
  </template>
</dom-module>

<dom-module id="lausn-dialog-overlay-theme" theme-for="vaadin-dialog-overlay">
  <template>
    <style>
      /* :host {
        max-height:55%;
      } */
      
	[part="overlay"]{
		font-size: var(--lumo-font-size-s);
	}
	 [part="content"] {
    	padding: 0px !important;
	} 
        :host(.item-list-popup) { 
          top: 0 !important; 
          left: 0 !important; 
          right: 0 !important; 
          bottom: 0 !important; 
          padding: 0 !important; 
        } 
        :host(.item-list-popup) [part="overlay"] { 
          height: 100%; 
          width: 100%; 
          border-radius: 0 !important; 
        } 
        
        :host(.setting-menu) { 
         	display: inline-block !important;
         	width:100% !important;
         	left: 0 !important;
    		bottom: 0 !important;
        }
        
        :host(.setting-menu){
        	--_lumo-dialog-transform: translateY(0);
        }
        
        :host(.setting-menu) [part="overlay"] { 
          	position: absolute;
		    bottom: 0 !important;
		    width: 100% !important;
        }
        
        :host(.attachmment-popup) { 
          top: 0 !important; 
          left: 0 !important; 
          right: 0 !important; 
          bottom: 0 !important; 
          padding: 0 !important; 
        } 
        :host(.attachmment-popup) [part="overlay"] { 
      /*     height: 100%;  */
          width: 100%; 
          border-radius: 0 !important; 
        } 
      
    </style>
  </template>
</dom-module>


<dom-module id="jibby-upload" theme-for="vaadin-upload">
  <template>
    <style>
     :host #fileList{
     	display: none !important;
     }
         
    :host(#supportUploader) [id="fileList"]  {
	
	    display: block !important;
	}
	 
    </style>
  </template>
</dom-module>


<dom-module id="jibby-attachment-popup" theme-for="vaadin-vertical-layout">
  <template>
    <style>
     :host(.overflow){
     	overflow: auto;
     }
	 
    </style>
  </template>
</dom-module>

<dom-module id="assigneepopup" theme-for="vaadin-checkbox">
  <template>
    <style>
     :host([checked]) [part="checkbox"] {
    		background-color: #116686 !important;
	 }
	 
	:host(.task-checkbox) [part=checkbox]::before {
	    color: transparent;
	    display: inline-block;
	    width: 100%;
	    height: 100%;
	    border-radius: inherit;
	    background-color: inherit;
	    transform: scale(1.4);
	    opacity: 0;
	    transition: transform .1s,opacity .8s;
    }
    
    :host(.task-checkbox[checked]) [part=checkbox]{
    	background-color: #f3f3f3 !important;
    }
    
    :host(.task-checkbox) [part=checkbox]::after {
	    border-color: var(--lumo-primary-text-color);
	    transform: translate(-4px,-1px) scale(1) rotate(-135deg);
	    border-width: 2px 0 0 2px;
	 }
	 
	 
	 label{
		width: 100% !important;
		/* display: block !important; */
	}
	
	label [part="label"]:not(:empty) {
    	width: 100%;
	}
	 
    </style>
  </template>
</dom-module>

<dom-module id="radio-button-jibby" theme-for="vaadin-radio-button">
  <template>
    <style>
	    :host([checked]) [part="radio"] {
	    		background-color: #116686 !important;
		}
		
		[part="label"]:not([empty]){
			margin-left: 2px !important;
			margin-right: 4px !important;
		}
		
    </style>
  </template>
</dom-module>


<custom-style>
  <style>
  	.pad-10px{
  		padding: 10px;
  	}
:root {
 
    --app-layout-bar-height: 50px !Important;
    --app-layout-bar-background-color: #10708e !Important;

  	--lumo-font-size-s:0.7rem;
  	
  	--app-layout-bar-shadow: none !important;

  	
}


    @keyframes v-progress-start {
      0% {
        width: 0%;
      }
      100% {
        width: 50%;
      }
    }
    
    body{
    	font-size: 16px;
    }
    select, textarea, input{ 
     	font-size: 16px; 
    }

    .v-loading-indicator,
    .v-system-error,
    .v-reconnect-dialog {
    	position: absolute;
    	left: 0;
    	top: 0;
    	border: none;
    	z-index: 10000;
    	pointer-events: none;
    }

    .v-system-error,
    .v-reconnect-dialog {
    	display: flex;
    	right: 0;
    	bottom: 0;
    	background: var(--lumo-shade-40pct);
    	flex-direction: column;
      align-items: center;
      justify-content: center;
      align-content: center;
    }

    .v-system-error .caption,
    .v-system-error .message,
    .v-reconnect-dialog .text {
    	width: 30em;
    	max-width: 100%;
    	padding: var(--lumo-space-xl);
    	background: var(--lumo-base-color);
    	border-radius: 4px;
    	text-align: center;
    }

    .v-system-error .caption {
    	padding-bottom: var(--lumo-space-s);
    	border-bottom-left-radius: 0;
    	border-bottom-right-radius: 0;
    }

    .v-system-error .message {
    	pointer-events: all;
    	padding-top: var(--lumo-space-s);
    	border-top-left-radius: 0;
    	border-top-right-radius: 0;
    	color: grey;
    }

    .v-loading-indicator {
    	position: fixed !important;
    	width: 50%;
    	opacity: 0.6;
    	height: 4px;
    	background: #fff !Important;
    	transition: none;
    	animation: v-progress-start 1000ms 200ms both;
    }

    .v-loading-indicator[style*="none"] {
    	display: block !important;
    	width: 100% !important;
    	opacity: 0;
    	transition: opacity 400ms 300ms, width 300ms;
    	animation: none;
    }
    
  </style>
</custom-style>

`;

document.head.appendChild($_documentContainer.content);
