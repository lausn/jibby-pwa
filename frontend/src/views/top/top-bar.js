import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/iron-icon/iron-icon.js';

/**
 * `top-bar`
 *
 * TopBar element.
 *
 * @customElement
 * @polymer
 */
class TopBar extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: auto;
                    margin-left: auto;
                }
                .setting {
        		white-space: nowrap;
        		position: relative;
        		display: inline-block;
        		height: 35px;
        		width: 35px;
        		text-align: center;
        	}
.flag-style {
	height: 25px;
	
	background: none;
    padding: 0px 7px 0px 7px;
    cursor: pointer;
    border-radius: 0 !important;
    border-right: 1px solid rgb(243, 243, 247);
    border-left: 1px solid rgb(243, 243, 247);
}
.item, .item>span {
	width: 100%;
	display: block;
	height: 100%;
		font-family: "Neo Sans Std";
}
.item:hover {
	cursor: pointer;
	background-color: #dff3f0;
}
.dropdown {
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .14), 0 1px 5px 0
		rgba(0, 0, 0, .12), 0 3px 1px -2px rgba(0, 0, 0, .2);
	position: relative;
	border-radius: 2px;
	background-color: var(- -paper-menu-button-dropdown-background, var(-
		-primary-background-color));
	display: none;
	color: rgb(102, 104, 115);
	font-size: .97rem;
	background: #fff;
	min-width: 170px;
}

.open {
	outline: none;
	position: absolute;
	    right: 24px;
    top: 30px;
	display: block !Important;
}

</style>
            
<div style="width:100%; height:100%;">
 <vaadin-horizontal-layout theme="spacing" style="height:100%;">
  <vaadin-button class="flag-style" id="flag" theme="icon"></vaadin-button>
  <div class="setting"  on-mouseover="onHovered" on-mouseout="onUnhovered">
	<iron-icon icon="vaadin:cog" style = "fill:grey;"></iron-icon>
  </div>
   <div class="dropdown" on-mouseover="onHovered" on-mouseout="onUnhovered">
    <vaadin-item class="item">
     [[userName]]
   </vaadin-item>
   <vaadin-item class="item" style = "color:red;" on-click="logoutProcess">
     Logout
   </vaadin-item>
  </div>
 
 </vaadin-horizontal-layout>
</div>
        `;
    }

    static get is() {
        return 'top-bar';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    
    onHovered() {
	       this.hovered = true;
	       this.shadowRoot.querySelector('.dropdown').classList.add("open");
	      }
	      onUnhovered() {
	        this.hovered = false;
	        this.shadowRoot.querySelector('.dropdown').classList.remove("open");
	      }
}

customElements.define(TopBar.is, TopBar);
