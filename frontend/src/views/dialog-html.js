import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `dialog-html`
 *
 * DialogHtml element.
 *
 * @customElement
 * @polymer
 */
class DialogHtml extends PolymerElement {

    static get template() {
        return html`
  <style> #btnCancel{
	--lumo-primary-color: red;
	}
</style>
<div style="width: 100%; height: 100%;">
 <div id="dialogtext"></div>
 <div>
  <vaadin-button style="color:#fff;float:right;" id=" btncancel" theme="primary small" class="btn-tab">
    Cancel 
  </vaadin-button>
  <vaadin-button style="color:#fff;float:right;" id=" btnsave" theme="primary small" class="btn-tab">
    Ok 
  </vaadin-button>
 </div>
</div>
        `;
    }

    static get is() {
        return 'dialog-html';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(DialogHtml.is, DialogHtml);
