import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `user-settings-popup-page`
 *
 * UserSettingsPopupPage element.
 *
 * @customElement
 * @polymer
 */
class UserSettingsPopupPage extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
            <vaadin-vertical-layout style="width: 100%; height: 100%; padding:30px;">
            	<div style="width: 100%;">
            		<div id="uploadLayout" style="display: inline-block;width:48%;height:110px">
            			
            		</div>
            		<div style="display: inline-block;width:48%;vertical-align: top;margin-left: 6px;">
            			<label style="font-size: 12px;width: 100%;display: block;">First Name:</label>
            			<vaadin-text-field id="tfFirstName" style="width: 100%;" theme="small"></vaadin-text-field>
            			<label style="font-size: 12px;width: 100%;display: block;">Last Name:</label>
            			<vaadin-text-field id="tfLastName" style="width: 100%;" theme="small"></vaadin-text-field>
            		</div>
        		</div>
        		<label style="font-size: 12px;width: 100%;display: block;margin-top: 5px;">Email:</label>
            	<vaadin-text-field id="tfEmail" style="width: 100%;" theme="small" readonly></vaadin-text-field>
            	<label style="font-size: 12px;width: 100%;display: block;margin-top: 5px;">Email about unfinished activities:</label>
            	<div id="divEmailAboutItemChangeSelect" style="width: 100%;"></div>
            	<label style="font-size: 12px;width: 100%;display: block;margin-top: 5px;">Email about unfinished activities:</label>
            	<div id="divEmailAboutUnfinishedSelect" style="width: 100%;"></div>
            	<label style="font-size: 12px;width: 100%;display: block;margin-top: 5px;">Date Format:</label>
            	<div id="divDateFormat" style="width: 100%;"></div>
            	<div style="text-align: center;align-items: center;justify-items: center;margin-top: 10px;width: 100%;">
					<vaadin-button style="margin-right: 5px;color:#fff;width:47%;--lumo-primary-color: red;" id="btnCancel" theme="primary small" class="btn-tab">Cancel</vaadin-button>
					<vaadin-button style="color:#fff;width:47%;" id="btnSave" theme="primary small" class="btn-tab">Save</vaadin-button>
		        </div>
            </vaadin-vertical-layout>
        `;
    }

    static get is() {
        return 'user-settings-popup-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(UserSettingsPopupPage.is, UserSettingsPopupPage);
