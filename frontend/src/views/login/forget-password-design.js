import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/src/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/iron-icon/iron-icon.js';
import '../../../styles/shared-styles.js';
/**
 * `forget-password-design`
 *
 * ForgetPasswordDesign element.
 *
 * @customElement
 * @polymer
 */
class ForgetPasswordDesign extends PolymerElement {

    static get template() {
        return html`
  
  
              <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                      --lumo-primary-color: #10708e;
                }
				  .margin-top25px {
					 height: 100%;
					text-align: center;
				}
				.txtfieldstyle{
					width :250px;
				}
				.lblmessage {
				text-align: center;
				font-weight: 400;
				width: 90%;
				padding-left: 15px;
				
				
			}
			.padtop10 {
				padding-top: 10px;
				text-align: center;
			}
			.flayoutstyle{
				margin-top: 10%;
			}
            </style>

  
		 <div class="margin-top25px">
		   <div  class="flayoutstyle" id="flayout" >
		   
		   <label style="margin-right: 10px;">Email</label>
			<vaadin-text-field  class="txtfieldstyle"  id="txtEmail">Email</vaadin-text-field>
		   </div>
		   <div class="lblmessage" id="lblmessage"></div>
		   <div class="padtop10" >
				<vaadin-button theme="primary"  id="btnSend" style="margin-right: 7px;">
					<iron-icon icon="vaadin:forward" style="fill:#fff"></iron-icon> Send 
					</vaadin-button>
				<vaadin-button theme="primary"  id="btnCancel">
					<iron-icon icon="vaadin:close" style="fill:#fff"></iron-icon>Cancel 
				</vaadin-button>
		   </div>
		  </div>
        `;
    }

    static get is() {
        return 'forget-password-design';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(ForgetPasswordDesign.is, ForgetPasswordDesign);
