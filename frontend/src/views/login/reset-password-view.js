import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/src/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/iron-icon/iron-icon.js';
import '../../../styles/shared-styles.js';

/**
 * `reset-password-view`
 *
 * ResetPasswordView element.
 *
 * @customElement
 * @polymer
 */
class ResetPasswordView extends PolymerElement {

    static get template() {
        return html`
    <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                    --lumo-primary-color: #10708e;
                }
				              
                 .TopNav{
				    min-height:40px;
				    box-shadow:0 0 3px #aaa;
				    border-bottom:1px solid #e1e8ed;
				    border-bottom:0 solid rgba(0,0,0,0.25);
				    color:#66757f;
				    font-size:14px;
				    -moz-box-sizing:border-box;
				    box-sizing:border-box;
				    position:relative;
				    z-index:1;
				    background-image: linear-gradient(#10708e, #024f67);
				    background-image: -webkit-linear-gradient(#10708e, #024f67);
				    padding: 0 15%;
				}
				
				.TopNav--container{
				    max-width:590px;
				    margin:0 auto;
				    position:relative;
				    top:1px
				    padding-top: 10px;
				    padding-bottom: 10px;
				}
				.TopNav--container>select,.TopNav--container>ul,.TopNav--container>.TopNav-title,.TopNav--container>.Icon--logo{
				
				}
				.TopNav .Icon--logo{
				    margin-right: 6px;
				    color: #ffffff;
				    padding-top: 10px;
				}
				.TopNav ul{
				    list-style-type:none;
				    margin:0
				}
				.TopNav li{
				    display:inline-block
				}
				.TopNav-title{
				    font-size: 16px;
				    color: #ffffff;
				    padding-top: 15px;
				}
				.TopNav-languageSelector .Dropdown-menu{
				    width:250px;
				    font-size:12px
				}
				.TopNav-languageSelector select{
				    width:125px
				}
				
				.u-cf:before,.u-cf:after{
					    content:" ";
					    display:table
					}
					.u-cf:after{
					    clear:both
					}
					
					.Icon--logo img{
				   
				    vertical-align:text-bottom
				}
				.u-pullLeft{
					    float:left
					}
					
					.TopNav-title{
				    font-size: 16px;
				    color: #ffffff;
				    padding-top: 15px;
				}
				.centering-layout{
					margin-top: 2%;
				}
				.label-style{
				width: 100px;
				display: inline-block;
				text-align: left;
				}
				.textfielddiv{
				    margin-bottom: 5px;
				}
				
				
            </style>
  
  
			   <div style="height:100%; text-align: center;">
				<div class="TopNav">
						 <div class="TopNav--container u-cf" style="margin-right: 43px;">
							<div class="Icon--logo u-pullLeft">
							   <a href="https://app.jibby.com"><img alt="jibby" src="./images/jibby.png"></a>
							</div>
							<div class="TopNav-title u-pullLeft">Reset Password</div>
						 </div>
				</div>
				<div class="centering-layout" spacing="false" style="height:80%;">
				 <div class="login-form" >
					<div class="textfielddiv">
						<label class="label-style">Email</label>
						<vaadin-text-field   id="txtUserName"></vaadin-text-field>
					</div>
					<div class="textfielddiv">
						<label class="label-style">New password</label>
						<vaadin-password-field  id="txtPassword"></vaadin-password-field>
					</div>
					<div class="textfielddiv">
						<label class="label-style">Retype password</label>
						<vaadin-password-field  id="txtRePassword"></vaadin-password-field>
					
					</div>
					<div style="width:100%; text-align:center;">
						<vaadin-button  theme="primary"  id="btnReset">Reset </vaadin-button>
					</div>
				  <label id="lblMessage"></label>
				 </div>
				</div>
			   </div>
        `;
    }

    static get is() {
        return 'reset-password-view';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(ResetPasswordView.is, ResetPasswordView);
