import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/iron-icon/iron-icon.js';

class LoginPage extends PolymerElement {

    static get template() {
        return html`
           <style include="shared-styles">
                :host {
		                 display: block;
		                
			           --lumo-primary-color: #10708e;
					   width: 100%;
					   height: 100vh;
					   --app-primary-color: var(--lumo-primary-color);
					   --app-secondary-color: var(--lumo-body-text-color);
					   background-color: var(--lumo-shade-5pct);
					   display: flex;
					   flex-direction: column;
					   color: var(--lumo-body-text-color);
					   font-family: var(--lumo-font-family);
					   /*justify-content: center;*/
			        	overflow: auto;
			        	-webkit-overflow-scrolling: touch;
			        	position: absolute;
			        	background-image: url(../../jibbymobile/images/jibby-banner-page-new.png);
                }
                
            .color-overlay{
		   width: 100%;
		   height: 265px;
		   position: absolute;
		   background: linear-gradient(25deg,rgba(183, 183, 183, 0),rgba(105, 105, 105, 0.23) 50%);
		   opacity: 0.5;
		   }
		   .full-width{
		   width:100%;
		   }
		   .login-banner{
		   background-image: url(../../jibbymobile/images/jibby-banner-page-new.png);
		   width: 100%;
		   background-size: cover;
		   height: 265px;
		   }
		   .login-circle{
		   margin: auto;
		   background-image: -webkit-linear-gradient(#3e90b7, #1b6880);
		   width: 115px;
		   height: 115px;
		   border-radius: 50%;
		   box-shadow: 0 10px 30px 0 rgba(0,0,0,0.15) !Important;
		   margin-top: -98px;
		   z-index: 1;
		   position: relative;
		   }
		   .sign-up{
		   font-size: 13px;
		   color: #d81a1a;
		   text-align: center;
		   width: 100%;
		   margin-top: 14px;
		   font-weight: 450;
		   }
		   .overlay{
		   position: absolute;
		   width: 100%;
		   height: 266px;
		   background: linear-gradient(25deg,#f2f5f9,#f1f4f8 80%);
		   clip-path: polygon(100% 100%, 100% 74%, 0% 100%);
		   -webkit-clip-path: polygon(100% 100%, 100% 74%, 0% 100%);
		   }
		   .error_text{
			font-size: var(--lumo-font-size-xs);
    		color: var(--lumo-error-text-color);
		    width: calc(100% - 20px);
		    float: left;
		    margin: 0;
		    margin-left: 5px;
		    margin-top: 2px;
		   }
		   .error_icon{
		   	width: 14px;
		    float: left;
		    color: var(--lumo-error-text-color);
		   }
		   .error{
		   	width: 100%;
		   }
		   
		   
		   
		   .logui{
			background-color: transparent;
			background-size: cover;
			width: 100%;
			padding: 20px;
			margin: auto 0;
		}
	.login-content{
        margin: auto;
      
    }

		.label1{
           font-size: 1rem;
        line-height: 19px;
        font-weight: 300 !important;
        color: white !important;
        letter-spacing: 0px;
        white-space: nowrap;
        opacity: 1;
	}
	.label2{
    white-space: nowrap;
    font-size: 1.5rem;
    font-weight: 600;
    color: white;
    letter-spacing: 0px;
    font-family: "Montserrat";
    opacity: 1;
    margin-top:10px;
	}
	
	.label3{
        	white-space: normal;
		    line-height: 22px;
		    font-weight: 400;
		    color: white;
		    letter-spacing: 0px;
		    font-family: "Roboto";
		    text-align: center;
		    margin-top: 8px;
		    height: 110px;
		    font-size: 0.75rem;
	}
	
	.span1{
	color: #ff7028;
    font-size: 14px;
    text-align: inherit;
    line-height: 32px;
    letter-spacing: 0px;
    font-weight: 400;
    text-decoration: underline !important;

	}
	
	.logo-content{
		display: inline-block;
		text-align: center;
		max-width:365px;
		overflow: auto;
	}
	
	.brifcase{
	display: inline-block;
 
    background-image: url(../../jibbymobile/images/75904_w.png);
    background-repeat: no-repeat;
    background-size: 166px;
    float: left;
    margin-top: 110px;
	}
	
	.logo{
        height: 40px;
        margin-top: 15px;
	}
	
	.info-style{
		color: #fff;
	    text-align: center;
	    float: left;

		display: inline-block;
	}
	
	.bottom_quick_button{
      margin-bottom:10px;
	}
	
	.form-style{
		
	    display: inline-block;
	    background: #fff;
	    height: 385px;
	    padding: 45px 28px 28px 28px;
	    border-radius: 0 5px 5px 0;
	}
	
.username-filed-icon{
   		text-indent: 28px;
    	background-size: 18px 20px;	
    	margin-bottom: 10px;
	background: url(../../jibbymobile/images/loginUser.png) no-repeat;
}  
    .btnSignIn{
		background-color: #024f67;
	    color: #fff;
		margin-left: 30px;
        border-radius: inherit !important;
    }
	.login-hl-btn{
        margin-top: 5px;
        width:100%;
	}
	.forgot-style{
	    color: #fff;
		font-weight: 550;
		float:left;
		margin-top: 5px;
	}
	
	.goggle-icon-style{
		vertical-align: middle;
	    margin: -1px;
	}
	
	
	.loginOrText{
		height: 22px;
   		width: 22px;
   		border-radius: 11px;
        background-color: #9e9e9e;
        text-align: center;
        font-style: italic;
        font-weight: bold;
        margin-left: 45%;
		color: #fff;
	}
	
		
   .loginfooter{
   	 margin: 0 auto;
   }
   
   .copyright-style{
    text-decoration: none;
	color: #fff;
   }
   
   .signupPageFooterLink{
	color: #fff !important;
    font-weight: 500;
    margin-left: 15px;
    text-decoration: none !important;
   }
   
   .signin-label-style{
   font-weight: bold;
   }
   
   .signin-div-style{
    
     margin-top: 0px;
   }
   
	.quick_intro .v-button-wrap{
	display: inline-block;
}

.quick_intro iron-icon{
		font-size: 1rem;
	}
	
 
  .quick_intro{
	       white-space: nowrap;
		    font-size: 0.7rem;
		    line-height: 20px;
		    font-weight: 400;
		    color: rgb(255, 255, 255);
		    font-family: "Roboto";
		    text-transform: uppercase;
		    background-color: rgb(2, 80, 104);
		    border-color: rgb(0, 0, 0);
		    border-radius: 30px !important;
		    outline: none;
		    box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;
		    box-sizing: border-box;
		    letter-spacing: 1.5px;
		    cursor: pointer;
		
		    margin: 0px;
		    padding: 10px 10px !important;

   }
   
     .siguup_button{
	    text-decoration: none !important;
	    white-space: nowrap;
	    font-size: 0.7rem;
	    line-height: 20px;
	    font-weight: 400;
	    color: rgb(255, 255, 255);
	    font-family: "Roboto";
	    text-transform: uppercase;
	    background-color: rgb(221, 75, 57);
	    border-color: rgb(0, 0, 0);
	    border-radius: 30px;
	    outline: none;
	    box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;
	    box-sizing: border-box;
	    letter-spacing: 1.2px;
	    cursor: pointer;
	    padding: 8px 11px !important;
	    height: 37px !important;
	    float: left;
  }
  .btnsign-in{
    white-space: nowrap;
    font-size: 0.8rem;
    line-height: 20px;
    font-weight: 400;
    color: rgb(255, 255, 255);
    font-family: "Roboto";
    text-transform: uppercase;
    background-color: rgb(2, 80, 104);
    border-color: rgb(0, 0, 0);
    width:100%;
    outline: none;
    box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;
    box-sizing: border-box;
    letter-spacing: 1.5px;
    cursor: pointer;
    margin: 0px;
    padding: 14px 10px 10px !important;
    height: 46px;
  }
  
  .siguup_button iron-icon{
		float: right;
    	margin-left: 8px;
    	
    	width: 14px;
        margin-top: -2px;
	}

 .whitecolor{
      color:#fff;
 }

.signupbtn{
    position:relative;
    top:11px;
    left:10px;
}

.btn-google{
        background: #4285F4 none repeat scroll 0 0 !important;
	    border-image: none !important;
	    color: #ffffff !important;
	    cursor: pointer !important;
	    outline: medium none !important;
	    position: relative !important;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.05) !important;
	    white-space: nowrap !important;
	    font-size: 14px;
	    font-weight: 600 !important;
	    display: inline-block;
	    padding-right: 8px !important;
	    height: 44px !important;
}

.btn-facebook {
        background: #4267b2 none repeat scroll 0 0 !important;
	    border-color: #4267b2 #4267b2 #4267b2 !important;
	    border-image: none !important;
	    border-radius: 2px !important;
	    color: #ffffff !important;
	    cursor: pointer !important;
	    font-weight: 500 !important;
	    outline: medium none !important;
	    position: relative !important;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.05) !important;
	    white-space: nowrap !important;
	    height: 41px !important;
	    padding-left: 1px !important;
	    padding-top: 1px !important;
	    padding-right: 0px !important;
	    font-size: 14px;
	    font-weight: 600 !important;
	    display: inline-block;
}

.btn-facebook-icon{
    border-radius: 2px;
    height: 38px;
    padding-right: 11px !important;
    padding-top: 5px;
    padding-left: 5px;
    background: #fff;
    color: #4267b2;
    font-size: 19px;
    width: 40px;
}
.btn-facebook span {
        margin-left: 10px;
	    height: 30px;
	    padding-top: 5px;
	    display: inline-block;
	    padding-right: 12px;
	    font-family: Roboto;
	    font-weight: 600;
}
.btn-google span {
	    font-family: Roboto;
	    color: #fff;
	    font-size: 14px !important;
	    padding-left: 16px !important;
	    display: inline-block;
	    margin-right: 10px;
}
.btn-facebook a {
    text-decoration: none;
    color: white;
}

	.facebook-icon-style{
	    padding: 7px;
	    background: #fff;
	    color: #4267b2;
	    font-size: 18px;
	    float: left;
	    border-radius: 1px;
	    padding-right: 6px;
	    padding-top: 8px;
	}
                
            </style>
            
            <div class="logui">
 <div class="login-content full-width">
        
    <div id="info" margin="false" spacing="false" class="info-style full-width">
		
	<vaadin-vertical-layout class="logo-content full-width" margin="false" spacing="true">
      <div class="label1">
        Your team needs to 
		</div>
		<div class="label2">
        GET ORGANIZED WITH
		</div>
		<img width="130px" class="logo" src="images/logo.png"/>
      <div class="label3">
       <div>
         	<div>
         		<span>Share and manage any </span>
        		<span class="span1">PROJECT ORIENTED</span> operation 
        	</div>
        <div><span> Construction - Development - Groups ... anything</span></div> 
        <div><span>Simple - Flexible - Scaleable</span> </div>
       </div>
      </div>
      
      <div class="bottom_quick_button">
       <div  style="display:inline-block;text-align: left;">
        <vaadin-button class="primary quick_intro" id="btnquick" on-click="_play">
          QUICK INTRO 
          <iron-icon icon="vaadin:caret-right" style="fill:#fff"></iron-icon>
        </vaadin-button>
       </div>
	   
	   <div style="display:inline-block;  text-align: left;" class="signupbtn">
			<a class="siguup_button"  style="margin-left: 5px; display: inline-block;" href="./signup" id="easySignup">SIGN UP - IT'S FREE
			<iron-icon icon="vaadin:sign-in" style="fill:#fff"></iron-icon>
			</a>
		</div>

      </div>
      
  <vaadin-vertical-layout theme="spacing" style="overflow: auto;">
   <template is="dom-if" if="[[error]]">
    <div class="error">
     <iron-icon icon="vaadin:exclamation-circle-o" class="error_icon"></iron-icon>
     <p class="error_text" style="loginErrorText">Invalid username or password! Please try again.</p>
    </div>
   </template>
   <vaadin-text-field class="full-width whitecolor" label="Email" id="email" required="true" autofocus="true" error-message="Email shouldn't be emply!"></vaadin-text-field>
   <vaadin-password-field style="padding-top:0px" class="full-width whitecolor" label="Password" id="password" required="true" error-message="Password shouldn't be emply!" invalid></vaadin-password-field>
   <vaadin-button class="btnsign-in" style="width:100%;margin-top:15px;" theme="primary" id="signin">
    <iron-icon icon="vaadin:sign-in"></iron-icon> SIGN IN 
   </vaadin-button>
   <div>
					<a  id="lblForget" class="forgot-style">Forgot your Password?  </a>

	</div>
   <div style="width:100%;text-align: center;margin-bottom: 8px;">
  
					<div style="margin-bottom: 10px;">
						<div class="btn-google">
							<a id="gplusLoginButton" style="text-decoration: none;">
								<img class="goggle-icon-style" src="./images/btn-google-signin.svg">
								<span>Sign in with Google</span>
							</a>
						</div>
					</div>
						<div class="btn-facebook">
							<a id="facebookLoginButton">
								<iron-icon icon="vaadin:facebook" class="btn-facebook-icon"></iron-icon>
								<span>Sign in with Facebook</span>
							</a>
						</div>
				
   </div>
   </vaadin-vertical-layout>
  <vaadin-dialog id="playyoutube" aria-label="simple">
        <template>
        	<iframe width="100%" height="315" src="https://www.youtube.com/embed/fUUuYF8gLnY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </template>
  </vaadin-dialog>
  </vaadin-vertical-layout>
 </div>
</div>
        `;
    }

    static get is() {
        return 'login-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    
    _play(){
    	this.$.playyoutube.opened = true;
    }
}

customElements.define(LoginPage.is, LoginPage);
