import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `descriptionpopup-page`
 *
 * DescriptionpopupPage element.
 *
 * @customElement
 * @polymer
 */
class DescriptionpopupPage extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;  
                }
            </style>
            <div style="width: 100%; height: 100%;padding: 30px;">
            	<vaadin-rich-text-editor id="rte" style="width: 100%; height: 300px;" theme="compact"></vaadin-rich-text-editor>
	            <div style="text-align: center;align-items: center;justify-items: center;margin-top: 10px;">
					<vaadin-button style="margin-right: 5px;color:#fff;width:47%;--lumo-primary-color: red;" id="btnCancel" theme="primary small" class="btn-tab">Cancel</vaadin-button>
					<vaadin-button style="color:#fff;width:47%;" id="btnSave" theme="primary small" class="btn-tab">Save</vaadin-button>
		        </div>
	        </div>
        `;
    }

    static get is() {
        return 'descriptionpopup-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        	rtevalue:{
        		type:String,
        		notify:true,
        		observer:'_rtevalue'
        	},
        };
    }
    _rtevalue(){
    	var delta = this.$.rte._editor.clipboard.convert(this.rtevalue);
    	this.$.rte._editor.setContents(delta);
    }
}

customElements.define(DescriptionpopupPage.is, DescriptionpopupPage);
