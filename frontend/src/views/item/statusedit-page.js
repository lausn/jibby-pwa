import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';


/**
 * `statusedit-page`
 *
 * StatuseditPage element.
 *
 * @customElement
 * @polymer
 */
class StatuseditPage extends PolymerElement {

    static get template() {
        return html`
 <style include="shared-styles">
            :host {
                display: block;
            }
    		
        </style>
        <div style="width: 100%; height: 100%;padding: 30px;">
        <label>Status</label> 
        <div id="selectStatusWrapper" style="width: 100%; margin-bottom: 5px;"></div>
        <label>Priority</label> 
        <div id="selectPriorityWrapper" style="width: 100%; margin-bottom: 5px;"></div>
        <label>Start</label>
        <vaadin-date-picker id="dpStart" style="margin-bottom: 5px;width: 100%;"></vaadin-date-picker>
        <label>Finish</label> 
        <vaadin-date-picker id="dpFinish" style="width: 100%;"></vaadin-date-picker>
        <div style="    text-align: center;border-top: 1px solid #ddd;padding-top: 5px;padding-bottom: 5px;align-items: center;justify-items: center;margin-top: 15px;">
		  <vaadin-button style="color:#fff;width:45%;--lumo-primary-color: red;" id="btnCancel" theme="primary small" class="btn-tab">
		    Cancel 
		  </vaadin-button>
		  <vaadin-button style="color:#fff;width:45%;" id="btnSave" theme="primary small" class="btn-tab">
		    Save 
		  </vaadin-button>
        </div>
        </div>
        `;
    }

    static get is() {
        return 'statusedit-page';
    }

    static get properties() {
        return {

        };
    }
    

}

customElements.define(StatuseditPage.is, StatuseditPage);
