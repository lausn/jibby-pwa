import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `assigned-history-page`
 *
 * AssignedHistoryPage element.
 *
 * @customElement
 * @polymer
 */
class UserAccessView extends PolymerElement {

    static get template() {
        return html`
 	<style include="shared-styles">
 	
 	
 	     :host{
 	         height: 100%;
		     width: 100%;
		     display: inline-block;
 	         --lumo-primary-color: #10708e;
 	     }
        .boxWrapper{
			    width: 100%;
			    height: auto;
			    box-sizing: border-box;
			    white-space: normal;
		}
	    </style>
 <div style="width:100%;position: absolute;z-index: 10;padding-top: var(--safe-area-inset-top);height: 60px; background: #10708e;">
       <vaadin-button id="btnBackNotiDialog" style="display: inline-block;height: 55px;margin: 0px;" theme="small icon"><iron-icon icon="vaadin:chevron-left" style = "fill:#fff;width: 20px;height: 20px;"></iron-icon></vaadin-button>
       <label style="font-weight: 600;display: inline-block;background: #28708f;color: #fff;font-size: 13px;margin-left: 2px;margin-top: 20px;" id="headerCaption"></label>
 </div>
   <div style="display: inline-block;width: 100%; height: calc(100% - 135px - var(--safe-area-inset-top));vertical-align: top; margin-top:calc(60px + var(--safe-area-inset-top));">
           <vaadin-grid id="gridUser" style="width:100%;height: 100%;"></vaadin-grid>
   </div>
   
   <div style="width:100%; text-align:center; display:inline-block;">
   	<vaadin-button id="btnSave" theme="primary large" style="margin-top: 15px; vertical-align:top; font-size:15px; width: 160px;">
        			<iron-icon style="fill:#fff" icon="vaadin:records"></iron-icon> Save
    </vaadin-button>
    </div>
	
        `;
    }

    static get is() {
        return 'user-access-view';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(UserAccessView.is, UserAccessView);
