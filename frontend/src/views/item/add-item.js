import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-area.js';
import '@vaadin/vaadin-button/vaadin-button.js';


class AddItem extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
			    overflow: auto;
				height: calc(100% - 50px);
			    width: 100%;
			    position: absolute;
                }
                .hide-alloption-except1>option:not(:nth-child(1)){
				display: none !important;
			 }
            </style>
				<div style="width:93%; height:100%;padding-left: 15px;margin-top :15px;" id="div">
				 <div style="color: var(--lumo-secondary-text-color);margin-bottom: 4px;">
				   Category 
				 </div>
				 <select value="{{entityType::change}}" id="nsType" style="width: 100%; height:35px;   background: #d7e2ed;
				    border: none;color: var(--lumo-secondary-text-color);">
				    <option value="1" style="background-image:url('images/itemtype/1.png');">Project</option>
				    <option value="2">Information</option>
				    <option value="3">Deliverable</option>
				    <option value="4">Task</option>
				    <option value="5">Timesheet</option>
				    <option value="6">Event</option>
				    <option value="7">Meeting &amp; notes</option>
				    <option value="8">Memo</option>
				    <option value="9">Document</option>
				    <option value="10">Financial</option>
				    <option value="11">JibbyMail</option>
				    <option value="12">Blog</option>
				    <option value="13">Contact</option>
				 </select>
				 <vaadin-text-field id="tfName" style="width:100%;" label="Name"></vaadin-text-field>
				 <vaadin-text-area style="width:100%;   height: 200px;" label="Description" id="taDescription"></vaadin-text-area>
				 <div style="margin-top: 5px;">
				  <vaadin-button id="btnShowAssignedUser" theme="small" style=" background: #10708e; color: #fff;width:50%;">
				    Select users to assign 
				  </vaadin-button>
				  <vaadin-button style=" background: #10708e; color: #fff;width:48%;  margin-left: 2px;" id="btnSave" theme="small">
				    Save 
				  </vaadin-button>
				 </div>
				 <div id="lblAssignee" style="width:100% !important;	
				 border-radius: 0px !important;
				 padding: 5px;
				 color: #10708e !important;
				 margin-top: 5px;
				 border-top: 1px solid #ddd;
				"></div>
				 <div style="text-align : center;"></div>
				</div>
        `;
    }

    static get is() {
        return 'add-item';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    disableNsTypeSelect(){
    	var nsType = this.shadowRoot.querySelector("#nsType");
    	nsType.setAttribute("disabled",true);
    }
}

customElements.define(AddItem.is, AddItem);
