import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `assigned-history-page`
 *
 * AssignedHistoryPage element.
 *
 * @customElement
 * @polymer
 */
class AssignedHistoryPage extends PolymerElement {

    static get template() {
        return html`
 	<style include="shared-styles">
 	
        .boxWrapper{
			    width: 100%;
			    height: auto;
			    box-sizing: border-box;
			    white-space: normal;
			    display: inline-flex;
			    color: #000 !important;
		}
		
		
	    </style>
	    <div class="boxWrapper">
        <span style="width:33%;color: #000; font-size:15px;">Assigned To :</span> 
        <div style="width: calc(100% - 110px);padding-top: 3px;">{{assignedHistory}}</div>
		</div>
        `;
    }

    static get is() {
        return 'assigned-history-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(AssignedHistoryPage.is, AssignedHistoryPage);
