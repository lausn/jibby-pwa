import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `item-page`
 *
 * ItemPage element.
 *
 * @customElement
 * @polymer
 */
class ItemPage extends PolymerElement {

    static get template() {
        return html`
<style>
	
	:host {
        overflow: auto;
        -webkit-overflow-scrolling: touch;
	    width: 100vw;
	    position: absolute;
	    overflow-x: hidden;
	    height:100%;
	    
      }
	.pad-10px{
  		padding: 10px;
  	}
  	.itemrow{
	    height: 40px;
	    border-bottom: 1px solid #d4e1e0;
	    display: inline-block;
	    width: 100%;
    	float: left;
	}
	
	#itemname span{
		margin-left: 5px;
		
	}
	#itemname img{
		position: absolute;
    	top: 8px;
	}
	#parentItem{
		padding-left: 20px;
		padding-right:10px;
    	border-bottom: 1px solid #d4e1e0;
    	width: calc(100% - 30px);
    	padding-bottom: 10px;
    	padding-top: 10px;
	}
	
	 #add-button {
        position: fixed;
        height: 56px;
        width: 56px;
        right: 32px;
        bottom: 70px;
        z-index: 1;
        background: #10708e;
        color: var(--lumo-error-contrast-color);
        box-shadow: var(--lumo-box-shadow-m);
        border-radius: 50%;
      }
      .add-item{
      	background: transparent;
      	border: none;
      	color: #fff;
      	top: -3px;
    	left: -7px;
      }
      
      /* .toggle{
     	position: fixed !important;
   		z-index:0 !important;
      	max-height: 450px !important; 
      	transition : max-height 400ms cubic-bezier(0.4, 0, 0.2, 1) !important;
      } */
      #childItems{
      	background: #FFF;
    	z-index: 0;
    	height: 100%;
      }
	.jibby-item{
    	display: flex;
    	align-items: center;
    	padding: 10px 0px 10px 10px;
	}
	.jibby-item-text{
		white-space: nowrap;
    	overflow: hidden;
    	text-overflow: ellipsis;
    	max-width:calc(100% - 35px);
	}
	.iconStyle{
		width: 14px;
	    height: 14px;
	    float: right;
	    margin-right: 5px;
	    padding-top: 13px;
	}
	.itemRowPaperBtn{
		width: 20px;
        height: 100%;
	    padding-right: 5px;
	    padding-left: 5px;
	    float: right;
	    
	}
</style>
<vaadin-dialog id="dialog" no-close-on-outside-click style="width:100vw;">
 <template>
  <style>
  .active{
		background: #dae8e7;
	}
  .childitemrow{
		padding-left: 30px !important; 
	}
  	
 		:host{
          top: 0 !important;
          left: 0 !important;
          right: 0 !important;
          bottom: 0 !important;
          padding: 0 !important;
          width:100vw;
        }
         [part="overlay"] {
          flex: 1 !important;
          width: 100vw !important;
          height: 100% !important;
          border-radius: 0 !important;
        } 
 	</style>
  <div style="height: calc(100vh - 50px);;font-size: 0.9rem;">
   <label style="width: 100%;
    font-weight: 600;
    float: left;
    display: inline-block;
    border-bottom: 1px solid #ddd;
    text-indent: 15px;
    background: #10708e;
    color: #fff;
    padding-top: calc(12px + var(--safe-area-inset-top));
    padding-bottom: 12px;
    
    ">Select a parent item</label>
   <div style="width: 100vw; height: calc(100% - 150px);  overflow-y: auto;">
    <div style="width: 100vw; height: 100%;">
    
    <div style="display:flex;width:100vw" class$="{{_getClass(index)}} itemrow">
    <div class="jibby-item" style="width:calc(100% - 20px)" id="itemname" >
        <iron-icon style="width:17px;height:17px;" src="./images/itemtype/1.png" slot="prefix"></iron-icon>
        <span style="width:100%;" class="jibby-item-text" on-click="_makeTopLabelProject">Make a top label project</span>
     </div>
     </div>
     
     <vaadin-vertical-layout style="width: 100vw; height: 100%;overflow-x: hidden;">
      <div id="childItemList" style="width: 100vw;">
       <dom-repeat items="[[itemList]]" as="item" indexas="index">
        <template>
         <div style="display:flex;width:100vw" class$="{{_getClass(index)}} itemrow">
          <div class="jibby-item" style="width:calc(100% - 20px)" id="itemname" >
           <iron-icon style="width:17px;height:17px;" src="./images/itemtype/{{item.entityType}}.png" slot="prefix"></iron-icon>
           <span style="width:100%;" class="jibby-item-text" on-click="_selectItem">{{item.name}}</span>
          </div>
         </div>
        </template>
       </dom-repeat>
      </div>
     </vaadin-vertical-layout>
    </div>
   </div>
   <div style="margin-top: 10px;padding: 5px;">
    <vaadin-button id="btnOk" class="jibbySubmit" style="width:50%;" theme="small" on-click="_btnDialogOk">
      Ok 
    </vaadin-button>
    <vaadin-button id="btnCancel" class="jibbyCancel" style="width: 48%;margin-left: 2px;" theme="small" on-click="_btnDialogCancel">
      Cancel 
    </vaadin-button>
   </div>
  </div>
 </template>
</vaadin-dialog>
<vaadin-vertical-layout style="width: 100%; height: 100%;">
 <div id="childItems" style="width: 100%;padding-top: 3px;">
  <dom-repeat items="[[items]]">
   <template>
    <div class="itemrow" style="display:flex">
     <div class="jibby-item" style="width:100%;float: left;" id="itemname" on-click="_showdetails">
      <iron-icon style="width:17px;height:17px;" src="./images/itemtype/{{item.entityType}}.png" slot="prefix"></iron-icon>
      <span class="jibby-item-text">{{item.name}}</span>
     </div>
     
      <template is="dom-if" if="{{item.isAttachment}}">
      <iron-icon class="iconStyle" src="./images/paperclip.png"></iron-icon>
     </template>
     
     <template is="dom-if" if="{{item.isChild}}">
      <iron-icon class="itemRowPaperBtn" style="" id="nextbtn" icon="vaadin:chevron-right" on-click="_nextView">
      </iron-icon>
     </template>
     
     <!-- <template is="dom-if" if="{{item.isChecklist}}">
      <iron-icon style="margin-left: 4px;" class="iconStyle" src="./images/checklist_close.png"></iron-icon>
     </template> -->

    </div>
   </template>
  </dom-repeat>
 </div>
</vaadin-vertical-layout>
<paper-icon-button-light on-click="" id="add-button">
 <vaadin-button class="add-item" aria-label="Add item" style="width:100%;height:100%" id="addItem">
  <iron-icon icon="vaadin:plus"></iron-icon>
 </vaadin-button>
</paper-icon-button-light>
        `;
    }

    static get is() {
        return 'item-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    _getClass(index) {
		if (index != 0) {
		   return "childitemrow";
		}
		
		return "";
		}
	
	
	 _selectItem(e) {
		 	
		 	var selectedItem = document.getElementById("overlay").shadowRoot.querySelector('#content').shadowRoot.querySelector('.active');
		
		  	 if(selectedItem != null){
            	selectedItem.classList.remove("active");
            }
            var item = e.target.parentElement.parentElement;
            item.classList.add("active"); 
		}
	 
	 _makeTopLabelProject(e) {
		 	
		 var selectedItem = document.getElementById("overlay").shadowRoot.querySelector('#content').shadowRoot.querySelector('.active');
		
		 if(selectedItem != null){
         	selectedItem.classList.remove("active");
         }
         var item = e.target.parentElement.parentElement;
         item.classList.add("active"); 
	}
}

customElements.define(ItemPage.is, ItemPage);
