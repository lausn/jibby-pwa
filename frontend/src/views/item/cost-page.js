import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
/**
 * `cost-page`
 *
 * CostPage element.
 *
 * @customElement
 * @polymer
 */
class CostPage extends PolymerElement {

    static get template() {
        return html`
	<style>
 .boxWrapper{
		    width: 100%;
		    height: auto;
		    box-sizing: border-box;
		    display:flex;
		    color:#464646;
	}
    </style>
    <div class="boxWrapper">
		<!-- <div class="boxtitle">Costs & Quantities: </div> -->
	  	<div style="width:90%;display:inline-flex;margin-top: 5px;">
	  		<span style="margin-right: 5px;">[[summary]] </span>
		 	<span style="width: 38%;display: inline-block;">
		 		<div style="display: inline-block;">Quantity :  </div>
		 		<div style="display: inline-block;"><b>[[quantity]]</b></div>
		 	</span>
		 	<span style="width: 38%;display: inline-block;">
		 		<div style="display: inline-block;">Cost :  </div>
		 		<div style="display: inline-block;"><b>[[cost]]</b></div>
		 	</span>
	  	</div>
	  	<div style="width:10%">
	  	<paper-icon-button hidden={{showedit}} on-click="_editCostAndQuantities" icon="vaadin:pencil" style="width: 100%;height: 32px;color: #10708e;"></paper-icon-button>
	  	</div>
	</div>
        `;
    }

    static get is() {
        return 'cost-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(CostPage.is, CostPage);
