import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

/**
 * `costedit-page`
 *
 * CosteditPage element.
 *
 * @customElement
 * @polymer
 */
class CosteditPage extends PolymerElement {

    static get template() {
        return html`
 <style include="shared-styles">
            :host {
                display: inline-block;
                padding: 30px;
            }
            #btnCancel{
				--lumo-primary-color: red;
			}
        </style>
<div id="typewrapper" style="width: 100%; height: 100%;"></div>
<vaadin-form-layout id="unitwrapper">
 <vaadin-text-field id="unit" pattern="[0-9.]*" label="Unit Quantity" prevent-invalid-input="true" name="txtUnit" theme="align-right"></vaadin-text-field>
 <vaadin-text-field id="cost" pattern="[0-9]*" label="Cost per unit" prevent-invalid-input="true" name="txtCost" theme="align-right"></vaadin-text-field>
</vaadin-form-layout>
<div style="    text-align: center;
    border-top: 1px solid #ddd;
    padding-top: 5px;
    padding-bottom: 5px;
    align-items: center;
    justify-items: center;
margin-top: 15px;">
 <vaadin-button style="color:#fff;width:45%" id="btnCancel" theme="primary small" class="btn-tab">
   Cancel 
 </vaadin-button>
 <vaadin-button style="color:#fff;width:45%;" id="btnSave" theme="primary small" class="btn-tab">
   Save 
 </vaadin-button>
</div>
        `;
    }

    static get is() {
        return 'costedit-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(CosteditPage.is, CosteditPage);
