import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';

/**
 * `history-and-comments-page`
 *
 * HistoryAndCommentsPage element.
 *
 * @customElement
 * @polymer
 */
class HistoryAndCommentsPage extends PolymerElement {

    static get template() {
        return html`
<style>
    :host{
        height: 100%;
        width: 100%;
        display: inline-block;
    }
   	.boxWrapper{
		    width: 100%;
		    height: auto;
		    box-sizing: border-box;
		    white-space: normal;
		    color:#464646;
	}
	
	.username{
		color: #000;
   		font-weight: 500;
   		float: left;
	}
	.chatItemWrapper{
   		width: 98%;
   		margin-bottom: 35px;
	}
	.messageText{
		 background: #d7e6eb;
   		 padding: 10px;
   		 color: #000;
   		 border-radius: 5px;
   		 margin-top: -10px;
   		 width: calc(100% - 50px);
    	 margin-bottom: 10px;
    	 float: right;
    	 word-break: break-all;
	}
	.userImage{
		width:27px;
		height:27px;
		border-radius:100%;
		float:left;
		margin-right: 5px;
    	margin-top: -3px;
	}
	.date{
		float:right;
		font-weight: 500;	
    	color: #6d8997;
    	font-size: 11px;
    	margin-top: 3px;
	}
	
	.creatorName{
		border-bottom: 1px solid #ddd;
	    padding-bottom: 25px;
	    margin-bottom: 5px;
	    height: 20px;
   		width: 96%;
	}
	.commentTask{
		display: inline-block;
	    color: #63686a;
	    border-bottom: 1px solid #d7e6eb;
	    width: 92%;
	    padding: 3px 5px 5px 10px;
	    margin-bottom: 3px;
	}
	.fleft{
		float:left;
	}
	.historyDate{
		width: 100%;
		font-weight: 500;
    	color: #6d8997;
	}
	.attchmentWrapper{
		display: inline-block;
	    color: #63686a;
	    border-bottom: 1px solid #d7e6eb;
	    width: 94%;
	    padding: 5px 5px 5px 10px;
	    margin-bottom: 10px;
	}
	.attachIcon{
		height: 15px;
    	width: 14px;
	}
	.jbColor{
		color: #000;
		word-break: break-word;
	}
	
	.btn-add-Chat{
		min-width: 15px;
	    width: 24px;
	    height: 24px;
	    float: left; 
	    background: #10708e;
	   /*  border-radius: 100%; */
	    color: #fff;
	    padding: 0px;
	    margin-top: 8px;
	    margin-left: 5px;
	}
	
	
    </style>
<div class="boxWrapper">
 <div style="float:left;color: #6d8997;font-weight: 500;">
   [[entityCreateDate]] 
 </div>
 <div class="creatorName">
  <div style="float:left;color: #000;font-weight: 500;width:100%;">
    [[creatorName]] created that ITEM 
  </div>
 </div>
 <div style="width:98%;padding-bottom: 3px;display:inline-block;border-bottom: 1px solid #ddd;">
  <vaadin-text-field id="tfComments" style="width:calc(100% - 30px);float: left;" placeholder="Add Comments here" theme="small"></vaadin-text-field>
  
   <vaadin-button class="btn-add-Chat" aria-label="Add Comments" id="btnAddChat">
    <iron-icon icon="vaadin:arrow-right"></iron-icon>
   </vaadin-button>
 </div>
 <dom-repeat items="{{chatItems}}" as="item">
  <template>
   <template is="dom-if" if="{{item.isChat}}">
    <div class="chatItemWrapper">
     <div style="display: inline-block;width: 98%;">
      <div class="username">
       <img class="userImage" onerror="javascript:this.src='./images/avatar.png'" src$="{{_getUserImage(item.sender)}}" alt="">{{item.username}} 
      </div>
      <div class="date">
        {{item.date}} 
      </div>
     </div>
     <div inner-h-t-m-l="{{item.text}}" tabindex="1" class="messageText" on-focus="_selectedChat" on-blur="_deselectChat"></div>
    </div>
   </template>
   <template is="dom-if" if="{{item.isHistory}}">
    <div>
     <div inner-h-t-m-l="{{_getHistory(item)}}"></div>
    </div>
   </template>
   <template is="dom-if" if="{{item.isAttachment}}">
    <div class="chatItemWrapper">
     <div style="width: 100%;">
      <div class="username">
       <img class="userImage" onerror="javascript:this.src='./images/avatar.png'" src$="{{_getUserImage(item.sender)}}" alt="">{{item.username}} &nbsp;&nbsp; 
       <img class="attachIcon" src="./images/paperclip.png">&nbsp;&nbsp;attached 
      </div>
      <div class="date">
        {{item.date}} 
      </div>
     </div>
     <div class="attchmentWrapper">
      <div inner-h-t-m-l="{{_getAttachmentHtml(item)}}"></div>
     </div>
    </div>
   </template>
  </template>
 </dom-repeat>
</div>
        `;
    }

    static get is() {
        return 'history-and-comments-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
      
    _getUserImage(id){
    	var url = 'https://app.jibby.com/';
    	return url+"jibby/usercontent/avatars/"+id;
    }
    
    _getAttachImage(id){
    	var url = 'https://app.jibby.com/';
    	return url+"jibby/uploads/"+id;
    }
    
    _getAttachmentHtml(item){
    	var html = "";
    	var imageType = ["image/jpeg","image/png","image/jpg"];
    	
    	if(imageType.includes(item.mimetype)){
    		html = "<a target='_blank' href='"+this._getAttachImage(item.id)+"' class='jbColor'>"+item.text+"</a>"+
    			   "<a target='_blank' href='"+this._getAttachImage(item.id)+"'><div class=''><img style='max-width:100%;' onerror=javascript:this.src='./images/avatar.png' src='"+this._getAttachImage(item.id)+"'></div></a>";
    	}else{
    		html = "<a target='_blank' href='"+this._getAttachFile(item.id)+"' class='jbColor' download='hello.text'>"+item.text+"</a>";
    	}
    	
    	return html;
    }
    
    _getAttachFile(id){
   		var url = 'https://app.jibby.com/';
    	return url+"jibby/uploads/"+id;
    }
    
    _selectedChat(e){
    	this.$server._selectedChat(e.model.item.id,e.model.item.sender,e.model.item.text);
    }
    
    
    _getHistory(item){
    
  		var itemModificationStatus = ["attachment","edit", "done", "undone","move",
			"assigntoother","myitem","mytoday","mytommorrow","unsubscribe",
			"addowner","removeowner","addassigner","removeassigner","addfollower","removefollower","addconnection","removeconnection",
			"addchecklist","removechecklist","editchecklist","donechecklist","undonechecklist","movechecklist"]; // null for comments index

  		var str = "";
			var html="";
			if(item.types == itemModificationStatus.indexOf("done")+1){
    		 html="<div class='chatUserName commentTask'><span class='fleft historyDate'>"+item.date+"</span><img class='fleft' src='./images/ic_pdf_checked.png'><span class='fleft completTask editTask'>"+item.username+" completed that task.</span></div>";
    			
    	}else if(item.types == itemModificationStatus.indexOf("undone")+1){
    		html="<div class='chatUserName commentTask'><span class='fleft historyDate'>"+item.date+"</span><img class='fleft' src='./images/ic_pdf_unchecked.png'><span class='fleft completTask editTask'>"+item.username+" uncompleted that task.</span></div>";
  	
    	}
    	//console.log(html);
    	else if(item.types == itemModificationStatus.indexOf("edit")+1){
    		str="edited that task.";
    	}else if(item.types == itemModificationStatus.indexOf("move")+1){
    		str="move that task.";
    	}else if(item.types == itemModificationStatus.indexOf("unsubscribe")+1){
    		str="unsubscribe that task.";
    	}else if(item.types == itemModificationStatus.indexOf("addchecklist")+1){
    		str="added a new checklist item that task";
    	}else if(item.types == itemModificationStatus.indexOf("editchecklist")+1){
    		str="edited a new checklist item that task";
    	}
		
		// Remove Checklist
		else if(item.types== itemModificationStatus.indexOf("removechecklist")+1){
			str="removed a checklist item that task";
		}
		// Move Checklist
		else if(item.types== itemModificationStatus.indexOf("movechecklist")+1){
			str="move a checklist item that task";
		}
		// Done Checklist
		else if(item.types== itemModificationStatus.indexOf("donechecklist")+1){
			str="done a checklist item that task";
		}
		// Undone Checklist
		else if(item.types== itemModificationStatus.indexOf("undonechecklist")+1){
			str="undone a checklist item that task";
		}


		/*------ My Actionlist Part -------------*/
		// Assigned To Others
		else if(item.types == itemModificationStatus.indexOf("assigntoother")+1){
			str="assign to <b>"+item.assigneeName+"</b>";
		}
		// My Items
		else if(item.types== itemModificationStatus.indexOf("myitem")+1){
			str="added that task in my items.";
		}
		// My Today
		else if(item.types== itemModificationStatus.indexOf("mytoday")+1){
			str="added that task in my today items.";
		}
		// My Tommorrow
		else if(item.types== itemModificationStatus.indexOf("mytommorrow")+1){
			str="added that task in my tommorrow items.";
		}

		/*-------User Roles Part ----------------*/
		// Add Owner
		 else if(item.types== itemModificationStatus.indexOf("addowner")+1){
			str="added new owner to <b>"+item.assigneeName+"</b>";
		}
		// Remove Owner
		else if(item.types== itemModificationStatus.indexOf("removeowner")+1){
			str="remove from ownership to <b>"+item.assigneeName+"</b>";
		}
		// Add Assigner
		else if(item.types== itemModificationStatus.indexOf("addassigner")+1){
			str="added new editor to <b>"+item.assigneeName+"</b>";
		}
		// Remove Assigner
		else if(item.types== itemModificationStatus.indexOf("removeassigner")+1){
			str="remove from editor to <b>"+item.assigneeName+"</b>";
		}
		// Add Follower
		else if(item.types== itemModificationStatus.indexOf("addfollower")+1){
			str="added new reader to <b>"+item.assigneeName+"</b>";
		}
		// Remove Follower
		else if(item.types== itemModificationStatus.indexOf("removefollower")+1){
			str="remove from reader to <b>"+item.assigneeName+"</b>";
		}
		// Add Connection
		else if(item.types== itemModificationStatus.indexOf("addconnection")+1){
			str="added new connection to <b>"+item.assigneeName+"</b>";
		}
		// Remove Connection
		else if(item.types== itemModificationStatus.indexOf("removeconnection")+1){
			str="remove from connection to <b>"+item.assigneeName+"</b>";
		}

		if(str!=""){
			html="<div class='commentTask'><span class='fleft historyDate'>"+item.date+"</span><span class='fleft'><b>"+item.username+"</b> "+str+"</span></div>";
			return html;
		} 
	
		return html;
	}

}

customElements.define(HistoryAndCommentsPage.is, HistoryAndCommentsPage);
