import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `user-contact-page`
 *
 * UserContactPage element.
 *
 * @customElement
 * @polymer
 */
class UserContactPage extends PolymerElement {

    static get template() {
        return html`
    <style>
    
    	:host{
 	         width:100%;
 	         display:inline-block;
 	         --lumo-primary-color: #10708e;
 	     }
 	     
  	.boxWrapper{
		    width: 100%;
		    height: auto;
		    box-sizing: border-box;
		    white-space : normal;
	}
	
	.button-style{
        		height: 40px;
        		font-size: 12px;
        		padding-left: 0px;
        		padding-right: 0px;
        		margin-right: 5px;
		}
		
		@media(min-width: 0px) and (max-width: 450px){
			.button-style{
				width: calc(50% - 10px);
			}
		}
		
    </style>
	<div class="boxWrapper">
		<div hidden={{showBtn}} style="width:100%;margin-bottom:5px;">
		    <vaadin-button class="button-style" theme = "primary large" id="btnAddOwner"> Add Owner
	        	<iron-icon icon="vaadin:angle-double-right" style="fill: #fff;width: 25px;"></iron-icon>
		    </vaadin-button>
		    <vaadin-button class="button-style" theme = "primary large" id="btnAddEditor"> Add Editor
	        	<iron-icon icon="vaadin:angle-double-right" style="fill: #fff;width: 25px;"></iron-icon>
		    </vaadin-button>
		    <vaadin-button class="button-style" theme = "primary large" id="btnAddReader"> Add Reader
	        	<iron-icon icon="vaadin:angle-double-right" style="fill: #fff;width: 25px;"></iron-icon>
		    </vaadin-button>
		    <vaadin-button class="button-style" theme = "primary large" id="btnConnection"> Connections 
	        	<iron-icon icon="vaadin:angle-double-right" style="fill: #fff;width: 25px;"></iron-icon>
		    </vaadin-button>
	    </div>
	  	<div inner-h-t-m-l={{html}}>{{html}}</div>
	</div>
        `;
    }

    static get is() {
        return 'user-contact-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(UserContactPage.is, UserContactPage);
