import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `chat-invitation-user-popup-page`
 *
 * ChatInvitationUserPopupPage element.
 *
 * @customElement
 * @polymer
 */
class ChatInvitationUserPopupPage extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
            <div style="width: 100%; height: 100%;padding: 30px;">
            	<vaadin-grid id="gridUser" style="width:100%;height: 90%;"></vaadin-grid>
            	<div style="text-align: center;align-items: center;justify-items: center;margin-top: 10px;">
					<vaadin-button style="margin-right: 5px;color:#fff;width:47%;--lumo-primary-color: red;" id="btnInvite" theme="primary small" class="btn-tab"><iron-icon icon="vaadin:comments"></iron-icon> Invite User</vaadin-button>
					<vaadin-button style="color:#fff;width:47%;" id="btnCancel" theme="primary small" class="btn-tab"><iron-icon icon="vaadin:close"></iron-icon> Cancel</vaadin-button>
		        </div>
            </div>
        `;
    }

    static get is() {
        return 'chat-invitation-user-popup-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(ChatInvitationUserPopupPage.is, ChatInvitationUserPopupPage);
