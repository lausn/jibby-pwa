import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `myactionlistpopup-page`
 *
 * MyactionlistpopupPage element.
 *
 * @customElement
 * @polymer
 */
class MyactionlistpopupPage extends PolymerElement {

    static get template() {
        return html`
<style include="shared-styles">
            :host {
                display: block;
            }
            #btnCancel{
				--lumo-primary-color: red;
			}
			.userCheck{
					
					padding-left: 0px;
					width:100vh !important;
				}
			
			.userCheck vaadin-checkbox{
				width: 100%;
				padding-top: 3px;
			    padding-bottom: 3px;
			
			}
        </style>
<vaadin-vertical-layout style="width: 100%; height: 100%; padding:30px;">
 <vaadin-button style="color:#fff;width:100%;" id="btnMytoday" theme="primary small" class="btn-tab">
   ADD TO MY TODAY LIST 
 </vaadin-button>
 <vaadin-button style="color:#fff;width:100%;margin-top: 15px;" id="btnMytomorrow" theme="primary small" class="btn-tab">
   ADD TO MY TOMORROW LIST 
 </vaadin-button>
 <vaadin-button style="color:#fff;width:100%;margin-top: 15px;" id="btnMyactionlist" theme="primary small" class="btn-tab">
   ADD TO MY ACTION LIST 
 </vaadin-button>
 <div style="width:100%;margin-top:15px;border: 1px solid #ddd;padding:5px;">
  <div id="assigneeWrapper" style="width:100%;min-height: 30px;border: 1px solid #ddd;padding:5px;overflow: auto;
    max-height: 150px;"></div>
  <vaadin-button style="color:#fff;width:100%;margin-top: 10px;" id="btnSelectAssignTo" theme="primary small" class="btn-tab">
    SELECT USER TO ASSIGN 
  </vaadin-button>
 </div>
 <vaadin-button style="color:#fff;width:100%;margin-top: 15px;" id="btnCancel" theme="primary small" class="btn-tab">
   CANCEL 
 </vaadin-button>
</vaadin-vertical-layout>
        `;
    }

    static get is() {
        return 'myactionlistpopup-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MyactionlistpopupPage.is, MyactionlistpopupPage);
