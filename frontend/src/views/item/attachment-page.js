import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-upload/vaadin-upload.js';
import '@vaadin/vaadin-upload/vaadin-upload.js';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout.js';
/**
 * `attachment-page`
 *
 * AttachmentPage element.
 *
 * @customElement
 * @polymer
 */
class AttachmentPage extends PolymerElement {

    static get template() {
        return html`
<style>
    :host{
        display: flex;
    }
   	.boxWrapper{
		    width: 100%;
		    height: auto;
		    box-sizing: border-box;
		    white-space: normal;
	}
	
	.attachmentWrapper{
		width: 100%;
	    color: #000;
	    float: left;
	    padding: 10px;
	    border-bottom: 1px solid #d8e4ed;
	    margin-bottom: 5px;
	}
	.fileIcon{
		float: left;
	    margin-right: 7px;
	    width: 20px;
	    height: 20px;
	    top: -2px;
	}
	.overflow{
        min-height:200px;
	}
	.description-style{
        width: 100%;
	    height: auto;
	    box-sizing: border-box;
	    white-space: normal;
	    color: #464646;
	    word-break: break-word;
	    padding: 10px;
	    
	}
    </style>
<div class="boxWrapper">
 <!-- <div class="boxtitle">Status: </div> -->
 <div class="attachment">
  <div id="uploadWrapper"></div>
  <dom-repeat items="[[attachments]]" as="attachment">
   <template>
    <div class="attachmentWrapper">
     <iron-icon class="fileIcon" icon="vaadin::vaadin:file-picture"></iron-icon>
     <div style="width: 48%;float:left;margin-right: 10px;word-break: break-word;">
       {{attachment.name}} 
     </div>
     <div style="width: 15%;float:left;margin-right: 5px;">
       {{_getAttachmentSizeInKb(attachment.size)}} 
     </div>
     <template is="dom-if" if="{{_getAttachmentHtml(attachment)}}">
      <paper-icon-button style="float: left;width:25px;height:15px;padding: 0;" src="images/ic_preview.png" on-click="_filePreview"></paper-icon-button>
     </template>
     <template is="dom-if" if="{{!_getAttachmentHtml(attachment)}}">
      <a target="_blank" href="{{_getAttachFile(attachment.id)}}" download="{{attachment.name}}">
       <paper-icon-button style="float: left;width:25px;height:15px;padding: 0;color:#000" icon="vaadin:download"></paper-icon-button></a>
     </template>
     <paper-icon-button style="float: left;width:5%;height:15px;padding: 0;margin-left: 5px;" icon="vaadin:pencil" on-click="_attachmentDescription"></paper-icon-button>
     <paper-icon-button style="float: left;width:5%;height:15px;padding: 0;margin-left: 5px;color:red" icon="vaadin:trash" on-click="_deleteAttachment"></paper-icon-button>
    </div>
    <div class="description-style" inner-h-t-m-l={{_replaceNewLineWithBr(attachment.description)}}></div>
   </template>
  </dom-repeat>
 </div>
</div>
        `;
    }

    static get is() {
        return 'attachment-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
            attachLink:{
            	type:String,
            	observer:'_value'
            },
            url:String,
        };
    }
    
    _replaceNewLineWithBr(description){
    	return description.replace(/(?:\r\n|\r|\n)/g, '<br>');
    }
    
    _value(){
    	this.url = this.attachLink;
    }
    
    _getAttachmentSizeInKb(attachSize){
    
    	return parseInt(attachSize/1024)+"KB";
    }
    
    _filePreview(e){
    	//console.log(e.model.attachment);
    	this.$server._filePreview(e.model.attachment);
    }
    
    _attachmentDescription(e){
    	//console.log(e.model.attachment);
    	this.$server._attachmentDescription(e.model.attachment);
    }
    
    _deleteAttachment(e){
    	this.$server._deleteAttachment(e.model.attachment);
    }
    
    _getAttachmentHtml(item){
    	
    	var imageType = ["image/jpeg","image/png","image/jpg","image/gif"];
    	
   		 if(imageType.includes(item.type)){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    _getAttachFile(id){
    	//var url = 'http://localhost:8080/jibby/';
    	return this.url+"jibby/uploads/"+id;
    }
}

customElements.define(AttachmentPage.is, AttachmentPage);
