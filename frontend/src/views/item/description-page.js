import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `description-page`
 *
 * DescriptionPage element.
 *
 * @customElement
 * @polymer
 */
class DescriptionPage extends PolymerElement {

    static get template() {
        return html`
    <style>
  	.boxWrapper{
		    width: 100%;
		    height: auto;
		    box-sizing: border-box;
		    white-space : normal;
		     color:#464646;
		     
	}
    </style>
<div class="boxWrapper">
	<!-- <div class="boxtitle">Description: </div> -->
  	<div style="word-break: break-word;"  inner-h-t-m-l={{html}}></div>
</div>
        `;
    }

    static get is() {
        return 'description-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(DescriptionPage.is, DescriptionPage);
