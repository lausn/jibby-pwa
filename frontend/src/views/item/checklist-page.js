import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';

import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-menu-button/paper-menu-button.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/iron-icon/iron-icon.js';



class ChecklistPage extends PolymerElement {

    static get template() {
        return html`
          <style>
   	.boxWrapper{
		width: 100%;
		height: auto;
		box-sizing: border-box;
		white-space: normal;
		color:#464646;
	}
	.btn-add-checklist{
		min-width: 15px;
        width: 50%;
        height: 27px;
        background: #10708e;
 
        color: #fff;
        margin: auto;
	}
	paper-listbox{
    	padding: 0px;
    	background: #e9f4f7;
    	color: #0f495b;
    }
    .extraIconbox{
    	width: 20px;
    	padding-right: 8px;
    	margin-top: -2px;
    	color: #0c5166;
    }
   
    paper-item{
    	 border-bottom: 1px solid #e5e6e6;
    }
    iron-icon{
    	width: var(--iron-icon-width, 16px);
    	height: var(--iron-icon-height, 16px);
    }
    vaadin-checkbox[disabled]{
		color: #706e6e !important;
	}
	.new-task-input {
	    font: inherit;
	    width: 100%;
	    caret-color: var(--lumo-primary-text-color);
	    border-radius: var(--lumo-border-radius-m);
	    padding: var(--lumo-space-s) 0;
	    line-height: var(--lumo-line-height-s);
	}
	.borderless{
	}
    </style>
<div class="boxWrapper">
 <!-- <div class="boxtitle">Status: </div> -->
 <div class="checklist">
  
  <div style="width:100%;padding-bottom: 3px;display:inline-block;border-bottom: 1px solid #ddd;margin-top: -7px;">
 
    <vaadin-text-area disabled="{{!isAssignee}}" id="tfChecklist" class="new-task-input borderless" placeholder="Press enter/return to add" clear-button-visible="" tabindex="0" style="width: 100%;">
         <iron-icon slot="prefix" icon="vaadin:plus" style="font-size:0px;display: inline-block;"></iron-icon>
    </vaadin-text-area>
  </div>
  <dom-repeat items="[[checklists]]" as="checklist">
   <template>
    <div class="checkItem" style="width: 100%;margin-bottom: 5px; display: flex;margin-top: 5px;">
    <div style="width:89%;display: flex;display: inline-block;">
     <vaadin-checkbox class="task-checkbox" style="height: 30px;vertical-align: top;" disabled="{{!isAssignee}}"  data-itemvalue$="{{checklist.id}}" on-change="_checkChanged" checked="{{_getCheckValue(checklist.isChecked)}}">
     </vaadin-checkbox>
     
     <template is="dom-if" if="{{!isAssignee}}">
        <div style="display: inline-block;width: 89%;">
        	<span style="width: 89%;overflow: hidden;white-space: normal;word-break: break-word;display: inline-block;">{{checklist.name}} </span>
        </div>
     </template>
     
     <template is="dom-if" if="{{isAssignee}}">
        <div style="display: inline-block;width: 89%;">
        	<span on-click="_onEditItem" style="width: 89%;overflow: hidden;white-space: normal;word-break: break-word;display: inline-block;">{{checklist.name}} </span>
        </div>
     </template>
  	
  	</div>
     <div style="height: 30px;">
	    <vaadin-context-menu>
		    <template>
			    <vaadin-list-box>
			      <vaadin-item on-click="_onAddmyItem" style="font-size: 20px;">
						<iron-icon icon="vaadin:plus-circle-o" style="padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>
						Add to myaction list
				</vaadin-item>
			     <vaadin-item on-click="_onAddItem"  style="font-size: 20px;">
						<iron-icon icon="vaadin:cart-o" style="padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>
						Add to the item
				  </vaadin-item>
			      <vaadin-item on-click="_onDeleteItem" style="font-size: 20px;color:red;">
						<iron-icon icon="vaadin:trash" style="color:red;padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>
						Delete item
				  </vaadin-item>
			    </vaadin-list-box>
        	</template>
		</vaadin-context-menu>
		<vaadin-button style="font-size:0px;background-color: #f7f7f7;min-width: 10px;height: 15px;" disabled="{{!isAssignee}}" on-click="onOptionClick">
			<iron-icon icon="vaadin:ellipsis-dots-v"></iron-icon>
		</vaadin-button>
     </div>
    </div>
   </template>
  </dom-repeat>
 </div>
</div>        `;
    }

    static get is() {
        return 'checklist-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    
    _getCheckValue(isChecked){
    	if(isChecked == 1)
    		return true;
    	else
    		return false;
    	
    }
    _checkChanged(e){
    	this.$server.checklistChange(e.model.checklist);
    }
    
    _onEditItem(e){
    	//console.log(e.target.id+" sd "+e.model.checklist);
//    	var tfChecklist = this.shadowRoot.querySelector('#tfChecklist');
//    	tfChecklist.value=e.model.checklist.name;
    	
    	this.$server._onEditItem(e.model.checklist);
    }
     _onDeleteItem(e){
    	//console.log(e.target.id+" sd "+e.model.checklist);
    	var tfChecklist = this.shadowRoot.querySelector('#tfChecklist');
    	tfChecklist.value="";
    	this.$server._onDeleteItem(e.model.checklist);
    }
     _onAddItem(e){
    	//console.log(e.target.id+" sd "+e.model.checklist);
    	this.$server._onAddItem(e.model.checklist);
    }
    _onAddmyItem(e){
    	this.$server._onAddmyItem(e.model.checklist);
    }
    
    onOptionClick(e){
    	var contextMenu = e.target.parentElement.querySelector("vaadin-context-menu");
    	var overlay = contextMenu.shadowRoot.querySelector("vaadin-context-menu-overlay");
    	overlay.setAttribute("opened", true);
    }
}

customElements.define(ChecklistPage.is, ChecklistPage);
