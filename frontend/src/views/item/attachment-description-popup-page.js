import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `attachment-description-popup-page`
 *
 * AttachmentDescriptionPopupPage element.
 *
 * @customElement
 * @polymer
 */
class AttachmentDescriptionPopupPage extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;  
                }
            </style>
            <div style="width: 100%; height: 100%;padding: 30px;">
            	<vaadin-text-area id="taDescription" style="width: 70vw; height: 200px;" theme="small" placeholder="Edit Attachment"></vaadin-text-area>
	            <div style="text-align: center;align-items: center;justify-items: center;margin-top: 10px;">
					<vaadin-button style="margin-right: 5px;color:#fff;width:47%;--lumo-primary-color: red;" id="btnCancel" theme="primary small" class="btn-tab">Cancel</vaadin-button>
					<vaadin-button style="color:#fff;width:47%;" id="btnSave" theme="primary small" class="btn-tab">Save</vaadin-button>
		        </div>
	        </div>
        `;
    }

    static get is() {
        return 'attachment-description-popup-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(AttachmentDescriptionPopupPage.is, AttachmentDescriptionPopupPage);
