import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `status-page`
 *
 * StatusPage element.
 *
 * @customElement
 * @polymer
 */
class StatusPage extends PolymerElement {

    static get template() {
        return html`
     <style>
   	.status>span{
   		margin-right: 10px;
   		float: left;
   		display: inline-block;
   	}
   	.boxWrapper{
		    width: 100%;
		    height: auto;
		    box-sizing: border-box;
		    display:flex;
		    color: #000;
	}
    </style>
<div  class="boxWrapper">
 <!-- <div class="boxtitle">Status: </div> -->
	  <div class="status" style="width:90%;">
		 <span><span>Status : </span><span><b>[[done]]</b></span></span>
		 <span><span>Priority : </span><span><b>[[priority]]</b></span></span>
		 <span><span>Duration : </span><span><b>[[duration]]</b></span></span>
	 </div>
 	<div style="width:10%">
	  <paper-icon-button hidden={{showedit}} on-click="_editStatus" icon="vaadin:pencil" style="width: 100%;height: 32px;color: #10708e;"></paper-icon-button>
	</div>
        `;
    }

    static get is() {
        return 'status-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(StatusPage.is, StatusPage);
