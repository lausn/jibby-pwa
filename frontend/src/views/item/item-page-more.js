import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';


/**
 * `item-page-more`
 *
 * ItemPageMore element.
 *
 * @customElement
 * @polymer
 */
class ItemPageMore extends PolymerElement {

    static get template() {
        return html`
<style>
	:host{
		  --lumo-primary-color: #10708e;
	}
	.footer{
		height:30px;
	}
	.pull-right{
		float:right;
	}
</style>
<div id="childItems" style="padding-left: 5px;padding-right: 5px;padding-top: 5px;">
 <dom-repeat items="[[items]]">
  <template>
   <div style="    background: #fff; box-shadow: 1px 2px 6px #e8e8e8; -webkit-box-shadow: 1px 2px 6px #e8e8e8; padding: 0px 10px 10px 10px; border: 1px solid #ededed;margin-bottom: 5px;">
    <div>
     <iron-icon src="images/itemtype/{{item.entityType}}.png" slot="prefix"></iron-icon>
     <span style="font-size: 0.9rem;font-weight: 400; color: #10536f;">{{item.name}} </span>
    </div>
    <div>
     <span style="font-size: 0.7rem;">This may be necessary f.ex. in a town where there are many buildings and one person (usually a building</span>
    </div>
    <div class="footer" style="border-top:1px solid #e9e9e9;">
     <span style="font-size: 0.8rem;    display: inline-flex; height: 100%;  align-items: center;">Status: Done</span>
     <vaadin-button class="pull-right" theme="primary small" style="padding: 0px !important;min-width: 60px;">
      <iron-icon icon="vaadin:plus"></iron-icon>Add 
     </vaadin-button>
    </div>
   </div>
  </template>
 </dom-repeat>
</div>
        `;
    }

    static get is() {
        return 'item-page-more';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(ItemPageMore.is, ItemPageMore);
