import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-dialog/vaadin-dialog.js';
import '@polymer/paper-ripple/paper-ripple.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-details/vaadin-details.js';
import '@vaadin/vaadin-button/vaadin-button.js';

/**
 * `item-details`
 *
 * ItemDetails element.
 *
 * @customElement
 * @polymer
 */
class ItemDetails extends PolymerElement {

    static get template() {
        return html`
			<style include="paper-collapse">
			:host {
			    overflow-x: hidden;
			    -webkit-overflow-scrolling: touch;
			   height: calc(100vh - 60px);
			 	width:100%;
			 	overflow-y:auto;
			}
			.detailsPageCollapse {
				
				width: 100%;
				border:1px solid #ddd;  
				margin-bottom: 5px;
				background: #e8eef7 !important;
				display: inline-block;

			    		--paper-item-focused-before: {
							padding: 0px 10px;background: none !important;
						}
						
			}
			.item-content{
				
			    font-size: 0.9rem !important;
				background: #f4f5fa !important;
			    padding: 10px 10px 8px 10px;
			    margin-bottom: 12px;	
			    color:#464646;
			}
			.wrapperIcon{
				height: 15px;
			    width: 15px;
			    border: 2px solid #10708e!important;
			    padding: 6px 6px 7px 8px;
			    border-radius: 100%;
			    margin-right: 5px;
			}					
			.detailsLayout{
				width: 100%;
			    height: 100%;
			    font-size: var(--lumo-font-size-s);
			  
			}
			.itemicon{
					float:left;
					margin-right: 12px;
					height: 20px;
					display: inline-block;
        			width: 7%;
			}
			
			.itemnamewrapper{
					width: 100%;
				    box-sizing: border-box;
					font-size: .9rem !important;
				    font-weight: 500;
				    padding: 12px 5px 5px 10px;
				    background: #e8eef7;
				    margin-bottom: 5px;
				    border: 1px solid #ddd;
				    word-break: break-word;
				    height: fit-content;
					} 
					
			.itemname{
				width: 88%;
				display: inline-block;
			    float: left;
			    font-size: .9rem !important;
			}

			.collapseItemWrapper{
				width: 100%;
				color: #10708e;
			    font-weight: 500;
			    font-size: .9rem!important;
			}
			.status{
				color:#464646;
			}
			
			 #uploadLayout {
		        
		        height: 56px;
		        width: 56px;
		        
		        background: #10708e;
		        color: var(--lumo-error-contrast-color);
		        box-shadow: var(--lumo-box-shadow-m);
		        border-radius: 50%;
		        
		        position: -webkit-sticky;
				position: sticky;
				z-index: 10;
				bottom: calc(135px + var(--safe-area-inset-bottom));
				margin-left: auto;
				margin-right: 25px;
				    
		      }
		      .add-image{
		      	height: 56px;
        		width: 56px;
			    background: transparent;
			    border: none;
			    color: #fff;
			  
		      }
			
				@media (max-width: 500px), (max-height: 500px) {
				:host {
				  background-color:var(--lumo-base-color)
				}
				
				
				
				:host([enter]),:host([exit]) {
				  --animation:350ms cubic-bezier(.215, .61, .355, 1);
				  animation:exit var(--animation);
				  
				}
				
				:host([enter]) {
				  animation-name:enter
				}
				
				:host([enter=forward]),:host([exit=backward]) {
				  box-shadow:var(--lumo-box-shadow-m)
				}
				
				@keyframes exit {
				100% {
				  transform:translateX(-30%)
				}
				
				}
				
				@keyframes enter {
				0% {
				  transform:translateX(100%)
				}
				
				}
				
				:host([enter=backward]),:host([exit=backward]),:host([modal][exit]) {
				  --animation:400ms reverse cubic-bezier(.550, .055, .675, .19);animation:enter var(--animation)
				}
				
				:host([enter=backward]) {
				  animation-name:exit
				}
				
				:host([enter=forward]:not([modal]):not([no-anim])) {
				  animation:dummy 560ms,enter var(--animation) forwards
				}
				
				:host([enter=forward]:not([modal]):not([no-anim])) [theme~=fab],:host([enter=forward]:not([modal]):not([no-anim])) section[fold],:host([enter=forward]:not([modal]):not([no-anim])) section[main] {
				  animation:fade-in .2s .2s backwards,slide .2s .2s backwards
				}
				
				:host([enter=forward]:not([modal]):not([no-anim])) section[main] {
				  animation-duration:240ms;animation-delay:240ms
				}
				
				:host([enter=forward]:not([modal]):not([no-anim])) [theme~=fab] {
				  animation-duration:280ms;animation-delay:280ms
				}
				
				:host([enter=forward]:not([modal]):not([no-anim])) attribution-component {
				  animation:fade-in .5s .4s backwards
				}
				
				@keyframes slide {
				0% {
				  transform:translateY(-30px)
				}
				
				}
				
				@keyframes fade-in {
				0% {
				  opacity:0
				}
				
				}
				
				:host([no-anim]) {
				  animation:dummy var(--animation)
				}
				
				@keyframes dummy {
				100% {
				  opacity:1
				}
				
				}
				
				:host([modal][enter]) {
				  animation:modal var(--animation)
				}
				
				:host([modal][exit]) {
				  animation:modal var(--animation)
				}
				
				@keyframes modal {
				0% {
				  transform:translateY(100%)
				}
				
				}
				
				@media (prefers-reduced-motion:reduce) {
				[theme~=fab],section {
				  animation:none!important
				}
				
				}
				
				:host([skip-animations][enter]),:host([skip-animations][exit]) {
				  animation:none!important
				}
				
				}

			</style>
			<vaadin-vertical-layout class="detailsLayout">
			
			 <div class="itemnamewrapper" style="width: 100%;">
			  <iron-icon class="itemicon" src="images/itemtype/[[itemicon]].png" icon=""></iron-icon>
			  <span class="itemname">[[itemname]]</span>
			 </div>
			 
			 
			<div id="collapseStatusWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse" opened>
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_status.png"></iron-icon>
					 	Status
					 </div>
					  <div id="statusWrapper" class="item-content">
					     <status-page id="item-status"></status-page>
					   </div>
				</vaadin-details>
			</div>
			
			 
			 <div id="collapseDescriptionWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse" opened>
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_description.png"></iron-icon>
					 	Description
					 </div>
					  <div id="descriptionWrapper"  class="item-content">
					    <description-page id="item-description"></description-page>
					   </div>
				</vaadin-details>
			</div>
			 
			 
			 <div id="collapseChecklistWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_checklist.png"></iron-icon>
					 	Checklist
					 </div>
					   <div id="checklistWrapper"  class="item-content">
					    <checklist-page id="checklistPage"></checklist-page>
					   </div>
				</vaadin-details>
			</div>
		
			  <div id="collapseCostWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse" id="costAndQuantityDetails">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_costs.png"></iron-icon>
					 	Cost &amp; Quantities
					 </div>
					    <div id="costWrapper"  class="item-content">
					    <cost-page id="item-cost"></cost-page>
					   </div>
				</vaadin-details>
			</div>

			 <div id="collapseAttachmentWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_attachment.png"></iron-icon>
					 	Attachments
					 </div>
					   <div id="attachmentWrapper"  class="item-content">
					    <attachment-page id="attachmentPage"></attachment-page>
					   </div>
				</vaadin-details>
			</div>
			 
			 <div id="collapseHistoryWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_checklist.png"></iron-icon>
					 	Assigned History
					 </div>
					  <div id="assignedHistoryWrapper"  class="item-content">
					    <assigned-history-page id="assignedHistoryPage"></assigned-history-page>
					   </div>
				</vaadin-details>
			</div>

			 
			  <div id="collapseChatWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse" id="historyAndCommentsDetails">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_chat.png"></iron-icon>
					 	History &amp; Comments
					 </div>
					   <div id="chatWrapper"  class="item-content">
					    <history-and-comments-page id="historyAndCommentsPage"></history-and-comments-page>
					   </div>
				</vaadin-details>
			</div>

			 
			  <div id="collapseUserContactWrapper" class="collapseItemWrapper">
				 <vaadin-details class="detailsPageCollapse" id="userContactDetail">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_usercontacts.png"></iron-icon>
					 	User and Contact Rights
					 </div>
					    <div id="userContactWrapper"  class="item-content">
					    <user-contact-page id="userContactPage"></user-contact-page>
					 </div>
				</vaadin-details>
			</div>

			 
			<div id="uploadLayout">
          	</div>
			
			</vaadin-vertical-layout>
        `;
    }

    static get is() {
        return 'item-details';
    }

    static get properties() {
        return {
        };
    }
    
    ready(){
    	super.ready();
    	this.setAttribute("enter","forward");
    }
    
//    _handleTapHistoryAndComments(e){
//    	//console.log(e.target.getAttribute('opened'));
//    	if(e.target.getAttribute('opened')!=null)
//    		this.$server._handleTapHistoryAndComments();
//    	
//    }
//    _handleTapCostAndQuantity(e){
//    	//console.log(e.target.getAttribute('opened'));
//    	if(e.target.getAttribute('opened')!=null){
//    		this.$server._handleTapCostAndQuantity()
//    	};
//    }
//     _handleTapUserContact(e){
//    	//console.log(e.target);
//    	if(e.target.getAttribute('opened')!=null)
//    		this.$server._handleTapUserContact();
//    }     

}

customElements.define(ItemDetails.is, ItemDetails);
