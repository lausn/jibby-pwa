import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '../../../styles/shared-styles.js';

/**
 * `customer-view`
 *
 * CustomerView element.
 *
 * @customElement
 * @polymer
 */
class CustomerView extends PolymerElement {

    static get template() {
        return html`
          <style include="shared-styles">
	
	:host{
		display: block;
		 width: 100%;
		 --lumo-primary-color: #10708e;
	}
	
	
</style>
<div style="color: white;background: #055269;padding: 5px;font-weight: 600;">Customers</div>
<div style="display: inline-block;text-align: center;padding: 10px;">
       <label style="color: #055269; width: 100%;white-space: normal;"></label>
        <vaadin-select id="nSelect" style="width: 220px;"></vaadin-select>
        <vaadin-button  id="btnSelect" theme="primary" style="margin-top: 10px;">
			<iron-icon icon="vaadin:sign-in" style="fill:#fff"></iron-icon>
		Select
		</vaadin-button>
</div>

        `;
    }

    static get is() {
        return 'customer-view';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(CustomerView.is, CustomerView);
