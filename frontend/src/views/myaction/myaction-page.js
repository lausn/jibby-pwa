import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `myaction-page`
 *
 * MyactionPage element.
 *
 * @customElement
 * @polymer
 */
class MyactionPage extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host{
				      	--lumo-font-size-m:0.73rem;
				      	width: 100%;
	      			}
				     iron-pages{
				     	
				     }
				     .myWrapper{
				     	
				     	height: 100%;
					    position: relative;
					    overflow-y: auto;
					    overflow-x:hidden;
        				padding-bottom: 40px;
				     } 
				     paper-listbox{
				    	padding: 0px;
				    	background: #e9f4f7;
				    	color: #0f495b;
				    }
				    .extraIconbox{
				    	width: 20px;
				    	padding-right: 8px;
				    	margin-top: -2px;
				    	color: #0c5166;
				    }
				   
				    paper-item{
				    	 border-bottom: 1px solid #e5e6e6;
				    }
				    iron-icon{
				    	width: var(--iron-icon-width, 16px);
				    	height: var(--iron-icon-height, 16px);
				    }
				    .displayNone{
				    	display:none !important;
				    }
				    .myactiontab{
				    		width: 100%;
						    display: flex;
						    z-index: 1;
						    background-color: #10708e;
						    box-shadow: var(--lumo-box-shadow-s);
						    position: relative;
						    color: #fff;
				    }
				    vaadin-tab{
				    	color:#fff;
				    }
				    
				    .hide{
				       display:none !important;
				    }
            </style>
            <div class="myactiontab">
				<div style="width: 90%;" id="tabs">
			
				</div>
			<div style="width: 10%;box-shadow: inset 0 -1px 0 0 var(--lumo-contrast-10pct);">
			     <vaadin-button id="btnMenu" theme="small icon" style="font-size:0px; color:white; background: #10708e;">
			     <iron-icon icon="vaadin:ellipsis-dots-v"></iron-icon>
			      </vaadin-button>
			      
			       <vaadin-button id="btnAssignToOthersMenu" theme="small icon" style="font-size:0px; color:white; background: #10708e;">
			     <iron-icon icon="vaadin:ellipsis-dots-v"></iron-icon>
			      </vaadin-button>
			</div>
			</div>
		
			  <div id="actiontab" class="myWrapper"></div>
		
        `;
    }

    static get is() {
        return 'myaction-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
   
  
}

customElements.define(MyactionPage.is, MyactionPage);
