import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `assigned-history-page`
 *
 * AssignedHistoryPage element.
 *
 * @customElement
 * @polymer
 */
class SelectedItemView extends PolymerElement {

    static get template() {
        return html`
 	<style include="shared-styles">
 	
 	
 	     :host{
 	         width:100%;
 	         --lumo-primary-color: #10708e;
 	     }
        .boxWrapper{
			    width: 100%;
			    height: auto;
			    box-sizing: border-box;
			    white-space: normal;
		}
		
		.detailsLayout{
				width: 100%;
			    height: 100%;
			    font-size: var(--lumo-font-size-s);
			  
			}
			
			.itemnamewrapper{
					width: 100%;
				    box-sizing: border-box;
			
				    font-weight: 500;
				    display: var(--layout-horizontal_-_display);
				        padding: 12px 5px 5px 10px;
				    background: #e8eef7;
				    margin-bottom: 5px;
				    border: 1px solid #ddd;
					} 
					
				.itemicon{
					float:left;
					margin-right: 12px;
					height: 20px;
		    	}
		    	
		       .itemname{
				width: 100%;
			    float: left;
			    font-size: .9rem!important;
			}
			
			.collapseItemWrapper{
				width: 100%;
				color: #10708e;
			    font-weight: 500;
			    font-size: .9rem!important;
			}
			
			.detailsPageCollapse {
				
				width: 100%;
				border:1px solid #ddd;  
				margin-bottom: 5px;
				background: #e8eef7 !important;
				display: inline-block;

			    		--paper-item-focused-before: {
							padding: 0px 10px;background: none !important;
						}
						
			}
			
				.wrapperIcon{
				height: 30px;
			    width: 30px;
			    border: 2px solid #10708e!important;
			    padding: 6px 6px 7px 8px;
			    border-radius: 100%;
			    margin-right: 5px;
			}
			
				.item-content{
				
			    font-size: 0.9rem !important;
				background: #f4f5fa !important;
			    padding: 10px 0px 8px 10px;
			    margin-bottom: 12px	
			    color:#464646;
			}
			
		
	    </style>
 <div style="width:100%;height: 57px;z-index: 10; position: fixed; padding-top:var(--safe-area-inset-top); background: #28708f;">
       <vaadin-button id="btnBackNotiDialog" style="display: inline-block;margin-left: -15px; margin-top:10px;"><iron-icon icon="vaadin:chevron-left" style = "fill:#fff;"></iron-icon></vaadin-button>
       <label style="font-weight: 600;display: inline-block;background: #28708f;color: #fff;font-size: 14px;margin-left: -15px;" id="headerCaption"></label>
       <vaadin-button id="btnInvite" style="display: inline-block;margin-left: -15px; margin-top:10px; float:right; background: #28708f;"><iron-icon icon="vaadin:comment-ellipsis-o" style = "fill:#fff;"></iron-icon></vaadin-button>
 </div>
 
 <vaadin-vertical-layout class="detailsLayout">
 <div class="itemnamewrapper" style="width: 100%; margin-top:57px;">
		 <iron-icon class="itemicon" src="images/itemtype/[[itemicon]].png" icon=""></iron-icon>
	     <span class="itemname">[[itemname]]</span>
</div>

 <div id="collapseDescriptionWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse" opened>
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_description.png"></iron-icon>
					 	Description
					 </div>
					  <div id="descriptionWrapper"  class="item-content">
					    <description-page id="item-description"></description-page>
					   </div>
				</vaadin-details>
  </div>
  
  	  <div id="collapseChatWrapper" class="collapseItemWrapper">
				 <vaadin-details class=" detailsPageCollapse" id="historyAndCommentsDetails">
					 <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="wrapperIcon" src="images/ic_colc_chat.png"></iron-icon>
					 	History &amp; Comments
					 </div>
					   <div id="chatWrapper"  class="item-content">
					    <history-and-comments-page id="historyAndCommentsPage"></history-and-comments-page>
					   </div>
				</vaadin-details>
	</div>
 
 </vaadin-vertical-layout>
	
        `;
    }

    static get is() {
        return 'selected-item-view';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(SelectedItemView.is, SelectedItemView);
