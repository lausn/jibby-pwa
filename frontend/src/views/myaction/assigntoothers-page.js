import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-menu-button/paper-menu-button.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-details/vaadin-details.js';

/**
 * `assigntoothers-page`
 *
 * AssigntoothersPage element.
 *
 * @customElement
 * @polymer
 */
class AssigntoothersPage extends PolymerElement {

    static get template() {
        return html`
 <style>
	vaadin-checkbox{
		margin-right: -8px;
	}
	.topitem{
		display: flex;
    	width: 100%;
    	border-bottom: 1px solid #eee;
    	padding-bottom:6px;
	}
	.topitem iron-icon{
		width: auto;
	    height: fit-content;
	    margin-right: 8px;
	    margin-top: 2px;
	}
	
	.topitem span{
		text-overflow: ellipsis;
	    overflow: hidden;
	    width: 86%;
	    float: left;
	    margin-top:5px;
	}
	.myitemicon{
		float:left;
		height: fit-content;
		margin-left:15px;
   	 }
	.myitemwrapper{
			width: 100%;
			display: flex;
			box-sizing: border-box;
			border-bottom: 1px solid #eee;
			margin-bottom: 8px;
			margin-left: 15px;
	} 
	.myitemname{
		width: 100%;
	    float: left;
	    padding-bottom: 10px;
	    text-overflow: ellipsis;
    	overflow: hidden;
    	white-space: normal;
    	margin-left:10px;
	}
	paper-listbox{
    	padding: 0px;
    	background: #e9f4f7;
    	color: #0f495b;
    }
    .extraIconbox{
    	width: 20px;
    	padding-right: 8px;
    	margin-top: -2px;
    	color: #0c5166;
    }
   
    paper-item{
    	 border-bottom: 1px solid #e5e6e6;
    }
    .extraIconbox iron-icon{
    	width: var(--iron-icon-width, 16px);
    	height: var(--iron-icon-height, 16px);
    }
	
</style>
<custom-style>
 <style is="custom-style">
						.styled {
							--paper-collapse-item-header: {
								border-bottom: 1px solid #eee;
							    font-size: var(--lumo-font-size-s);
							}
							--paper-collapse-item-content: {
								margin-top: 10px;
							    font-size: var(--lumo-font-size-s);
							}
							--paper-item-focused-before: {
							  	background: none !important;
							}
							--paper-item: {
							    font-weight: var(--paper-item-selected-weight, normal) !important;
							};
						}
						.userIcon{
							--img:{
								border-radius: 100%;
							}
						}
					</style>
</custom-style>
<dom-repeat items="{{myItems}}" as="useritem">
<template>
 <vaadin-details style="margin-left:5px; margin-bottom: -10px;" class="" opened>
	 	<div style="width: 100vw;"; slot="summary" class="topitem">
	 		<div><img class='' style='width:23px;height:23px;border-radius:100%;float:left;margin-right: 8px;' alt='' src="{{useritem.userPic}}"></div>
		   <span style="margin-top: 5px;">{{useritem.userName}}</span>
	  </div>
	  <dom-repeat items="{{useritem.myItems}}" as="topitem">
		  <template>
		   	<vaadin-details style="margin-left:20px; margin-top: -14px;" class="">
				 <div style="width: 100vw;" slot="summary" class="topitem">
				   <iron-icon src="images/itemtype/{{topitem.entityType}}.png" icon=""></iron-icon>
				   <span>{{topitem.name}}</span>
				  </div>
				  <dom-repeat items="{{topitem.myItems}}" as="childitem">
					  <template>
					   <div class="myitemwrapper" style="width: 100%;">
						   <div style="width:84%;display: flex;">
						    <vaadin-checkbox on-change="_assigntoothersChanged" checked="{{childitem.status}}"></vaadin-checkbox>
						    <template is="dom-if" if="{{_isItemtype(childitem.itemType)}}">
						    	<iron-icon class="myitemicon" src="images/itemtype/{{childitem.entityType}}.png" icon=""></iron-icon>
						    </template>
						    <span on-click="_assigntoothersSelectedItem" class="myitemname">{{childitem.name}}</span>
						    </div>
						    <div style="width: 10%; margin-top: -7px;">
							       <vaadin-context-menu>
        								<template>
        									<vaadin-list-box>
        										<vaadin-item on-click="_onDeleteItem" style="font-size: 20px;">
			      									<iron-icon icon="vaadin:trash" style="padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>Delete item</vaadin-item>
			      								<vaadin-item on-click="_onAddmyTodayItem" style="font-size: 20px;">
			      									<iron-icon icon="vaadin:plus-circle-o" style="padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>Add to my today list</vaadin-item>
        										<vaadin-item on-click="_onAddmyTomorrowItem" style="font-size: 20px;">
        											<iron-icon icon="vaadin:plus-circle-o" style="padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>Add to my tomorrow list</vaadin-item>
			    							</vaadin-list-box>
        								</template>
									</vaadin-context-menu>
							<vaadin-button theme = "icon" style="background-color: white !important; color: black !important; min-width: 10px;height: 15px; margin-left:2px;" on-click="onOptionClick">
								<iron-icon icon="vaadin:ellipsis-dots-v"></iron-icon>
							</vaadin-button>
						     </div>
					   </div>
					  </template>
				  </dom-repeat>
			 </vaadin-details>
		  </template>
	  </dom-repeat>
 </vaadin-details>
 </template>
 </dom-repeat>
        `;
    }

    static get is() {
        return 'assigntoothers-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
            topItems:{
            	type:String,
            	observer:'_value'
            },
            myItems:Array,
        };
    }
    
    _isItemtype(e){
    	if(e=='items')
    		return true;
    	else
    		return false;
    }
    
     _value(){
    	this.myItems=JSON.parse(this.topItems);
    }
    _assigntoothersChanged(e){
    	//console.log(JSON.stringify(e.model.childitem));
    	//var checked= Polymer.dom(e).localTarget.checked;
    	//e.target.parentElement.remove();
    	this.$server.assigntoothersChanged(JSON.stringify(e.model.childitem),e.target.checked);
    	//e.target.remove();
    }
     _onDeleteItem(e){
    	this.$server._onDeleteItem(JSON.stringify(e.model.childitem));
    }
    _onAddmyTodayItem(e){
    	this.$server._onAddmyTodayItem(JSON.stringify(e.model.childitem));
    }
     _onAddmyTomorrowItem(e){
    	this.$server._onAddmyTomorrowItem(JSON.stringify(e.model.childitem));
    }
     
     _assigntoothersSelectedItem(e){
    	 console.log("item is clicked");
     	this.$server._assigntoothersSelectedItem(JSON.stringify(e.model.childitem));
     }
     
     onOptionClick(e){
     	var contextMenu = e.target.parentElement.querySelector("vaadin-context-menu");
     	var overlay = contextMenu.shadowRoot.querySelector("vaadin-context-menu-overlay");
     	overlay.setAttribute("opened", true);
     }
}

customElements.define(AssigntoothersPage.is, AssigntoothersPage);
