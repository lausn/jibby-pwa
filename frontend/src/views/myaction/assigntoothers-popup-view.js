import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `assigntoothers-popup-view`
 *
 * AssigntoothersPopupView element.
 *
 * @customElement
 * @polymer
 */
class AssigntoothersPopupView extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                    width:100%;
                     --lumo-primary-color: #10708e;
                }
                
                .hr-style{
                	width: 100%;
                	border-width: 0.1px;
                	background: #000;
                	height: 1px;
                }
                
            </style>
            
            <div style="width:100%; height:100%; padding: 10px;">
              <div style="display:inline-block; width:100%;" id="assignmentCritria">
              
              </div>
              <hr class="hr-style">
              <div style="display:inline-block; width:100%;" id="statusWrapper">
              
              </div>
              
              <div style="display:inline-block; width:100%;">
                   <vaadin-button style="color:#fff;width:45%;font-size: 14px;margin-left: 10px;" id="btnSave" theme="primary small">
        				Save 
        			</vaadin-button>
        			
        			 <vaadin-button style="--lumo-primary-color: red;width:45%;font-size: 14px; margin-left:5px;" id="btnCancel" theme="primary small">
        				Cancel 
        			 </vaadin-button>
              </div>
            
            </div>
        `;
    }

    static get is() {
        return 'assigntoothers-popup-view';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(AssigntoothersPopupView.is, AssigntoothersPopupView);
