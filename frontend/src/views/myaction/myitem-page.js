import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-menu-button/paper-menu-button.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-details/vaadin-details.js';
/**
 * `myitem-page`
 *
 * MyitemPage element.
 *
 * @customElement
 * @polymer
 */
class MyitemPage extends PolymerElement {

    static get template() {
        return html`
      <style>
	vaadin-checkbox{
		margin-right: -8px;
	}
	.topitem{
		display: flex;
    	width: 100%;
	}
	.topitem iron-icon{
		width: auto;
	    height: fit-content;
	    margin-right: 8px;
	    margin-top: 2px;
	}
	
	.topitem span{
		text-overflow: ellipsis;
	    overflow: hidden;
	    width: 86%;
	    float: left;
	}
	.myitemicon{
		float:left;
		height: fit-content;
		margin-left:15px;
   	 }
	.myitemname{
		width: 100%;
	    float: left;
	    padding-bottom: 10px;
	    text-overflow: ellipsis;
    	overflow: hidden;
    	white-space: normal;
    	margin-left:10px;
	}
	paper-listbox{
    	padding: 0px;
    	background: #e9f4f7;
    	color: #0f495b;
    }
    .extraIconbox{
    	width: 20px;
    	padding-right: 8px;
    	margin-top: -2px;
    	color: #0c5166;
    }
   
    paper-item{
    	 border-bottom: 1px solid #e5e6e6;
    }
    .extraIconbox iron-icon{
    	width: var(--iron-icon-width, 16px);
    	height: var(--iron-icon-height, 16px);
    }
    .collapseItemWrapper{
        border-bottom: 1px solid #eee;
         display: flex;
        padding: 5px;
	}
	.detailsPageCollapse {
        padding-right: 5px;
        padding-left:5px;
        margin:0px;
	}
	.myitemwrapper{
		width: 100%;
		display: flex;
	    box-sizing: border-box;
	    border-bottom: 1px solid #eee;
	    padding-top: 7px;
	    margin-left: 15px;
	} 
    
</style>
<custom-style>
 <style is="custom-style">
						.styled {
							--paper-collapse-item-header: {
								border-bottom: 1px solid #eee;
							    font-size: var(--lumo-font-size-s);
							}
							--paper-collapse-item-content: {
								margin-top: 10px;
							    font-size: var(--lumo-font-size-s);
							}
							--paper-item-focused-before: {
							  	background: none !important;
							}
							--paper-item: {
							    font-weight: var(--paper-item-selected-weight, normal) !important;
							};
						}
					</style>
</custom-style>
<template is="dom-repeat" items="{{myItems}}" as="topitem">
 <vaadin-details class="detailsPageCollapse" opened>
	 <div style="width: 100vw;" slot="summary" class="collapseItemWrapper">
	   <iron-icon style="width: auto; height: fit-content; margin-top:-2px; margin-right:5px;" src="images/itemtype/{{topitem.entityType}}.png" icon=""></iron-icon>
	   <span>{{topitem.name}}</span>
	  </div>
	  <template is="dom-repeat" items="{{topitem.myItems}}" as="childitem">
	   <div class="myitemwrapper" style="width: 100%;">
	   	<div style="width:85%;display: flex;">
	   		<template is="dom-if" if="{{_isItemtype(childitem.itemId)}}">
		    	<vaadin-checkbox on-change="_myitemChanged" checked="{{childitem.status}}"></vaadin-checkbox>
		     </template>
		     <template is="dom-if" if="{{_isItemtype(childitem.entityType)}}">
		    	<iron-icon class="myitemicon" src="images/itemtype/{{childitem.entityType}}.png" icon=""></iron-icon>
		    </template>
		    <span on-click="_myitemsSelectedItem" class="myitemname">{{childitem.name}}</span>
	    </div>
	    <div style="width: 10%; margin-top: -7px;">
		      <vaadin-context-menu>
        		<template>
        		  	<vaadin-list-box>
        					<vaadin-item on-click="_onDeleteItem" style="font-size: 20px;">
			      					<iron-icon icon="vaadin:trash" style="padding-right: 8px;width: 22px;height: 22px;padding-bottom: 2px;"></iron-icon>Delete item</vaadin-item>
			    	</vaadin-list-box>
        								</template>
									</vaadin-context-menu>
				   <vaadin-button theme = "icon" style="background-color: white !important; color: black !important; min-width: 10px;height: 15px; margin-left:5px;" on-click="onOptionClick">
								<iron-icon icon="vaadin:ellipsis-dots-v"></iron-icon>
				   </vaadin-button>
	     </div>
	   </div>
	  </template>
 </vaadin-details>
</template>
        `;
    }

    static get is() {
        return 'myitem-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        	topItems:{
            	type:String,
            	observer:'_value'
            },
            myItems:Array,
        };
    }
    
    _isItemtype(e){
    	if(e>0)
    		return true;
    	else
    		return false;
    }
    
    _value(){
    	this.myItems=JSON.parse(this.topItems);
    }
    _myitemChanged(e){
    	//console.log("aadddd "+Polymer.dom(e).localTarget.dataset.itemvalue+" "+Polymer.dom(e).localTarget.checked);
    	//console.log(JSON.stringify(e.model.childitem));
    	//var checked= Polymer.dom(e).localTarget.checked;
    	this.$server.myitemChanged(JSON.stringify(e.model.childitem),e.target.checked);
    	//e.target..remove();
    }
    
     _onDeleteItem(e){
    	this.$server._onDeleteItem(JSON.stringify(e.model.childitem));
    }
     
     onOptionClick(e){
      	var contextMenu = e.target.parentElement.querySelector("vaadin-context-menu");
      	var overlay = contextMenu.shadowRoot.querySelector("vaadin-context-menu-overlay");
      	overlay.setAttribute("opened", true);
      }
     
     _myitemsSelectedItem(e){
     	this.$server._myitemsSelectedItem(JSON.stringify(e.model.childitem));
     }
}

customElements.define(MyitemPage.is, MyitemPage);
