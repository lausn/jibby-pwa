import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-tabs/vaadin-tabs.js';
import '@vaadin/vaadin-tabs/vaadin-tab.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-icon/iron-icon.js';

class FiltersPage extends PolymerElement {

    static get template() {
        return html`
		 <style>
		.tab{
			 color:#fff;
			 padding-left: 5px;
		}
		
		</style>
		<vaadin-tabs selected="{{tabpage}}" style="background: #28708f;    padding-top: 5px;">
		 <vaadin-tab selected class="tab">
		  <iron-icon icon="vaadin:filter"></iron-icon> Filters 
		 </vaadin-tab>
		 <vaadin-tab class="tab" style="padding: 5px;">
		  <iron-icon icon="vaadin:cog"></iron-icon> Categories 
		 </vaadin-tab>
		 <vaadin-tab class="tab">
		  <iron-icon icon="vaadin:cluster"></iron-icon> Sort Project 
		 </vaadin-tab>
		 <vaadin-tab class="tab">
		  <iron-icon icon="vaadin:sort"></iron-icon> Sort 
		 </vaadin-tab>
		</vaadin-tabs>
		<iron-pages selected="[[tabpage]]" style="padding-left: 10px;padding-right: 10px;height: 445px;">
		 <page>
		  <div id="optionPage"></div>
		 </page>
		 <page>
		  <div id="categoriesPage"></div>
		 </page>
		 <page>
		  <div id="sortProjectPage"></div>
		 </page>
		 <page>
		  <div id="sortPage"></div>
		 </page>
		</iron-pages>
        `;
    }

    static get is() {
        return 'filters-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(FiltersPage.is, FiltersPage);
