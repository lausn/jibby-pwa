import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-ripple/paper-ripple.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';


/**
 * `jibby-item`
 *
 * JibbyItem element.
 *
 * @customElement
 * @polymer
 */
class JibbyItem extends PolymerElement {

    static get template() {
        return html`
<style>
	:host vaadin-button{
		font-size: 0.8rem;
    	font-weight: normal;
    	color:#000000;
	}
	.itemrow{
		height:45px;
		padding-top: 5px;
		padding-left: 10px;
    	border-bottom: 1px solid #d4e1e0;
	}
	#itemname span{
		margin-left: 25px;
	}
	#itemname img{
		position: absolute;
    	top: 8px;
	}
	
</style>
<div class="itemrow" style="    background: #fff; box-shadow: 1px 2px 6px #e8e8e8; -webkit-box-shadow: 1px 2px 6px #e8e8e8; padding: 0px 10px 10px 10px; border: 1px solid #ededed;">
 <div>
  <iron-icon src="frontend/images/itemtype/1.png" slot="prefix"></iron-icon>
  <span>Text</span>
 </div>
 <div>
  <span>Text</span>
 </div>
 <div>
  <span>Text</span>
  <vaadin-button>
    Button 
  </vaadin-button>
 </div>
</div>
        `;
    }

    static get is() {
        return 'jibby-item';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(JibbyItem.is, JibbyItem);
