import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/iron-icon/iron-icon.js';
/**
 * `setting-page`
 *
 * SettingPage element.
 *
 * @customElement
 * @polymer
 */
class SettingPage extends PolymerElement {

    static get template() {
        return html`
 <style include="shared-styles">
            :host {
                display: block;
            }
            paper-listbox{
		    	padding: 0px;
		    	background: #fafafa;
		    	color: #0f495b;
		    }
		    .extraIconbox{
		    	width: 20px;
		    	padding-right: 35px;
		    	margin-top: -2px;
		    	color: #0c5166;
		    }
		   
		    paper-item{
		    	/*  border-bottom: 1px solid #e5e6e6; */
		    	padding: 0px 20px !important;
		    }
		    iron-icon{
		    	width: var(--iron-icon-width, 20px);
		    	height: var(--iron-icon-height, 20px);
		    }
		    .userImg{
		    	width: 28px;
			    height: 28px;
			    border-radius: 100%;
			    margin-top: 6px;
			    margin-left: -4px;
		    }
        </style>
<div style="width:100%;">
<paper-listbox>
  <paper-item id="btnProjectview" on-tap="_onMenuselect">
   <div class="extraIconbox">
    <span hidden="[[projectview]]">
     <iron-icon icon="vaadin:check"></iron-icon></span>
   </div> Project View 
  </paper-item>
  <paper-item id="btnMyAction" on-tap="_onMenuselect">
   <div class="extraIconbox">
    <span hidden="[[myitemview]]">
     <iron-icon icon="vaadin:check"></iron-icon></span>
   </div> My Actionlist 
  </paper-item>
  
  <paper-item hidden="[[filter]]" id="btnFilter" on-tap="_onMenuselect">
   <div class="extraIconbox">
   	<iron-icon icon="vaadin:filter"></iron-icon>
   </div> Filter 
  </paper-item>
  <paper-item>
   <div class="extraIconbox">
	<img class="userImg" onerror=javascript:this.src='./images/avatar.png' src="https://app.jibby.com/jibby/usercontent/avatars/{{userId}}">
</div> <span style="margin-left:3px;">{{username}}</span>
  </paper-item>
  <paper-item id="btnLogout" on-tap="_onMenuselect">
   <div class="extraIconbox">
  	 <iron-icon icon="vaadin:sign-out"></iron-icon>
   </div> Logout 
  </paper-item>
 </paper-listbox>
</div>
        `;
    }

    static get is() {
        return 'setting-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    
    _onMenuselect(e){
    	//console.log(e.target.id);
    	this.$server.menuChangedProcess(e.target.id);
    }
}

customElements.define(SettingPage.is, SettingPage);
