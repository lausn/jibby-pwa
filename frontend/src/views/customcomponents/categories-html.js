import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js';


/**
 * `categories-html`
 *
 * CategoriesHtml element.
 *
 * @customElement
 * @polymer
 */
class CategoriesHtml extends PolymerElement {

    static get template() {
        return html`
			<style>
			    .typesCheckbox {
			   		display:inline-block !important;
				}
				.typesCheckbox vaadin-checkbox{
					 width: 50% !important;
				}
				 #btnCancel{
				--lumo-primary-color: red;
			}
			    </style>
			<div style="width:100%;">
			 <div style="width:100%;padding-top: 10px;" id="typeWrapper"></div>
			 <div style="    text-align: center;
			    border-top: 1px solid #ddd;
			    padding-top: 5px;
			    padding-bottom: 5px;
			    align-items: center;
			    justify-items: center;
			margin-top: 15px;">
			  <vaadin-button style="color:#fff;width:45%;font-size: 14px;" id="btnCancel" theme="primary small" class="btn-tab">
			    Cancel 
			  </vaadin-button>
			  <vaadin-button style="color:#fff;width:45%;font-size: 14px;" id="btnSave" theme="primary small" class="btn-tab">
			    Submit 
			  </vaadin-button>
			 </div>
			</div>
        `;
    }

    static get is() {
        return 'categories-html';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(CategoriesHtml.is, CategoriesHtml);
