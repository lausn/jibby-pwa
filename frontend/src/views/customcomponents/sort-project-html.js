import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `sort-project-html`
 *
 * SortProjectHtml element.
 *
 * @customElement
 * @polymer
 */
class SortProjectHtml extends PolymerElement {

    static get template() {
        return html`
           <style>
			    .sortRadio vaadin-radio-button{
					width:50%;
					padding-top: 5px;
				    padding-bottom: 5px;
				}
				#btnCancel{
					--lumo-primary-color: red;
				}
		   </style>
		   <div style="width:100%;">
			 <div style="padding-top: 10px;width:100%;" id="sortwrapper"></div>
			 <div style="    text-align: center;
			    border-top: 1px solid #ddd;
			    padding-top: 5px;
			    padding-bottom: 5px;
			    align-items: center;
			    justify-items: center;
			margin-top: 15px;">
			  <vaadin-button style="color:#fff;width:90%;font-size: 14px;" id="btnCancel" theme="primary small" class="btn-tab" tabindex="0">
			    Cancel 
			  </vaadin-button>
			 </div>
			</div>
        `;
    }

    static get is() {
        return 'sort-project-html';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(SortProjectHtml.is, SortProjectHtml);
