import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-group.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-button.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-button/vaadin-button.js';

/**
 * `option-html`
 *
 * OptionHtml element.
 *
 * @customElement
 * @polymer
 */
class OptionHtml extends PolymerElement {

    static get template() {
        return html`
   <style>
    #btnCancel{
	--lumo-primary-color: red;
}
.bottom-div{
    text-align: center;
    margin-bottom: 5px;
}
.public-wrapper{
	padding-bottom: 5px;
	width: 100%;
	display: inline-block;
	border-top: 1px solid #ddd;
    padding-top: 5px;
}
    </style>
<div style="width: 100%;padding-top: 10px;">
 <div style="width:100%;">
  <div style="width: 100%;display:inline-block;" id="statusWrapper"></div>
   <div style="width: 100%;margin-bottom: 10px;  border-top: 1px solid #ddd;display: inline-block; padding-top: 7px;font-size: 16px;">
  <vaadin-checkbox id="cbAllRoles">
    View all rules 
  </vaadin-checkbox>
  <vaadin-checkbox id="cbEditor">
    Editor 
  </vaadin-checkbox>
  <vaadin-checkbox id="cbOwner">
    Owner 
  </vaadin-checkbox>
  <vaadin-checkbox id="cbReader">
    Reader 
  </vaadin-checkbox>
 </div>
  <div style="width:100%;text-align:left;">
    <label style="text-decoration: underline;width:60px;display: inline-block;font-size: 16px;">Start</label>
    <vaadin-date-picker value="" max="" style="width:50%;display: inline-block;" theme="small" id="dtStart"></vaadin-date-picker>
    <div id="startWrapper"></div>
  </div>
  <div style="width:100%;text-align:left;">
    <label style="text-decoration: underline;width:60px;display: inline-block;font-size: 16px;">Finish</label>
    <vaadin-date-picker value="" max="" style="width: 50%;display: inline-block;" theme="small" id="dtFinish"></vaadin-date-picker>
   <div id="finishWrapper"></div>
  </div>
 </div>
 <div class="public-wrapper" id="publicWrapper"></div>
</div>
<div class="bottom-div">
 <vaadin-button style="color:#fff;width:45%;font-size: 14px;" id="btnCancel" theme="primary small" class="btn-tab">
   Cancel 
 </vaadin-button>
 <vaadin-button style="color:#fff;width:45%;font-size: 14px;margin-left: 10px;" id="btnSave" theme="primary small" class="btn-tab">
   Submit 
 </vaadin-button>
</div>
        `;
    }

    static get is() {
        return 'option-html';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(OptionHtml.is, OptionHtml);
