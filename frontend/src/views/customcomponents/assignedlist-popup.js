import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';

class AssignedlistPopup extends PolymerElement {

    static get template() {
        return html`
 <style include="assigneepopup">
	.userCheck{
		
		padding-left: 0px;
		width:100vh !important;
	}

.userCheck vaadin-checkbox{
	width: 100%;
	padding-top: 3px;
    padding-bottom: 3px;

}

.userCheck label{
	display:inline-flex !important;
}

</style>
<div style="font-size: 0.8rem;">
 <vaadin-button id="btnClose" style="position: absolute;
    right: 0px;
    background: transparent;
    color: #fff;">
  <iron-icon icon="vaadin:close"></iron-icon>
 </vaadin-button>
 <label style="
    font-weight: 600;
    float: left;
    display: inline-block;
    border-bottom: 1px solid #ddd;
    padding-top: 12px;
    padding-bottom: 12px;
    text-indent: 15px;
    background: #28708f;
    color: #fff;
    " id="label">Select users to assign </label>
 <div style="width: 100%;
    height: calc(100vh - 440px);
    overflow: auto;
    padding-top: 10px;
    padding-bottom: 25px;
" id="assignedUserWrapper"></div>
 <vaadin-button id="btnOk" style="width:98%;background:hsl(198, 56%, 36%);color: #fff;margin-left: 3px;">
   Ok 
 </vaadin-button>
</div>
        `;
    }

    static get is() {
        return 'assignedlist-popup';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(AssignedlistPopup.is, AssignedlistPopup);
