import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-details/vaadin-details.js';

class NotificationViewPopup extends PolymerElement {

    static get template() {
        return html`
 <style>

:host{
	height: 100%;
	width: 100%;
	display:inline-block;
}

.detailsPageCollapse {
	width: calc(100% - 2px);
	border:1px solid #ddd;  
	margin-bottom: 1px;
	background: #e8eef7 !important;
	display: inline-block;
		
		--paper-collapse-item-icon: {
				margin-right: 10px !important;
				height: 15px !important;
				width: 15px !important;
				border: 2px solid #10708e !important;
				padding: 7px 8px;
				border-radius : 100%;
				color: #10708e !important;
			}
			--paper-collapse-item-header: {
				color: #10708e;
    			font-weight: 500;
    			padding-left: 10px; 
    			font-size: 0.8rem !important;
    		}
    		
    		--paper-collapse-item-content: {
    		     font-size: 0.8rem !important;
				 background: #f4f5fa !important;
    			 padding: 10px 0px 8px 10px;
    			 margin-bottom: 12px	
    		}
    	
    		--paper-item-focused-before: {
				background: none !important;
			}
			}	

			.pre-icon{
				float: left;
			    margin-right: 5px;
			    margin-top: -3px;
			    width: 16px;
			    color: #27708f;
			}
			.text{
				color: #6f7d84;
			    overflow: hidden;
			    white-space: normal;
			}
			.topLabelname{
				color: #27708f;
	    		margin-left: 2px;
    		}
    		.wrapper{
    			width: calc(100% - 35px);
    			display: inline-block;
    			float: left;
    			padding: 3px;
    			border-bottom: 1px solid #e9ebee;
    			background: #f6f6f6 !important;
    			margin-left: 15px;
        		margin-right: 15px;
        		font-size: 13px;
        		margin-bottom: 8px;
    		}
    		.date{
    			width: 100%;
			    color: #085576;
			    font-weight: 600;
    		}
    		.header-icon{
    			height: 15px !important;
			    width: 15px !important;
			    border: 2px solid #10708e !important;
			    padding: 7px 8px;
			    border-radius: 100%;
			    color: #10708e !important;
			    margin-right: 5px;
    		}
</style>
 <div style="width:100%;height: 57px;position: fixed;z-index: 10;padding-top:var(--safe-area-inset-top); background: #10708e;">
  <vaadin-button id="btnBackNotiDialog" style="display: inline-block;" theme="small icon"><iron-icon icon="vaadin:chevron-left" style = "fill:#fff;"></iron-icon></vaadin-button>
  <label style="font-weight: 600;display: inline-block;background: #28708f;color: #fff;font-size: 13px;margin-left: 2px;margin-top: 20px;">Notifications</label>
 </div>
 
 <div style="width: 100%;margin-top: 53px !important;">
 
  <vaadin-details class=" detailsPageCollapse" opened>
        <div slot="summary" class="collapseItemWrapper">
        	<label><iron-icon class="header-icon" icon="vaadin:list"></iron-icon>Items</label>
		</div>
		<dom-repeat items="[[items]]" as="item">
		    <template>
		     <div class="wrapper" on-click="_detailsItemClick">
		      <iron-icon class="pre-icon" icon="vaadin:time-forward"></iron-icon>
		      <div class="date">
		        {{item.created}}: 
		      </div>
		      <div class="text">
		        {{item.message}} 
		       <span class="topLabelname">({{item.toplabelname}})</span>
		      </div>
		     </div>
		    </template>
		   </dom-repeat>
	</vaadin-details>
				

  <vaadin-details class="detailsPageCollapse">
    <div slot="summary" class="collapseItemWrapper">
					 	<iron-icon class="header-icon" icon="vaadin:check-square-o"></iron-icon>
					 	Checklist
					 </div>
   <dom-repeat items="[[checklists]]" as="checklist">
    <template>
     <div class="wrapper">
      <iron-icon class="pre-icon" icon="vaadin:time-forward"></iron-icon>
      <div class="date">
        {{checklist.created}}: 
      </div>
      <div class="text">
        {{checklist.message}} 
       <span class="topLabelname">({{checklist.toplabelname}})</span>
      </div>
     </div>
    </template>
   </dom-repeat>
  </vaadin-details>
  <vaadin-dialog id="details" aria-label="simple">
        <template>
        	<iframe width="100%" height="315" src="https://www.youtube.com/embed/fUUuYF8gLnY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </template>
  </vaadin-dialog>
 </div>
        `;
    }

    static get is() {
        return 'notification-view-popup';
    }

    static get properties() {
        return {
            // Declare your properties here.
            notificationItems:{
            	type:String,
            	observer:'_value'
            },
            items:Array,
            
            notificationChecklists:{
            	type:String,
            	observer:'_value'
            },
            checklists:Array,
        };
    }
    
    _detailsItemClick(e){
    	this.$server._detailsItemClick(JSON.stringify(e.model.item));
    }
    _value(){
    	if(this.notificationItems == '')
    		this.items = '';
    	else
    		this.items=JSON.parse(this.notificationItems);
    		
    	if(this.notificationChecklists == '')
    		this.checklists = '';
    	else
    		this.checklists = JSON.parse(this.notificationChecklists);
    }
}

customElements.define(NotificationViewPopup.is, NotificationViewPopup);
