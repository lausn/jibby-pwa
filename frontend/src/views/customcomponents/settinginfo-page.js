import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-menu-button/paper-menu-button.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-button/vaadin-button.js';

/**
 * `settinginfo-page`
 *
 * SettinginfoPage element.
 *
 * @customElement
 * @polymer
 */
class SettinginfoPage extends PolymerElement {

    static get template() {
        return html`
<style>
	:host{
		margin-left: 0px !important;
	}
   .settingusername,vaadin-button{
    	font-size: var(--lumo-font-size-s);
    	min-height: var(--paper-item-min-height, 35px);
    	font-weight: 400;
    	display: block;
    }
    .paperitemwrapper{
    	border-bottom: 1px solid #ccc;
    	box-sizing: border-box;
    	display:flex;
    	padding: 0px 10px;
    	outline: none;
    }
    paper-listbox{
    	padding: 0px;
    	background: #e9f4f7;
    	color: #0f495b;
    }
    .extraIconbox{
    	width: 20px;
    	padding-right: 8px;
    	margin-top: -2px;
    	color: #0c5166;
    }
   
    paper-item{
    	 border-bottom: 1px solid #e5e6e6;
    }
    iron-icon{
    	width: var(--iron-icon-width, 20px);
    	height: var(--iron-icon-height, 20px);
    }
    .userImg{
    	width: 28px;
	    height: 28px;
	    border-radius: 100%;
	    margin-top: 6px;
	    margin-left: -4px;
    }
   
    </style>
<paper-menu-button no-animations horizontal-align="right" no-overlap close-on-activate>
 <paper-icon-button slot="dropdown-trigger" icon="vaadin:ellipsis-dots-v" alt="align" style="width: 30px;height: 35px;"></paper-icon-button>
 <paper-listbox slot="dropdown-content">
  <paper-item id="btnProjectview" on-tap="_onMenuselect">
   <div class="extraIconbox">
    <span hidden="[[projectview]]">
     <iron-icon icon="vaadin:check"></iron-icon></span>
   </div> Project View 
  </paper-item>
  <paper-item id="btnMyAction" on-tap="_onMenuselect">
   <div class="extraIconbox">
    <span hidden="[[myitemview]]">
     <iron-icon icon="vaadin:check"></iron-icon></span>
   </div> My Actionlist 
  </paper-item>
  <div id="paperFilterWrapper">
  <paper-item id="btnFilter" on-tap="_onMenuselect">
   <div class="extraIconbox">
   	<iron-icon icon="vaadin:filter"></iron-icon>
   </div> Filter 
  </paper-item>
  </div>
  <paper-item>
   <div class="extraIconbox">
	<img class="userImg" onerror=javascript:this.src='./images/avatar.png' src="https://app.jibby.com/jibby/usercontent/avatars/{{userId}}">
</div> <span style="margin-left:3px;">{{username}}</span>
  </paper-item>
  <paper-item id="btnLogout" on-tap="_onMenuselect">
   <div class="extraIconbox">
  	 <iron-icon icon="vaadin:sign-out"></iron-icon>
   </div> Logout 
  </paper-item>
 </paper-listbox>
</paper-menu-button>
        `;
    }

    static get is() {
        return 'settinginfo-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    _onMenuselect(e){
    	//console.log(e.target.id);
    	this.$server.menuChangedProcess(e.target.id);
    }
}

customElements.define(SettinginfoPage.is, SettinginfoPage);
