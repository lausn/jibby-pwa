import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `title-page`
 *
 * TitlePage element.
 *
 * @customElement
 * @polymer
 */
class TitlePage extends PolymerElement {

    static get template() {
        return html`
<style>
:host{
	width: 100%;
}
.titlepage{
	width: 100%;
    height: 25px;
    font-size: 13px;
    text-overflow: ellipsis;
    overflow: hidden;
    margin-top: 2px;
}
</style>
<div class="titlepage">{{titlename}}</div>
        `;
    }

    static get is() {
        return 'title-page';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(TitlePage.is, TitlePage);
