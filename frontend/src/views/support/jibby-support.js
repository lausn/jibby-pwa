import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `jibby-support`
 *
 * JibbySupport element.
 *
 * @customElement
 * @polymer
 */
class JibbySupport extends PolymerElement {

    static get template() {
        return html`
	<style>

			:host{
				height: 100%;
				width: 100%;
				display:inline-block;
				background: #f8f8f8;
				--lumo-primary-color: #10708e;
				overflow : hidden;
			}

			.contactSupportFormWrapper {
				
			    padding: 10px;
        		padding-top: 65px;
        		overflow: hidden;
        		position: absolute;
        		height: 78%;
				
        	}
				.contactSupportFormTitile {
						font-size: 1.5em;
					    margin: 0px 0px 15px;
					    font-weight: bold;
					    display: block;
					    border-bottom: 1px solid #e6e6e6;
					    color: #000000d1;
			}
			.supportFormTitle {
				width: 100%;
				color: #0a617d;
				font-weight: 500;
				display: inline-block;
				font-size: 14px;
				
			}
			 .nameandemailstyle{
					width:100%;
					margin-top: 20px;
              }
			.supportTfStyle {
        		width: 76%;
			}
			.supportTfEmail {
				margin-left: 20px;
				width: 233px;
			}
			 .supportMsgStyle{
                margin-top: 15px;
        		display: inline-block;
        		font-size: 14px;
                
             }
			.supportTaMessage{
				width:100%;
				height: 15%;
				margin-top: 5px;
				border-radius: 0px !important;
             }
			.supportAttachment {
				width: 100%;
        		margin-top: 10px;
			}
			.btnSendContactSupport {
				float: right;
				 width: 135px;
			    display: inline-block;
			    height: 30px;
			}
			.lblmsgstyle{
				width: 100%;

          }
		  .supportTfLabel{
			    width: 22%;
        		display: inline-block;
        		font-size: 14px;
        		
        	}
		  
		  .add-image{
		      	height: 50px;
        		width: 50px;
			    border: none;
			    color: #fff;
			    border-radius: 50%;
			  
		  }
		  .uploaderLayout-style{
		  	display: inline-block;
		  
		  }
		  
		</style>

<div style="width:100%;height: 60px;position: absolute;z-index: 10;padding-top:var(--safe-area-inset-top); background: #10708e;">
  <vaadin-button id="btnBackSupportDialog" style="display: inline-block;" theme="small icon"><iron-icon icon="vaadin:chevron-left" style = "fill:#fff;"></iron-icon></vaadin-button>
  <label style="font-weight: 600;display: inline-block;background: #28708f;color: #fff;font-size: 13px;margin-left: 2px;margin-top: 20px;">Support and feedback</label>
 </div>
 
<div class="contactSupportFormWrapper" id="supportView">
				<label class="contactSupportFormTitile">Contact Jibby Support </label>
				<label class="supportFormTitle" style="width:100%;">Please fill the following form to contact Jibby Support. You will receive reply in email to your address. </label>
				<div  class="nameandemailstyle">
					<div><label class="supportTfLabel">Your name</label><vaadin-text-field  class="supportTfStyle" id="tfName"></vaadin-text-field></div>
					<div><label class="supportTfLabel" style="margin-right: -1px;">Your email</label> <vaadin-text-field  class="supportTfStyle" id="tfEmail"></vaadin-text-field></div>
				</div>
				<label class="supportMsgStyle">Your message</label>
				<vaadin-text-area  rows="4" class="supportTaMessage" theme="small" id="taMessage"></vaadin-text-area>
				<div  class="supportAttachment" spacing="false"  margin="false" id="attachmentLayout">
					<div id="uploadLayout" class="uploaderLayout-style"></div>
					<vaadin-button theme="primary small" class="btnSendContactSupport"  id="btnSend">SEND TO SUPPORT </vaadin-button>
					<div  class="lblmsgstyle" id="lblMessage"> &nbsp; </div>
				</div>
				
				
</div>


        `;
    }

    static get is() {
        return 'jibby-support';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(JibbySupport.is, JibbySupport);
