import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';

/**
 * `sign-up`
 *
 * SignUp element.
 *
 * @customElement
 * @polymer
 */
class SignUp extends PolymerElement {

    static get template() {
        return html`
 <style>
	 :host {
        	    display: block;
		                
			           --lumo-primary-color: #10708e;
					   width: 100%;
					   height: 100vh;
					   --app-primary-color: var(--lumo-primary-color);
					   --app-secondary-color: var(--lumo-body-text-color);
					   background-color: var(--lumo-shade-5pct);
					   display: flex;
					   flex-direction: column;
					   color: var(--lumo-body-text-color);
					   font-family: var(--lumo-font-family);
					   justify-content: center;
			        	overflow: auto;
			        	-webkit-overflow-scrolling: touch;
			        	position: absolute;
			        	background-image: url(../../jibbymobile/images/jibby-banner-page-new.png);
		   }
		   .full-width{
		   width:100%;
		   }
		   .sign-in{
		   font-size: 13px;
		   text-align: center;
		   width: 100%;
		   margin-top: 14px;
		   font-weight: 450;
		   color: #fff;
		   }
		   .overlay{
		   position: absolute;
		   width: 100%;
		   height: 197px;
		   background: linear-gradient(25deg,#f2f5f9,#f1f4f8 80%);
		   clip-path: polygon(100% 100%, 100% 74%, 0% 100%);
		   }
		   .error_text{
			font-size: var(--lumo-font-size-xs);
    		color: var(--lumo-error-text-color);
		    width: calc(100% - 20px);
		    float: left;
		    margin: 0;
		    margin-left: 5px;
		    margin-top: 2px;
		   }
		   .error_icon{
		   	width: 14px;
		    float: left;
		    color: var(--lumo-error-text-color);
		   }
		   .error{
		   	width: 100%;
		   }
		   .quick_intro{
		  width: 35%;
    white-space: nowrap;
    font-size: 0.7rem;
    line-height: 20px;
    font-weight: 400;
    color: rgb(255, 255, 255);
    font-family: "Roboto";
    text-transform: uppercase;
    background-color: rgb(2, 80, 104);
    border-color: rgb(0, 0, 0);
    border-radius: 30px !important;
    outline: none;
    box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;
    box-sizing: border-box;
    letter-spacing: 1.5px;
    cursor: pointer;
    margin: 0px;
    padding: 10px 10px !important;
    display: inline-block;
    height: 40px;
    margin-right: 5px;
        text-decoration: none;
   }
		   .label1{
           font-size: 1rem;
        line-height: 19px;
        font-weight: 300 !important;
        color: white !important;
        letter-spacing: 0px;
        white-space: nowrap;
        opacity: 1;
	}
	.label2{
    white-space: nowrap;
    font-size: 1.5rem;
    font-weight: 600;
    color: white;
    letter-spacing: 0px;
    font-family: "Montserrat";
    opacity: 1;
    margin-top:10px;
    margin-bottom: 10px;
	}
	
	.label3{
        	white-space: normal;
		    line-height: 22px;
		    font-weight: 400;
		    color: white;
		    letter-spacing: 0px;
		    font-family: "Roboto";
		    text-align: center;
		    margin-top: 8px;
		    height: 110px;
		    font-size: 0.75rem;
	}
	
	.btnsign-in{
    white-space: nowrap;
    font-size: 0.8rem;
    line-height: 20px;
    font-weight: 400;
    color: rgb(255, 255, 255);
    font-family: "Roboto";
    text-transform: uppercase;
    background-color: rgb(2, 80, 104);
    border-color: rgb(0, 0, 0);
    width:100%;
    outline: none;
    box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;
    box-sizing: border-box;
    letter-spacing: 1.5px;
    cursor: pointer;
    margin: 0px;
    padding: 14px 10px 10px !important;
    height: 46px;
  }
  
  .whitecolor{
      color:#fff;
 }
	
	.span1{
	color: #ff7028;
    font-size: 14px;
    text-align: inherit;
    line-height: 32px;
    letter-spacing: 0px;
    font-weight: 400;
    text-decoration: underline !important;

	}
	.goggle-icon-style{
		vertical-align: middle;
	    margin: -1px;
	}
	.btn-google{
        background: #4285F4 none repeat scroll 0 0 !important;
	    border-image: none !important;
	    color: #ffffff !important;
	    cursor: pointer !important;
	    outline: medium none !important;
	    position: relative !important;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.05) !important;
	    white-space: nowrap !important;
	    font-size: 14px;
	    font-weight: 600 !important;
	    display: inline-block;
	    padding-right: 8px !important;
	    height: 44px !important;
}

.btn-facebook {
        background: #4267b2 none repeat scroll 0 0 !important;
	    border-color: #4267b2 #4267b2 #4267b2 !important;
	    border-image: none !important;
	    border-radius: 2px !important;
	    color: #ffffff !important;
	    cursor: pointer !important;
	    font-weight: 500 !important;
	    outline: medium none !important;
	    position: relative !important;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.05) !important;
	    white-space: nowrap !important;
	    height: 41px !important;
	    padding-left: 1px !important;
	    padding-top: 1px !important;
	    padding-right: 0px !important;
	    font-size: 14px;
	    font-weight: 600 !important;
	    display: inline-block;
}

.btn-facebook-icon{
    border-radius: 2px;
    height: 38px;
    padding-right: 11px !important;
    padding-top: 2px;
    padding-left: 5px;
    background: #fff;
    color: #4267b2;
    font-size: 19px;
}
.btn-facebook span {
        margin-left: 10px;
	    height: 30px;
	    padding-top: 5px;
	    display: inline-block;
	    padding-right: 12px;
	    font-family: Roboto;
	    font-weight: 600;
}
.btn-google span {
	    font-family: Roboto;
	    color: #fff;
	    font-size: 14px !important;
	    padding-left: 16px !important;
	    display: inline-block;
	    margin-right: 10px;
}
.btn-facebook a {
    text-decoration: none;
    color: white;
}

	.facebook-icon-style{
	    padding: 7px;
	    background: #fff;
	    color: #4267b2;
	    font-size: 18px;
	    float: left;
	    border-radius: 1px;
	    padding-right: 6px;
	    padding-top: 8px;
	}
	
</style>
<div style="width: 100%; height: 100%;text-align: center;">
  
  <div class="label1" style="margin-top:40px;">Your team needs to</div>
  <div class="label2">GET ORGANIZED WITH</div>
  <img width="130px" class="logo" src="images/logo.png"/>
  <div class="label3">
  <div>
	  <span>Share and manage any </span>
	  <span class="span1">PROJECT ORIENTED</span> operation 
  </div>
  <div><span> Construction - Development - Groups ... anything</span></div> 
  <div><span>Simple - Flexible - Scaleable</span> </div>
  </div>
  <div>
  <a class="primary quick_intro" id="btnquick" on-click="_play">QUICK INTRO 
       <iron-icon icon="vaadin:caret-right" style="fill:#fff;width: 20px;"></iron-icon>
  </a>
  <a href="login" class="primary quick_intro"> SIGN IN 
       <iron-icon icon="vaadin:sign-in" style="fill:#fff;width:16px;padding-left: 5px;"></iron-icon>
  </a>
  </div>
  <vaadin-vertical-layout theme="margin">
   
   <vaadin-text-field id="fullname" autofocus="true" label="Full Name" class="full-width whitecolor" required="true" error-message="Name shouldn't be empty!" name="" placeholder="Full Name"></vaadin-text-field>
   <vaadin-text-field id="email" class="full-width whitecolor" label="Email" required="true" error-message="Email shouldn't be empty!" name="" placeholder="Email"></vaadin-text-field>
   <vaadin-password-field class="full-width whitecolor" label="Password" required="true" error-message="Password shouldn't be empty!" name="" placeholder="Password" id="password"></vaadin-password-field>
   <vaadin-password-field class="full-width whitecolor" label="Retype-Paswword" required="true" error-message="Re-type password shouldn't be empty!" name="" placeholder="Re-type" id="re-password"></vaadin-password-field>
   <vaadin-button class="btnsign-in" style="width:100%;margin-top:15px;margin-bottom: 15px;" theme="primary" id="signup">
    <iron-icon icon="vaadin:sign-in"></iron-icon> SIGN UP
   </vaadin-button>
   <div style="width:100%;text-align: center;">
		<div style="margin-bottom: 10px;">
						<div class="btn-google">
							<a id="gplusLoginButton" style="text-decoration: none;">
								<img class="goggle-icon-style" src="./images/btn-google-signin.svg">
								<span>Sign in with Google</span>
							</a>
						</div>
					</div>
						<div class="btn-facebook">
							<a id="facebookLoginButton">
								<iron-icon icon="vaadin:facebook" class="btn-facebook-icon"></iron-icon>
								<span>Sign in with Facebook</span>
							</a>
						</div>
   </div>
   <vaadin-dialog id="playyoutube" aria-label="simple">
        <template>
        	<iframe width="100%" height="315" src="https://www.youtube.com/embed/fUUuYF8gLnY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </template>
  </vaadin-dialog>
  </vaadin-vertical-layout>
</div>
        `;
    }

    static get is() {
        return 'sign-up';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
    
    _play(){
    	this.$.playyoutube.opened = true;
    }
}

customElements.define(SignUp.is, SignUp);
